<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['namespace' => 'Api\Http\Controllers', 'as' => 'api.' ], function() {
    
    // FILTERS
    Route::get('occupations', 'DataController@occupations')->name('occupations');
    Route::get('interests', 'DataController@interests')->name('interests');
    Route::get('hobbies', 'DataController@hobbies')->name('hobbies');


    // REGIONS
    Route::get('getCityByProvinceId/{id}', 'DataController@getCityByProvinceId')->name('province');
    Route::get('getDistrictByCityId/{id}', 'DataController@getDistrictByCityId')->name('city');
    Route::get('getVillageByDistrictId/{id}', 'DataController@getVillageByDistrictId')->name('village');
    Route::get('provinces/{id?}', 'DataController@provinces')->name('provinces');
    Route::get('cities/{id?}', 'DataController@cities')->name('cities');
    Route::get('districts/{id?}', 'DataController@districts')->name('districts');
    Route::get('villages/{id?}', 'DataController@villages')->name('villages');

    // USER
    Route::post('login', 'UserController@login')->name('login');
    Route::post('login/verifyotp', 'UserController@authOtp')->name('verifyotp');
    Route::post('send/otp', 'UserController@resendOtp')->name('sendSMS');
    Route::get('users', 'UserController@getUsers')->name('users');
    Route::get('users/verified', 'UserController@getVerifiedUsers')->name('users.verified');
    Route::get('users/unverified', 'UserController@getUnverifiedUsers')->name('users.unverified');
    Route::get('users/{id}', 'UserController@getUserById')->name('users.byId');

    // PROFILE
    Route::get('profile/check', 'UserController@delteProfileWithoutUser');

    // TASKS
    Route::get('tasks/unassigned', 'TaskController@getUnassigned')->name('api.tasks.unassigned');

    // ARTICLES CODE
    Route::get('article_code', 'DataController@getArticle')->name('articles.code');
    Route::get('article_subcode/by_article_code/{id}', 'DataController@getArticleSubCode')->name('articles.subcode');
    
    // FAQ
    Route::post('faq', 'DataController@faq');

    // FAQ MOBILE
    Route::get('faqs', 'FaqController@allFaq');
    Route::get('faqs/{id}', 'FaqController@byId');
    Route::get('faqs/code/{id}', 'FaqController@getByCode');
    Route::get('faqs/subcode/{id}', 'FaqController@getBySubcode');
    Route::get('faqs/komentar/{id}', 'FaqController@getKomentar');

    // FAQ CODE MOBILE
    Route::get('kategori', 'KategoriController@allCode');

    // PERPRES & PASAL
    Route::get('pasal', 'PasalController@allPasal');
    Route::get('pasal/{pasal_id}', 'PasalController@getPasal');
    Route::get('pasal/perpres/{perpres_id}', 'PerpresController@getPasal');
    Route::get('pasal/kategori/{kategori_id}', 'PasalController@getPasalByCategory');
    Route::get('perpres', 'PerpresController@allPerpres');
    Route::get('perpres/{perpres_id}', 'PerpresController@listPasal');

    // QUESTION || DISKUSI
    Route::get('diskusi/all', 'DiskusiController@getAll');
    Route::get('diskusi/detail/{question_id}', 'DiskusiController@detail');
    Route::get('diskusi/jawab/{question_id}', 'DiskusiController@getResponse');

    // SEARCH
    Route::get('search', 'SearchController@search');
    Route::get('search/popular', 'SearchController@popSearch');

    // POOLING
    Route::get('pooling/answer/{id}', 'ResponseController@loadMore');

    // HIGHLIGHT
    Route::get('comment/highlight/{id}', 'HighlightController@loadMore');
    
    // DISCLAIMER
    Route::get('disclaimer', 'DisclaimerController@view');
    Route::get('disclaimer/unduh', 'DisclaimerController@unduh');

    Route::get('terbilang/{number}', 'DataController@terbilang')->name('terbilang');
    Route::get('packet/offer/{packetId}/{userId}','PacketController@getOffer')->name('offer');

    // USULAN
    Route::get('usulan/list', 'UsulanController@all');
    Route::get('usulan/detail/{id}', 'UsulanController@detail');

    // BANTUAN
    Route::get('bantuan', 'BantuanController@all');

    // FCM TEST
    Route::get('fcm/list', 'FcmtokenController@all');
    Route::get('fcm/testing', 'FcmtokenController@testing');

    // GENERATE SLUG
    Route::get('generate/slug', 'LikeController@slug');
    Route::get('generate/highlight', 'DbController@remove');

    // CHECK TABLE
    // Route::post('check/table', 'DbController@viewData');
    // Route::post('generate/something', 'DbController@generate');

    // USER SEARCH
    Route::post('user/search', 'UserController@search');

    // PASPORT MOBILE APPS API MIDDLEWARE
    Route::group(['middleware' => ['auth:api']], function(){
        // DISUKSI
        Route::post('diskusi', 'DiskusiController@postDiskusi');
        Route::get('diskusi', 'DiskusiController@getByUser');
        Route::post('diskusi/jawab/{question_id}', 'DiskusiController@jawabDiskusi');

        // FCM TOKEN
        Route::post('fcm', 'FcmtokenController@store');

        // KOMENTAR
        Route::post('komentar', 'KomentarController@store');
        Route::get('komentar/delete/{id}', 'KomentarController@delete');

        // POOLING
        Route::get('pooling', 'ResponseController@getPooling');
        Route::post('pooling', 'ResponseController@postPooling');
        
        // USULAN
        Route::post('usulan', 'UsulanController@postUsulan');
        Route::get('usulan', 'UsulanController@getUsulan');

        // LIKE USULAN
        Route::get('usulan/like/{id}', 'LikeController@like');
        Route::get('usulan/unlike/{id}', 'LikeController@unlike');

        // NOTIFY
        Route::post('notifikasi', 'NotifikasiController@store');
        Route::get('notifikasi', 'NotifikasiController@get');

        // NOTIFICATION LIST
        Route::get('list/notifikasi', 'UserNotificationController@all');
        Route::get('read/notifikasi/{id}', 'UserNotificationController@read');
        Route::get('delete/notifikasi/{id}', 'UserNotificationController@delete');
        
        // BOOKMARK
        Route::get('bookmark/get', 'BookmarkController@get');
        Route::post('bookmark/add', 'BookmarkController@store');
        Route::post('bookmark/remove', 'BookmarkController@remove');

        // PROFILE
        Route::get('profile/user', 'ProfileController@view');
        Route::post('profile/user', 'ProfileController@update');

        // HIGHLIGHT
        Route::get('highlights', 'HighlightController@allHighlight');
        Route::post('highlights', 'HighlightController@saveHighlight');
        Route::get('highlights/{id}', 'HighlightController@detailHighlight');
        Route::get('highlights/pasal/{id}', 'HighlightController@highlightPasal');
        Route::post('highlights/comment', 'HighlightController@commentSave');
        Route::get('highlights/comment/{id}', 'HighlightController@listComment');
        Route::get('highlights/comment/delete/{id}', 'HighlightController@deleteComment');
    });
});
