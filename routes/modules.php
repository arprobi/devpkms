<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

// AUTH 
// Route::group(['namespace' => 'Admin\Http\Controllers'], function() {
//     Route::get('login', 'AuthController@login')->name('login');
//     Route::post('login', 'AuthController@handleLogin');
// });


// PKMS
Route::group(['namespace' => 'Main\Http\Controllers', 'as' => 'main.', 'middleware' => []], function() {
    Route::get('/', 'MainController@index')->name('index');
    Route::get('search', 'MainController@search')->name('search');
    Route::get('view/{id}', 'MainController@view')->name('view');
    Route::get('detail/{klasifikasi}/{id}/{slug}', 'MainController@detail')->name('detail');
    Route::post('comment/{id}', 'MainController@comment')->name('comment');
    Route::get('questions', 'MainController@question')->name('getQuestion');
    Route::post('questions', 'MainController@postQuestion')->name('postQuestion');
    Route::get('questions/detail/{id}', 'MainController@question_detail')->name('getQuestionDetail');
    Route::post('answer/{id}', 'MainController@postAnswer')->name('postAnswer');
    Route::get('invitation/{code}', 'MainController@getInvited')->name('invited');
    Route::get('offer/{code}', 'MainController@getOffer')->name('offer');
    Route::get('disclaimer/', 'MainController@disclaimer')->name('disclaimer');
});

// EXPERTS
Route::group(['prefix' => 'experts', 'namespace' => 'Experts\Http\Controllers', 'middleware' => ['auth.sso', 'expert'], 'as' => 'expert.'], function() {
    Route::get('/', function() {
        return redirect()->route('expert.dashboard');
    });
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    // PROFILE
    Route::get('profile', 'ProfileController@index')->name('profile');
    Route::post('profile', 'ProfileController@save')->name('profile.save');
    Route::post('update_account', 'ProfileController@updateAccount')->name('profile.update_account');
    Route::post('update_password', 'ProfileController@updatePassword')->name('profile.update_password');
    Route::post('upload_avatar', 'ProfileController@uploadAvatar')->name('profile.upload_avatar');

    // MESSAGE
    Route::get('messages', 'MessageController@index')->name('messages');
    Route::post('messages', 'MessageController@send')->name('messages.send');
    Route::get('messages/detail/{id}', 'MessageController@detail')->name('messages.detail');
    Route::post('messages/detail/{id}', 'MessageController@reply')->name('messages.reply');

    // EDUCATION
    Route::get('educations', 'EducationController@index')->name('educations');
    Route::post('educations', 'EducationController@create')->name('educations.create');
    Route::get('educations/edit/{education_id}', 'EducationController@edit')->name('educations.edit');
    Route::post('educations/edit/{education_id}', 'EducationController@update')->name('educations.update');
    Route::delete('educations/delete/{education_id}', 'EducationController@delete')->name('educations.delete');

    // EXPERIENCE
    Route::get('experiences', 'ExperienceController@index')->name('experiences');
    Route::post('experiences', 'ExperienceController@create')->name('experiences.create');
    Route::get('experiences/edit/{experience_id}', 'ExperienceController@edit')->name('experiences.edit');
    Route::post('experiences/edit/{experience_id}', 'ExperienceController@update')->name('experiences.update');
    Route::delete('experiences/delete/{experience_id}', 'ExperienceController@delete')->name('experiences.delete');

    // TASKS
    Route::get('tasks', 'TaskController@index')->name('tasks');
    Route::get('tasks/accept/{id}', 'TaskController@accept')->name('tasks.accept');
    Route::get('tasks/deny/{id}', 'TaskController@deny')->name('tasks.deny');
    Route::get('tasks/detail/{id}', 'TaskController@detail')->name('tasks.detail');
    Route::post('tasks/update/{id}', 'TaskController@update')->name('tasks.update');
    Route::post('tasks/add_comment/{id}', 'TaskController@addComment')->name('tasks.add_comment');
    Route::get('tasks/add_article/{id}', 'TaskController@addArticle')->name('tasks.add_article');
    
    //PACKETS
    Route::get('packets', 'PacketController@index')->name('packets');
    Route::get('packets/new', 'PacketController@baru')->name('packets.new');
    Route::get('packets/follow', 'PacketController@follow')->name('packets.follow');
    Route::get('packets/ongoing', 'PacketController@onGoing')->name('packets.ongoing');
    Route::get('packets/offer/{packetId}', 'PacketController@offerPage')->name('packets.offer.page');
    Route::post('packets/offer/{packetId}', 'PacketController@offer')->name('packets.offer');
    Route::get('packets/accept/{id}', 'PacketController@accept')->name('packets.accept');
    Route::get('packets/deny/{id}', 'PacketController@deny')->name('packets.deny');
    Route::get('packets/detail/{id}', 'PacketController@detail')->name('packets.detail');
    Route::post('packets/update/{id}', 'PacketController@update')->name('packets.update');
    Route::post('packets/add_comment/{id}', 'PacketController@addComment')->name('packets.add_comment');
    Route::get('packets/add_article/{id}', 'PacketController@addArticle')->name('packets.add_article');
    Route::get('packets/nego/{id}', 'PacketController@negoPage')->name('packets.nego.page');
    Route::post('packets/nego/{id}', 'PacketController@nego')->name('packets.nego');
    Route::get('packets/paids/{packetId}', 'PacketController@paidsPage')->name('packets.paids.page');
    Route::get('packets/paid/{paidId}/detail', 'PacketController@paidDetail')->name('packets.paid.detail');

    // ARTICLES
    Route::get('articles', 'ArticleController@index')->name('articles');
    Route::post('articles/{taskId}', 'ArticleController@create')->name('articles.create');
    Route::get('articles/view/{id}', 'ArticleController@view')->name('articles.view');
    Route::get('articles/edit/{id}', 'ArticleController@edit')->name('articles.edit');
    Route::post('articles/update/{id}', 'ArticleController@update')->name('articles.update');
    Route::delete('articles/delete/{id}', 'ArticleController@delete')->name('articles.delete');

    // ATTACHMENTS
    Route::get('attachments/download/{shorturl}', 'AttachmentController@download')->name('attachments.download');
    Route::get('attachments/destroy/{shorturl}', 'AttachmentController@delete')->name('attachments.delete');
});

// ADMIN
Route::group(['prefix' => 'admin', 'namespace' => 'Admin\Http\Controllers', 'middleware' => ['auth', 'admin'], 'as' => 'admin.'], function() {

    Route::get('/', function() {
        return redirect()->route('admin.experts');
    });

    // EXPERTS
    Route::get('experts', 'ExpertController@index')->name('experts');
    Route::get('experts/detail/{id}', 'ExpertController@detail')->name('experts.detail');
    Route::post('experts/verify/{id}', 'ExpertController@verify')->name('experts.verify');
    Route::post('experts/assign/{userId}', 'ExpertController@assignTask')->name('experts.assign');
    Route::get('experts/task_waiting/{userId}', 'ExpertController@task_waiting')->name('experts.task_waiting');
    Route::get('experts/task_refused/{userId}', 'ExpertController@task_refused')->name('experts.task_refused');
    Route::get('experts/task_ongoing/{userId}', 'ExpertController@task_ongoing')->name('experts.task_ongoing');
    Route::get('experts/task_finished/{userId}', 'ExpertController@task_finished')->name('experts.task_finished');
    Route::get('unverified_lists', 'ExpertController@unverified_lists')->name('unverified_list');

    // USERS
    Route::get('users', 'UserController@index')->name('users');
    Route::get('users/detail/{id}', 'UserController@detail')->name('users.detail');
    Route::get('users/edit/{id}', 'UserController@edit')->name('users.edit');
    Route::post('users/update/{id}', 'UserController@update')->name('users.update');
    Route::delete('users/delete/{id}', 'UserController@delete')->name('users.delete');
    Route::get('users/reset_password/{id}', 'UserController@resetPassword')->name('users.reset_password');

    // ADMINS
    Route::get('admin', 'AdminController@index')->name('admins');
    Route::get('admin/create', 'AdminController@create')->name('admins.create');
    Route::post('admin', 'AdminController@store')->name('admins.store');
    Route::get('admin/detail/{id}', 'AdminController@detail')->name('admins.detail');
    Route::get('admin/edit/{id}', 'AdminController@edit')->name('admins.edit');
    Route::post('admin/update/{id}', 'AdminController@update')->name('admins.update');
    Route::delete('admin/delete/{id}', 'AdminController@delete')->name('admins.delete');

    // PROFILE
    Route::get('profile', 'ProfileController@index')->name('profile');
    Route::post('profile', 'ProfileController@save')->name('profile.save');
    Route::post('update_account', 'ProfileController@updateAccount')->name('profile.update_account');
    Route::post('update_password', 'ProfileController@updatePassword')->name('profile.update_password');
    Route::post('upload_avatar', 'ProfileController@uploadAvatar')->name('profile.upload_avatar');

    // MESSAGE
    Route::get('messages', 'MessageController@index')->name('messages');
    Route::post('messages', 'MessageController@send')->name('messages.send');
    Route::get('messages/detail/{id}', 'MessageController@detail')->name('messages.detail');
    Route::post('messages/detail/{id}', 'MessageController@reply')->name('messages.reply');

    // TASKS
    Route::get('tasks', 'TaskController@index')->name('tasks');
    Route::post('task', 'TaskController@create')->name('tasks.create');
    Route::get('tasks/ongoing', 'TaskController@onGoing')->name('tasks.ongoing');
    Route::get('tasks/waiting', 'TaskController@waitingApproval')->name('tasks.waiting_approval');
    Route::get('tasks/complete', 'TaskController@complete')->name('tasks.complete');
    Route::get('tasks/detail/{id}', 'TaskController@detail')->name('tasks.detail');
    Route::post('tasks/update/{id}', 'TaskController@update')->name('tasks.update');
    Route::post('tasks/update/add_attachment/{id}', 'TaskController@addAttachment')->name('tasks.add_attachment');
    Route::get('tasks/assign/{taskId}', 'TaskController@assignPage')->name('tasks.assign.page');
    Route::post('tasks/assign/{taskId}', 'TaskController@assign')->name('tasks.assign');
    Route::get('tasks/cancel/{id}', 'TaskController@cancel')->name('tasks.cancel');
    Route::post('tasks/approve', 'TaskController@approve')->name('tasks.approve');
    Route::post('tasks/add_comment/{id}', 'TaskController@addComment')->name('tasks.add_comment');

    // ARTICLES
    Route::get('articles', 'ArticleController@index')->name('articles');
    Route::get('articles/view/{id}', 'ArticleController@view')->name('articles.view');
    Route::get('articles/edit/{id}', 'ArticleController@edit')->name('articles.edit');
    Route::post('articles/update/{id}', 'ArticleController@update')->name('articles.update');
    Route::delete('articles/delete/{id}', 'ArticleController@delete')->name('articles.delete');

    // FAQ
    Route::get('faqs', 'FaqController@index')->name('faqs');
    Route::get('faqs/unpublish', 'FaqController@unpublish')->name('faqs.unpublish');
    Route::get('faqs/create', 'FaqController@form')->name('faqs.new');
    Route::post('faqs/create', 'FaqController@create')->name('faqs.create');
    Route::get('faqs/edit/{id}', 'FaqController@edit')->name('faqs.edit');
    Route::post('faqs/edit/{id}', 'FaqController@update')->name('faqs.update');
    Route::delete('faqs/delete/{id}', 'FaqController@delete')->name('faqs.delete');
    Route::get('faqs/check/{id}', 'FaqController@check')->name('faqs.check');

    // QUESTIONS
    Route::get('questions', 'QuestionController@index')->name('questions');
    Route::get('questions/{id}', 'QuestionController@detail')->name('questions.detail');
    Route::post('questions/{id}', 'QuestionController@postAnswer')->name('questions.answer');

    // INVITATIONS
    Route::get('invitations', 'InvitationController@index')->name('invitations');
    Route::get('invitations/view/{id}', 'InvitationController@show')->name('invitations.show');
    Route::get('invitations/create', 'InvitationController@form')->name('invitations.new');
    Route::post('invitations/create', 'InvitationController@create')->name('invitations.create');
    Route::get('invitations/edit/{id}', 'InvitationController@edit')->name('invitations.edit');
    Route::post('invitations/update/{id}', 'InvitationController@update')->name('invitations.update');
    Route::post('invitations/resend/{id}', 'InvitationController@resend')->name('invitations.resend');
    Route::delete('invitations/delete/{id}', 'InvitationController@delete')->name('invitations.delete');

    // MASTER EDUCATIONS
    Route::resource('educations', 'EducationController', ['except' => 'show']);
    Route::resource('interests', 'InterestController', ['except' => 'show']);
    Route::resource('codes', 'CodeController', ['except' => 'show']);
    Route::resource('subcodes', 'SubcodeController', ['except' => 'show']);

    // ATTACHMENTS
    Route::get('attachments/download/{shorturl}', 'AttachmentController@download')->name('attachments.download');
    Route::get('attachments/destroy/{shorturl}', 'AttachmentController@delete')->name('attachments.delete');

    //PAKET
    Route::get('packets', 'PacketController@index')->name('packets');
    Route::get('packet', 'PacketController@add')->name('packets.add');
    Route::post('packet', 'PacketController@create')->name('packets.create');
    Route::get('packets/publish', 'PacketController@publish')->name('packets.publish');
    Route::get('packets/publishing/{id}', 'PacketController@publishing')->name('packets.publishing');
    Route::get('packets/ongoing', 'PacketController@onGoing')->name('packets.ongoing');
    Route::get('packets/waiting', 'PacketController@waitingApproval')->name('packets.waiting_approval');
    Route::get('packets/complete', 'PacketController@complete')->name('packets.complete');
    Route::get('packets/detail/{id}', 'PacketController@detail')->name('packets.detail');
    Route::post('packets/update/{id}', 'PacketController@update')->name('packets.update');
    Route::post('packets/update/add_attachment/{id}', 'PacketController@addAttachment')->name('packets.add_attachment');
    Route::get('packets/invite/{packetId}', 'PacketController@invitePage')->name('packets.invite.page');
    Route::post('packets/invite/{packetId}', 'PacketController@invite')->name('packets.invite');
    Route::get('packets/assign/{packetId}', 'PacketController@assignPage')->name('packets.assign.page');
    Route::post('packets/assign/{packetId}', 'PacketController@assign')->name('packets.assign');
    Route::get('packets/schedule/{packetId}', 'PacketController@schedulePage')->name('packets.schedule.page');
    Route::post('packets/schedule/{packetId}', 'PacketController@schedule')->name('packets.schedule');
    Route::get('packets/document/{offerId}', 'PacketController@offerDocument')->name('packets.document.offer');
    Route::get('packets/cancel/{id}', 'PacketController@cancel')->name('packets.cancel');
    Route::post('packets/approve', 'PacketController@approve')->name('packets.approve');
    Route::post('packets/add_comment/{packetId}', 'PacketController@addComment')->name('packets.add_comment');
    Route::get('packets/invitations/{packetId}', 'PacketController@invitations')->name('packets.invitations');
    Route::get('packets/invitations/view/{id}', 'PacketController@invitationShow')->name('packets.invitations.show');
    Route::get('packets/invitations/create/{packetId}', 'PacketController@invitationForm')->name('packets.invitations.new');
    Route::post('packets/invitations/create/{packetId}', 'PacketController@invitationCreate')->name('packets.invitations.create');
    Route::get('packets/invitations/edit/{id}', 'PacketController@invitationEdit')->name('packets.invitations.edit');
    Route::post('packets/invitations/update/{id}', 'PacketController@invitationUpdate')->name('packets.invitations.update');
    Route::post('packets/invitations/resend/{id}', 'PacketController@invitationResend')->name('packets.invitations.resend');
    Route::delete('packets/invitations/delete/{id}', 'PacketController@invitationDelete')->name('packets.invitations.delete');
    Route::get('packets/nego/{packetId}/{userId}', 'PacketController@negoPage')->name('packets.nego.page');
    Route::post('packets/nego/{packetId}/{userId}', 'PacketController@nego')->name('packets.nego');
    Route::get('packets/paids/{id}', 'PacketController@paidsPage')->name('packets.paids.page');
    Route::get('packets/paid/{id}/detail', 'PacketController@paidDetail')->name('packets.paid.detail');
    Route::get('packets/paid/{paid_id}', 'PacketController@paidPage')->name('packets.paid.page');
    Route::post('packets/paid/{paid_id}', 'PacketController@paid')->name('packets.paid');
    
    // USULAN
    Route::get('usulans', 'UsulanController@index')->name('usulans');
    Route::get('usulans/history', 'UsulanController@history')->name('usulans.history');
    Route::get('usulans/delete/{id}', 'UsulanController@delete')->name('usulans.delete');
    Route::get('usulans/process/{id}', 'UsulanController@process')->name('usulans.process');

    // PERPRESS
    Route::get('perpres', 'PerpresController@index')->name('perpres');
    Route::get('perpres/create', 'PerpresController@create')->name('perpres.create');
    Route::post('perpres', 'PerpresController@store')->name('perpres.store');
    Route::get('perpres/edit/{id}', 'PerpresController@edit')->name('perpres.edit');
    Route::post('perpres/update/{id}', 'PerpresController@update')->name('perpres.update');
    Route::delete('perpres/delete/{id}', 'PerpresController@delete')->name('perpres.delete');

    // KATEGORIES
    Route::get('kategories', 'KategoriController@index')->name('kategories');
    Route::get('kategories/create', 'KategoriController@create')->name('kategories.create');
    Route::post('kategories', 'KategoriController@store')->name('kategories.store');
    Route::get('kategories/edit/{id}', 'KategoriController@edit')->name('kategories.edit');
    Route::post('kategories/update/{id}', 'KategoriController@update')->name('kategories.update');
    Route::delete('kategories/delete/{id}', 'KategoriController@delete')->name('kategories.delete');

    // PASAL
    Route::get('pasals', 'PasalController@index')->name('pasals');
    Route::get('pasals/create', 'PasalController@create')->name('pasals.create');
    Route::post('pasals', 'PasalController@store')->name('pasals.store');
    Route::get('pasals/{id}', 'PasalController@show')->name('pasals.show');
    Route::get('pasals/edit/{id}', 'PasalController@edit')->name('pasals.edit');
    Route::post('pasals/update/{id}', 'PasalController@update')->name('pasals.update');
    Route::delete('pasals/delete/{id}', 'PasalController@delete')->name('pasals.delete');

    // PERTANYAAN && RESPONSE
    Route::get('responses', 'ResponseController@index')->name('responses');
    Route::get('responses/create', 'ResponseController@create')->name('responses.create');
    Route::post('responses', 'ResponseController@store')->name('responses.store');
    Route::get('responses/{id}', 'ResponseController@show')->name('responses.show');
    Route::get('responses/edit/{id}', 'ResponseController@edit')->name('responses.edit');
    Route::post('responses/update/{id}', 'ResponseController@update')->name('responses.update');
    Route::delete('responses/delete/{id}', 'ResponseController@delete')->name('responses.delete');
    Route::get('responses/publish/{id}', 'ResponseController@publish')->name('responses.publish');

    // PEMBERITAHUAN FIREBASE MOBILE APP
    Route::get('firebase', 'FirebaseController@index')->name('firebase');
    Route::get('firebase/create', 'FirebaseController@create')->name('firebase.create');
    Route::post('firebase', 'FirebaseController@store')->name('firebase.store');

    // DISCLAIMER
    Route::get('disclaimer', 'DisclaimerController@index')->name('disclaimer');
    Route::get('disclaimer/create', 'DisclaimerController@create')->name('disclaimer.create');
    Route::post('disclaimer', 'DisclaimerController@store')->name('disclaimer.store');
    Route::get('disclaimer/edit/{id}', 'DisclaimerController@edit')->name('disclaimer.edit');
    Route::post('disclaimer/update/{id}', 'DisclaimerController@update')->name('disclaimer.update');

    // BANTUAN
    Route::get('bantuans', 'BantuanController@index')->name('bantuans');
    Route::get('bantuans/create', 'BantuanController@create')->name('bantuans.create');
    Route::post('bantuans', 'BantuanController@store')->name('bantuans.store');
    Route::get('bantuans/edit/{id}', 'BantuanController@edit')->name('bantuans.edit');
    Route::post('bantuans/update/{id}', 'BantuanController@update')->name('bantuans.update');
    Route::delete('bantuans/delete/{id}', 'BantuanController@delete')->name('bantuans.delete');

    // HIGHLIGHT
    Route::get('highlights', 'HighlightController@index')->name('highlights');
    Route::get('highlights/{id}', 'HighlightController@show')->name('highlights.show');
    Route::get('highlights/delete/{id}', 'HighlightController@delete')->name('highlights.delete');
    Route::get('highlights/export/{id}', 'HighlightController@export')->name('highlights.export');
});

Route::group(['prefix' => 'datatable', 'namespace' => 'Admin\Http\Controllers', 'middleware' => ['auth', 'admin'], 'as' => 'datatable.'], function() {
    Route::get('packet_user/{packet_id}', 'PacketController@dataTableUsers')->name('packet.user');
    Route::get('packet_user/evaluated/{packet_id}', 'PacketController@dataTableEvaluated')->name('packet.user.evaluated');
});
