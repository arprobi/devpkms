<?php

function check_status($faq)
{
    $completed = "Sudah Lengkap";
    $fields = array(
        'article_code',
        'article_subcode',
        'tags',
        'title',
        'faq',
        'qa'
    );

    if ($faq) {
        foreach ($fields as $field) {
            if (empty($faq[$field]) || !$faq[$field]) {
                $completed = "Belum Lengkap";
            }
        }
    } else {
        $completed = "Belum Lengkap";
    }

    return $completed;
}
