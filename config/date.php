<?php

date_default_timezone_set('Asia/Jakarta');

$years = range(1970, date('Y'));
$years = array_combine($years, $years);
return [
    'months' => [
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember'
    ],
    'years' => $years
];