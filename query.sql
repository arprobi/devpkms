CREATE TABLE `pkms_otp` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(45) NULL,
  `user_id` INT NOT NULL,
  `otp_code` VARCHAR(45) NOT NULL,
  `sent_at` DATETIME NULL,
  `expired_at` DATETIME NULL,
  `status` INT NOT NULL DEFAULT 1,
  `created_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `t_users` ADD COLUMN `created_by` INT NOT NULL DEFAULT 0 AFTER `password`;