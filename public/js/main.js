$(function(){
	var baseUrl = $('meta[name="prefix"]').attr('content'),
		Q = {
		loadDetail: function(id){
			var target = $('.result-detail');
			target.toggleClass('hide');
			$('.loading').toggleClass('hide');
			$.get(baseUrl + '/questions/detail/' + id, function(data){
				target.html(data);
				$('.loading').toggleClass('hide');
				target.toggleClass('hide');
			});
		},
		listener: function(){
			$('#list-pertanyaan').find('a.list-group-item').on('click', function(event){
				event.preventDefault();
				var $this = $(this);
				var id = $this.data('id');

				$('#list-pertanyaan').find('a.list-group-item').removeClass('active');
				$this.toggleClass('active');
				Q.loadDetail(id);
			});
		},
		init: function() {
			var initDetail = $('#list-pertanyaan').find('a.list-group-item.active');

			if(initDetail.length) {
				var id = initDetail.first().data('id');
				Q.loadDetail(id);
			}

			Q.listener();
		}
	};

	Q.init();
});