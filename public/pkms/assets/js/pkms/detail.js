$(function () {
    $('.info-grafis img:first-child').one("load", function () {
        $(this).parent().css({
            'background-image': 'url(' + $(this).attr('src') + ')',
            'background-size': 'cover'
        });
    }).each(function () {
        if (this.complete)
            $(this).load();
    });

    $("#share-button").jsSocials({
        shareIn: "popup",
        shares: ["twitter", "facebook", "googleplus", "linkedin", "pinterest"]
    });

    var params = {
        id_pkms: $("#list-comment").data('id_pkms')
    };
    HTML.Loader('main/get_comment', params, 'list-comment', function (res, elem) {
        handleSlimScroll(elem);
    });

    $('#form-comment').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serializeArray(),
            dataType: 'json',
            beforeSend: function () {
                $('#btn-simpan').button('loading');
            },
            success: function (response) {
                if (response.status) {
                    var params = {
                        id_pkms: $("#list-comment").data('id_pkms')
                    };
                    HTML.Loader('main/get_comment', params, 'list-comment', function (res, elem) {
                        handleSlimScroll(elem);
                    });
                    $('#form-comment').trigger('reset');
                } else {
                    alert(response.message);
                }

                $('#btn-simpan').button('reset');

            }
        });
    });

    $('#like:not(.disabled):not(.active):not(.done)').click(function (e) {
        $(this).addClass('active');
        var id_pkms = $('input[name="id_pkms"]', '#form-comment').val();
        var id_user_portal = $('input[name="id_user_portal"]', '#form-comment').val();
        $.ajax({
            url: site_url('main/insert_polling'),
            type: 'POST',
            data: {id_pkms: id_pkms, id_user_portal: id_user_portal, type: 'like'},
            dataType: 'json',
            beforeSend: function () {
//                $('#like').button('loading');
            },
            success: function (response) {
//                $('#like').button('reset');
                if (response.status) {
//                    $('#like').button('reset');
                    $('#count-like').text(response.count)
                } else {
                }



            }
        });
    });
    
    $('#dislike:not(.disabled):not(.active):not(.done)').click(function (e) {
        $(this).addClass('active');
        var id_pkms = $('input[name="id_pkms"]', '#form-comment').val();
        var id_user_portal = $('input[name="id_user_portal"]', '#form-comment').val();
        $.ajax({
            url: site_url('main/insert_polling'),
            type: 'POST',
            data: {id_pkms: id_pkms, id_user_portal: id_user_portal, type: 'dislike'},
            dataType: 'json',
            beforeSend: function () {
//                $('#like').button('loading');
            },
            success: function (response) {
//                $('#like').button('reset');
                if (response.status) {
//                    $('#like').button('reset');
                    $('#count-dislike').text("-"+response.count)
                } else {
                }



            }
        });
    });
});

