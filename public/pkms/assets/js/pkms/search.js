$(function () {
    $(document).on('click', '#load-more', function () {
        var page = $(this).data('page');
        $(this).data('page', page + 1);
        var data = $('#form-search').serializeArray();
        data.push({name: 'page', value: page});
        data.push({name: 'per_page', value: 10});
        $.ajax({
            url: site_url('pkms/get_search'),
            type: 'GET',
            data: data,
            dataType: 'json',
            beforeSend: function () {

            },
            success: function (response) {
                $('#list-article').append(response.html);
            }
        });
    });

    $(".tags").click(function (e) {
        var input = $(this).data('tag');

        if ($("input[name=tags]").length) {
            var tags = $("input[name=tags]").val();
            var arr_tag = tags.split(",");
            if ($.inArray(input, arr_tag) === -1) {
                arr_tag.push(input);
                var values_tag = arr_tag.join();
                $("input[name=tags]").val(values_tag);
                $("#form-search").submit();
            } else {
                alert("Tag has been add");
            }
        } else {
            $('<input>').attr({
                type: 'hidden',
                value: input,
                name: 'tags'
            }).appendTo('#form-search');
            $("#form-search").submit();
        }
    });

    $('.remove-tags').click(function (e) {
        var tag = $(this).data("tag");
        $('.tags[data-tag="' + tag + '"]').remove();
        var tags = $("input[name=tags]").val();
        var arr_tag = tags.split(",");
        var input = tag;
        arr_tag = $.grep(arr_tag, function (value) {
            return value !== input;
        });
        console.log(arr_tag);
        
        if (arr_tag.length > 0) {
            var values_tag = arr_tag.join();
            $("input[name=tags]").val(values_tag);
        } else {
            $("input[name=tags]").remove();
        }
        $("#form-search").submit();
    });
});