function callback() {
    $('#widget-list-pertanyaan .list-group-item').unbind('click').click(function () {
        var id_pertanyaan = $(this).attr('id');
        $('#widget-list-pertanyaan .list-group-item').removeClass('active');
        $(this).addClass('active');
        var params = {
            id: id_pertanyaan
        };
        HTML.Loader('main/get_detail_pertanyaan', params, 'result-detail', function () {
            widget_jawaban(id_pertanyaan);

            $('#form-jawaban').submit(function (e) {
                e.preventDefault();
                $.ajax({
                    url: $(this).attr('action'),
                    type: $(this).attr('method'),
                    data: $(this).serializeArray(),
                    dataType: 'json',
                    beforeSend: function () {
                        $('#btn-simpan').button('loading');
                    },
                    success: function (response) {
                        if (response.status) {
                            widget_jawaban(id_pertanyaan);
                            $('#form-jawaban').trigger('reset');
                        } else {
                            alert(response.message);
                        }

                        $('#btn-simpan').button('reset');

                    }
                });
            });
        });
    });

    if ($('#widget-list-pertanyaan .list-group-item:first').length) {
        $('#widget-list-pertanyaan .list-group-item:first').click();
    } else {
        $('#result-detail').html("<div class='text-center'><h3>Data Tidak Ada</h3></div>");
    }
}


function widget_pertanyaan(params) {
    HTML.Loader('main/get_pertanyaan', params, 'widget-list-pertanyaan', callback);
}

function widget_jawaban(id_pertanyaan) {
    var params = {
        id_pertanyaan: id_pertanyaan
    };
    HTML.Loader('main/get_jawaban_pertanyaan', params, 'list-jawaban');
}
$(function () {
    var params = {
        page: 1,
        per_page: 10
    };
    widget_pertanyaan(params);

    $(document).on('click', '#result-paging-page li:not(.disabled)', function () {
        var params = {
            page: $(this).attr('data-page'),
            per_page: 10
        };
        widget_pertanyaan(params);
    });

    $('#form-pertanyaan').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serializeArray(),
            dataType: 'json',
            beforeSend: function () {
                $('#btn-kirim').button('loading');
            },
            success: function (response) {
                if (response.status) {
                    var params = {
                        page: 1,
                        per_page: 10
                    };
                    widget_pertanyaan(params);
                    $('#form-pertanyaan').trigger('reset');
                } else {
                    alert(response.message);
                }

                $('#btn-kirim').button('reset');
            }
        });
    });
});

