$(function () {
    $('#banner').owlCarousel({
        loop: true,
        nav: true,
        center: true,
        items: 1,
        autoplay: true,
        autoplayTimeout:3000,
        autoplayHoverPause: true,
        smartSpeed: 450,
        animateOut: 'fadeOut',
//        animateIn: 'flipInX'
    });

    $('#banner .item img:first-child').one("load", function () {
        $(this).parent().css({
            'background-image': 'url(' + $(this).attr('src') + ')',
            'background-size': 'cover'
        });
    }).each(function () {
        if (this.complete)
            $(this).load();
    });
});