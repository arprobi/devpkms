
var handleDatePicker = function () {
    $('.datepicker').each(function () {
        $(this).datetimepicker({
            format: "YYYY-MM-DD",
            minDate: ($(this).data('tglawal')) ? $(this).data('tglawal') : false,
//            maxDate: ($(this).data('tglakhir')) ? $(this).data('tglakhir') : false,
        });
    });
}, handleDataTable = function (e) {
    "use strict";
    var elem = e ? e : "[data-datatables=true]";
    $(elem).each(function () {
        $(this).dataTable({
            "bAutoWidth": false,
            "bLengthChange": false,
            "serverSide": true,
            "ajax": {
                "url": site_url($(this).data('url')),
                "type": "POST"
            },
            "sDom": "<'row'<'col-md-6 col-sm-6'l><'col-md-6 col-sm-6'f>r>t<'row'<'col-md-6 col-sm-6'i><'col-md-6 col-sm-6'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ records per page",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ Data",
                "sInfoEmpty": "Data Kosong",
                "sSearch": "Cari",
                "sZeroRecords": "Tidak Ada Data"
            }
        });
        $(this).children('tbody').on('mouseover', 'tr', function () {
            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover'
            });
        });
    });
}, isset = function () {
    var a = arguments,
            l = a.length,
            i = 0,
            undef;
    if (l === 0) {
        throw new Error('Empty isset');
    }

    while (i !== l) {
        if (a[i] === undef || a[i] === null) {
            return false;
        }
        i++;
    }
    return true;
}, App = function () {
    "use strict";
    return {
        init: function () {
            handleDatePicker();
            handleDataTable();
        }
    }
}();

//for AJax
var HTML = {
    running: {},
    Loader: function (url, data, containerId, callback) {
        var self = this;
        containerId = containerId.replace(/^#+/g, '');

        // abort if widget loader is running
        if (isset(self.running[containerId])) {
            self.running[containerId].abort();
        }

        data.container = containerId; // passing containerId to widget

        // run widget loader
        self.running[containerId] = $.ajax({
            url: site_url(url),
            type: 'GET',
            data: data,
            dataType: 'json',
            beforeSend: function () {
                $('#' + containerId)
                        .empty()
                        .addClass('loading')
                        .append($('<div/>', {class: 'spinner'}));
            },
            success: function (response) {
                $('#' + containerId)
                        .empty()
                        .removeClass('loading');

                $('#' + containerId)
                        .html(response.html);

                if (typeof callback === "function") {
                    callback(response, '#' + containerId, data);
                }
            }
        });
    }
};

$(function () {
    App.init();
    
    $(document).on("click", ".need_confirm", function () {
        var text = ($(this).data('text_comfirm')) ? $(this).data('text_comfirm') : "Apakah Anda yakin?";
        var cek = confirm(text);
        return cek;
    });

    // Close Alert Click
    $('.alert').click(function () {
        $(this).fadeOut();
    });

    // Close Alert Automaticaly
    setTimeout(function () {
        $('.alert').fadeOut('normal', function () {
            $('.alert-layer').remove();
        });
    }, 5000); // 5 Secondss

    $('.submenu').click(function () {
        if ($(this).hasClass('open')) {
            $(this).children('ul').slideUp();
            $(this).removeClass('open');
        } else {
            $(this).children('ul').slideDown();
            $(this).addClass('open');
        }
    });
});

