$(function () {
    var editor = CKEDITOR.replace('editor1', {
        filebrowserUploadUrl: base_url('assets/includes/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'),
    });

    var editor2 = CKEDITOR.replace('editor2', {
        filebrowserUploadUrl: base_url('assets/includes/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'),
    });

    $('#image-upload').change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#image-preview').css({
                    'background-image': 'url(' + e.target.result + ')',
                    'background-size': 'contain'
                });
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    $('#image-preview img:first-child').one("load", function () {
        $(this).parent().css({
            'background-image': 'url(' + $(this).attr('src') + ')',
            'background-size': 'contain'
        });
    }).each(function () {
        if (this.complete)
            $(this).load();
    });

    $("#kodefikasi").change(function () {
        var id = $(this).val();
        $.ajax({
            url: site_url('admin/ajax/get_kodefikasi_detail/' + id),
            type: 'GET',
            dataType: 'json',
            beforeSend: function () {
                var option = '<option value="">Loading....</option>';
                $('#kodefikasi-detail').html(option);
            },
            success: function (response) {
                var option = '<option value="">---Pilih---</option>';
                if (response) {
                    for (var key in response) {
                        option += '<option value="' + response[key].id + '">' + response[key].kodefikasi_detail + '</option>';
                    }
                }
                $('#kodefikasi-detail').html(option);
            }
        });
    });
});