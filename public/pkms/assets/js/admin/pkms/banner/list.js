$(function () {
    $('#img-group .list-group-item img:first-child').one("load", function () {
        $(this).parent().css({
            'background-image': 'url(' + $(this).attr('src') + ')',
            'background-size': 'cover'
        });
    }).each(function () {
        if (this.complete)
            $(this).load();
    });

    $("#add-banner").click(function () {
        $('#modal-add-banner').modal('show');
    });

    $('#modal-add-banner')
            .on('hidden.bs.modal', function () {
                $('#form-add-banner [name="banner"]').val('');
            });

    $("#img-group .list-group-item").click(function () {
        var id = $(this).attr('id');
        if (id) {
            $('#detail').empty().addClass('loading');
            $('<div/>', {
                class: 'spinner'
            }).appendTo('#detail');

            $.ajax({
                url: site_url('admin/pkms/banner/detail/' + id),
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    $('#detail').removeClass('loading').html(response.html);
                    $('#delete-banner').click(function () {
                        if (confirm('Apakah anda yakin?')) {
                            var id = $(this).data('id');
                            if (id) {
                                $.ajax({
                                    url: site_url('admin/pkms/banner/delete/' + id),
                                    type: 'GET',
                                    dataType: 'json',
                                    success: function (response) {
                                        switch (response.status) {
                                            case 'success':
                                                window.location = site_url('admin/pkms/banner');
                                                break;
                                            case 'error':
                                                alert('Data gagal dihapus');
                                                break;
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    });


});