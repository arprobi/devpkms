var editor = CKEDITOR.replace('editor1', {
    filebrowserUploadUrl: base_url('assets/includes/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'),
});

function reload_data() {
    var params = {
        id_pertanyaan: $("input[name^='id_pertanyaan']").val()
    };
    HTML.Loader('admin/pkms/pertanyaan/get_jawaban', params, 'list-jawaban');
}

function CKupdate(){
    for ( instance in CKEDITOR.instances )
        CKEDITOR.instances[instance].updateElement();
}

$(function () {
    reload_data();
    $('#form-jawaban').submit(function (e) {
        e.preventDefault();
        CKupdate();
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: $(this).serializeArray(),
            beforeSend: function () {
                $('#btn-simpan').html('<i class="fa fa-spinner fa-pulse"></i>');
            },
            success: function (response) {
                reload_data();
                $('#btn-simpan').html('<i class="fa fa-save"></i> Simpan');
                $('#form-jawaban').trigger('reset');
            }
        });
    });
});


