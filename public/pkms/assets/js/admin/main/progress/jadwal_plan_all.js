$(function () {
    "use strict";

    $(".gantt").gantt({
        source: site_url($('#gantt-chart').data('url')),
        navigate: "scroll",
        scale: "days",
        maxScale: "months",
        minScale: "days",
        itemsPerPage: 10,
        onItemClick: function (data) {
            alert("Item clicked - show some details");
        },
        onAddClick: function (dt, rowId) {
            alert("Empty space clicked - add an item!");
        },
        onRender: function () {
            if (window.console && typeof console.log === "function") {
                console.log("chart rendered");
            }
        }
    });

    $(".gantt").popover({
        container: 'body',
        selector: ".bar",
        title: "Info Detail",
        content: function () {
            return $(this).text();
        },
        trigger: "hover"
    });
});