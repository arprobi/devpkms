$(function () {
    var grp = $('#tabledatagroup').DataTable({
        "bAutoWidth": false,
        "bLengthChange": false,
        "serverSide": true,
        "ajax": {
            url: site_url($("#tabledatagroup").data('url')),
            "type": "POST"
        },
        "sDom": "<'row'<'col-md-6 col-sm-6'l><'col-md-6 col-sm-6'f>r>t<'row'<'col-md-6 col-sm-6'i><'col-md-6 col-sm-6'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sLengthMenu": "_MENU_ records per page",
            "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ Data",
            "sInfoEmpty": "Data Kosong",
            "sSearch": "Cari",
            "sZeroRecords": "Tidak Ada Data"
        }
    });

    $('#tabledatagroup tbody').on('mouseover', 'tr', function () {
        $('[data-toggle="tooltip"]').tooltip({
            trigger: 'hover'
        });
    });

    $('#tabledatagroup tbody').on('click', 'td span.btn', function () {
        var urlchild = $('#tabledatagroup').data('urlchild');
        var tr = $(this).closest('tr');
        var row = grp.row(tr);
        var id = $(this).data('id');
        var id_konsolidasi = $(this).data('id_konsolidasi');
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(get_child_row(id, id_konsolidasi, urlchild)).show();
            tr.addClass('shown');
        }
    });

    $("#btn-rekomendasi").click(function () {
        $('#modal-rekomendasi').modal('show');
    });

    $('#modal-rekomendasi')
            .on('hidden.bs.modal', function () {
                $('#form-rekomendasi [name="keterangan"]').val('');
            });
});

function get_child_row(id, id_konsolidasi, urlchild) {
    var child = "";
    $.ajax({
        url: site_url("admin/main/progress/get_data_plan_detail/"),
        data: {'id_konsolidasi': id_konsolidasi, 'id_konsolidasi_plan': id},
        type: 'GET',
        async: false,
        success: function (res) {
            child = res;
        }
    });
    return child;
}