function openModal(id, tipe) {
    var url = (tipe === 'plan') ? 'admin/main/progress/get_detail_jadwal/' : 'admin/main/progress/get_detail_rapat/'
    var target = site_url(url + id);
    $("#modal").load(target, function () {
        $("#modal").modal("show");
    });
}
$(function () {
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: null
        },
        defaultView: 'month',
        editable: false,
        monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
        dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"],
        dayNamesShort: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"],
        buttonText: {
            today: "Hari Ini"
        },
        events: function (start, end, timezone, callback) {
            var date = $("#calendar").fullCalendar('getDate');
            var year = date.format('YYYY');
            var month = date.format('M');
            var url = $('#calendar').data('url');
            $.ajax({
                url: site_url(url),
                type: 'POST',
                dataType: 'JSON',
                data: {
                    month: month,
                    year: year
                },
                success: function (events) {
                    callback(events);
                }
            });

        },
        minTime: '8:00',
        maxTime: '17:00',
        eventRender: function (event, element) {
            $(element).click(function () {
                openModal(event.id, event.tipe)
            });
        }
    });

    $("#form_rapat").submit(function (e) {
        e.preventDefault();
        var d = confirm('Yakin akan mengundang rapat ??');
        if (d === true) {
            var postData = $(this).serializeArray();
            $.ajax({
                url: site_url('admin/main/progress/undang_rapat/'),
                type: "POST",
                data: postData,
                success: function (data) {
                    switch (data) {
                        case 'succsess':
                            $('#form_rapat').find('input[type=text],textarea').val('')
                            break;
                        case 'error':
                            alert('Error, Saat Menyimpan data jadwal');
                            break;
                        default:
                    }
                    $('#calendar').fullCalendar('refetchEvents');
                }
            });
        } else {
            return false;
        }
    });

    $(document)
            .on("submit", "#form_update_jadwal", function (event) {
                event.preventDefault();
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: site_url('admin/main/progress/update_plan/'),
                    type: 'POST',
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        switch (data) {
                            case 'succsess':
                                $('#modal').modal("hide");
                                break;
                            case 'error':
                                alert('Error, Saat Menyimpan Data');
                                break;
                            default:
                        }
                        $('#calendar').fullCalendar('refetchEvents');
                    }
                });
                return false;
            })
            .on("click", ".status_rapat", function (event) {
                var d = confirm('Yakin Mengubah Status data Rapat ??');
                if (d === true) {
                    event.preventDefault();
                    var id = $(".status_rapat").data('id');
                    var status = $(".status_rapat").data('status');
                    $.ajax({
                        url: site + 'admin/main/change_status_rapat/' + id + '/' + status,
                        type: 'GET',
                        success: function (data) {
                            switch (data) {
                                case 'succsess':
                                    jQuery('#modal').modal("hide");
                                    break;
                                case 'error':
                                    alert('Error, Saat Mengubah Status Data');
                                    break;
                                default:
                            }
                            jQuery('#calendar').fullCalendar('refetchEvents');
                        }
                    });
                }
                return false;
            })
            .on("click", "#edit_rapat", function (event) {
                $("#kegiatan_edit").replaceWith("<input type='text' class='form-control' name='kegiatan' value='" + $('#kegiatan_edit').html() + "'>"); //closing angle bracket added
                $("#tanggal_edit").replaceWith("<input type='text' class='form-control datepicker' name='tanggal' value='" + $('#tanggal_edit').data('tanggal') + "'>");
                $("#uraian_edit").replaceWith("<textarea type='text' class='form-control' name='uraian'>" + $('#uraian_edit').html() + "</textarea>");
                handleDatePicker();
            })
            .on("submit", "#form_update_rapat", function (event) {
                event.preventDefault();
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: site_url('admin/main/progress/update_rapat/'),
                    type: 'POST',
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        switch (data) {
                            case 'succsess':
                                $('#modal').modal("hide");
                                break;
                            case 'error':
                                alert('Error, Saat Menyimpan Data');
                                break;
                            default:
                        }
                        $('#calendar').fullCalendar('refetchEvents');
                    }
                });
                return false;
            })
            .on("click", "#hapus_rapat", function (event) {
                var d = confirm('Yakin Menghapus data Rapat ??');
                if (d === true) {
                    event.preventDefault();
                    var id = $("#hapus_rapat").data('id');
                    $.ajax({
                        url: site_url('admin/main/progress/delete_rapat/' + id),
                        type: 'GET',
                        success: function (data) {
                            switch (data) {
                                case 'succsess':
                                    jQuery('#modal').modal("hide");
                                    break;
                                case 'error':
                                    alert('Error, Saat Menghapus Data');
                                    break;
                                default:
                            }
                            jQuery('#calendar').fullCalendar('refetchEvents');
                        }
                    });
                }
                return false;
            })
            .on('shown.bs.modal', '#modal', function () {
                handleDatePicker();
            });

});