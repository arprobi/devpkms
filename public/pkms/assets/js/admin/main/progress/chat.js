function reload_chat() {
    var id_probity = $('#chats').data('id');
    $('#chats').empty().addClass('loading');
    $('<div/>', {
        class: 'spinner'
    }).appendTo('#chats');

    $.ajax({
        url: site_url($('#chats').data('url')),
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $('#chats').removeClass('loading').html(response.html);
        }
    });

}

$(function () {
    reload_chat();
    $('#form-chat').submit(function (e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url: $(this).attr("action"),
            type: 'POST',
            data: formData,
            contentType: false,
            cache: false,
            processData:false,
            success: function () {
                reload_chat();
                $("#form-chat")[0].reset();
            }
        });
    });
});