$(function () {
    $("#typethn").change(function () {
        switch ($(this).val()) {
            case 'T':
                $('#thn_tunggal').show();
                $('#thn_tunggal').find("input[name='tahun']").attr('required', true);

                $('#thn_jamak').hide();
                $('#thn_jamak').find("input[name='stahun']").attr('required', false);
                $('#thn_jamak').find("input[name='etahun']").attr('required', false);

                break;
            case 'J':
                $('#thn_tunggal').hide();
                $('#thn_tunggal').find("input[name='tahun']").attr('required', false);

                $('#thn_jamak').show();
                $('#thn_jamak').find("input[name='stahun']").attr('required', true);
                $('#thn_jamak').find("input[name='etahun']").attr('required', true);
                break;
            default :
                $('#thn_tunggal').hide();
                $('#thn_tunggal').find("input[name='tahun']").attr('required', false);

                $('#thn_jamak').hide();
                $('#thn_jamak').find("input[name='stahun']").attr('required', false);
                $('#thn_jamak').find("input[name='etahun']").attr('required', false);
                break;
        }
    });
    $("#province").change(function () {
        var str_id = $(this).val();
        var res = str_id.split('-');
        var id = res[1];
        $('#progress-cog').html('<i class="fa fa fa-cog fa-spin"></i>');
        $.ajax({
            url: site_url('admin/ajax/get_cities/' + id),
            type: 'GET',
            dataType: 'json',
            beforeSend: function () {
                var option = '<option value="">Loading....</option>';
                $('#kodefikasi-detail').html(option);
            },
            success: function (response) {
                var option = '<option value="">---Pilih Kota/Kabupaten---</option>';
                if (response) {
                    for (var key in response) {
                        option += '<option value="kota-' + response[key].id + '">' + response[key].nama_kota + '</option>';
                    }
                }
                $('#cities').html(option);
                $('#progress-cog').empty();
            }
        });
    });

    $("#satker_id").change(function () {
        $("select[name='kldi_id']").val("");
        $("input[name='nama_kldi']").val("");
        switch ($(this).val()) {
            case '1':
                $("#kldi_id").removeClass('hidden');
                $("#nama_kldi").addClass('hidden');
                $("#kldi_id select").prop('required', true);
                break;
            case '3':
            case '4':
                $("#kldi_id").addClass('hidden');
                $("#nama_kldi").removeClass('hidden');
                $("#kldi_id select").prop('required', false);
                break;
            case '2':
            default:
                $("#kldi_id").addClass('hidden');
                $("#nama_kldi").addClass('hidden');
                $("#kldi_id select").prop('required', false);
                break;

        }
    });
});

function addCommas(nStr, el) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    document.getElementById(el).innerHTML = "";
    document.getElementById(el).appendChild(document.createTextNode(x1 + x2));
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}


