function modal_upload(id, type) {
var modal = $('<div class="modal fade" id="modal-file">\n\
    <div class="modal-dialog">\n\
        <div class="modal-content">\n\
            <div class="modal-header">\n\
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>\n\
                <h4 class="modal-title">UBAH ' + type.replace(/\_/g, " ").toUpperCase() + '</h4>\n\
            </div>\n\
            <div class="modal-body">\n\
                <form accept-charset="utf-8" method="post" action="' + site_url('admin/main/detail/update_file') + '" class="form-horizontal" enctype="multipart/form-data">\n\
                    <input type="hidden" name="id" value="' + id + '">\n\
                    <input type="hidden" name="type" value="' + type + '">\n\
                    <div class="from-group">\n\
                        <label for="file_anggaran" class="col-sm-4 control-label">File Anggaran</label>\n\
                        <div class="col-sm-8">\n\
                            <input type="file" name="' + type + '"> \n\
                            <p class="help-block">Upload File</p>\n\
                        </div>\n\
                    </div>\n\
                    <div class="form-group">\n\
                        <div class="col-sm-offset-4 col-sm-10">\n\
                            <input type="submit" class="btn btn-default need_confirm" value="Simpan"/>\n\
                        </div>\n\
                    </div>\n\
                </form>\n\
            </div>\n\
        </div>\n\
    </div>\n\
</div>');
        modal.modal('show');
        }

$(function () {
$('#edit-anggaran').click(function () {
modal_upload($(this).data("id"), 'file_anggaran');
});
        $('#edit-sk').click(function () {
modal_upload($(this).data("id"), 'file_sk_in');
});
        $('#edit-other').click(function () {
modal_upload($(this).data("id"), 'file_other');
});
        });