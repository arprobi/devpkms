$(function () {
    $('table[data-datatables=true] tbody').on('click', '.detail', function () {
        var id = $(this).data('id');
        if (id) {
            $('#detail').empty().addClass('loading');
            $('<div/>', {
                class: 'spinner'
            }).appendTo('#detail');

            $.ajax({
                url: site_url('admin/master_tampilan/tautan/detail/' + id),
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    $('#detail').removeClass('loading').html(response.html);
                    $('#delete-banner').click(function () {
                        if (confirm('Apakah anda yakin?')) {
                            var id = $(this).data('id');
                            if (id) {
                                $.ajax({
                                    url: site_url('admin/master_tampilan/banner/delete/' + id),
                                    type: 'GET',
                                    dataType: 'json',
                                    success: function (response) {
                                        switch (response.status) {
                                            case 'success':
                                                window.location = site_url('admin/master_tampilan/banner');
                                                break;
                                            case 'error':
                                                alert('Data gagal dihapus');
                                                break;
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
    });


});