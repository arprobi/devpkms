<?php

namespace PKMS\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'PKMS\Events\UserRegistered' => [
            'PKMS\Listeners\UserRegisteredNotification',
        ],
        'PKMS\Events\TaskUpdate' => [
            'PKMS\Listeners\TaskUpdateLog',
            'PKMS\Listeners\TaskFinishedLog'
        ],
        'PKMS\Events\TaskAssigned' => [
            'PKMS\Listeners\TaskAssignedLog'
        ],
        'PKMS\Events\TaskAccepted' => [
            'PKMS\Listeners\TaskAcceptedLog'
        ],
        'PKMS\Events\TaskRefused' => [
            'PKMS\Listeners\TaskRefusedLog'
        ],
        'PKMS\Events\Invitation' => [
            'PKMS\Listeners\InvitationMailer',
            'PKMS\Listeners\InvitationSMS',
        ],
        'PKMS\Events\Offer' => [
            'PKMS\Listeners\OfferMailer'
        ],
        'PKMS\Events\PacketUpdate' => [
            'PKMS\Listeners\PacketUpdateLog',
        ],
        'PKMS\Events\PacketFinished' => [
            'PKMS\Listeners\PacketFinishedLog'
        ],
        'PKMS\Events\PacketAssigned' => [
            'PKMS\Listeners\PacketAssignedLog'
        ],
        'PKMS\Events\PacketAssignedChange' => [
            'PKMS\Listeners\PacketAssignedChangeLog'
        ],
        'PKMS\Events\PacketAccepted' => [
            'PKMS\Listeners\PacketAcceptedLog'
        ],
        'PKMS\Events\PacketRefused' => [
            'PKMS\Listeners\PacketRefusedLog'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
