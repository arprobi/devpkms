<?php

namespace PKMS\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Base;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use PKMS\Lib\Broker;
use PKMS\Models\User;

class Authenticate extends Base
{
	public function handle($request, Closure $next, ...$guards)
    {
        $this->authenticate($guards);
        return $next($request);
    }
}