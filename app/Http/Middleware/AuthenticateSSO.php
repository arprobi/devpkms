<?php

namespace PKMS\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Base;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use PKMS\Lib\Broker;
use PKMS\Models\User;
use PKMS\Models\Profile;

class AuthenticateSSO extends Base
{

    protected $broker;

    protected function authSSO()
    {
        $this->broker = request()->get('broker', null);
        $url = request()->get('sso_login_url', null);

        try {
            $user = $this->broker->getUserInfo();
        } catch (NotAttachedException $e) {
            header('Location: ' . $_SERVER['REQUEST_URI']);
            exit;
        } catch (SsoException $e) {
            header('Location: ' . $_SERVER['REQUEST_URI']);
            exit();
        }

        if (!$user) {
            header('Location: ' . $url);
            exit;
        } else {
            $registered = User::where('email', $user['email'])->first();

            if ($registered) {
                $profile = $registered->profile;
                if (!$profile) {
                    $registered->profile()->insert([
                        'user_id' => $registered->id,
                        'birth_place' => $user['birthplace'],
                        'birth_date' => $user['birthday'],
                        'mobile' => $user['phone']
                    ]);
                } else {
                    $profile->update([
                        'birth_place' => $user['birthplace'],
                        'birth_date' => $user['birthday'],
                        'mobile' => $user['phone']
                    ]);
                }
                Auth::login($registered);
            } else {
                $register = User::create([
                            'name' => $user['name'],
                            'username' => $user['username'],
                            'email' => $user['email'],
                            'password' => $user['username'],
                            'is_active' => 1
                ]);
                $register->profile()->insert([
                    'user_id' => $register->id,
                    'birth_place' => $user['birthplace'],
                    'birth_date' => $user['birthday'],
                    'mobile' => $user['phone']
                ]);
                Auth::login($register);
            }
        }
    }

    public function handle($request, Closure $next, ...$guards)
    {
        $this->authSSO();
        $this->authenticate($guards);
        return $next($request);
    }

}
