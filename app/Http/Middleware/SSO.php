<?php

namespace PKMS\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Jasny\SSO\NotAttachedException;
use Jasny\SSO\Exception as SsoException;
use PKMS\Lib\Broker;
use PKMS\Lib\Api;
use PKMS\Models\User;

class SSO
{
    protected $broker;
    protected $api;

    public function __construct()
    {
        $this->broker = new Broker(env('SSO_SERVER'), env('SSO_BROKER_ID'), env('SSO_BROKER_SECRET'));
        $this->api = new Api(env('SSO_API_SERVER'), env('SSO_API_KEY'));
    }

    public function handle($request, Closure $next)
    {
        $this->broker->attach(true);
        
        try
        {
            $user = $this->broker->getUserInfo();
        }
        catch (NotAttachedException $e)
        {
            header('Location: ' . $_SERVER['REQUEST_URI']);
            exit;
        }
        catch (SsoException $e)
        {
            header('Location: ' . $_SERVER['REQUEST_URI']);
            exit();
        }

        $request->attributes->add([
            'broker' => $this->broker,
            'api' => $this->api,
            'user_portal' => $user,
            'sso_login_url' => env('SSO_LOGIN_URL') . rawurlencode(url()->current())
        ]);

        return $next($request);
    }
}
