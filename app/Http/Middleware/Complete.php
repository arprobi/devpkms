<?php

namespace PKMS\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Complete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if(! $user)
        {
            abort(403, 'Unauthorized action.');
        }

        if(! $user->isComplete())
        {
            return redirect()->route('expert.dashboard');
        }

        return $next($request);
    }
}
