<?php

namespace PKMS\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check())
        {
            return redirect()->route('login');
        }

        $user = Auth::user();

        if(!$user->is_admin)
        {
            return redirect()->route('expert.dashboard');
        }

        $cache = Cache::remember('user_'.$user->id, 2, function () use($user) {
            return [
                'average_rating' => $user->averageRating()->first(),
                'unread_message' => $user->inbox()->where('read', 0)->get()
            ];
        });

        $isComplete = $user->isComplete();

        if(! $isComplete ) {
            $links = '<a href="'.route('expert.profile').'" class="alert-link">profil</a>, <a href="'.route('expert.educations').'" class="alert-link">edukasi</a> & <a href="'.route('expert.experiences').'" class="alert-link">pengalaman kerja</a>';
            request()->session()->flash('incomplete', 'Anda harus melengkapi '. $links .' sebelum dapat menggunakan aplikasi ini.');
        }

        if($isComplete && $user->is_verified == 0)
        {
            request()->session()->flash('incomplete', 'Menunggu verifikasi admin.');
        }

        return $next($request);
    }
}
