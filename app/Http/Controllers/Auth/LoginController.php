<?php

namespace PKMS\Http\Controllers\Auth;

use Illuminate\Http\Request;
use PKMS\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use PKMS\Models\User;
use PKMS\Models\Profile;

class LoginController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/experts';

    /**
     * SSO Broker
     * @var PKMS\Lib\Broker
     */
    protected $broker;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $validation = [
            $this->username() => 'required',
            'password' => 'required'
        ];
        if (env("APP_ENV") == "production") {
            $validation["g-recaptcha-response"] = "required|recaptcha";
        }
        $this->validate($request, $validation);
    }

    /**
     * Log the user in to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function credentials(Request $request)
    {
        if(is_numeric($request->get('email'))){
            $idphone    = substr_replace($request->get('email'), '+62', 0, 1);
            $profile    = Profile::where('mobile', $request->get('email'))->orWhere('mobile', $idphone)->first();
            if (!$profile) {
                return ['email' => 'notfound@mail.com', 'password'=>$request->get('password')];
            } else {
                $user       = User::find($profile->user_id);
                return ['email' => $user->email ? $user->email : 'notfound@mail.com', 'password'=>$request->get('password')];
            }
        }
        if (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
            return $request->only($this->username(), 'password');
        } else {
            $user = User::where('username', $request->get('email'))->first();
            if (!$user) {
                return ['email' => 'notfound@mail.com', 'password'=>$request->get('password')];
            }
            return ['email' => $user->email ? $user->email : 'notfound@mail.com', 'password'=>$request->get('password')];
        }
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        $broker = $request->get('broker', false);

        if ($broker) {
            $broker->logout();
        }

        return redirect('/');
    }

}
