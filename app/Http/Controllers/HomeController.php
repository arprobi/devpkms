<?php

namespace PKMS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PKMS\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $completed = Auth::user()->isComplete();

        if(!$completed) {
            $links = '<a href="javascript:;" class="alert-link">profile</a>, <a href="javascript:;" class="alert-link">education</a> & <a href="javascript:;" class="alert-link">experience</a>';
            request()->session()->flash('warning', 'You need to complete '. $links .' before you can use this platform.');
        }

        return view('home');
    }
}
