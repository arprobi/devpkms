<?php

namespace PKMS\Listeners;

use PKMS\Mail\Notification;
use PKMS\Events\PacketAssigned;
use PKMS\Models\PacketHistory as History;
use PKMS\Models\User;

class PacketAssignedLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PacketAssigned  $event
     * @return void
     */
    public function handle(PacketAssigned $event)
    {
        $old = $event->packet->getOriginal();
        $new = $event->packet->getAttributes();
        if(!$old['assigned_to'] && $new['assigned_to']) {
            History::create([
                'user_id' => $new['assigned_by'],
                'packet_id' => $new['id'],
                'action'  => 'assigned'
            ]);
        }
        
        $api = request()->get('api',null);
        $results = [];

        if(!$api)
            return false;

        // To Poster
        $author = $event->packet->postedby;

        if($author) {
            $data = [
                'name' => $author->name,
                'email' => $author->email,
                'content' => 'Paket pekerjaan dengan judul ' . $event->packet->title . ' telah dipilih pemenangnya. <a href="'.route('admin.packets.detail', $event->packet->id).'">Klik disini</a> untuk lihat lebih detail'
            ];

            $mailable = new Notification($data);
            $results[] = $api->sendEmail([
                'email_to' => $author->email,
                'subject'  => 'Pengumuman Pemenang Paket Pekerjaan',
                'content'  => $mailable->build()
            ]);
        }

        // To AssignedTo
        $assignedto = $event->packet->assigned_to;

        if($assignedto) {
            $user = User::find($assignedto);
            $data = [
                'name' => $user->name,
                'email' => $user->email,
                'content' => 'Anda menjadi pemenang paket pekerjaan dengan judul ' . $event->packet->title . '. <a href="'.route('expert.packets.detail', $event->packet->id).'">Klik disini</a> untuk lihat lebih detail'
            ];

            $mailable = new Notification($data);
            $results[] = $api->sendEmail([
                'email_to' => $user->email,
                'subject'  => 'Pengumuman Pemenang Paket Pekerjaan',
                'content'  => $mailable->build()
            ]);
        }
    }
}
