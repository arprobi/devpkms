<?php

namespace PKMS\Listeners;

use PKMS\Events\TaskUpdate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use PKMS\Models\TaskHistory as History;

class TaskRefusedLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TaskRefused  $event
     * @return void
     */
    public function handle(TaskUpdate $event)
    {
        History::create([
            'user_id' => $event->user->id,
            'task_id' => $event->task->id,
            'action'  => 'refused',
            'text' => $event->message
        ]);
    }
}
