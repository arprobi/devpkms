<?php

namespace PKMS\Listeners;

use PKMS\Mail\Notification;
use PKMS\Events\PacketAssignedChange;
use PKMS\Models\PacketHistory as History;

class PacketAssignedChangeLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PacketAssigned  $event
     * @return void
     */
    public function handle(PacketAssignedChange $event)
    {
        $old = $event->packet->getOriginal();
        $new = $event->packet->getAttributes();
        
        
        if(!$old['assigned_to'] && $new['assigned_to']) {
            History::create([
                'user_id' => $new['assigned_by'],
                'packet_id' => $new['id'],
                'action'  => 'assigned'
            ]);
        }
        
        $api = request()->get('api',null);
        $results = [];

        if(!$api)
            return false;

        // To Poster
        $author = $event->packet->postedby;

        if($author) {
            $data = [
                'name' => $author->name,
                'email' => $author->email,
                'content' => 'Paket pekerjaan dengan judul ' . $event->packet->title . ' telah diganti pemenangnya. <a href="'.route('admin.packets.detail', $event->packet->id).'">Klik disini</a> untuk lihat lebih detail'
            ];

            $mailable = new Notification($data);
            $results[] = $api->sendEmail([
                'email_to' => $author->email,
                'subject'  => 'Pengumuman Pergantian Pemenang Paket Pekerjaan',
                'content'  => $mailable->build()
            ]);
        }

        // To AssignedTo
        $assignedto = $event->packet->assignedto;

        if($assignedto) {
            $data = [
                'name' => $assignedto->name,
                'email' => $assignedto->email,
                'content' => 'Anda telah dibatalkan menjadi pemenang paket pekerjaan dengan judul ' . $event->packet->title . '. <a href="'.route('expert.packets.detail', $event->packet->id).'">Klik disini</a> untuk lihat lebih detail'
            ];

            $mailable = new Notification($data);
            $results[] = $api->sendEmail([
                'email_to' => $assignedto->email,
                'subject'  => 'Pengumuman Pembatalan Pemenang Paket Pekerjaan',
                'content'  => $mailable->build()
            ]);
        }
    }
}
