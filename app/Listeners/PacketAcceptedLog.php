<?php

namespace PKMS\Listeners;

use PKMS\Mail\Notification;
use PKMS\Events\PacketAccepted;
use PKMS\Models\PacketHistory as History;

class PacketAcceptedLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PacketAccepted  $event
     * @return void
     */
    public function handle(PacketAccepted $event)
    {
        $old = $event->packet->getOriginal();
        $new = $event->packet->getAttributes();

        if(!$old['is_accepted'] && $new['is_accepted']) {
            History::create([
                'user_id' => $new['assigned_to'],
                'packet_id' => $new['id'],
                'action'  => 'accepted'
            ]);
        }
        
        $api = request()->get('api',null);
        $results = [];

        if(!$api)
            return false;

        // To Poster
        $author = $event->packet->postedby;

        if($author) {
            $data = [
                'name' => $author->name,
                'email' => $author->email,
                'content' => 'Paket pekerjaan dengan judul ' . $event->packet->title . ' telah diterima dan akan segera dimulai pekerjaannya. <a href="'.route('admin.packets.detail', $event->packet->id).'">Klik disini</a> untuk lihat lebih detail'
            ];

            $mailable = new Notification($data);
            $results[] = $api->sendEmail([
                'email_to' => $author->email,
                'subject'  => 'Paket Pekerjaan Disetujui',
                'content'  => $mailable->build()
            ]);
        }

        // To AssignedTo
        $assignedto = $event->packet->assignedto;

        if($assignedto) {
            $data = [
                'name' => $assignedto->name,
                'email' => $assignedto->email,
                'content' => 'Anda telah menyetujui pengerjaan untuk paket pekerjaan dengan judul ' . $event->packet->title . '. <a href="'.route('expert.packets.detail', $event->packet->id).'">Klik disini</a> untuk lihat lebih detail'
            ];

            $mailable = new Notification($data);
            $results[] = $api->sendEmail([
                'email_to' => $assignedto->email,
                'subject'  => 'Menyetujui Paket Pekerjaan',
                'content'  => $mailable->build()
            ]);
        }
    }
}
