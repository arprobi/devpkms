<?php

namespace PKMS\Listeners;

use PKMS\Mail\Register;
use PKMS\Events\UserRegistered;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use PKMS\Lib\Broker;

class UserRegisteredNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $api = request()->get('api',null);
        $results = [];

        if(!$api)
            return false;

        $mailable = new Register($event->user);

        $results[] = $api->sendEmail([
            'email_to' => $event->user->email,
            'subject'  => 'Selamat Datang',
            'content'  => $mailable->build()
        ]);
    }
}
