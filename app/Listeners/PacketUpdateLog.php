<?php

namespace PKMS\Listeners;

use PKMS\Mail\Notification;
use PKMS\Events\PacketUpdate;

class PacketUpdateLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PacketUpdate  $event
     * @return void
     */
    public function handle(PacketUpdate $event)
    {
        $api = request()->get('api',null);
        $results = [];

        if(!$api)
            return false;


        // To Poster
        $author = $event->packet->postedby;

        if($author) {
            $data = [
                'name' => $author->name,
                'email' => $author->email,
                'content' => 'Paket pekerjaan dengan judul ' . $event->packet->title . ' telah diupdate. <a href="'.route('admin.packets.detail', $event->packet->id).'">Klik disini</a> untuk lihat lebih detail'
            ];

            $mailable = new Notification($data);
            $results[] = $api->sendEmail([
                'email_to' => $author->email,
                'subject'  => 'Update Paket Pekerjaan',
                'content'  => $mailable->build()
            ]);
        }

        // To AssignedTo
        $assignedto = $event->packet->assignedto;

        if($assignedto) {
            $data = [
                'name' => $assignedto->name,
                'email' => $assignedto->email,
                'content' => 'Paket pekerjaan dengan judul ' . $event->packet->title . ' telah diupdate. <a href="'.route('expert.packets.detail', $event->packet->id).'">Klik disini</a> untuk lihat lebih detail'
            ];

            $mailable = new Notification($data);
            $results[] = $api->sendEmail([
                'email_to' => $assignedto->email,
                'subject'  => 'Update Paket Pekerjaan',
                'content'  => $mailable->build()
            ]);
        }
    }
}
