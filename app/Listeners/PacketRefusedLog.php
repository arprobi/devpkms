<?php

namespace PKMS\Listeners;

use PKMS\Mail\Notification;
use PKMS\Events\PacketRefused;
use PKMS\Models\PacketHistory as History;

class PacketRefusedLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PacketRefused  $event
     * @return void
     */
    public function handle(PacketRefused $event)
    {
        History::create([
            'user_id' => $event->user->id,
            'packet_id' => $event->packet->id,
            'action'  => 'refused',
            'text' => $event->message
        ]);
    }
}
