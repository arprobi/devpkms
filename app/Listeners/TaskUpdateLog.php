<?php

namespace PKMS\Listeners;

use PKMS\Mail\Notification;
use PKMS\Events\TaskUpdate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TaskUpdateLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TaskUpdate  $event
     * @return void
     */
    public function handle(TaskUpdate $event)
    {
        $api = request()->get('api',null);
        $results = [];

        if(!$api)
            return false;


        // To Poster
        $author = $event->task->postedby;

        if($author) {
            $data = [
                'name' => $author->name,
                'email' => $author->email,
                'content' => 'Tugas dengan judul ' . $event->task->title . ' telah diupdate. <a href="'.route('admin.tasks.detail', $event->task->id).'">Klik disini</a> untuk lihat lebih detail'
            ];

            $mailable = new Notification($data);
            $results[] = $api->sendEmail([
                'email_to' => $author->email,
                'subject'  => 'Update tugas',
                'content'  => $mailable->build()
            ]);
        }

        // To AssignedTo
        $assignedto = $event->task->assignedto;

        if($assignedto) {
            $data = [
                'name' => $assignedto->name,
                'email' => $assignedto->email,
                'content' => 'Tugas dengan judul ' . $event->task->title . ' telah diupdate. <a href="'.route('expert.tasks.detail', $event->task->id).'">Klik disini</a> untuk lihat lebih detail'
            ];

            $mailable = new Notification($data);
            $results[] = $api->sendEmail([
                'email_to' => $assignedto->email,
                'subject'  => 'Update tugas',
                'content'  => $mailable->build()
            ]);
        }
    }
}
