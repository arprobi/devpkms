<?php

namespace PKMS\Listeners;

use PKMS\Events\Invitation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use PKMS\Mail\Invitation as InvitationMail;

class InvitationMailer
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Invitation  $event
     * @return void
     */
    public function handle(Invitation $event)
    {
        $api = request()->get('api',null);
        $results = [];

        if(!$api)
            return false;

        foreach($event->lists as $list)
        {
            $mailable = new InvitationMail($list);

            $results[] = $api->sendEmail([
                'email_to' => $list->email,
                'subject'  => $list->invitation->title,
                'content'  => $mailable->build()
            ]);
        }
    }
}
