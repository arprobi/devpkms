<?php

namespace PKMS\Listeners;

use PKMS\Mail\Notification;
use PKMS\Events\PacketFinished;
use PKMS\Models\PacketHistory as History;

class PacketFinishedLog
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PacketFinished  $event
     * @return void
     */
    public function handle(PacketFinished $event)
    {
        $old = $event->packet->getOriginal();
        $new = $event->packet->getAttributes();

        if (!$old['is_accepted'] && $new['is_accepted']) {
            History::create([
                'user_id' => $new['assigned_to'],
                'packet_id' => $new['id'],
                'action' => 'finished',
                'text' => $event->packet->rating->rating
            ]);
        }


        $api = request()->get('api', null);
        $results = [];

        if (!$api)
            return false;

        // To Poster
        $author = $event->packet->postedby;

        if ($author) {
            $data = [
                'name' => $author->name,
                'email' => $author->email,
                'content' => 'Paket pekerjaan dengan judul ' . $event->packet->title . ' telah dikerjakan dan selesai. <a href="' . route('admin.packets.detail', $event->packet->id) . '">Klik disini</a> untuk lihat lebih detail'
            ];

            $mailable = new Notification($data);
            $results[] = $api->sendEmail([
                'email_to' => $author->email,
                'subject' => 'Paket Pekerjaan Telah Selesai',
                'content' => $mailable->build()
            ]);
        }

        // To AssignedTo
        $assignedto = $event->packet->assignedto;

        if ($assignedto) {
            $data = [
                'name' => $assignedto->name,
                'email' => $assignedto->email,
                'content' => 'Anda telah menyelesaikan pekerjaan untuk paket pekerjaan dengan judul ' . $event->packet->title . ' review : "' . $event->review . '". <a href="' . route('expert.packets.detail', $event->packet->id) . '">Klik disini</a> untuk lihat lebih detail'
            ];

            $mailable = new Notification($data);
            $results[] = $api->sendEmail([
                'email_to' => $assignedto->email,
                'subject' => 'Pekerjaan Anda telah disetujui',
                'content' => $mailable->build()
            ]);
        }
    }

}
