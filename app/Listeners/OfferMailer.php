<?php

namespace PKMS\Listeners;

use PKMS\Events\Offer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use PKMS\Mail\Offer as OfferMail;

class OfferMailer
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Invitation  $event
     * @return void
     */
    public function handle(Offer $event)
    {
        $api = request()->get('api',null);
        $results = [];

        if(!$api)
            return false;

        foreach($event->lists as $list)
        {
            $mailable = new OfferMail($list);

            $results[] = $api->sendEmail([
                'email_to' => $list->email,
                'subject'  => $list->invitation->title,
                'content'  => $mailable->build()
            ]);
        }
    }
}
