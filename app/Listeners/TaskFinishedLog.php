<?php

namespace PKMS\Listeners;

use PKMS\Events\TaskUpdate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use PKMS\Models\TaskHistory as History;

class TaskFinishedLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TaskFinished  $event
     * @return void
     */
    public function handle(TaskUpdate $event)
    {
        $old = $event->task->getOriginal();
        $new = $event->task->getAttributes();

        if(!$old['is_accepted'] && $new['is_accepted']) {
            History::create([
                'user_id' => $new['assigned_to'],
                'task_id' => $new['id'],
                'action'  => 'finished',
                'text' =>    $event->task->rating->rating
            ]);
        }
    }
}
