<?php

namespace PKMS\Listeners;

use PKMS\Events\Invitation;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvitationSMS
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Invitation  $event
     * @return void
     */
    public function handle(Invitation $event)
    {
        //
    }
}
