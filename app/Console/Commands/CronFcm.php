<?php

namespace PKMS\Console\Commands;

use Illuminate\Console\Command;

use DB;
use Storage;
use PKMS\Models\User;
use PKMS\Models\FcmToken;
use PKMS\Models\FirebaseMobile;

class CronFcm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:fcm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send scheduled firebase cloud messaging';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = DB::table('pkms_firebase_mobiles')
            ->where('sending_time', 'LIKE', '%'.date('Y-m-d H').'%')
            ->where('sent_status', 0)
            ->get();

        $type = ['type' => 5, 'detail_id' => '0'];

        if ($data->count()) {
            Storage::append('/cron/cron.txt', date('Y-m-d H:m:s').' Get cron');
            foreach ($data as $send) {
                $fcm    = new FirebaseMobile();
                if ($send->user_id) {
                    $user   = DB::table('pkms_fcm_tokens')->where('user_id', $send->user_id)->first();
                    $result = $fcm->sendToDevice($user->token, $send->title, $send->message, [$user->id], $type, null);
                } else {
                    $result = $fcm->sendToAll($send->title, $send->message, $type, null);
                }

                //check if success
                if ($result['success']) {
                    # code...
                    Storage::append('/cron/cron.txt', date('Y-m-d H:m:s').' Notif sent');
                }else{
                    Storage::append('/cron/cron.txt', date('Y-m-d H:m:s').' Notif failed to send');
                }

                //update data status
                $update = DB::table('pkms_firebase_mobiles')
                            ->where('id', $send->id)
                            ->update(['sent_status' => $result['success'] ? '1' : '2']);
            }
        }else{
            Storage::append('/cron/cron.txt', date('Y-m-d H:m:s').' No cron found');
        }
        echo 'Cron executed';
    }
}
