<?php

namespace PKMS\Lib;

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;

class Api
{

    protected $client;
    protected $header;
    public $error;

    public function __construct($url, $key)
    {
        $this->client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $url,
        ]);

        $this->header = array(
            'X-API-KEY' => $key,
            'Accept' => 'application/json'
        );
    }

    public function getError()
    {
        return $this->error;
    }

    private function _get($url, $data = NULL)
    {
        $result = null;
        try {
            $response = $this->client->get($url, [
                'headers' => $this->header,
                'query' => $data,
            ]);

            $result = $response->getBody();
        } catch (ClientException $e) {
            $this->error = json_decode($e->getResponse()->getBody())->error;
        }
        return json_decode($result);
    }

    private function _post($url, $data = NULL)
    {
        $result = null;
        try {
            $response = $this->client->post($url, [
                'headers' => $this->header,
                'form_params' => $data
            ]);

            $result = $response->getBody();
        } catch (ClientException $e) {
            $this->error = json_decode($e->getResponse()->getBody())->error;
        }
        return json_decode($result);
    }

    public function sendEmail($params)
    {
        $params['broker_id'] = env("SSO_BROKER_ID");
        $params['email_from'] = env("SSO_EMAIL");
        $params['email_from_name'] = env("SSO_EMAIL_NAME");
        $url = "send_email";
        return $this->_post($url, $params);
    }

    public function getAgencies($params = null)
    {
        return $this->_get('kldis', $params);
    }

    public function getAgency($id)
    {
        $params = [
            'kldi_id' => $id
        ];
        return $this->_get('kldi', $params);
    }

    public function login($email, $password)
    {
        $params = [
            'email' => $email,
            'password' => $password
        ];
        return $this->_post('login', $params);
    }

}
