<?php

namespace PKMS\Lib;

use Jasny\SSO\Broker as JasnyBroker;

class Broker extends JasnyBroker
{

    protected function getRequestUrl($command, $params = [])
    {
        $params['command'] = $command;

        if ($this->isAttached()) {
            $params['sso_session'] = $this->getSessionId();
        }

        return $this->url . '?' . http_build_query($params);
    }

    public function __call($fn, $args)
    {
        $fn = preg_replace_callback('/^(get|delete)_(.)/', function($matches) {
            return $matches[1] . strtoupper($matches[2]);
        }, $fn);
        try {
            return parent::__call($fn, $args);
        } catch (SSO\Exception $e) {
            $this->error_message = $e->getMessage();
        }

        return FALSE;
    }

}
