<?php

namespace PKMS\Lib;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Custom implements FilterInterface
{
    public function applyFilter(Image $img)
    {
        return $img->resize(240, null, function ($constraint) {
		    $constraint->aspectRatio();
		})->crop(240, 180, 0, 0);
    }
}
