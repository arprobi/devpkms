<?php

namespace PKMS\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use PKMS\Models\Packet;

class PacketUpdate
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $packet;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Packet $packet)
    {
        $this->packet = $packet;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
