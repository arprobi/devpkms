<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use Mpociot\Firebase\SyncsWithFirebase;

use PKMS\Models\User;
use PKMS\Models\UserNotification;
use PKMS\Models\FcmToken;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class FirebaseMobile extends Model
{
	protected $table    = 'pkms_firebase_mobiles';
    protected $fillable = ['user_id', 'sending_time', 'message'];
    protected $appends  = ['status'];
    
    public function user()
    {
    	return $this->belongsTo('PKMS\Models\User', 'user_id');
    }

    public function getStatusAttribute()
    {
        if ($this->attributes['sent_status'] == 0) {
            return '<span class="label label-warning">Pending</span>';
        } else if ($this->attributes['sent_status'] == 2) {
            return '<span class="label label-danger">Gagal</span>';
        } else {
            return '<span class="label label-success">Terkirim</span>';
        }

    }

    public function sendToAll($title, $message, $type = [], $time = '')
    {
    	$optionBuilder  = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        // $notificationBuilder = new PayloadNotificationBuilder($title);
        // $notificationBuilder->setBody($message)->setSound('default');

        $dataBuilder    = new PayloadDataBuilder();

        $dataBuilder->addData(['title' => $title, 'body' => $message, 'time' => $time, 'icon' => 'default']);

        $option         = $optionBuilder->build();
        // $notification   = $notificationBuilder->build();
        $data           = $dataBuilder->build();

        $token          = FcmToken::where('status', 1)->pluck('token')->toArray();

        $downstreamResponse = FCM::sendTo($token, $option, null, $data);

        // insert user notfication
        $user_id = FcmToken::pluck('user_id')->toArray();
        $this->saveNotif($user_id, $title, $message, $type);

        $result = [
            'success'   => $downstreamResponse->numberSuccess(),
            'failed'    => $downstreamResponse->numberFailure()
        ];

        return $result;
    }

    public function sendToDevice($token, $title, $message, $user_id = [], $type = [], $time = '')
    {
    	$optionBuilder  = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        // $notificationBuilder = new PayloadNotificationBuilder($title);
        // $notificationBuilder->setBody($message)->setSound('default');

        $dataBuilder    = new PayloadDataBuilder();

        $dataBuilder->addData(['title' => $title, 'body' => $message, 'time' => $time, 'icon' => 'default']);

        $option         = $optionBuilder->build();
        // $notification   = $notificationBuilder->build();
        $data           = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($token, $option, null, $data);

        // insert user notfication
        $this->saveNotif($user_id, $title, $message, $type);

        $result = [
            'success'   => $downstreamResponse->numberSuccess(),
            'failed'    => $downstreamResponse->numberFailure()
        ];

        return $result;
    }

    public function saveNotif($user_id = [], $title, $body, $type = [])
    {
        if (count($user_id)) {
            for ($i=0; $i < count($user_id); $i++) { 
                $notif = new UserNotification();
                $notif->user_id = $user_id[$i];
                $notif->title   = $title;
                $notif->body    = $body;
                if (count($type)) {
                    $notif->type        = $type['type'];
                    $notif->detail_id   = $type['detail_id'];
                }
                $notif->read    = 0;
                $notif->save();
            }
        }
    }
}
