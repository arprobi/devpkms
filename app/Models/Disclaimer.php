<?php

namespace PKMS\Models;

use Response;
use View;
use Illuminate\Database\Eloquent\Model;

class Disclaimer extends Model
{
    protected $table = 'pkms_disclaimers';

    protected $rules = [
        'title' 	=> ['required'],
        'file' 		=> ['required']
    ];

    protected $appends = ['view'];

    public function getCreatedAtAttribute($value)
    {
        return date('d M Y', strtotime($value));
    }

    public function getUpdatedAtAttribute($value)
    {
        return date('d M Y', strtotime($value));
    }

    public function getViewAttribute($value='')
    {
        $html = View::make('modules.admin.disclaimer.show')
                    ->with('content', $this->attributes['content'])->render();

        return $html;
    }
}
