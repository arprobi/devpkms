<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = 't_ratings';
    protected $guarded = [];
}
