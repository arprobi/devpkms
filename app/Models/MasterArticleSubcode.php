<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use PKMS\Models\MasterArticleCode;
use Carbon\Carbon;

class MasterArticleSubcode extends Model
{
    protected $table = 'm_articlesubcode';
    protected $guarded = [];
    protected $appends = ['updated_faqs'];

    // getter
    public function getUpdatedFaqsAttribute()
    {
        $date   = $nowdate= Carbon::now()->subDays(3);
        $faq    = $this->faqs()->where('updated_at', '>=', $date->toDateTimeString() )->get()->count();
        return $faq ? 1:0;
    }

    // relation
    public function articlecode()
    {
    	return $this->belongsTo(MasterArticleCode::class, 'code');
    }

    public function faqs()
    {
    	return $this->hasMany(Faq::class, 'article_subcode');
    }
}
