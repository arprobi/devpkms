<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'm_cities';
    protected $hidden = ['id','province_id'];

    public function districts()
    {
        return $this->hasMany('PKMS\District','city_id','code');
    }

    public function getNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }
}
