<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;


class ResponseChoice extends Model
{
    use ValidatingModelTrait;

    protected $table = 'pkms_response_choices';

    protected $rules = [
        'choice' 				=> ['required'],
        'response_question_id' 	=> ['required']
    ];


    public function question()
    {
    	return $this->belongsTo('PKMS\Models\ResponseQuestion', 'response_question_id');
    }

    public function answers()
    {
        return $this->hasMany('PKMS\Models\ResponseAnswer', 'answer');
    }
}
