<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;


class Message extends Model implements ValidatingModelInterface
{
    use ValidatingModelTrait;

    protected $table = 't_messages';
    protected $guarded = [];

    protected $rules = [
        'from'      => ['required'],
        'to'        => ['required'],
        'message'   => ['required']
    ];

    public function fromUser()
    {
        return $this->belongsTo('PKMS\Models\User','from');
    }

    public function toUser()
    {
        return $this->belongsTo('PKMS\Models\User','to');
    }

    public function replies()
    {
        return $this->hasMany('PKMS\Models\Message', 'reply_id');
    }
}
