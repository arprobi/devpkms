<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use PKMS\Models\Task;

class TaskHistory extends Model implements ValidatingModelInterface
{
    use ValidatingModelTrait;

    protected $table = 't_tasks_history';
    protected $guarded = [];
    protected $rules = [
        'task_id'   => ['required'],
        'user_id'   => ['required'],
        'action'    => ['required']
    ];

    public function task()
    {
    	return $this->belongsTo(Task::class);
    }
}
