<?php

namespace PKMS\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;

class Pasal extends Model
{
    use ValidatingModelTrait;

    protected $table = 'pkms_pasals';

    protected $rules = [
        'nama_pasal' 	=> ['required'],
        'isi_pasal' 	=> ['required'],
        'perpres_id' 	=> ['required']
    ];

    protected $appends = ['kategori', 'bookmarked', 'custom_highlight'];

    protected $fillable = ['nama_pasal', 'penjelasan', 'isi_pasal', 'perpres_id'];

    /*
    Relation collections
    */
    public function perpres()
    {
    	return $this->belongsTo('PKMS\Models\Perpres', 'perpres_id');
    }

    public function categories()
    {
        return $this->belongsToMany('PKMS\Models\Kategori', 'pkms_kategori_pasal', 'pasal_id', 'kategori_id');
    }

    public function bookmarked()
    {
        return $this->belongsToMany('PKMS\Models\User', 'pkms_pasal_bookmarks', 'user_id', 'pasal_id');
    }

    public function highlights()
    {
        return $this->belongsTo(Highlight::class, 'pasal_id');
    }

    /*
    Setter end getter
    */

    public function getKategoriAttribute($value)
    {
        $kategories = $this->categories;
        $string = '';
        foreach ($kategories as $kategori) {
            $string .= '<span class="label label-info">'.$kategori->nama_kategori.'</span> ';
        }
        return $string;
    }

    public function getBookmarkedAttribute()
    {
        if (Auth::guard('api')->check()) {
            $book = Auth::guard('api')->user()->pasals()->pluck('pasal_id')->toArray();
            if (in_array($this->attributes['id'], $book)) {
                return 1;
            }
            return 0;
        }
        return 0;
    }

    public function getCustomHighlightAttribute()
    {
        $anchor = str_replace('hgltid="', 'href="pkms://highlight/', $this->attributes['highlighted']);

        $custom = str_replace('<highlighted', '<a', $anchor);
        $final = str_replace('</highlighted>', '</a>', $custom);
        
        return $final;
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('order', 'asc')->get();
    }
}
