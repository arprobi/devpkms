<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;


class ResponseQuestion extends Model
{
    use ValidatingModelTrait;

    protected $table = 'pkms_response_questions';

    protected $rules = [
        'question' 		=> ['required'],
        'is_selection' 	=> ['required']
    ];

    protected $append = ['jenis'];

    public function getJenisAttribute()
    {
        $status = '';
        if ($this->attributes['is_selection'] == 1) {
            $status = 'Pilihan';
        } 

        if ($this->attributes['is_selection'] == 0) {
            $status = 'Isian';
        }

        return $status;
    }


    public function choices()
    {
    	return $this->hasMany('PKMS\Models\ResponseChoice', 'response_question_id');
    }

    public function answers()
    {
    	return $this->hasMany('PKMS\Models\ResponseAnswer', 'response_question_id');
    }
}
