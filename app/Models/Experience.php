<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use Esensi\Model\Contracts\PurgingModelInterface;
use Esensi\Model\Traits\PurgingModelTrait;

class Experience extends Model implements ValidatingModelInterface, PurgingModelInterface
{
    use ValidatingModelTrait;
    use PurgingModelTrait;

    protected $table = 't_experiences';
    protected $guarded = [];
    protected $rules = [
        'title'         => ['required'],
        'user_id'       => ['required'],
        'position'      => ['required'],
        'from'          => ['required','date']
    ];
    protected $purgeable = [
        'lampiran'
    ];

    public function setFromAttribute($value)
    {
        $this->attributes['from'] = date('Y-m-01', strtotime($value));
    }

    public function getFromAttribute($value)
    {
        return date('F Y', strtotime($value));
    }

    public function setToAttribute($value)
    {
        $this->attributes['to'] = ($value) ? date('Y-m-01', strtotime($value)) : null;
    }

    public function getToAttribute($value)
    {
        return ($value) ? date('F Y', strtotime($value)) : '-';
    }

    public function attachments()
    {
        return $this->belongsToMany('PKMS\Models\Attachment', 'p_attachment_experience');
    }
}
