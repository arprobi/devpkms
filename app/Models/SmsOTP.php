<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

class SmsOTP extends Model
{
    protected $table = 'pkms_otp';
}
