<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use PKMS\Models\User;
use PKMS\Models\InvitationList;

class Invitation extends Model
{
    protected $table = 't_invitations';
    protected $guarded = [];

    /**
     * Mutators
     */

     public function getCreatedAtAttribute($value)
     {
         return date('d M Y H:i', strtotime($value));
     }

    public function getUpdatedAtAttribute($value)
    {
        return date('d M Y H:i', strtotime($value));
    }

    public function getFromAttribute($value)
     {
         return date('d M Y H:i', strtotime($value));
     }

    public function getUntilAttribute($value)
    {
        return date('d M Y H:i', strtotime($value));
    }

    public function setFromAttribute($value)
    {
        $this->attributes['from'] = date('Y-m-d H:i:s', strtotime($value));
    }

    public function setUntilAttribute($value)
    {
        $this->attributes['until'] = date('Y-m-d H:i:s', strtotime($value));
    }

    public function lists()
    {
        return $this->hasMany(InvitationList::class);
    }
}
