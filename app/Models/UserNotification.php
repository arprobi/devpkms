<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
    protected $table = 'pkms_user_notifications';

    protected $fillable = ['user_id', 'title', 'body', 'read'];

    public function user()
    {
    	return $this->belongsTo('PKMS\Models\User', 'user_id');
    }
}
