<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

class Highlight extends Model
{
    protected $table    = 'pkms_highlights';
    protected $fillable = ['words', 'user_id', 'pasal_id'];
    protected $appends  = ['original'];

    /**
     * Setter and Getter
     */
    public function getOriginalAttribute()
    {
        return strip_tags($this->attributes['words']);
    }

    /**
     * Relationships
     */
    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function pasal()
    {
    	return $this->belongsTo(Pasal::class, 'pasal_id');
    }

    public function comments()
    {
        return $this->hasMany(HighlightComment::class, 'highlight_id');
    }
}
