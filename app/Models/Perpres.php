<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;

class Perpres extends Model
{
    use ValidatingModelTrait;

    protected $table = 'pkms_perpres';

    protected $rules = [
        'nama_perpres' 	=> ['required']
    ];

    protected $fillable = ['nama_perpres'];

    public function pasals()
    {
    	return $this->hasMany('PKMS\Models\Pasal', 'perpres_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return date('d M Y', strtotime($value));
    }
}
