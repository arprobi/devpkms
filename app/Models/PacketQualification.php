<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use PKMS\Models\User;
use PKMS\Models\Rating;
use PKMS\Models\Article;
use PKMS\Models\Attachment;
use PKMS\Models\Comment;
use PKMS\Events\TaskUpdate;

class PacketQualification extends Model implements ValidatingModelInterface
{

    use ValidatingModelTrait;

    protected $table = 't_packet_qualifications';
    protected $guarded = [];
    protected $rules = [
        'name' => ['required']
    ];

    /**
     * Relationships
     */
    public function attachments()
    {
        return $this->belongsToMany(Attachment::class, 'p_attachment_packet_qualification');
    }

    public function Packet()
    {
        return $this->belongsTo(Packet::class);
    }

}
