<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;


class ResponseAnswer extends Model
{
    protected $table = 'pkms_response_answers';
    protected $appends = ['choice_answer'];
    protected $fillable = ['response_question_id', 'user_id', 'is_selection', 'answer'];

    public function question()
    {
    	return $this->belongsTo('PKMS\Models\ResponseQuestion', 'response_question_id');
    }

    public function choice()
    {
    	if ($this->attributes['is_selection'] == 1) {
    		return $this->belongsTo('PKMS\Models\ResponseChoice', 'response_question_id');
    	}
    }

    public function user()
    {
        return $this->belongsTo('PKMS\Models\User', 'user_id');
    }

    public function getChoiceAnswerAttribute()
    {
        if ($this->attributes['is_selection']) {
            if ($data = ResponseChoice::findOrFail($this->attributes['answer'])) {
                return $data->choice;
            }           
            return 0;
        }

        return 0;
    }
}
