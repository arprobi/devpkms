<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

class FcmToken extends Model
{
    protected $table = 'pkms_fcm_tokens';
    protected $fillable = ['user_id', 'token'];

    public function user()
    {
    	return $this->belongsTo('PKMS\Models\User', 'user_id');
    }
}
