<?php

namespace PKMS\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use Esensi\Model\Contracts\PurgingModelInterface;
use Esensi\Model\Traits\PurgingModelTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use PKMS\Events\UserRegistered;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements ValidatingModelInterface, PurgingModelInterface
{
    use HasApiTokens;
    use Notifiable;
    use ValidatingModelTrait;
    use PurgingModelTrait;

    protected $table = 't_users';
    protected $guarded = [];
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $rules = [
        'name'          => ['required','min:3'],
        'email'         => ['required', 'email', 'unique'],
        'username'      => ['required','min:3', 'unique'],
        'password'      => ['required']
    ];
    protected $purgeable = [
        'old_password'
    ];
    protected $events = [
        'created' => UserRegistered::class
    ];

    /**
     * Mutators
     */

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    // public function getAvatarAttribute($value)
    // {
    //     return asset('storage/'. $value);
    // }

    /**
     * Helper
     */

    public function averageRating()
    {
        return $this->ratings()->selectRaw('avg(rating) as aggeragate, user_id')
            ->groupBy('user_id');
    }

    // For expert user to gain acces to task, they need to completed their profile, education & experiences
    public function isComplete()
    {
        return $this->hasProfile() && $this->hasExperience() && $this->educations();
    }

    public function hasProfile()
    {
        return $this->profile()->count();
    }

    public function hasExperience()
    {
        return $this->experiences()->count();
    }

    public function hasEducation()
    {
        return $this->educations()->count();
    }

    public function knowledges()
    {
        return $this->belongsToMany('PKMS\Models\Faq', 'pkms_faq_bookmarks', 'user_id', 'faq_id');
    }

    public function pasals()
    {
        return $this->belongsToMany('PKMS\Models\Pasal', 'pkms_pasal_bookmarks', 'user_id', 'pasal_id');
    }

    public function discuss()
    {
        return $this->belongsToMany('PKMS\Models\Question', 'pkms_diskusi_bookmarks', 'user_id', 'question_id');
    }

    /**
     * Relationships
     */

    public function inbox()
    {
        return $this->hasMany('PKMS\Models\Message', 'to');
    }

    public function outbox()
    {
        return $this->hasMany('PKMS\Models\Message', 'from');
    }

    public function my_tasks()
    {
        return $this->hasMany('PKMS\Models\Task', 'assigned_to');
    }
    
    public function my_packets()
    {
        return $this->hasMany('PKMS\Models\Packet', 'assigned_to');
    }

    public function task_history()
    {
        return $this->hasMany('PKMS\Models\TaskHistory', 'user_id');
    }
    
    public function packet_history()
    {
        return $this->hasMany('PKMS\Models\PacketHistory', 'user_id');
    }

    public function profile()
    {
        return $this->hasOne('PKMS\Models\Profile');
    }

    public function experiences()
    {
        return $this->hasMany('PKMS\Models\Experience');
    }

    public function educations()
    {
        return $this->hasMany('PKMS\Models\Education');
    }

    public function ratings()
    {
        return $this->hasMany('PKMS\Models\Rating');
    }

    public function interests()
    {
        return $this->belongsToMany('PKMS\Models\Interest', 'p_user_interest');
    }

    public function tasks()
    {
        return $this->hasMany('PKMS\Models\Task', 'assigned_to');
    }
    
    public function packets()
    {
        return $this->hasMany('PKMS\Models\Packet', 'assigned_to');
    }

    public function offers()
    {
        return $this->hasMany('PKMS\Models\PacketOffer', 'user_id');
    }

    public function usulans()
    {
        return $this->hasMany('PKMS\Models\Usulan', 'user_id');
    }
    
    public function packetRatings()
    {
        return $this->hasMany('PKMS\Models\PacketOffer', 'user_id');
    }

    public function token()
    {
        return $this->hasOne('PKMS\Models\FcmToken', 'user_id');
    }

    public function likes()
    {
        return $this->belongsToMany('PKMS\Models\Faq', 'pkms_user_like_usulans', 'user_id', 'usulan_id')->withTimestamps();
    }

    public function highlights()
    {
        return $this->belongsTo(Highlight::class, 'user_id');
    }
}
