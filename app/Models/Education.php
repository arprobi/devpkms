<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;

class Education extends Model implements ValidatingModelInterface
{
    use ValidatingModelTrait;

    protected $table = 't_educations';
    protected $guarded = [];
    protected $rules = [
        'user_id'       => ['required'],
        'education_id'  => ['required'],
        'name'          => ['required']
    ];

    public function detail()
    {
        return $this->hasOne('PKMS\Models\MasterEducation','id','education_id');
    }
}
