<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use Auth;

class Usulan extends Model
{
    use ValidatingModelTrait;

    protected $table = 'pkms_usulans';

    protected $rules = [
        'user_id' 	=> ['required'],
        'usulan' 	=> ['required'],
        'status' 	=> ['required']
    ];

    protected $fillable = ['user_id', 'usulan', 'status'];

    protected $appends = ['status_usulan', 'like', 'is_liked'];

    public function user()
    {
    	return $this->belongsTo('PKMS\Models\User', 'user_id');
    }

    public function likes()
    {
        return $this->belongsToMany('PKMS\Models\User', 'pkms_user_like_usulans', 'usulan_id', 'user_id')->withTimestamps();
    }

    public function getStatusUsulanAttribute()
    {
        $status = '<span class="label label-primary label-sm"> Baru</span>';

        if ($this->attributes['status'] == 1) {
            $status = '<span class="label label-success label-sm"> Diproses</span>';
        } 

        if ($this->attributes['status'] == 3) {
            $status = '<span class="label label-danger label-sm"> Ditolak</span>';
        }

        return $status;
    }

    public function getLikeAttribute()
    {
        if (Auth::guard('api')->check()) {
            $usulan = Auth::guard('api')->user()->likes()->pluck('usulan_id')->toArray();
            if (in_array($this->attributes['id'], $usulan)) {
                return 1;
            }
            return 0;
        }

        return 0;
    }

    public function getIsLikedAttribute()
    {
        return $this->likes()->count();
    }

    public function getCreatedAtAttribute($value)
    {
        return date('d M Y', strtotime($value));
    }

    public function getUpdatedAtAttribute($value)
    {
        return date('d M Y', strtotime($value));
    }
}
