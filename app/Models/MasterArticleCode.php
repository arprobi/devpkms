<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use PKMS\Models\MasterArticleSubcode;

class MasterArticleCode extends Model
{
    protected $table = 'm_articlecode';
    protected $guarded = [];

    public function subcodes()
    {
    	return $this->hasMany(MasterArticleSubcode::class, 'code');
    }
}
