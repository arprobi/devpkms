<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use PKMS\Models\Packet;

class PacketOffer extends Model implements ValidatingModelInterface
{
    use ValidatingModelTrait;

    protected $table = 't_packet_offer';
    protected $guarded = [];
    protected $rules = [
        'packet_id'   => ['required'],
        'user_id'   => ['required'],
        'offer'    => ['required']
    ];

    public function packet()
    {
    	return $this->belongsTo(Packet::class);
    }
    
    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
