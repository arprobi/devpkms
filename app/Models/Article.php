<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use PKMS\Events\ArticleUpdated;
use PKMS\Models\Attachment;
use PKMS\Models\User;
use PKMS\Models\Task;

class Article extends Model
{
    protected $table = 't_articles';
    protected $guarded = [];

    /**
     * Mutators
     */

     public function getCreatedAtAttribute($value)
     {
         return date('d M Y', strtotime($value));
     }

    public function getUpdateAtAttribute($value)
    {
        return date('d M Y', strtotime($value));
    }

    /**
     * Relationships
     */

    public function tasks()
    {
        return $this->belongsToMany(Task::class, 'p_article_task');
    }

    public function task()
    {
        return $this->belongsTo(Task::class, 'p_article_task');
    }

    public function attachments()
    {
        return $this->belongsToMany(Attachment::class, 'p_attachment_article');
    }

    public function author()
    {
        return $this->belongsTo(User::class);
    }
}
