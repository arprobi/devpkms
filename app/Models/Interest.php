<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;

class Interest extends Model implements ValidatingModelInterface
{
	use ValidatingModelTrait;

    protected $table = 'm_interests';
    protected $guarded = [];
    protected $rules = [
        'name' => ['required']
    ];
}
