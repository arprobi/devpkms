<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $table = 'm_villages';
    protected $hidden = ['id','district_id'];

    public function getNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }
}
