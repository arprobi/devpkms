<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use PKMS\Models\User;
use PKMS\Models\Invitation;

class InvitationList extends Model
{
    protected $table = 't_invitations_list';
    protected $guarded = [];

    /**
     * Mutators
     */

     public function getCreatedAtAttribute($value)
     {
         return date('d M Y H:i', strtotime($value));
     }

    public function getUpdatedAtAttribute($value)
    {
        return date('d M Y H:i', strtotime($value));
    }

    public function invitation()
    {
        return $this->belongsTo(Invitation::class);
    }

    public function isRegistered()
    {
        $user = User::where('email', $this->email)->first();

        if($user)
            return true;
        else
            return false;
    }
}
