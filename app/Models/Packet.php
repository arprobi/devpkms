<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use PKMS\Models\User;
use PKMS\Models\PacketQualification;
use PKMS\Models\PacketSchedule;
use PKMS\Models\PacketPaid;
use PKMS\Models\Invitation;
use PKMS\Events\PacketUpdate;

class Packet extends Model implements ValidatingModelInterface
{

    use ValidatingModelTrait;

    protected $table = 't_packets';
    protected $guarded = [];
    protected $rules = [
//        'title' => ['required'],
//        'description' => ['required'],
//        'instansi_id' => ['required'],
//        'satuan_kerja' => ['required'],
//        'tahun_anggaran' => ['required'],
//        'kode_anggaran' => ['required'],
//        'anggaran' => ['required'],
//        'jangka_waktu' => ['required'],
//        'interval_waktu' => ['required'],
//        'jenis_pengadaan' => ['required'],
//        'jenis_penyedia' => ['required'],
//        'ppk_nama' => ['required'],
//        'ppk_nip' => ['required'],
//        'ppk_jabatan' => ['required'],
//        'pejabat_nama' => ['required'],
//        'pejabat_nip' => ['required'],
//        'pejabat_jabatan' => ['required'],
//        'province_code' => ['required'],
//        'city_code' => ['required'],
//        'cara_pembayaran' => ['required']
    ];

//    protected $events = [
//        'updated' => PacketUpdate::class
//    ];

    public function completed()
    {
        return $this->where('is_completed', 1);
    }

    /**
     * Mutators
     */
    public function getAssignedAtAttribute($value)
    {
        return ($value) ? date('d M y', strtotime($value)) : '-';
    }

    public function getAcceptedAtAttribute($value)
    {
        return ($value) ? date('d M y', strtotime($value)) : '-';
    }

    public function getCompletedAtAttribute($value)
    {
        return ($value) ? date('d M y', strtotime($value)) : '-';
    }

    public function getCreatedAtAttribute($value)
    {
        return ($value) ? date('d M y', strtotime($value)) : '-';
    }

    public function getUpdatedAtAttribute($value)
    {
        return ($value) ? date('d M y', strtotime($value)) : '-';
    }

    /**
     * Relationships
     */
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_code', 'code');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_code', 'code');
    }

    public function assignedby()
    {
        return $this->hasOne(User::class, 'id', 'assigned_by');
    }

    public function assignedto()
    {
        return $this->hasOne(User::class, 'id', 'assigned_to');
    }

    public function postedby()
    {
        return $this->hasOne(User::class, 'id', 'posted_by');
    }

    public function qualifications()
    {
        return $this->hasMany(PacketQualification::class);
    }

    public function schedule()
    {
        return $this->hasMany(PacketSchedule::class);
    }

    public function paid()
    {
        return $this->hasMany(PacketPaid::class);
    }

    public function offers()
    {
        return $this->hasMany(PacketOffer::class);
    }

    public function comments()
    {
        return $this->belongsToMany(Comment::class, 'p_comment_packet');
    }

    public function negotiations()
    {
        return $this->belongsToMany(Comment::class, 'p_comment_negotiation')
                        ->withPivot(['user_id','offer']);
    }

    public function attachments()
    {
        return $this->belongsToMany(Attachment::class, 'p_attachment_packet');
    }

    public function invitations()
    {
        return $this->belongsToMany(Invitation::class, 'p_packet_invitation');
    }

    public function interests()
    {
        return $this->belongsToMany(Interest::class, 'p_packet_interest');
    }

    public function userInvites()
    {
        return $this->belongsToMany(User::class, 'p_packet_user');
    }
    
    public function rating()
    {
        return $this->hasOne(PacketRating::class);
    }

}
