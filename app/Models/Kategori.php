<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;

class Kategori extends Model
{
    use ValidatingModelTrait;

    protected $table = 'pkms_kategoris';

    protected $rules = [
        'nama_kategori' 	=> ['required']
    ];

    protected $fillable = ['nama_kategori'];

    public function pasals()
    {
    	return $this->belongsToMany('PKMS\Models\Pasal', 'pkms_kategori_pasal', 'kategori_id', 'pasal_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return date('d M Y', strtotime($value));
    }
}
