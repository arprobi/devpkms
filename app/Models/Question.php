<?php

namespace PKMS\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;
use PKMS\Models\User;
use PKMS\Models\Answer;

class Question extends Model
{
    protected $table = 'pkms_questions';
    protected $guarded = [];
    protected $appends = ['bookmarked'];

    /**
     * Mutators
     */
    public function getCreatedAtAttribute($value)
    {
    	return date('d F Y', strtotime($value));
    }

    /**
     * Relationships
     */

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function bookmarked()
    {
        return $this->belongsToMany('PKMS\Models\User', 'pkms_diskusi_bookmarks', 'user_id', 'question_id');
    }

    public function getBookmarkedAttribute()
    {
        if (Auth::guard('api')->check()) {
            $book = Auth::guard('api')->user()->discuss()->pluck('question_id')->toArray();
            if (in_array($this->attributes['id'], $book)) {
                return 1;
            }
            return 0;
        }
        return 0;
    }

}
