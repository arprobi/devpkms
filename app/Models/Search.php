<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    protected $table = 'pkms_searches';

    public function getKeywordAttributes()
    {
    	return ucwords($this->keyword);
    }
}
