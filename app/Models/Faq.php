<?php

namespace PKMS\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use PKMS\Events\ArticleUpdated;
use PKMS\Models\User;
use PKMS\Models\Task;
use PKMS\Models\Komment;
use PKMS\Models\MasterArticleCode;
use PKMS\Models\MasterArticleSubcode;
use Carbon\Carbon;

class Faq extends Model
{
    protected $table = 'pkms_faqs';
    protected $guarded = [];
    protected $appends = ['share_url', 'bookmarked', 'small_img', 'medium_img', 'original_img', 'pembaruan', 'slug'];

    /**
     * Mutators
     */

    public function getCreatedAtAttribute($value)
    {
        return date('d M Y', strtotime($value));
    }

    public function getUpdatedAtAttribute($value)
    {
        return date('d M Y', strtotime($value));
    }

    public function setTagsAttribute($value)
    {
        $this->attributes['tags'] = ($value) ? implode(',', $value) : NULL;
    }

    public function getShareUrlAttribute()
    {
        return url('view', ['id' => $this->attributes['id']]);
    }

    public function getSmallImgAttribute()
    {
        return '/images/small/'.$this->attributes['image'];
    }

    public function getMediumImgAttribute()
    {
        return '/images/medium/'.$this->attributes['image'];
    }

    public function getOriginalImgAttribute()
    {
        return '/images/original/'.$this->attributes['image'];
    }

    public function getBookmarkedAttribute()
    {
        if (Auth::guard('api')->check()) {
            $book = Auth::guard('api')->user()->knowledges()->pluck('faq_id')->toArray();
            if (in_array($this->attributes['id'], $book)) {
                return 1;
            }
            return 0;
        }
        return 0;
    }

    public function getPembaruanAttribute()
    {
        $update = Carbon::parse($this->attributes['updated_at']);
        $nowdate= Carbon::now();
        if ($update->diffInDays($nowdate) <= 3) {
            return 1;
        }
        return 0;
    }

    public function getSlugAttribute()
    {
        return str_slug($this->attributes['title'], '-');
    }

    public function bookmarked()
    {
        return $this->belongsToMany('PKMS\Models\User', 'pkms_faq_bookmarks', 'user_id', 'faq_id');
    }

    public function getNmklasifikasiAttribute()
    {
        if ($this->attributes['klasifikasi'] == 1) {
            return 'Infografis';
        }

        if ($this->attributes['klasifikasi'] == 2) {
            return 'Studi kasus';
        }

        if ($this->attributes['klasifikasi'] == 3) {
            return 'Video';
        }

        if ($this->attributes['klasifikasi'] == 4) {
            return 'Materi paparan';
        }
    }

    /**
     * Relationships
     */

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function articlecode()
    {
        return $this->hasOne(MasterArticleCode::class, 'id', 'article_code');
    }

    public function articlesubcode()
    {
        return $this->hasOne(MasterArticleSubcode::class, 'id', 'article_subcode');
    }

    public function comments()
    {
        return $this->hasMany(Komment::class);
    }
}
