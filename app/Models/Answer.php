<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use PKMS\Models\User;
use PKMS\Models\Question;

class Answer extends Model
{
    protected $table = 'pkms_answers';
    protected $guarded = [];

    /**
     * Mutators
     */

     public function getCreatedAtAttribute($value)
     {
         return date('d M Y', strtotime($value));
     }

    public function getUpdatedAtAttribute($value)
    {
        return date('d M Y', strtotime($value));
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
