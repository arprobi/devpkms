<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use PKMS\Models\User;
use PKMS\Models\Rating;
use PKMS\Models\Article;
use PKMS\Models\Attachment;
use PKMS\Models\Comment;
use PKMS\Events\TaskUpdate;

class Task extends Model implements ValidatingModelInterface
{
    use ValidatingModelTrait;

    protected $table = 't_tasks';
    protected $guarded = [];
    protected $rules = [
        'title'         => ['required'],
        'description'   => ['required']
    ];
    protected $events = [
        'updated' => TaskUpdate::class
    ];

    public function completed()
    {
        return $this->where('is_completed',1);
    }

    /**
     * Mutators
     */

    public function getAssignedAtAttribute($value)
    {
        return ($value) ? date('d M y', strtotime($value)) : '-';
    }

    public function getAcceptedAtAttribute($value)
    {
        return ($value) ? date('d M y', strtotime($value)) : '-';
    }

    public function getCompletedAtAttribute($value)
    {
        return ($value) ? date('d M y', strtotime($value)) : '-';
    }

    public function getCreatedAtAttribute($value)
    {
        return ($value) ? date('d M y', strtotime($value)) : '-';
    }

    public function getUpdatedAtAttribute($value)
    {
        return ($value) ? date('d M y', strtotime($value)) : '-';
    }

    /**
     * Relationships
     */

    public function assignedby()
    {
        return $this->hasOne(User::class,'id', 'assigned_by');
    }

    public function assignedto()
    {
        return $this->hasOne(User::class, 'id', 'assigned_to');
    }

    public function postedby()
    {
        return $this->hasOne(User::class, 'id', 'posted_by');
    }

    public function rating()
    {
        return $this->hasOne(Rating::class);
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class, 'p_article_task');
    }

    public function comments()
    {
        return $this->belongsToMany(Comment::class, 'p_comment_task');
    }

    public function attachments()
    {
        return $this->belongsToMany(Attachment::class, 'p_attachment_task');
    }
}
