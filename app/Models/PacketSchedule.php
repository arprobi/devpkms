<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use PKMS\Models\Packet;

class PacketSchedule extends Model implements ValidatingModelInterface
{

    use ValidatingModelTrait;

    protected $table = 't_packet_schedule';
    protected $guarded = [];
    protected $rules = [
        'packet_id' => ['required'],
        'name' => ['required'],
        'start_date' => ['required'],
        'end_date' => ['required']
    ];

    /**
     * Mutators
     */
    public function getStartDateAttribute($value)
    {
        return ($value) ? date('Y-m-d H:i', strtotime($value)) : '-';
    }
    
    public function getEndDateAttribute($value)
    {
        return ($value) ? date('Y-m-d H:i', strtotime($value)) : '-';
    }

    
    /**
     * Relationships
     */
    

    public function Packet()
    {
        return $this->belongsTo(Packet::class);
    }

}
