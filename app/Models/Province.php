<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'm_provinces';
    protected $hidden = ['id'];

    public function cities()
    {
        return $this->hasMany('PKMS\City','province_id','code');
    }

    public function getNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }
}
