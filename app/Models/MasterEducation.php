<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;

class MasterEducation extends Model implements ValidatingModelInterface
{
	use ValidatingModelTrait;

    protected $table = 'm_educations';
    protected $guarded = [];
    protected $rules = [
        'name'          => ['required'],
        'code'         	=> ['required'],
    ];
}
