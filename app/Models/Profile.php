<?php

namespace PKMS\Models;

use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use Illuminate\Database\Eloquent\Model;
use PKMS\Models\Province;
use PKMS\Models\City;
use PKMS\Models\District;
use PKMS\Models\Village;

class Profile extends Model implements ValidatingModelInterface
{
    use ValidatingModelTrait;

    protected $table = 't_profiles';
    protected $guarded = ['user_id'];
    protected $rules = [
        'user_id'       => ['required'],
        'gender'        => ['required'],
        'birth_place'   => ['required'],
        'birth_date'    => ['required'],
        'address'       => ['required'],
        'province_code' => ['required'],
        'city_code'     => ['required'],
        'district_code' => ['required'],
        'village_code'  => ['required'],
        'zip_code'      => ['required'],
        'phone'         => ['required'],
        'mobile'        => ['required']
    ];

    public function getBirthDateAttribute($value)
    {
        return date('d-m-Y',strtotime($value));
    }

    public function setBirthDateAttribute($value)
    {
        $this->attributes['birth_date'] = date('Y-m-d',strtotime($value));
    }

    public function getHobbyAttribute($value)
    {
        return explode(',', $value);
    }

    public function setHobbyAttribute($value)
    {
        $this->attributes['hobby'] = ($value) ? implode(',', $value) : NULL;
    }

    public function setAddressAttribute($value)
    {
        $this->attributes['address'] = trim($value);
    }

    // Relationships
    public function province()
    {
        return $this->belongsTo(Province::class, 'province_code', 'code');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_code', 'code');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_code', 'code');
    }

    public function village()
    {
        return $this->belongsTo(Village::class, 'village_code', 'code');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
