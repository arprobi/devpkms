<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use PKMS\Models\Attachment;
use PKMS\Models\User;

class Comment extends Model
{
    protected $table = 't_comments';
    protected $guarded = [];

    /**
     * Mutators
     */

     public function getCreatedAtAttribute($value)
     {
         return date('d M Y H:i', strtotime($value));
     }

    public function getUpdatedAtAttribute($value)
    {
        return date('d M Y H:i', strtotime($value));
    }

    /**
     * Relationships
     */

    public function author()
    {
        return $this->belongsTo(User::class, 'posted_by');
    }

    public function attachments()
    {
        return $this->belongsToMany(Attachment::class, 'p_attachment_comment');
    }
}
