<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;

class Bantuan extends Model
{
	use ValidatingModelTrait;

    protected $table = 'pkms_bantuans';
    protected $fillable = ['often_asked', 'question', 'answer'];

    protected $rules = [
        'question' 	    => ['required'],
        'answer'        => ['required'],
        'often_asked' 	=> ['required']
    ];

    protected $appends = ['kategori'];


    public function getKategoriAttribute()
    {
    	if ($this->attributes['often_asked']) {
    		return 'Sering ditanyakan';
    	}else{
    		return 'Bantuan lainnya';
    	}
    }
}
