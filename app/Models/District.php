<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'm_districts';
    protected $hidden = ['id','city_id'];

    public function villages()
    {
        return $this->hasMany('PKMS\Village','district_id','code');
    }

    public function getNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }
}
