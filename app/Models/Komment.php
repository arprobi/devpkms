<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use PKMS\Models\User;

class Komment extends Model
{
    protected $table = 'pkms_comments';
    protected $guarded = [];

    /**
     * Mutators
     */

     public function getCreatedAtAttribute($value)
     {
         return date('d M Y', strtotime($value));
     }

    public function getUpdatedAtAttribute($value)
    {
        return date('d M Y', strtotime($value));
    }

    /**
     * Relationships
     */

    public function faq()
    {
        return $this->belongsTo(Faq::class);
    }
}
