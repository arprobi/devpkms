<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

class HighlightComment extends Model
{
    protected $table    = 'pkms_highlight_comments';
    protected $fillable = ['highlight_id', 'user_id', 'comment'];

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }
}
