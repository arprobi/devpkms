<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $table = 't_attachments';
    protected $guarded = [];
}
