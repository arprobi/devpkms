<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use PKMS\Models\Packet;

class PacketHistory extends Model implements ValidatingModelInterface
{
    use ValidatingModelTrait;

    protected $table = 't_packet_history';
    protected $guarded = [];
    protected $rules = [
        'packet_id'   => ['required'],
        'user_id'   => ['required'],
        'action'    => ['required']
    ];

    public function packet()
    {
    	return $this->belongsTo(Packet::class);
    }
}
