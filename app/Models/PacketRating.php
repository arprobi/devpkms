<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;

class PacketRating extends Model
{
    protected $table = 't_packet_ratings';
    protected $guarded = [];
}
