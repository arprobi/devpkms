<?php

namespace PKMS\Models;

use Illuminate\Database\Eloquent\Model;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use PKMS\Models\Packet;

class PacketPaid extends Model implements ValidatingModelInterface
{

    use ValidatingModelTrait;

    protected $table = 't_packet_paid';
    protected $guarded = [];
    protected $rules = [
        'packet_id' => ['required'],
        'name' => ['required'],
    ];

    /**
     * Mutators
     */
    public function getPaidDateAttribute($value)
    {
        return ($value) ? date('d M y', strtotime($value)) : '-';
    }

    public function setPaidDateAttribute($value)
    {
        $this->attributes['paid_date'] = date('Y-m-d', strtotime($value));
    }

    /**
     * Relationships
     */
    public function attachments()
    {
        return $this->belongsToMany(Attachment::class, 'p_attachment_packet_paid');
    }

    public function Packet()
    {
        return $this->belongsTo(Packet::class);
    }

}
