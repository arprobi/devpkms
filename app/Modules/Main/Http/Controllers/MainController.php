<?php

namespace PKMS\Modules\Main\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PKMS\Lib\Broker;
use PKMS\Models\Disclaimer;
use PKMS\Models\User;
use PKMS\Models\Faq;
use PKMS\Models\Question;
use PKMS\Models\Answer;
use PKMS\Models\InvitationList;
use PKMS\Models\Komment;
use PKMS\Models\MasterArticleCode;
use PKMS\Models\MasterArticleSubcode;

class MainController extends BaseController
{
    public function index()
    {
        return view('modules.main.index');
    }

    /**
     * Search
     * @return [type] [description]
     */
    public function search()
    {
    	$q = request()->get('q', false);
    	$codes = [];
    	$subcodes = [];
    	$code = request()->get('code', false);
    	$subcode = request()->get('subcode', false);
    	$tag = request()->get('tags', false);

    	$faqs = Faq::with('author', 'articlecode', 'articlesubcode');

    	if($q)
    	{
    		$faqs = $faqs->orWhere('title','like', "%$q%")->orWhere('tags','like', "%$q%");
    	}

    	if($code)
    	{
    		$faqs = $faqs->orWhere('article_code', "$code");
    		$codes = MasterArticleCode::find($code);
    	}

    	if($subcode)
    	{
    		$faqs = $faqs->orWhere('article_subcode', "$subcode");
    		$subcodes = MasterArticleSubcode::with('articlecode')->find($subcode);
    	}

    	if($tag)
    	{
    		$faqs = $faqs->orWhere('tags','like', "%$tag%");
    	}

    	$faqs = $faqs->paginate();

    	return view('modules.main.search', compact('faqs', 'codes', 'subcodes'));
    }

    /**
     * View detail page
     * @param $id integer
     * @return [type] [description]
     */
    public function view($id, $slug='')
    {
    	$faq = Faq::with(['author', 'articlecode', 'articlesubcode', 'comments'])->find($id);

    	if(!$faq)
    	{
    		return redirect()->route('main.search');
    	}

    	$titles = explode(' ', $faq->title);
    	$suggestions = Faq::with(['articlecode', 'articlesubcode'])->where('id', '!=',$faq->id)->where(function($query) use ($titles) {
    		foreach($titles as $title) {
    			$query->orWhere('title', 'like', "%$title%");
    		}
    	})->take(4)->get();

    	return view('modules.main.view', compact('faq', 'suggestions'));
    }

    /**
     * post comment
     * @return [type] [description]
     */
    public function comment($id)
    {
    	$faq = Faq::find($id);
    	$user = request()->get('user_portal', null);

    	if(!$faq)
    	{
    		return redirect()->route('main.search');
    	}

    	$comment = [
			'name'    => $user['name'],
			'email'   => $user['email'],
			'faq_id'  => $faq->id,
			'content' => request()->get('content', '')
    	];

    	$comment = new Komment($comment);

    	if($comment->save())
    	{
    		return redirect()->back()->withSuccess('Komentar berhasil disimpan');
    	}
    	else
    	{
    		return redirect()->back()->withErrors($comment->getErrors())->withError('Komentar berhasil disimpan');
    	}
    }

    /**
     * get question page
     * @return [type] [description]
     */
    public function question()
    {
        $questions = Question::paginate(5);
    	return view('modules.main.question', compact('questions'));
    }

    /**
     * post question
     * @return void
     */
    public function postQuestion(Request $request)
    {
        $this->validate($request, [
            'name'                 => 'required',
            'email'                => 'email|required',
            'title'                => 'required',
            'content'              => 'required',
            'g-recaptcha-response' => 'required|recaptcha'
        ]);

        $question = new Question(request()->only(['name','email','title','content']));

        if($question->save()) {
            return redirect()->back()->withSuccess('Pertanyaan berhasil dikirim');
        } else {
            return redirect()->back()->withError('Pertanyaan tidak berhasil dikirim');
        }
    }

    public function question_detail($id)
    {
        $question = Question::with('answers')->find($id);

        if(!$question)
            return '<div>Pertanyaan tidak ditemukan</div>';

        return view('modules.main.question_detail', compact('question'));
    }

    /**
     * post answer
     * @return void
     */
    public function postAnswer($id)
    {
        $data = request()->except(['captcha','captcha_confirmation']);
        $captcha = request()->only(['captcha','captcha_confirmation']);

        if($captcha['captcha'] !== $captcha['captcha_confirmation']) {
            return redirect()->back()->withError('Kode captcha salah.');
        }

        $question = Question::find($id);

        if(!$question){
            return redirect()->back()->withError('Pertanyaan tidak ditemukan');
        }

        $answer = new Answer($data);

        if($question->answers()->save($answer)) {
            return redirect()->back()->withSuccess('Tanggapan berhasil dikirim.');
        }

        return redirect()->back()->withSuccess('Tanggapan tidak berhasil dikirim.');
    }

    public function getInvited($key)
    {
        $broker = request()->get('broker', null);
        $url = request()->get('sso_login_url', null);

        if(!$broker)
            return abort(403);

        try
        {
            $user = $broker->getUserInfo();
        }
        catch (NotAttachedException $e)
        {
            header('Location: ' . $_SERVER['REQUEST_URI']);
            exit;
        }
        catch (SsoException $e)
        {
            header('Location: ' . $_SERVER['REQUEST_URI']);
            exit();
        }

        if(!$user)
        {
            header('Location: ' . $url);
            exit;
        }
        else
        {
            if(!$user['is_email_confirmed']) {
                abort(403, 'Anda belum melakukan aktifasi akun. <br>Periksa email anda untuk melihat email konfirmasi akun yang dikirimkan sebelumnya atau kunjungi alamat ini <a href="https://hukum-sanggah.lkpp.go.id/login">Hukum Sanggah LKPP</a> untuk informasi lebih lanjut.');
            }

            if($user['is_blocked']) {
                abort(403, 'Maaf akun anda diblokir. <br>Kunjungi alamat ini <a href="https://hukum-sanggah.lkpp.go.id/login">Hukum Sanggah LKPP</a> untuk informasi lebih lanjut.');
            }

            $list = InvitationList::with('invitation')->where('email', $user['email'])->where('key', $key)->first();

            if(!$list) {
                return abort(403, 'Maaf, kode undangan yang anda gunakan tidak valid.');
            }

            $now = strtotime('now');
            $from = strtotime($list->invitation->from);
            $until = strtotime($list->invitation->until);

            if(!$now >= $from && $now <= $until) {
                return abort(403, 'Maaf, kode undangan yang anda gunakan sudah berakhir masa aktif nya.');
            }

            $check = User::where('email', $user['email'])->first();

            if(!$check) {
                $user = new User([
                    'name' => $user['name'],
                    'username' => $user['username'],
                    'email' => $user['email'],
                    'password' => str_random(8),
                    'is_active' => 1
                ]);

                if(!$user->save()) {
                    return abort(500, 'Maaf terjadi kesalahan pada server. Silahkan coba kembali beberapa saat kemudian.');
                }

                Auth::login($user);
            } else {
                Auth::login($check);
            }

            return redirect()->route('expert.dashboard');
        }

    }
    
    public function getOffer($key)
    {
        $redirect_url = explode("-",base64_decode($key));
        
        
        $broker = request()->get('broker', null);
        $url = request()->get('sso_login_url', null);

        if(!$broker)
            return abort(403);

        try
        {
            $user = $broker->getUserInfo();
        }
        catch (NotAttachedException $e)
        {
            header('Location: ' . $_SERVER['REQUEST_URI']);
            exit;
        }
        catch (SsoException $e)
        {
            header('Location: ' . $_SERVER['REQUEST_URI']);
            exit();
        }

        if(!$user)
        {
            header('Location: ' . $url);
            exit;
        }
        else
        {
            if(!$user['is_email_confirmed']) {
                abort(403, 'Anda belum melakukan aktifasi akun. <br>Periksa email anda untuk melihat email konfirmasi akun yang dikirimkan sebelumnya atau kunjungi alamat ini <a href="https://hukum-sanggah.lkpp.go.id/login">Hukum Sanggah LKPP</a> untuk informasi lebih lanjut.');
            }

            if($user['is_blocked']) {
                abort(403, 'Maaf akun anda diblokir. <br>Kunjungi alamat ini <a href="https://hukum-sanggah.lkpp.go.id/login">Hukum Sanggah LKPP</a> untuk informasi lebih lanjut.');
            }

            $list = InvitationList::with('invitation')->where('email', $user['email'])->where('key', $key)->first();

            if(!$list) {
                return abort(403, 'Maaf, kode undangan yang anda gunakan tidak valid.');
            }

            $now = strtotime('now');
            $from = strtotime($list->invitation->from);
            $until = strtotime($list->invitation->until);

            if(!$now >= $from && $now <= $until) {
                return abort(403, 'Maaf, kode undangan yang anda gunakan sudah berakhir masa aktif nya.');
            }

            $check = User::where('email', $user['email'])->first();

            if(!$check) {
                $user = new User([
                    'name' => $user['name'],
                    'username' => $user['username'],
                    'email' => $user['email'],
                    'password' => str_random(8),
                    'is_active' => 1
                ]);

                if(!$user->save()) {
                    return abort(500, 'Maaf terjadi kesalahan pada server. Silahkan coba kembali beberapa saat kemudian.');
                }

                Auth::login($user);
            } else {
                Auth::login($check);
            }

            return redirect($redirect_url[1]);
        }

    }

    /**
     * get question page
     * @return [view] [knowledges]
     */
    public function detail($klasifikasi = NULL, $id = NULL, $slug = NULL)
    {
        $kategori = ['infografis', 'studikasus', 'materipaparan', 'video'];
        
        // if klasifikasi not match
        if (!in_array($klasifikasi, $kategori)) {
            return redirect()->route('main.search');
        }

        $faq = Faq::with(['author', 'articlecode', 'articlesubcode', 'comments'])->find($id);

        // if slug not match
        if ($slug !=  str_slug($faq->title, '-')) {
            return redirect()->route('main.search');
        }

        // if faq not found
        if(!$faq)
        {
            return redirect()->route('main.search');
        }

        // process the article if found
        $titles = explode(' ', $faq->title);
        $suggestions = Faq::with(['articlecode', 'articlesubcode'])->where('id', '!=',$faq->id)->where(function($query) use ($titles) {
            foreach($titles as $title) {
                $query->orWhere('title', 'like', "%$title%");
            }
        })->take(4)->get();

        return view('modules.main.view', compact('faq', 'suggestions'));
    }

    public function disclaimer()
    {
        $disclaimer     = Disclaimer::first();
        return view('modules.main.disclaimer', compact('disclaimer'));
    }
}
