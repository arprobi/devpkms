<?php

namespace PKMS\Modules\Main\Http\Controllers;

use Illuminate\Http\Request;
use PKMS\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function __construct() {}
}
