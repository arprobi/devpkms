<?php

namespace PKMS\Modules\Experts\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PKMS\Events\PacketAccepted;
use PKMS\Events\PacketRefused;
use PKMS\Models\User;
use PKMS\Models\Packet;
use PKMS\Models\PacketQualification;
use PKMS\Models\PacketPaid;
use PKMS\Models\Comment;
use PKMS\Models\Attachment;

class PacketController extends BaseController
{

    public function __construct()
    {
        $this->middleware('complete');
    }

    public function index()
    {
        $user = Auth::user();
        $data = [
            'completed_packets' => Packet::select(['id', 'accepted_at', 'title', 'description'])->with(['rating' => function($query) {
                    $query->select(['rating', 'packet_id', 'review']);
                }])->where('assigned_to', $user->id)->where('is_completed', 1)->get(),
            'my_packets' => Packet::where('assigned_to', $user->id)->where('is_completed', 0)->where('is_accepted', 1)->get(),
            'new_packets' => Packet::where('assigned_to', $user->id)->where('is_accepted', 0)->get(),
            'invite_packets' => Packet::with(['userInvites' => function($query) use($user) {
                    $query->where('user_id', $user->id);
                }])->where('is_invited', true)->whereNull('assigned_to')->where('is_accepted', 0)->get()
        ];

        return view('modules.packet.index', $data);
    }

    public function baru()
    {
        $user = Auth::user();
        $packets = Packet::whereHas('offers', function($query) use($user) {
                            $query->where('user_id', $user->id);
                        }, '<', 1)
                        ->where('is_publish', true)
                        ->where('is_assigned', false)
                        ->where('is_accepted', false)
                        ->where('is_completed', false)->paginate();
        return view('modules.packet.new', compact('packets'));
    }

    public function follow()
    {
        $user = Auth::user();
        $packets = Packet::whereHas('offers', function($query) use($user) {
                            $query->where('user_id', $user->id);
                        })
                        ->where('is_accepted', false)
                        ->where('is_completed', false)->paginate();
        return view('modules.packet.follow', compact('packets'));
    }

    public function onGoing()
    {
        $user = Auth::user();
        $packets = Packet::where('assigned_to', $user->id)->where('is_completed', 0)->where('is_accepted', 1)->paginate();
        return view('modules.packet.ongoing', compact('packets'));
    }

    public function offer($packetId)
    {
        $user = Auth::user();
        $packet = Packet::find($packetId);
        $files = request()->file('lampiran');
        $invalidFiles = false;

        if (count($files)) {
            foreach ($files as $qualificationId => $file) {
                $qualification = PacketQualification::find($qualificationId);
                $attachment = $this->attachToQualification($file);

                if ($attachment['status']) {
                    $qualification->attachments()->attach($attachment['file']->id);
                } else {
                    if ($invalidFiles) {
                        $invalidFiles .= $attachment['filename'] . ' untuk syarat ' . $qualification->name . ' tidak berhasil disimpan.';
                    } else {
                        $invalidFiles = $attachment['filename'] . ' untuk syarat ' . $qualification->name . ' tidak berhasil disimpan.';
                    }
                }
            }
        }

        $offer = [
            'user_id' => $user->id,
            'packet_id' => $packetId,
            'offer' => str_replace(".", "", request()->get('offer'))
        ];
        $packet->offers()->insert($offer);

        if ($invalidFiles) {
            return redirect()->back()->withError('Penawaran berhasil dikirim, tetapi lampiran ' . $invalidFiles);
        }

        return redirect()->route('expert.packets')->withSuccess('Penawaran berhasil dikirim.');
    }

    public function offerPage($packetId)
    {
        $api = request()->get('api', null);
        $paket = Packet::with([
                    'qualifications',
                    'attachments'
                ])->find($packetId);
        if ($paket) {
            $paket->instansi = $api->getAgency($paket->instansi_id);
        }
        $data['packet'] = $paket;

        if (!$data['packet'])
            return redirect()->route('expert.packets.new')->withError('Tugas tidak ditemukan.');

        return view('modules.packet.offer', $data);
    }

    public function accept($packetId)
    {
        $packet = Packet::where('assigned_to', Auth::user()->id)->find($packetId);

        if (!$packet)
            return redirect()->back()->withError('Tugas tidak ditemukan.');

        if ($packet->update([
                    'is_accepted' => 1,
                    'accepted_at' => date('Y-m-d H:i:s')
                ])) {
            event(new PacketAccepted($packet));
            return redirect()->back()->withSuccess('Tugas berhasil ditambahkan sebagai tugas anda.');
        } else {
            return redirect()->back()->withError('Terjadi kesalahan. Tugas tidak berhasil di terima.');
        }
    }

    public function deny($packetId)
    {
        $packet = Packet::where('assigned_to', Auth::user()->id)->find($packetId);
        $message = request()->get('message', '');

        if (!$packet)
            return redirect()->back()->withError('Tugas tidak ditemukan.');

        if ($packet->update([
                    'assigned_to' => NULL,
                    'assigned_at' => NULL,
                    'assigned_by' => NULL
                ])) {
            event(new PacketRefused(Auth::user(), $packet, $message));
            return redirect()->back()->withSuccess('Tugas telah berhasil di tolak.');
        } else {
            return redirect()->back()->withError('Terjadi kesalahan. Tugas tidak berhasil di tolak.');
        }
    }

    public function detail($packetId)
    {
        $data['packet'] = Packet::with([
                    'comments' => function($query) {
                        $query->with(['attachments', 'author'])->orderBy('created_at', 'asc');
                    }
                ])->where('assigned_to', Auth::user()->id)->find($packetId);

        if (!$data['packet'])
            return redirect()->route('expert.packets')->withError('Paket tidak ditemukan.');

        return view('modules.packet.detail', $data);
    }

    public function update($packetId)
    {
        $packet = Packet::where('assigned_to', Auth::user()->id)->find($packetId);
        $data = request()->only('progress');

        if ($packet && $packet->update($data)) {
            return redirect()->back()->withSuccess('Tugas berhasil disimpan');
        } else {
            return redirect()->back()->withErrors($this->getErrors())->withError('Tugas tidak berhasil di simpan.');
        }
    }

    // ARTICLES

    public function addArticle($packetId)
    {
        $packet = Packet::where('assigned_to', Auth::user()->id)->find($packetId);

        if (!$packet)
            return redirect()->back()->withError('Tugas tidak ditemukan.');

        return view('packets_add_article', compact('packet'));
    }

    // COMMENTS

    public function addComment($packetId)
    {
        $packet = Packet::where('assigned_to', Auth::user()->id)->find($packetId);
        $data = request()->only(['text']);
        $files = request()->file('lampiran');
        $data['posted_by'] = Auth::user()->id;
        $invalidFiles = false;

        if (!$packet)
            return redirect()->back()->withError('Pesan tidak berhasil dikirim.');

        // Save Comment
        $comment = new Comment($data);
        if (!$comment->save()) {
            return redirect()->back()->withError('Pesan tidak berhasil dikirim');
        }

        $packet->comments()->attach($comment->id);

        // Save Attachment
        if (count($files)) {
            foreach ($files as $file) {
                $attachment = $this->attachToComment($file);

                if ($attachment['status']) {
                    $comment->attachments()->attach($attachment['file']->id);
                } else {
                    if ($invalidFiles) {
                        $invalidFiles .= $attachment['filename'] . ' tidak berhasil disimpan.';
                    } else {
                        $invalidFiles = $attachment['filename'] . ' tidak berhasil disimpan.';
                    }
                }
            }
        }

        if ($invalidFiles) {
            return redirect()->back()->withError('Pesan berhasil dikirim, tetapi lampiran ' . $invalidFiles);
        }

        return redirect()->back()->withSuccess('Pesan berhasil dikirim.');
    }

    private function attachToComment($file)
    {
        $acceptedFiles = ['jpg', 'jpeg', 'png', 'bmp', 'pdf', 'docx', 'doc', 'ppt', 'pptx', 'zip', 'rar', 'xls', 'xlsx'];

        if (!in_array($file->extension(), $acceptedFiles))
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName()
            ];

        $attachment = [
            'title' => $file->getClientOriginalName(),
            'size' => $file->getClientSize(),
            'mime' => $file->getMimeType(),
            'path' => $file->store('uploaded', 'public'),
            'short_url' => uniqid('exp' . Auth::user()->id),
            'owner_id' => Auth::user()->id
        ];

        $attachment = new Attachment($attachment);

        if ($attachment->save()) {
            return [
                'status' => true,
                'file' => $attachment
            ];
        } else {
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName()
            ];
        }
    }

    private function attachToQualification($file)
    {
        $acceptedFiles = ['jpg', 'jpeg', 'pdf', 'docx', 'doc', 'ppt', 'pptx', 'zip', 'rar', 'xls', 'xlsx'];

        if (!in_array($file->extension(), $acceptedFiles))
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName()
            ];

        $attachment = [
            'title' => $file->getClientOriginalName(),
            'size' => $file->getClientSize(),
            'mime' => $file->getMimeType(),
            'path' => $file->store('uploaded', 'public'),
            'short_url' => uniqid('exp' . Auth::user()->id),
            'owner_id' => Auth::user()->id
        ];

        $attachment = new Attachment($attachment);

        if ($attachment->save()) {
            return [
                'status' => true,
                'file' => $attachment
            ];
        } else {
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName()
            ];
        }
    }

    public function negoPage($packetId)
    {
        $api = request()->get('api', null);
        $paket = Packet::with([
                    'attachments',
                    'negotiations' => function($q) {
                        $q->where('user_id', Auth::user()->id);
                    }
                ])->find($packetId);


        if ($paket) {
            $paket->instansi = $api->getAgency($paket->instansi_id);
        }
        $data['packet'] = $paket;
        if (!$data['packet'])
            return redirect()->route('admin.packets')->withError('paket tidak ditemukan.');

        return view('modules.packet.nego', $data);
    }

    public function nego($packetId)
    {
        $packet = Packet::find($packetId);
        $data = request()->only(['text']);
        $offer = str_replace(".", "", request()->get('offer'));
        $data['posted_by'] = Auth::user()->id;


        if (!$packet)
            return redirect()->back()->withError('Nego tidak berhasil dikirim.');

        // Save Comment
        $comment = new Comment($data);
        if (!$comment->save()) {
            return redirect()->back()->withError('Nego tidak berhasil dikirim');
        }

        $packet->negotiations()->attach($comment->id, ['user_id' => Auth::user()->id, 'offer' => $offer]);
        $packet->pembayaran = $offer;
        $packet->save();
        return redirect()->back()->withSuccess('Nego berhasil dikirim.');
    }

    public function paidsPage($packetId)
    {
        $paket = Packet::find($packetId);

        $data['packet'] = $paket;
        if (!$data['packet'])
            return redirect()->route('experts.packets')->withError('paket tidak ditemukan.');

        return view('modules.packet.paids', $data);
    }

    public function paidDetail($paidId)
    {
        $paid = PacketPaid::find($paidId);

        $data['paid'] = $paid;
        if (!$data['paid'])
            return redirect()->route('experts.packets.paids.page', $paid->packet->id)->withError('pembayaran tidak ditemukan.');

        return view('modules.packet.paid_detail', $data);
    }

}
