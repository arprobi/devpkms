<?php

namespace PKMS\Modules\Experts\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends BaseController
{
    public function index()
    {
    	if(Auth::user()->is_verified)
        	return redirect()->route('expert.tasks');

        return view('home');
    }
}
