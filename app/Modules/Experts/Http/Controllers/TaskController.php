<?php

namespace PKMS\Modules\Experts\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PKMS\Events\TaskAccepted;
use PKMS\Events\TaskRefused;
use PKMS\Models\User;
use PKMS\Models\Task;
use PKMS\Models\Comment;
use PKMS\Models\Attachment;

class TaskController extends BaseController
{
    public function __construct()
    {
        $this->middleware('complete');
    }

    public function index()
    {
        $user = Auth::user();
        $data = [
            'completed_tasks' => Task::select(['id','accepted_at','title','description'])->with(['rating' => function($query){
                $query->select(['rating','task_id','review']);
            }])->where('assigned_to', $user->id)->where('is_completed', 1)->get(),
            'my_tasks'  => Task::where('assigned_to', $user->id)->where('is_completed', 0)->where('is_accepted', 1)->get(),
            'new_tasks' => Task::where('assigned_to', $user->id)->where('is_accepted', 0)->get()
        ];

        return view('tasks', $data);
    }

    public function accept($taskId)
    {
        $task = Task::where('assigned_to', Auth::user()->id)->find($taskId);

        if(!$task)
            return redirect()->back()->withError('Tugas tidak ditemukan.');

        if($task->update([
            'is_accepted' => 1,
            'accepted_at' => date('Y-m-d H:i:s')
        ]))
        {
            event( new TaskAccepted(Auth::user(), $task) );
            return redirect()->back()->withSuccess('Tugas berhasil ditambahkan sebagai tugas anda.');
        }
        else
        {
            return redirect()->back()->withError('Terjadi kesalahan. Tugas tidak berhasil di terima.');
        }
    }

    public function deny($taskId)
    {
        $task = Task::where('assigned_to', Auth::user()->id)->find($taskId);
        $message = request()->get('message', '');

        if(!$task)
            return redirect()->back()->withError('Tugas tidak ditemukan.');

        if($task->update([
            'assigned_to' => NULL,
            'assigned_at' => NULL,
            'assigned_by' => NULL
        ]))
        {
            event( new TaskRefused(Auth::user(), $task, $message) );
            return redirect()->back()->withSuccess('Tugas telah berhasil di tolak.');
        }
        else
        {
            return redirect()->back()->withError('Terjadi kesalahan. Tugas tidak berhasil di tolak.');
        }
    }

    public function detail($taskId)
    {
        $data['task'] = Task::with([
            'articles' => function($query){
                $query->with(['attachments','author']);
            },
            'comments' => function($query){
                $query->with(['attachments', 'author'])->orderBy('created_at','asc');
            }
        ])->where('assigned_to', Auth::user()->id)->find($taskId);

        if(!$data['task'])
            return redirect()->route('expert.tasks')->withError('Tugas tidak ditemukan.');

        return view('tasks_detail', $data);
    }

    public function update($taskId)
    {
        $task = Task::where('assigned_to', Auth::user()->id)->find($taskId);
        $data = request()->only('progress');

        if($task && $task->update($data))
        {
            return redirect()->back()->withSuccess('Tugas berhasil disimpan');
        }
        else
        {
            return redirect()->back()->withErrors($this->getErrors())->withError('Tugas tidak berhasil di simpan.');
        }
    }

    // ARTICLES

    public function addArticle($taskId)
    {
        $task = Task::where('assigned_to', Auth::user()->id)->find($taskId);

        if(!$task)
            return redirect()->back()->withError('Tugas tidak ditemukan.');

        return view('tasks_add_article', compact('task'));
    }

    // COMMENTS

    public function addComment($taskId)
    {
        $task = Task::where('assigned_to', Auth::user()->id)->find($taskId);
        $data = request()->only(['text']);
        $files = request()->file('lampiran');
        $data['posted_by'] = Auth::user()->id;
        $invalidFiles = false;

        if(!$task)
            return redirect()->back()->withError('Pesan tidak berhasil dikirim.');

        // Save Comment
        $comment = new Comment($data);
        if(!$comment->save())
        {
            return redirect()->back()->withError('Pesan tidak berhasil dikirim');
        }

        $task->comments()->attach($comment->id);

        // Save Attachment
        if(count($files))
        {
            foreach($files as $file)
            {
                $attachment = $this->attachToComment($file);

                if($attachment['status'])
                {
                    $comment->attachments()->attach($attachment['file']->id);
                }
                else
                {
                    if($invalidFiles)
                    {
                        $invalidFiles .= $attachment['filename'] . ' tidak berhasil disimpan.';
                    }
                    else
                    {
                        $invalidFiles = $attachment['filename'] . ' tidak berhasil disimpan.';
                    }
                }
            }
        }

        if($invalidFiles)
        {
            return redirect()->back()->withError('Pesan berhasil dikirim, tetapi lampiran '.$invalidFiles);
        }

        return redirect()->back()->withSuccess('Pesan berhasil dikirim.');
    }

    private function attachToComment($file)
    {
        $acceptedFiles = ['jpg', 'jpeg', 'png', 'bmp', 'pdf', 'docx', 'doc','ppt','pptx','zip','rar','xls','xlsx'];

        if(! in_array($file->extension(), $acceptedFiles))
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName
            ];

        $attachment = [
            'title' => $file->getClientOriginalName(),
            'size'  => $file->getClientSize(),
            'mime'  => $file->getMimeType(),
            'path'  => $file->store('uploaded', 'public'),
            'short_url' => uniqid('exp'.Auth::user()->id),
            'owner_id' => Auth::user()->id
        ];

        $attachment = new Attachment($attachment);

        if($attachment->save())
        {
            return [
                'status' => true,
                'file' => $attachment
            ];
        }
        else
        {
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName
            ];
        }
    }
}
