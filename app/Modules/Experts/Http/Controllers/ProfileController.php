<?php

namespace PKMS\Modules\Experts\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use PKMS\Models\User;
use PKMS\Models\Profile;

class ProfileController extends BaseController
{
    public function index()
    {
        $user = User::with('profile','interests')->find(Auth::id());
        return view('profile', compact('user'));
    }

    public function save()
    {

        if(Auth::user()->profile)
        {
            $this->__update();
        }
        else
        {
            $this->__create();
        }

        return redirect()->back();
    }

    public function updateAccount()
    {
        $user = Auth::user();
        $data = request()->only(['name','username','email']);
        $interests = request()->get('interests', false);

        if($user->update($data))
        {

            if($interests)
            {
                $user->interests()->sync($interests);
            }

            request()->session()->flash('success', 'Akun berhasil disimpan.');
        }
        else
        {
            request()->session()->flash('error', 'Akun tidak berhasil disimpan.');
            request()->session()->flash('errors', $user->getErrors());
        }

        return redirect()->back();
    }

    public function updatePassword()
    {
        $user = Auth::user();
        $data = request()->all();

        if(Hash::check($data['old_password'], $user->password))
        {
            unset($data['old_password']);
            if($user->update($data))
            {
                request()->session()->flash('success', 'Password berhasil diganti.');
            }
            else
            {
                request()->session()->flash('error', 'Password tidak berhasil diganti.');
                request()->session()->flash('errors', $user->getErrors());
            }
        }
        else
        {
            request()->session()->flash('error', 'Password salah.');
        }

        return redirect()->back();
    }

    public function uploadAvatar()
    {
        $user = Auth::user();
        $avatar = request()->file('avatar');

        if($avatar && in_array($avatar->extension(), ['jpg','jpeg','png','bmp'])) {
            $path = $avatar->storeAs('avatars', 'avatar_'.$user->id, 'public');

            if($path)
            {
                $user->avatar = $path;

                if($user->save())
                {
                    request()->session()->flash('success', 'Avatar berhasil disimpan.');
                }
                else
                {
                    request()->session()->flash('error', 'Avatar tidak berhasil disimpan.');
                }
            }
            else
            {
                request()->session()->flash('error', 'Avatar tidak berhasil disimpan');
                Log::error('User ID: ' . $user->id . ', gagal melakukan penyimpanan file avatar.');
            }
        }
        else
        {
            request()->session()->flash('error', 'Avatar tidak berhasil disimpan. Pastikan avatar yg digunakan adalah image dengan tipe <code>jpg, jpeg, bmp, png</code>');
        }

        return redirect()->back();
    }

    public function __create()
    {
        $user = Auth::user();
        $profile = new Profile(request()->all());

        if($user->profile()->save($profile))
        {
            request()->session()->flash('success', 'Profil berhasil disimpan.');
        }
        else
        {
            request()->session()->flash('error', 'Profil tidak berhasil disimpan.');
            request()->session()->flash('errors', $user->profile->getErrors());
        }
    }

    public function __update()
    {
        $profile = Auth::user()->profile;

        if(! $profile) {
            request()->session()->flash('error', 'Profil tidak ditemukan.');
            return;
        }

        if($profile->update(request()->all()))
        {
            request()->session()->flash('success', 'Profil berhasil disimpan');
        }
        else
        {
            request()->session()->flash('error', 'Profil tidak berhasil disimpan.');
            request()->session()->flash('errors', $profile->getErrors());
        }
    }
}
