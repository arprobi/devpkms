<?php

namespace PKMS\Modules\Experts\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PKMS\Models\Article;
use PKMS\Models\Task;
use PKMS\Models\Attachment;

class ArticleController extends BaseController
{
    public function __construct()
    {
        $this->middleware('complete');
    }

    public function index()
    {
        $articles = Article::with(['tasks','author'])->where('author_id', Auth::user()->id)->paginate();
        return view('modules.expert.articles.index', compact('articles'));
    }

    public function create($taskId)
    {
        $task = Task::where('assigned_to', Auth::user()->id)->find($taskId);

        if(!$task)
            return redirect()->back()->withError('Tugas tidak ditemukan.');

        $data = request()->except('lampiran');
        $data['author_id'] = Auth::user()->id;
        $files = request()->file('lampiran');
        $invalidFiles = false;

        $article = new Article($data);

        if(!$article->save())
        {
            return redirect()->back()->withError('Artikel tidak berhasil disimpan.');
        }

        $task->articles()->attach($article->id);

        // Save Attachment
        if(count($files))
        {
            foreach($files as $file)
            {
                $attachment = $this->attachToArticle($file);

                if($attachment['status'])
                {
                    $article->attachments()->attach($attachment['file']->id);
                }
                else
                {
                    if($invalidFiles)
                    {
                        $invalidFiles .= $attachment['filename'] . ' tidak berhasil disimpan.';
                    }
                    else
                    {
                        $invalidFiles = $attachment['filename'] . ' tidak berhasil disimpan.';
                    }
                }
            }
        }

        if($invalidFiles)
        {
            return request()->session()->setFlash('error','Artikel berhasil disimpan, tetapi lampiran '.$invalidFiles);
        }

        return redirect()->route('expert.tasks.detail', $task->id)->withSuccess('Artikel berhasil disimpan.');
    }

    public function view($articleId)
    {
        $article = Article::with('attachments')->where('author_id', Auth::user()->id)->find($articleId);

        if(!$article)
        {
            if(request()->ajax())
            {
                abort(404, 'Artikel tidak ditemukan');
            }
            else
            {
                return redirect()->back()->withError('Artikel tidak ditemukan.');
            }
        }

        if(request()->ajax())
        {
            return view('modules.expert.articles._view', compact('article'));
        }
        else
        {
            return view('modules.expert.articles.view', compact('article'));
        }
    }

    public function edit($articleId)
    {
        $article = Article::with('attachments','tasks')->where('author_id', Auth::user()->id)->find($articleId);

        if(!$article)
        {
            return redirect()->route('expert.articles')->withError('Artikel tidak ditemukan');
        }

        if($article->is_published)
        {
            return redirect()->route('expert.articles')->withError('Artikel yang sudah terbit tidak dapat di edit.');
        }


        return view('modules.expert.articles.edit', compact('article'));
    }

    public function update($articleId)
    {
        $article = Article::with('attachments','tasks')->where('author_id', Auth::user()->id)->find($articleId);

        if(!$article)
        {
            return redirect()->route('expert.articles')->withError('Artikel tidak ditemukan');
        }

        $data = request()->except(['lampiran','files']);
        $files = request()->file('lampiran');
        $invalidFiles = false;

        if($article->update($data))
        {
            // Save Attachment
            if(count($files))
            {
                foreach($files as $file)
                {
                    $attachment = $this->attachToArticle($file);

                    if($attachment['status'])
                    {
                        $article->attachments()->attach($attachment['file']->id);
                    }
                    else
                    {
                        if($invalidFiles)
                        {
                            $invalidFiles .= $attachment['filename'] . ' tidak berhasil disimpan.';
                        }
                        else
                        {
                            $invalidFiles = $attachment['filename'] . ' tidak berhasil disimpan.';
                        }
                    }
                }
            }

            if($invalidFiles)
            {
                return request()->session()->setFlash('error','Artikel berhasil disimpan, tetapi lampiran '.$invalidFiles);
            }

            return redirect()->route('expert.tasks.detail', $article->tasks->first()->id)->withSuccess('Artikel berhasil disimpan.');
        }
        else
        {
            return redirect()->back()->withError('Artikel tidak berhasil disimpan.');
        }
    }

    public function delete($articleId)
    {
        $article = Article::where('author_id', Auth::user()->id)->find($articleId);

        if(!$article)
        {
            return redirect()->route('expert.articles')->withError('Artikel tidak ditemukan');
        }

        if($article->delete())
        {
            return redirect()->back()->withSuccess('Artikel berhasil dihapus.');
        }
        else
        {
            return redirect()->back()->withError('Artikel tidak berhasil dihapus.');
        }
    }

    private function attachToArticle($file)
    {
        $acceptedFiles = ['jpg', 'jpeg', 'png', 'bmp', 'pdf', 'docx', 'doc','ppt','pptx','zip','rar','xls','xlsx'];

        if(! in_array($file->extension(), $acceptedFiles))
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName
            ];

        $attachment = [
            'title' => $file->getClientOriginalName(),
            'size'  => $file->getClientSize(),
            'mime'  => $file->getMimeType(),
            'path'  => $file->store('uploaded', 'public'),
            'short_url' => uniqid('exp'.Auth::user()->id),
            'owner_id' => Auth::user()->id
        ];

        $attachment = new Attachment($attachment);

        if($attachment->save())
        {
            return [
                'status' => true,
                'file' => $attachment
            ];
        }
        else
        {
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName
            ];
        }
    }
}
