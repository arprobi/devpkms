<?php

namespace PKMS\Modules\Experts\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PKMS\Models\User;
use PKMS\Models\Education;

class EducationController extends BaseController
{
    public function index()
    {
        $data['user'] = User::with('educations.detail')->find(Auth::id());
        return view('educations', $data);
    }

    public function create()
    {
        $data = request()->only(['name', 'major', 'gpa', 'education_id']);
        $data['user_id'] = Auth::id();
        $education = new Education($data);

        if($education->save())
        {
            return redirect()->back()->withSuccess('Edukasi berhasil disimpan.');
        }

        return redirect()->back()->withError('Edukasi tidak berhasil disimpan.');
    }

    public function edit($education_id)
    {
        $data['education'] = Education::find($education_id);
        $data['education_id'] = $education_id;
        return view('educations_edit', $data);
    }

    public function update($education_id)
    {
        $education = Education::find($education_id);
        $data = request()->only(['education_id','name','major','gpa']);

        if(!$education)
            return redirect()->route('expert.educations')->withError('Data tidak ditemukan');

        $data['user_id'] = Auth::id();

        if(!$education->update($data))
            return redirect()->back()->withErrors($education->getErrors());

        return redirect()->route('expert.educations')->withSuccess('Edukasi berhasil disimpan.');
    }

    public function delete($education_id)
    {
        if(Education::where('id',$education_id)->where('user_id', Auth::id())->first()->delete())
        {
            request()->session()->flash('success', 'Edukasi berhasil dihapus.');
        }
        else
        {
            request()->session()->flash('error', 'Edukasi tidak berhasil dihapus.');
        }

        return redirect()->route('expert.educations');
    }
}
