<?php

namespace PKMS\Modules\Experts\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PKMS\Models\User;
use PKMS\Models\Experience;
use PKMS\Models\Attachment;

class ExperienceController extends BaseController
{
    public function index()
    {
        $data['user'] = User::with(['experiences' => function($query){
            $query->orderBy('from', 'desc');
        }, 'my_tasks' => function($query){
            $query->with(['assignedby', 'rating'])->orderBy('assigned_at', 'desc');
        }])->find(Auth::user()->id);
        return view('modules.expert.experiences.index', $data);
    }

    public function create()
    {
        $data = request()->only(['title', 'position', 'description', 'from', 'to']);
        $data['user_id'] = Auth::user()->id;
        $files = request()->file('lampiran');
        $experience = new Experience($data);

        if(!$experience->save())
        {
            return redirect()->back()->withError('Pengalaman kerja tidak berhasil disimpan.')->withErrors($experience->getErrors());
        }

        if(count($files))
        {
            foreach($files as $file)
            {
                if(! in_array($file->extension(), ['pdf','doc','docx']))
                    return redirect()->back()->withError('Pastikan lampiran yang diunggah bertipe <code>.pdf, .doc & .docx</code>.');

                $attachment = [
                    'title' => $file->getClientOriginalName(),
                    'size'  => $file->getClientSize(),
                    'mime'  => $file->getMimeType(),
                    'path'  => $file->store('uploaded', 'public'),
                    'short_url' => uniqid('exp'.Auth::user()->id),
                    'owner_id' => Auth::user()->id
                ];

                $attachment = new Attachment($attachment);

                if($attachment->save())
                {
                    $experience->attachments()->attach($attachment->id);
                }
            }
        }

        return redirect()->route('expert.experiences')->withSuccess('Pengalaman kerja berhasil disimpan.');
    }

    public function edit($experience_id)
    {
        $data['experience'] = Experience::with('attachments')->find($experience_id);
        $data['experience_id'] = $experience_id;
        return view('modules.expert.experiences.edit', $data);
    }

    public function update($experience_id)
    {
        $experience = Experience::where('user_id', Auth::user()->id)->where('id', $experience_id)->first();
        $data = request()->only(['title', 'position', 'description', 'from', 'to']);
        $files = request()->file('lampiran', false);
        $data['to'] = ($data['to'] == '-') ? null : $data['to'];

        if(count($files))
        {
            foreach($files as $file)
            {
                if(! in_array($file->extension(), ['pdf','doc','docx']))
                    return redirect()->back()->withError('Pastikan lampiran yang diunggah bertipe <code>.pdf, .doc & .docx</code>.');

                $attachment = [
                    'title' => $file->getClientOriginalName(),
                    'size'  => $file->getClientSize(),
                    'mime'  => $file->getMimeType(),
                    'path'  => $file->store('uploaded', 'public'),
                    'short_url' => uniqid('exp'.Auth::user()->id),
                    'owner_id' => Auth::user()->id
                ];

                $attachment = new Attachment($attachment);

                if($attachment->save())
                {
                    $experience->attachments()->attach($attachment->id);
                }
            }
        }

        if($experience->update($data))
        {
            return redirect()->route('expert.experiences')->withSuccess('Pengalaman kerja berhasil disimpan.');
        }
        else
        {
            return redirect()->back()->withError('Pengalaman kerja tidak berhasil disimpan.')->withErrors($experience->getErrors());
        }
    }

    public function delete($experience_id)
    {
        $experience = Experience::where('user_id', Auth::user()->id)->where('id', $experience_id)->first();

        if($experience->delete())
        {
            return redirect()->back()->withSuccess('Pengalaman kerja berhasil dihapus.');
        }
        else
        {
            return redirect()->back()->withError('Pengalaman kerja tidak berhasil dihapus.');
        }
    }

}
