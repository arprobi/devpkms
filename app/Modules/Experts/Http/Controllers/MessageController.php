<?php

namespace PKMS\Modules\Experts\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PKMS\Models\Message;

class MessageController extends BaseController
{
    public function index()
    {
    	$messages = Message::with('fromUser', 'toUser','replies')
    		->whereNull('reply_id')
    		->where(function($query){
    			$query->where('to', Auth::id())
    				  ->orWhere('from', Auth::id());
    		})
    		->orderBy('created_at','desc')
    		->paginate();

        return view('modules.expert.inbox.index', compact('messages'));
    }

    public function detail($id)
    {
    	$message = Message::with(['fromUser','toUser','replies' => function($query){
    		$query->with('fromUser')->orderBy('created_at','asc');
    	}])->where(function($query){
            $query->where('to', Auth::id())
                  ->orWhere('from', Auth::id());
        })->find($id);

    	if(!$message)
    	{
    		return redirect()->back()->withError('Pesan tidak ditemukan');
    	}
    	else
    	{
    		if($message->read == 0 && $message->to == Auth::id())
    		{
    			$message->read = 1;
    		}

    		if($message->replies->count())
    		{
    			$message->replies()->where('to', Auth::id() )->where('read', 0)->update(['read' => 1]);
    		}

    		$message->save();
    	}

    	return view('modules.expert.inbox.detail', compact('message'));
    }

    public function send()
    {
        $data = request()->only(['to','subject','message']);
        $data['from'] = Auth::id();
        $message = new Message($data);

        if(!$message->save())
        {
            return redirect()->back()->withErrors($message->getErrors())->withError('Pesan tidak berhasil dikirim');
        }

        return redirect()->back()->withSuccess('Pesan berhasil dikirim');
    }

    public function reply($id)
    {
    	$message = Message::find($id);

    	if(!$message)
    		return redirect()->route('expert.messages')->withError('Pesan tidak berhasil dikirim.');

    	$data = request()->only(['message']);
    	$data['from'] = Auth::id();
    	$data['to'] = $message->from;
    	$data['reply_id'] = $message->id;

    	$reply = new Message($data);

    	if($reply->save())
    	{
    		return redirect()->back()->withSuccess('Pesan berhasil dikirim.');
    	}
    	else
    	{
    		return redirect()->back()->withErrors($reply->getErrors())->withError('Pesan tidak berhasil dikirim');
    	}
    }
}
