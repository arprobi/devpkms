<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use PKMS\Models\Invitation;
use PKMS\Models\InvitationList;
use PKMS\Events\Invitation as InvitationEvent;
use PKMS\Mail\Invitation as InvitationMail;

class InvitationController extends BaseController
{
    public function index()
    {
        $invitations = Invitation::with('lists')->paginate();
    	return view('modules.admin.invitations.index', compact('invitations'));
    }

    public function show($id)
    {
        $invitation = Invitation::with('lists')->find($id);

        if(!$invitation) {
            return redirect()->back()->withError('Data tidak ditemukan.');
        }

    	return view('modules.admin.invitations.view', compact('invitation'));
    }

    public function form()
    {
    	return view('modules.admin.invitations.create');
    }

    public function create()
    {
        $data  = request()->except('invitation_lists');
        $data_lists = request()->get('invitation_lists', null);

        if(!count($data_lists))
        {
            return redirect()->back()->withError('Daftar undangan tidak boleh kosong.');
        }

        $data['posted_by'] = Auth::id();
        $invitation = new Invitation($data);

        if($invitation->save())
        {
            foreach($data_lists as $list)
            {
                $list = [
                    'name'  => null,
                    'email' => $list,
                    'key'   => str_random(64)
                ];
                $invitation->lists()->save(new InvitationList($list));
            }

            event(new InvitationEvent($invitation->lists));

            return redirect()->route('admin.invitations')->withSuccess('Undangan berhasil dibuat.');
        }

        return redirect()->back()->withErrors($invitation->getErrors())->withError('Undangan tidak berhasil dibuat.');
    }

    public function edit($id)
    {
        $invitation = Invitation::with('lists')->find($id);

        if(!$invitation) {
            return redirect()->back()->withError('Data tidak ditemukan.');
        }

    	return view('modules.admin.invitations.edit', compact('invitation'));
    }

    public function update($id)
    {
        $data  = request()->except('invitation_lists');
        $data_lists = request()->get('invitation_lists', null);

        if(!count($data_lists))
        {
            return redirect()->back()->withError('Daftar undangan tidak boleh kosong.');
        }

        $data['posted_by'] = Auth::id();

        $invitation = Invitation::find($id);

        if(!$invitation) {
            return redirect()->back()->withError('Data tidak ditemukan.');
        }

        if(!$invitation->update($data)) {
            return redirect()->back()->withError('Data tidak berhasil di update');
        }

        $lists = InvitationList::where('invitation_id', $invitation->id)->get(['email']);

        if($lists->count())
        {
            $new_lists = array_reduce($lists->pluck('email')->toArray(), function($carry, $item) use ($data_lists) {
                if(!in_array($item, $data_lists))
                    return $item;
            });
        }

        // NEW LISTS
        $new_lists = InvitationList::whereNotIn('email', $data_lists)->where('invitation_id', $invitation->id)->get();

        if($new_lists->count())
        {
            foreach($new_lists as $list)
            {
                $list = [
                    'name'  => null,
                    'email' => $list,
                    'key'   => str_random(64)
                ];

                $invitation->lists()->save(new InvitationList($list));
            }
        }

        return redirect()->route('admin.invitations')->withSuccess('Undangan berhasil update.');
    }

    public function delete($id)
    {
        $invitation = Invitation::find($id);

        if(!$invitation) {
            return redirect()->back()->withError('Data tidak ditemukan.');
        }

        if(!$invitation->delete()) {
            return redirect()->back()->withError('Data tidak berhasil dihapus.');
        }

        return redirect()->back()->withSucces('Data berhasil dihapus');
    }

    public function resend($id)
    {
        $email = request()->get('_email', null);

        if(!$email) {
            return redirect()->back()->withError('Email tidak ditemukan.');
        }

        $list = InvitationList::with('invitation')->where('email', $email)->where('invitation_id', $id)->first();

        if(!$list) {
            return redirect()->back()->withError('Email tidak ditemukan.');
        }

        $api = request()->get('api',null);
        $results = [];

        if(!$api)
            return false;

        $mailable = new InvitationMail($list);
        $api->sendEmail([
            'email_to' => $list->email,
            'subject'  => $list->invitation->title,
            'content'  => $mailable->build()
        ]);

        return redirect()->back()->withSuccess('Email telah dikirimkan.');
    }
}