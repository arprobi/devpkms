<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PKMS\Models\Bantuan;

class BantuanController extends BaseController
{

    public function index()
    {
        $jenis    = request()->get('often_asked', false);
        $question = request()->get('question', false);
        $answer   = request()->get('answer', false);

        $bantuans = Bantuan::orderBy('created_at', 'desc');
        if ($question) {
            $bantuans = $bantuans->where('question', 'like', "%$question%");
        }

        if ($answer) {
            $bantuans = $bantuans->where('answer', 'like', "%$answer%");
        }
        
        if ($jenis === '0' || $jenis === '1') {
            $bantuans = $bantuans->where('often_asked', $jenis);
        }
        
        $bantuans = $bantuans->paginate();

        return view('modules.admin.bantuans.index', compact('bantuans'));
    }

    public function create()
    {
        $create = 1;
        return view('modules.admin.bantuans.create', compact('create'));
    }

    public function store(Request $request)
    {
        $bantuan                = new Bantuan();
        $bantuan->question      = request()->question;
        $bantuan->answer        = request()->answer;
        $bantuan->often_asked   = request()->often_asked ? 1 : 0;

        if ($bantuan->save()) {
            return redirect('admin/bantuans')->withSuccess('Bantuan berhasil di simpan.');
        }

        return redirect()->back()->withError('Bantuan gagal di simpan.');
    }
    
    public function edit($id)
    {
        $bantuan = Bantuan::findOrFail($id);
        $create  = 0;
        if ($bantuan) {
            return view('modules.admin.bantuans.create', compact('bantuan', 'create'));
        }

        return redirect()->back()->withError('Bantuan tidak ditemukan.');
    }

    public function update($id)
    {
        $bantuan = Bantuan::findOrFail($id);

        if ($bantuan) {
            $bantuan->question      = request()->question;
            $bantuan->answer        = request()->answer;
            $bantuan->often_asked   = request()->often_asked ? 1 : 0;;

            if ($bantuan->update()) {
                return redirect('admin/bantuans')->withSuccess('Bantuan berhasil di simpan.');
            }
            
            return redirect()->back()->withError('Bantuan gagal diubah.');
        
        }
        
        return redirect()->back()->withError('Bantuan tidak ditemukan.');
    }

    public function delete($id)
    {
        $bantuan = Bantuan::findOrFail($id);
        
        if (!$bantuan->delete()) {
            return redirect()->back()->withError('Bantuan gagal di hapus.');
        }

        return redirect()->back()->withSuccess('Bantuan berhasil di hapus.');
    }

}
