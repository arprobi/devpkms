<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PKMS\Models\Question;
use PKMS\Models\Answer;

class QuestionController extends BaseController
{
	public function index()
	{
		$questions = Question::with('answers')->paginate();
		return view('modules.admin.questions.index', compact('questions'));
	}

	public function detail($id)
	{
		$question = Question::with('answers')->find($id);

		if(!$question) {
			return redirect()->route('admin.questions')->withError('Pertanyaan tidak ditemukan');
		}

		return view('modules.admin.questions.detail', compact('question'));
	}

	public function postAnswer($id)
	{
		$question = Question::with('answers')->find($id);

		if(!$question) {
			return redirect()->route('admin.questions')->withError('Pertanyaan tidak ditemukan');
		}

		$answer = new Answer([
			'question_id' => $question->id,
			'name' => 'System Administrator',
			'email' => 'no-reply@lkpp.go.id',
			'content' => request()->get('content', '')
		]);

		if($answer->save())
		{
			return redirect()->back()->withSuccess('Jawaban berhasil disimpan.');
		}
		else
		{
			return redirect()->back()->withError('Jawaban tidak berhasil disimpan');
		}
	}
}