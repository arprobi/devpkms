<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PKMS\Models\User;
use PKMS\Models\FcmToken;
use PKMS\Models\FirebaseMobile;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class FirebaseController extends BaseController
{

    public function index()
    {
        $firebase = FirebaseMobile::with('user');

        $query = request()->get('q', false);

        if ($query) {
            $firebase = $firebase->where('message', 'like', "%$query%");
        }

        $firebase = $firebase->orderBy('created_at', 'DESC')->paginate();

        return view('modules.admin.firebase.index', compact('firebase'));
    }

    public function create()
    {
        return view('modules.admin.firebase.create');   
    }

    public function store(Request $request)
    {
        $firebase = new FirebaseMobile();

        $firebase->title        = request()->title;
        $firebase->message      = request()->message;
        $firebase->user_id  = request()->destination;

        if ($firebase->user_id == 0) {
            $send = $this->sendToAll(request()->title, request()->message, date('Y-m-d H:m:s'));
            $firebase->sent_status   = 1;
        }

        if ($firebase->user_id == 1) {
            $user   = User::where('is_admin', 1)->pluck('id')->toArray();
            $token  = FcmToken::whereIn('user_id', $user)->pluck('token')->toArray();
            if (count($token) > 1) {
                $send   = $this->sendToDevice($token, request()->title, request()->message, date('Y-m-d H:m:s'));
                $firebase->sent_status   = 1;
            } else {
                return redirect()->back()->withError('Pesan gagal dikirim, pengguna tidak ditemukan');
            }
        }
        
        if ($firebase->user_id == 2) {
            $user   = User::where('is_admin', 0)->pluck('id')->toArray();
            $token  = FcmToken::whereIn('user_id', $user)->pluck('token')->toArray();
            if (count($token)) {
                $send   = $this->sendToDevice($token, request()->title, request()->message, date('Y-m-d H:m:s'));
                $firebase->sent_status   = 1;
            } else {
                return redirect()->back()->withError('Pesan gagal dikirim, pengguna tidak ditemukan');
            }
        }

        
        if ($firebase->save()) {
            return redirect()->route('admin.firebase')->withSuccess('Pesan berhasil di kirim.');
        }

        return redirect()->back()->withError('Pesan gagal dikirim.');
    }

    public function sendToDevice($token, $title, $message, $time = '')
    {
        $optionBuilder  = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        // $notificationBuilder = new PayloadNotificationBuilder($title);
        // $notificationBuilder->setBody($message)->setSound('default');

        $dataBuilder    = new PayloadDataBuilder();

        $dataBuilder->addData(['title' => $title, 'body' => $message, 'time' => $time, 'icon' => '']);

        $option         = $optionBuilder->build();
        // $notification   = $notificationBuilder->build();
        $data           = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($token, $option, null, $data);

        $result = [
            'success'   => $downstreamResponse->numberSuccess(),
            'failed'    => $downstreamResponse->numberFailure()
        ];
        
        return $result;

    }

    public function sendToAll($title, $message, $time = '')
    {
        $optionBuilder  = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        // $notificationBuilder = new PayloadNotificationBuilder($title);
        // $notificationBuilder->setBody($message)->setSound('default');

        $dataBuilder    = new PayloadDataBuilder();

        $dataBuilder->addData(['title' => $title, 'body' => $message, 'time' => $time, 'icon' => '']);

        $option         = $optionBuilder->build();
        // $notification   = $notificationBuilder->build();
        $data           = $dataBuilder->build();

        $token          = FcmToken::where('status', 1)->pluck('token')->toArray();

        $downstreamResponse = FCM::sendTo($token, $option, null, $data);

        $result = [
            'success'   => $downstreamResponse->numberSuccess(),
            'failed'    => $downstreamResponse->numberFailure()
        ];
        
        return $result;
    }

}
