<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PKMS\Models\FirebaseMobile;
use PKMS\Models\Usulan;

class UsulanController extends BaseController
{

    public function index()
    {
        $usulans = Usulan::with('user')->where('status', 0);

        $usulan = request()->get('q', false);

        if ($usulan) {
            $usulans = $usulans->where('usulan', 'like', "%$usulan%");
        }

        $usulans = $usulans->orderBy('created_at', 'DESC')->paginate();

        return view('modules.admin.usulans.index', compact('usulans'));
    }

    public function history()
    {
        $usulans = Usulan::with('user');

        $status = request()->get('status', false);
        $usulan = request()->get('usulan', false);
        
        if ($status) {
            $usulans = $usulans->where('status', $status);
        }else{
            $usulans = $usulans->where('status', '!=', 0);
        }

        if ($usulan) {
            $usulans = $usulans->where('usulan', 'like', "%$usulan%");
        }

        $usulans = $usulans->orderBy('created_at', 'DESC')->paginate();

        return view('modules.admin.usulans.history', compact('usulans'));   
    }

    public function process($id)
    {
        $usulan = Usulan::findOrFail($id);
        $usulan->status = 1;

        $type  = ['type' => 3, 'detail_id' => $usulan->id];

        if ($usulan->update()) {
            if($usulan->user->token) {
                $this->sendNotification($usulan->user->token->token, 'Usulan di terima', 'Terima kasih telah berpartisipasi, usulan anda telah di proses!', $usulan->user->id, $type);
            }
            return 'success';
        }

        return 'failed';
    }

    public function delete($id)
    {
        $usulan = Usulan::findOrFail($id);
        $usulan->status = 3;

        $type  = ['type' => 3, 'detail_id' => $usulan->id];


        if ($usulan->update()) {
            if($usulan->user->token) {
                $this->sendNotification($usulan->user->token->token, 'Usulan di tolak', 'Terima kasih telah berpartisipasi, mohon maaf usulan anda belum bisa kami terima', $usulan->user->id, $type);
            }
            return 'success';
        }

        return 'failed';
    }

    public function sendNotification($token, $title, $message, $user_id, $type = [])
    {
        $fcm    = new FirebaseMobile();
        $fcm->sendToDevice($token, $title, $message, [$user_id], $type);
        return true;
    }

}
