<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PKMS\Models\Kategori;

class KategoriController extends BaseController
{

    public function index()
    {
        $kategories = Kategori::paginate();

        $query = request()->get('q', false);

        if ($query) {
            $kategories = Kategori::where('nama_kategori', 'like', "%$query%");
            $kategories = $kategories->paginate();
        }


        return view('modules.admin.kategories.index', compact('kategories'));
    }

    public function create()
    {
        return view('modules.admin.kategories.create');
    }

    public function store(Request $request)
    {
        $kategori = new Kategori();

        $kategori->nama_kategori = request()->nama_kategori;

        if ($kategori->save()) {
            return redirect()->route('admin.kategories')->withSuccess('Kategori berhasil di simpan.');
        }

        return redirect()->back()->withError('Kategori tidak berhasil di simpan.');
    }

    public function edit($id)
    {
        $kategori = Kategori::findOrFail($id);

        if ($kategori) {
            return view('modules.admin.kategories.edit', compact('kategori'));
        }

        return redirect()->back()->withError('Kategori tidak ditemukan.');
    }

    public function update($id)
    {
        $kategori = Kategori::findOrFail($id);

        if ($kategori) {
            $kategori->nama_kategori = request()->nama_kategori;
            
            if ($kategori->update()) {
                return redirect()->route('admin.kategories')->withSuccess('Kategori berhasil di simpan.');
            }
            
            return redirect()->back()->withError('Kategori gagal diubah.');
        
        }
        
        return redirect()->back()->withError('Kategori tidak ditemukan.');
    }

    public function delete($id)
    {
        $kategori = Kategori::findOrFail($id);
        $kategori->pasals()->detach();
        if (!$kategori->delete()) {
            return redirect()->back()->withError('Kategori gagal di hapus.');
        }

        return redirect()->back()->withSuccess('Kategori berhasil di hapus.');
    }

}
