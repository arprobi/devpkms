<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use View;
use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PKMS\Models\Disclaimer;

class DisclaimerController extends BaseController
{

    public function index()
    {
        $disclaimer = Disclaimer::all();

        return view('modules.admin.disclaimer.index', compact('disclaimer'));
    }

    public function create()
    {
        $create = 1;
        return view('modules.admin.disclaimer.create', compact('create'));
    }

    public function store(Request $request)
    {
        $disclaimer = new Disclaimer();

        $disclaimer->title      = request()->title;
        $disclaimer->content    = request()->content;
        $disclaimer->version    = request()->version ? request()->version : 1;

        if ($disclaimer->save()) {
            return redirect('admin/disclaimer')->withSuccess('Disclaimer berhasil di simpan.');
        }

        return redirect()->back()->withError('Disclaimer gagal di simpan.');
    }

    public function edit($id)
    {
        $disclaimer = Disclaimer::findOrFail($id);
        $create     = 0;
        if ($disclaimer) {
            return view('modules.admin.disclaimer.create', compact('disclaimer', 'create'));
        }

        return redirect()->back()->withError('Disclaimer tidak ditemukan.');
    }

    public function update($id)
    {
        $disclaimer = Disclaimer::findOrFail($id);

        if ($disclaimer) {

            $disclaimer->title      = request()->title;
            $disclaimer->content    = request()->content;
            $disclaimer->version    = request()->version ? (request()->version + 1) : $disclaimer->version;           

            if ($disclaimer->update()) {
                return redirect('admin/disclaimer')->withSuccess('Disclaimer berhasil di simpan.');
            }
            
            return redirect()->back()->withError('Disclaimer gagal diubah.');
        
        }
        
        return redirect()->back()->withError('Disclaimer tidak ditemukan.');
    }

    public function delete($id)
    {
        $disclaimer = Disclaimer::findOrFail($id);

        if (!$disclaimer->delete()) {
            return redirect()->back()->withError('Disclaimer gagal di hapus.');
        }

        return redirect()->back()->withSuccess('Disclaimer berhasil di hapus.');
    }

}
