<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use PKMS\Models\User;
use PKMS\Models\Task;
use PKMS\Models\Interest;
use PKMS\Models\Experience;
use PKMS\Models\Rating;

class ExpertController extends BaseController
{
    public function index()
    {
        $data['users'] = User::with(['interests','profile','task_history' =>function($query){
            $query->where('action','refused');
        }])->where('is_admin', 0)
            ->where('is_active', 1)
            ->where('is_verified', 1)
            ->select(['id','name','email','avatar','created_at']);

        $filters = request()->get('filters');
        $data['users'] = $this->filter($data['users'], $filters);
        $data['users'] = $data['users']->paginate();

        return view('modules.admin.experts.index', $data);
    }

    public function unverified_lists()
    {
        $filters = request()->get('filters');
        $users = User::with([
            'profile' => function($query){
                $query->with(['province', 'city', 'district', 'village']);
            },
            'educations.detail',
            'experiences',
            'task_history' => function($query){
                $query->where('action', 'refused');
            }
        ])->where('is_admin', 0)
          ->where('is_active', 1)
          ->where('is_verified', 0)
          ->select(['id','name','email','avatar','is_verified','created_at']);

        $users = $this->filter($users, $filters);
        $users = $users->paginate();

        return view('modules.admin.experts.unverified', [
            'data' => [
                'users' => $users
            ]
        ]);
    }

    public function detail($userId)
    {
        $user = User::with(['profile', 'experiences.attachments', 'educations.detail'])->find($userId);

        if(!$user)
            return redirect()->back()->withError('Pengguna tidak ditemukan.');

        return view('modules.admin.experts.detail', compact('user'));
    }

    public function verify($userId)
    {
        $user = User::find($userId);

        if(!$user)
            return redirect()->back()->withError('Pengguna tidak ditemukan.');

        if( $user->update(['is_active' => 1,'is_verified' => 1]) )
        {
            return redirect()->back()->withSuccess('Pengguna berhasil diverifikasi.');
        }
        else
        {
            return redirect()->back()->withError('Pengguna tidak berhasil diverifikasi.');
        }
    }

    public function assignTask($userId)
    {
        $user = User::find($userId);

        if(!$user)
            return redirect()->back()->withError('Pengguna tidak ditemukan.');

        $taskIds = request()->get('task_id');

        if(!$taskIds)
            return redirect()->back()->withError('Tidak ada tugas yang dipilih.');

        $task = Task::whereIn('id', $taskIds)->update([
            'assigned_to' => $userId,
            'assigned_at' => date('Y-m-d H:i:s'),
            'assigned_by' => Auth::user()->id,
            'is_assigned' => 1
        ]);

        if(!$task)
            return redirect()->back()->withError('Tugas tidak ditemukan.');

        return redirect()->back()->withSuccess('Penugasan berhasil dilakukan.');
    }

    public function task_waiting($userId)
    {
        $user = User::with(['tasks' => function($query){
            $query->where('is_accepted', 0);
        }])->find($userId);

        if(!$user)
            return abort(404,'Tenaga Ahli tidak ditemukan.');

        return view('modules.admin.experts._task_waiting', compact('user'));
    }

    public function task_refused($userId)
    {
        $user = User::with(['task_history' => function($query){
            $query->with('task')->where('action','refused');
        }])->find($userId);

        if(!$user)
            return abort(404,'Tenaga Ahli tidak ditemukan.');

        return view('modules.admin.experts._task_refused', compact('user'));
    }

    public function task_ongoing($userId)
    {
        $user = User::with(['tasks' => function($query){
            $query->where('is_accepted', 1)->where('is_completed', 0);
        }])->find($userId);

        if(!$user)
            return abort(404,'Tenaga Ahli tidak ditemukan.');

        return view('modules.admin.experts._task_ongoing', compact('user'));
    }

    public function task_finished($userId)
    {
        $user = User::with(['tasks' => function($query){
            $query->with('rating')->where('is_completed', 1);
        }])->find($userId);

        if(!$user)
            return abort(404,'Tenaga Ahli tidak ditemukan.');

        return view('modules.admin.experts._task_finished', compact('user'));
    }

    /**
     * Filtering query
     * @param  Builder $builder
     * @param  array   $filters
     * @return Builder $builder
     */
    private function filter(Builder $builder, $filters = [])
    {
        if(isset($filters['name']) && $filters['name']) {
            $builder = $builder->where('name', 'like', '%'.$filters['name'].'%');
        }

        if(isset($filters['occupations']) && $filters['occupations']) {
            $builder = $builder->whereHas('experiences', function($query) use ($filters) {
                $i = 0;
                foreach($filters['occupations'] as $experience) {
                    if(!$i)
                        $query->where('position', 'like', '%'.$experience.'%');
                    else
                        $query->orWhere('position', 'like', '%'.$experience.'%');

                    $i++;
                }
            });
        }

        if(isset($filters['rating']) && $filters['rating']) {
            $rating = Rating::select(DB::raw('user_id, avg(rating) as rating'))->groupBy('user_id')->get();

            $rating = ($rating->count()) ? $rating->where('rating', '>', $filters['rating']) : null;
            $rating = ($rating->count()) ? $rating->pluck('user_id') : null;
            $rating = ($rating) ? $rating : [];
            $builder = $builder->whereIn('id', $rating);

        }

        if(isset($filters['interests']) && count($filters['interests'])) {
            $builder = $builder->whereHas('interests', function($query) use ($filters) {
                $i = 0;
                foreach($filters['interests'] as $interest) {
                    if(!$i)
                        $query->where('name', 'like', '%'.$interest.'%');
                    else
                        $query->orWhere('name', 'like', '%'.$interest.'%');

                    $i++;
                }
            });
        }

        if(isset($filters['hobbies']) && count($filters['hobbies'])) {
            $builder = $builder->whereHas('profile', function($query) use ($filters) {
                $i = 0;
                foreach($filters['hobbies'] as $hobby) {
                    if(!$i)
                        $query->where('hobby', 'like', '%'.$hobby.'%');
                    else
                        $query->orWhere('hobby', 'like', '%'.$hobby.'%');

                    $i++;
                }
            });
        }

        return $builder;
    }
}