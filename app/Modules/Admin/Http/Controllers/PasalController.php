<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PKMS\Models\Pasal;
use PKMS\Models\Highlight;

class PasalController extends BaseController
{

    public function index()
    {
        $pasals = Pasal::with('perpres');

        $perpres    = request()->get('perpres', false);
        $nama_pasal = request()->get('nama_pasal', false);
        $isi_pasal  = request()->get('isi_pasal', false);

        if ($nama_pasal || $isi_pasal) {
            $pasals = $pasals->where('nama_pasal', 'like', "%$nama_pasal%")
                        ->where('isi_pasal', 'like', "%$isi_pasal%");
        }
        if ($perpres) {
            $pasals = $pasals->where('perpres_id', $perpres)->where('nama_pasal', 'like', "%$nama_pasal%")
                        ->where('isi_pasal', 'like', "%$isi_pasal%");
        }
        
        $pasals = $pasals->orderBy('perpres_id', 'asc')->orderBy('order', 'asc')->paginate();

        return view('modules.admin.pasals.index', compact('pasals'));
    }

    public function create()
    {
        return view('modules.admin.pasals.create');
    }

    public function store(Request $request)
    {
        $pasal             = new Pasal();
        $pasal->nama_pasal = request()->nama_pasal;
        $pasal->penjelasan = request()->penjelasan;
        $pasal->isi_pasal  = request()->isi_pasal;
        $pasal->highlighted= request()->isi_pasal;
        $pasal->perpres_id = request()->perpres_id;
        $pasal->order      = $this->orderPosition(request()->posisi, request()->pasal, request()->perpres_id);

        if ($pasal->save()) {
            $pasal->categories()->attach(request()->kategori_id);
            return redirect()->back()->withSuccess('Pasal berhasil di simpan.');
        }

        return redirect()->back()->withError('Pasal tidak berhasil di simpan.');
    }

    public function show($id)
    {
        $pasal = Pasal::findOrFail($id);

        return view('modules.admin.pasals.show', compact('pasal'));
    }

    public function edit($id)
    {
        $pasal = Pasal::findOrFail($id);

        if ($pasal) {
            return view('modules.admin.pasals.edit', compact('pasal'));
        }

        return redirect()->back()->withError('Pasal tidak ditemukan.');
    }

    public function update($id)
    {
        $pasal = Pasal::findOrFail($id);

        $testing    = request()->highlighted;
        $highlighs  = Highlight::where('pasal_id', $id)->get();
        // $res        = '';

        foreach ($highlighs as $higt) {
            $stringtag  = '<highlighted hgltid="'. $higt->id .'" pslid="'. $id .'">';
            
            if (strpos(request()->highlighted, $stringtag) === false) {
                $delh  = Highlight::findOrFail($higt->id);
                $delh->comments()->delete();
                if (!$delh->delete()) {
                    return redirect()->back()->withError('Pasal gagal diubah.');
                }
            }

        }

        if ($pasal) {
            $pasal->nama_pasal = request()->nama_pasal;
            $pasal->penjelasan = request()->penjelasan;

            // Remove highlighted
            $removehighlight   = preg_replace("/<\\/?highlighted(.|\\s)*?>/", '', request()->highlighted);

            $pasal->isi_pasal  = $removehighlight;
            $pasal->highlighted= request()->highlighted;
            $pasal->perpres_id = request()->perpres_id;

            if (request()->posisi != 4) {
                if (request()->posisi == 3) {
                    $pasal->order      = $this->rePosisi('update', $id);
                } else {
                    $pasal->order      = $this->orderPosition(request()->posisi, request()->pasal, request()->perpres_id);
                }
            }


            if ($pasal->update()) {
                $pasal->categories()->sync(request()->kategori_id);
                return redirect()->back()->withSuccess('Pasal berhasil di simpan.');
            }
            
            return redirect()->back()->withError('Pasal gagal diubah.');
        
        }
        
        return redirect()->back()->withError('Pasal tidak ditemukan.');
    }

    public function delete($id)
    {
        $pasal = Pasal::findOrFail($id);
        
        $pasal->categories()->detach();

        $reposisi = $this->rePosisi('delete', $id);

        if (!$pasal->delete()) {
            return redirect()->back()->withError('Pasal gagal di hapus.');
        }

        return redirect()->back()->withSuccess('Pasal berhasil di hapus.');
    }

    public function orderPosition($posisi, $pasal_id='', $perpres_id)
    {
        if ($posisi == 0) {
            $allpasal   = Pasal::select('id', 'perpres_id', 'order')
                            ->where('perpres_id', $perpres_id)
                            ->ordered();

            $position   = 2;

            foreach ($allpasal as $key => $pasal) {
                $new = Pasal::findOrFail($pasal->id);
                $new->order = $position;
                $new->save();
                $position = $position + 1;
            }
            
            return 1;

        } elseif ($posisi == 1) {
            $after      = Pasal::findOrFail($pasal_id);
            $allpasal   = Pasal::select('id', 'perpres_id', 'order')
                            ->where('perpres_id', $perpres_id)
                            ->where('order', '>', $after->order)
                            ->ordered();

            $position   = $after->order + 2;

            foreach ($allpasal as $key => $pasal) {
                $new = Pasal::findOrFail($pasal->id);
                $new->order = $position;
                $new->save();
                $position = $position + 1;
            }

            return $after->order+1;
            
        }
         else {
            return Pasal::where('perpres_id', $perpres_id)->max('order') + 1;
        }
    }

    public function rePosisi($type, $id)
    {
        $nowpas     = Pasal::findOrFail($id);
        $allpasal   = Pasal::select('id', 'perpres_id', 'order')
                        ->where('perpres_id', $nowpas->perpres_id)
                        ->where('order', '>', $nowpas->order)
                        ->ordered();

        foreach ($allpasal as $key => $pasal) {
            $new = Pasal::findOrFail($pasal->id);
            $new->order = $new->order - 1;
            $new->save();

            if ($key+1 == $allpasal->count()) {
                if ($type == 'update') {
                    return Pasal::where('perpres_id', $nowpas->perpres_id)->max('order') + 1;
                }else {
                    return 1;
                }
            }
        }
        
    }

}
