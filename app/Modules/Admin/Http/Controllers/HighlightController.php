<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PKMS\Models\Highlight;
use PKMS\Models\Pasal;

use Dompdf\Dompdf;

class HighlightController extends BaseController
{

    public function index()
    {
        $highlights = Highlight::with('user');

        $query = request()->get('q', false);

        if ($query) {
            $highlights = $highlights->where('words', 'like', "%$query%");
        }

        $highlights = $highlights->orderBy('created_at', 'DESC')->paginate();

        return view('modules.admin.highlight.index', compact('highlights', 'query'));
    }

    public function show($id)
    {
        $highlight          = Highlight::findOrFail($id);
        $pasal              = Pasal::findOrFail($highlight->pasal_id);
        $highlight_tag      = '<highlighted hgltid="'. $highlight->id .'" pslid="'. $pasal->id .'">';
        
        $cut_start          = preg_split('#'.$highlight_tag.'#', $pasal->highlighted);
        $cut_end            = preg_split('#</highlighted>#', $cut_start[1]);

        $changed_words      = $highlight_tag.$cut_end[0].'</highlighted>';

        if ($changed_words != $highlight->words) {
            $highlight->words = $changed_words;
            $highlight->save();
        }

        return view('modules.admin.highlight.show', compact('highlight'));
    }

    public function export($id)
    {
        // Preparing content
        $highlight = Highlight::findOrFail($id);
        $content = view('modules.admin.highlight.pdf-template', compact('highlight'))->render();

        // return view('modules.admin.highlight.pdf-template', compact('highlight')); 
        // Setup library
        $dompdf = new Dompdf(array('enable_remote' => true));
        $dompdf->loadHtml($content);
        $dompdf->setPaper('A4', 'potrait');
        @$dompdf->render();
        @$dompdf->stream(strip_tags($highlight->pasal->nama_pasal));
    }

    public function delete($id)
    {
        $highlight = Highlight::findOrFail($id);

        if ($highlight->comments->count()) {
            $highlight->comments()->delete();
        }

        if ($highlight->delete()) {
            // change highlighted pasal
            $pasal = Pasal::findOrFail($highlight->pasal_id);
            $before = '<highlighted pslid=' . $pasal->id . ' hgltid=' . $highlight->id . '>' . $highlight->words . '</highlighted>';
            $after = $highlight->words;
            $updated = str_replace($before, $after, $pasal->highlighted);
            $pasal->highlighted = $updated;
            $pasal->update();

            return 1;
        } else {
            return 0;
        }
    }

}
