<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PKMS\Models\MasterArticleSubcode as Code;

class SubcodeController extends BaseController
{

    public function index()
    {
        $codes = Code::with('articlecode')->orderBy('code')->paginate();
        return view('modules.admin.subcodes.index', compact('codes'));
    }

    public function create()
    {
    	return view('modules.admin.subcodes.create');
    }

    public function store()
    {
    	$data = request()->only(['name','code']);
    	$code = new Code($data);

    	if(! $code->save())
    	{
    		return redirect()->back()->withErrors($code->getErrors())->withError('Data kode artikel tidak berhasil disimpan.');
    	}

    	return redirect()->route('admin.subcodes.index')->withSuccess('Data kode artikel berhasil disimpan.');
    }

    public function edit($id)
    {
    	$code = Code::find($id);

    	if(!$code)
    	{
    		return redirect()->back()->withError('Data kode artikel tidak ditemukan.');
    	}

    	return view('modules.admin.subcodes.edit', compact('code'));
    }

    public function update($id)
    {
    	$data = request()->only(['name','code']);
    	$code = Code::find($id);

    	if(!$code)
    	{
    		return redirect()->back()->withError('Data kode artikel tidak ditemukan.');
    	}

    	if(! $code->update($data))
    	{
    		return redirect()->back()->withErrors($code->getErrors())->withError('Data kode artikel tidak berhasil disimpan.');
    	}

    	return redirect()->route('admin.subcodes.index')->withSuccess('Data kode artikel berhasil disimpan.');
    }

    public function destroy($id)
    {
    	$code = Code::find($id);

    	if(!$code)
    	{
    		return redirect()->back()->withError('Data kode artikel tidak ditemukan.');
    	}

    	if(! $code->delete())
    	{
    		return redirect()->back()->withErrors($code->getErrors())->withError('Data kode artikel tidak berhasil dihapus.');
    	}

    	return redirect()->route('admin.subcodes.index')->withSuccess('Data kode artikel berhasil dihapus.');
    }
}