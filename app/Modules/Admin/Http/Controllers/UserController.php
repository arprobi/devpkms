<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use PKMS\Models\User;
use PKMS\Models\Task;

class UserController extends BaseController
{
    public function index()
    {
        $users = User::paginate();
        return view('modules.admin.users.index', compact('users'));
    }

    public function edit($userId)
    {
        $user = User::find($userId);

        if(!$user)
        {
            return redirect()->route('admin.users')->withError('Pengguna tidak ditemukan.');
        }

        return view('modules.admin.users.edit', compact('user'));
    }

    public function update($userId)
    {
        $user = User::find($userId);

        if(!$user)
        {
            return redirect()->route('admin.users')->withError('Pengguna tidak ditemukan.');
        }

        $data = request()->only(['name','email','is_admin','is_active','is_verified']);

        if(!$user->update($data))
        {
            return redirect()->back()->withErrors($user->getErrors())->withError('Perubahan tidak berhasil disimpan.');
        }

        return redirect()->route('admin.users')->withSuccess('Perubahan berhasil disimpan.');
    }

    public function delete($userId)
    {
        $user = User::find($userId);

        if ($user->hasProfile()) {
            $user->profile()->delete();
        }

        if(!$user)
        {
            return redirect()->route('admin.users')->withError('Pengguna tidak ditemukan.');
        }

        if(!$user->delete())
        {
            return redirect()->back()->withErrors($user->getErrors())->withError('Pengguna tidak berhasil dihapus.');
        }

        return redirect()->route('admin.users')->withSuccess('Pengguna berhasil dihapus.');
    }

    public function resetPassword($userId)
    {
        $user = User::find($userId);

        if(!$user)
            return redirect()->back()->withError('Pengguna tidak ditemukan.');

        $response = Password::broker()->sendResetLink(
            [ 'email' => $user->email ]
        );

        if($response == Password::RESET_LINK_SENT)
        {
            return redirect()->back()->withSuccess('Password berhasil direset.');
        }
        else
        {
            return redirect()->back()->withError('Password tidak berhasil direset.');
        }
    }


    public function detail($userId)
    {
        $user = User::with(['profile', 'experiences.attachments', 'educations.detail'])->find($userId);

        if(!$user)
            return redirect()->back()->withError('Pengguna tidak ditemukan.');

        return view('modules.admin.users.detail', compact('user'));
    }

    public function updateAccount($userId)
    {
        $user = User::find($userId);
        $data = request()->only(['name','email']);

        if(!$user)
            return redirect()->back()->withError('Akun tidak ditemukan.');

        if($user->update($data))
        {
            request()->session()->flash('success', 'Akun berhasil disimpan.');
        }
        else
        {
            request()->session()->flash('error', 'Akun tidak berhasil disimpan.');
            request()->session()->flash('errors', $user->getErrors());
        }

        return redirect()->back();
    }
}
