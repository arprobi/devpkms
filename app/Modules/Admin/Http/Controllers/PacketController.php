<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PKMS\Events\PacketAssigned;
use PKMS\Events\PacketAssignedChange;
use PKMS\Events\PacketFinished;
use PKMS\Events\Offer as OfferEvent;
use PKMS\Models\User;
use PKMS\Models\Packet;
use PKMS\Models\PacketQualification;
use PKMS\Models\PacketOffer;
use PKMS\Models\PacketSchedule;
use PKMS\Models\PacketPaid;
use PKMS\Models\Comment;
use PKMS\Models\Attachment;
use PKMS\Models\PacketRating;
use PKMS\Models\Invitation;
use PKMS\Models\InvitationList;
use PKMS\Mail\Offer as OfferMailer;
use DB;
use Datatables;
use HTML;
use Former;
use PKMS\Mail\Notification;

class PacketController extends BaseController
{

    // Unassigned
    public function index()
    {
        $packets = Packet::with('postedby')->where('is_publish', 0)->where('is_assigned', 0)->where('is_invited', 0)->paginate();
        return view('modules.admin.packets.index', compact('packets'));
    }

    public function add()
    {
        $agenciesDrop = null;
        $api = request()->get('api', null);
        $agencies = $api->getAgencies();
        foreach ($agencies as $agency) {
            $agenciesDrop[$agency->id] = $agency->name;
        }
        $data = [
            'agencies'  => $agenciesDrop,
            'usulan'    => request()->get('usulan', false)
        ];
        return view('modules.admin.packets.add', $data);
    }

    // Create
    public function create()
    {
        $data = request()->except(['lampiran', 'qualifications', 'interests']);
        $data['posted_by'] = Auth::user()->id;
        $data['anggaran'] = str_replace(".", "", $data['anggaran']);
        $files = request()->file('lampiran');
        $qualifications = request()->get('qualifications');
        $interests = request()->get('interests', false);
        $invalidFiles = false;
        $packet = new Packet($data);

        if ($packet->save()) {
            if ($qualifications) {
                $qualificationData = [];
                foreach ($qualifications as $qualification) {
                    $qualificationData[] = ['name' => $qualification];
                }
                $packet->qualifications()->createMany($qualificationData);
            }

            if ($interests) {
                $packet->interests()->sync($interests);
            }

            switch ($packet->cara_pembayaran) {
                case 'sekaligus':
                    $packet->paid()->create([
                        'name' => 'Pembayaran Sekaligus'
                    ]);
                    break;
                case 'bulanan':
                    for ($i = 1; $i <= $packet->jangka_waktu; $i++) {
                        $packet->paid()->create([
                            'name' => "Pembayaran Bulan Ke {$i}"
                        ]);
                    }
                    break;
                case 'termin':
                    for ($i = 1; $i <= $packet->termin; $i++) {
                        $packet->paid()->create([
                            'name' => "Pembayaran Termin Ke {$i}"
                        ]);
                    }
                    break;
            }

//             Save Attachment
            if (count($files)) {
                foreach ($files as $file) {
                    $attachment = $this->attachTo($file);

                    if ($attachment['status']) {
                        $packet->attachments()->attach($attachment['file']->id);
                    } else {
                        if ($invalidFiles) {
                            $invalidFiles .= $attachment['filename'] . ' tidak berhasil disimpan.';
                        } else {
                            $invalidFiles = $attachment['filename'] . ' tidak berhasil disimpan.';
                        }
                    }
                }
            }

            if ($invalidFiles) {
                return redirect()->back()->withError('Tugas berhasil disimpan, tetapi lampiran ' . $invalidFiles);
            }

            return redirect()->route('admin.packets')->withSuccess('Tugas telah berhasil disimpan.');
        } else {
            return redirect()->back()->withError('Tugas tidak berhasil disimpan.');
        }
    }

    public function publishing($packetId)
    {
        $packet = Packet::find($packetId);
        $packet->update(['is_publish' => true, 'publish_at' => date('Y-m-d H:i:s')]);
        return redirect()->route('admin.packets')->withSuccess('Tugas telah berhasil di publish.');
    }

    public function publish()
    {
        $packets = Packet::with('postedby')->where(function($q) {
                            $q->where('is_publish', true)
                            ->orWhere('is_invited', true);
                        })
                        ->where('is_assigned', false)
                        ->where('is_completed', false)->paginate();

        return view('modules.admin.packets.publish', compact('packets'));
    }

    // waiting approval
    public function waitingApproval()
    {
        $packets = Packet::with('postedby', 'assignedto')->where('is_accepted', 0)->where('is_assigned', 1)->paginate();
        return view('modules.admin.packets.waiting', compact('packets'));
    }

    // on going
    public function onGoing()
    {
        $packets = Packet::with('postedby', 'assignedto')->where('is_accepted', 1)->where('is_completed', 0)->paginate();
        return view('modules.admin.packets.ongoing', compact('packets'));
    }

    // complete
    public function complete()
    {
        $packets = Packet::with('postedby', 'rating')->where('is_completed', 1)->paginate();
        return view('modules.admin.packets.completed', compact('packets'));
    }

    public function detail($packetId)
    {
        $agenciesDrop = null;
        $api = request()->get('api', null);
        $agencies = $api->getAgencies();
        foreach ($agencies as $agency) {
            $agenciesDrop[$agency->id] = $agency->name;
        }
        $data['agencies'] = $agenciesDrop;
        $data['packet'] = Packet::with([
                    'qualifications',
                    'attachments',
                    'comments' => function($query) {
                        $query->with(['attachments', 'author'])->orderBy('created_at', 'asc');
                    }
                ])->find($packetId);

        if (!$data['packet'])
            return redirect()->route('admin.packets')->withError('Tugas tidak ditemukan.');

        return view('modules.admin.packets.detail', $data);
    }

    public function invite($packetId)
    {
        $packet = Packet::with('assignedto')->find($packetId);

        if (!$packet->is_invited)
            $packet->update(['is_invited' => true]);

        if (!$packet)
            return redirect()->back()->withError('Paket tidak ditemukan.');

        if ($packet->is_accepted)
            return redirect()->back()->withError('Penugasan tidak berhasil. Paket sudah diterima oleh pengguna dengan nama ', $packet->assignedto->name . ' pada tanggal ' . $packet->accepted_at);

        $userId = request()->get('user_id');

        if (!$userId)
            return redirect()->back()->withError('Tidak ada tenaga ahli yang dipilih.');

        $user = User::find($userId);

        if (!$user)
            return redirect()->back()->withError('Tenaga ahli tidak ditemukan.');

        if (!$packet->userInvites()->sync($user->id))
            return redirect()->back()->withError('Undangan tidak berhasil disimpan.');

        $api = request()->get('api', null);
        $results = [];

        if (!$api)
            return false;

        $data = [
            'name' => $user->name,
            'email' => $user->email,
            'content' => 'Anda Telah diundang untuk paket pengerjaan ' . $packet->title . '. <a href="' . route('admin.packets.detail', $packet->id) . '">Klik disini</a> untuk lihat lebih detail'
        ];

        $mailable = new Notification($data);
        $results[] = $api->sendEmail([
            'email_to' => $user->email,
            'subject' => 'Undangan Paket Pekerjaan',
            'content' => $mailable->build()
        ]);


        return redirect()->back()->withSuccess('Undangan berhasil dilakukan.');
    }

    public function invitePage($packetId)
    {
        $api = request()->get('api', null);
        $paket = Packet::with([
                    'attachments',
                ])->find($packetId);
        if ($paket) {
            $paket->instansi = $api->getAgency($paket->instansi_id);
        }
        $interestIds = $paket->interests()->pluck('id')->toArray();
        $data['userInvited'] = $paket->userInvites()->pluck('id')->toArray();
        $data['packet'] = $paket;
        $data['users'] = $users = User::with([
                    'packet_history' => function($query) {
                        $query->where('action', 'refused');
                    }
                ])->whereHas('interests', function($query) use ($interestIds) {
                    $query->whereIn('id', $interestIds);
                })
                ->where('is_admin', 0)
                ->where('is_verified', 1)
                ->get();


        if (!$data['packet'])
            return redirect()->route('admin.packets')->withError('Paket tidak ditemukan.');

        return view('modules.admin.packets.invite', $data);
    }

    public function assign($packetId)
    {
        $change = false;
        $packet = Packet::with('assignedto')->find($packetId);

        if (!$packet)
            return redirect()->back()->withError('Tugas tidak ditemukan.');

        if ($packet->is_accepted)
            return redirect()->back()->withError('Penugasan tidak berhasil. Tugas sudah diterima oleh pengguna dengan nama ', $packet->assignedto->name . ' pada tanggal ' . $packet->accepted_at);

        $userId = request()->get('user_id');
        $pembayaran = request()->get('pembayaran');

        if (!$userId)
            return redirect()->back()->withError('Tidak ada tenaga ahli yang dipilih.');

        $user = User::find($userId);

        if (!$user)
            return redirect()->back()->withError('Tenaga ahli tidak ditemukan.');

        if ($packet->assigned_to && $packet->assigned_to != $user->id)
            event(new PacketAssignedChange($packet));


        if (!$packet->update([
            'assigned_to' => $user->id,
            'assigned_at' => date('Y-m-d H:i:s'),
            'assigned_by' => Auth::user()->id,
            'is_assigned' => true,
            'pembayaran' => $pembayaran
                ]))
            return redirect()->back()->withError('Pemenang tidak berhasil disimpan.');

        event(new PacketAssigned($packet));

        return redirect()->back()->withSuccess('Pemenang berhasil dipilih.');
    }

    public function assignPage($packetId)
    {
        $api = request()->get('api', null);
        $paket = Packet::with([
                    'assignedto' => function($query) {
                        $query->with(['packet_history', 'my_packets']);
                    },
                    'attachments',
                    'offers'
                ])->find($packetId);
        if ($paket) {
            $paket->instansi = $api->getAgency($paket->instansi_id);
        }
        $data['packet'] = $paket;
        if (!$data['packet'])
            return redirect()->route('admin.packets')->withError('Tugas tidak ditemukan.');

        return view('modules.admin.packets.assign', $data);
    }

    public function offerDocument($offerId)
    {
        $offer = PacketOffer::find($offerId);
        $data['user'] = $offer->user;
        $data['packet'] = $offer->packet;
        return view('modules.admin.packets.document', $data);
    }

    public function dataTableEvaluated($packetId)
    {
        $search = Request()->get('params');
        $query = PacketOffer::with('user')->where('packet_id', $packetId);
        if (isset($search['interest']) && $search['interest']) {
            $query->whereHas('user.interests', function($q) use ($search) {
                $q->whereIn('id', $search['interest']);
            });
        }
        return Datatables::eloquent($query)
                        ->addColumn('avatar', function($offer) {
                            $path = $offer->user->avatar ? secure_asset('storage/' . $offer->user->avatar) : secure_asset('/img/default_avatar.jpg');
                            return HTML::image($path, 'Avatar', array('width' => 64, 'height' => 64));
                        })
                        ->editColumn('name', function($offer) {
                            return HTML::link(route('admin.experts.detail', $offer->user->id), $offer->user->name) . " <code>{$offer->user->email}</code>";
                        })
                        ->addColumn('kualifikasi', function($offer) {
                            $html = "";
                            foreach ($offer->user->interests as $interest) {
                                $html .= "<span class='label label-primary'>{$interest->name}</span> ";
                            }
                            return $html;
                        })
                        ->addColumn('penawaran', function($offer) {
                            return "Rp. " . number_format($offer->offer, 0, ',', '.');
                        })
                        ->addColumn('nego', function($offer) {
                            $nego = $offer->packet->negotiations()->where('user_id', $offer->user_id)->orderBy('created_at', 'desc')->first();
                            if (isset($nego->pivot->offer)) {
                                return "Rp. " . number_format($nego->pivot->offer, 0, ',', '.');
                            } else {
                                return "nego belum dilakukan";
                            }
                        })
                        ->addColumn('action', function($offer) use ($packetId) {
                            $html = HTML::link(route('admin.packets.document.offer', $offer->id), 'Dokumen', ['class' => 'btn btn-sm btn-default']) . " ";
                            $html .= HTML::link(route('admin.packets.nego.page', [$packetId, $offer->user->id]), 'Nego', ['class' => 'btn btn-sm btn-default']) . " ";
                            $html .= HTML::link('javascript:;', 'Pilih', [
                                        'class' => 'btn btn-sm btn-default',
                                        'data-packet-id' => $packetId,
                                        'data-user-id' => $offer->user->id,
                                        'data-packet-title' => $offer->packet->title,
                                        'data-user-name' => $offer->user->name,
                                        'data-toggle' => "modal",
                                        'data-target' => "#modal-assign-packet"
                            ]);
//                            $html .= Former::open()->style('display:inline-block');
//                            $html .= Former::hidden('user_id')->value($offer->user->id);
//                            $html .= '<button class="btn btn-assign btn-sm btn-default">Pilih</button>';
//                            $html .= Former::close();
                            return $html;
                        })
                        ->rawColumns(['avatar', 'name', 'kualifikasi', 'action'])->make(true);
    }

    public function dataTableUsers($packetId)
    {
        $paket = Packet::find($packetId);
        $interestIds = $paket->interests()->pluck('id')->toArray();
        $query = User::with([
                    'packet_history' => function($query) {
                        $query->where('action', 'refused');
                    }
                ])->whereHas('interests', function($query) use ($interestIds) {
                    $query->whereIn('id', $interestIds);
                })
                ->where('is_admin', 0)
                ->where('is_verified', 1);

        return Datatables::eloquent($query)
                        ->editColumn('name', function($user) {
                            return HTML::link(route('admin.experts.detail', $user->id), $user->name) . " <code>{$user->email}</code>";
                        })
                        ->addColumn('avatar', function($user) {
                            $path = $user->avatar ? secure_asset('storage/' . $user->avatar) : secure_asset('/img/default_avatar.jpg');
                            return HTML::image($path, 'Avatar', array('width' => 64, 'height' => 64));
                        })
                        ->addColumn('waiting', function($user) {
                            $count = $user->packets->where('is_accepted', 0)->count();
                            return $count . ' Paket';
                        })
                        ->addColumn('refused', function($user) {
                            $count = $user->packet_history->count();
                            return $count . ' Paket';
                        })
                        ->addColumn('progress', function($user) {
                            $count = $user->packets->where('is_completed', 0)->count();
                            return $count . ' Paket';
                        })
                        ->addColumn('done', function($user) {
                            $count = $user->packets->where('is_completed', 1)->count();
                            return $count . ' Paket';
                        })
                        ->addColumn('action', function() {
                            return '-';
                        })
                        ->rawColumns(['avatar', 'name', 'action'])->make(true);
    }

    public function schedule($packetId)
    {
        $packet = Packet::find($packetId);
        $schedules = request()->input('schedule');

        if (isset($schedules['update'])) {
            foreach ($schedules['update'] as $id => $schedule) {
                $packet->schedule()
                        ->where('id', $id)
                        ->where(function ($query) use($schedule) {
                            $query->where('start_date', '!=', $schedule['start_date'])
                            ->orWhere('end_date', '!=', $schedule['end_date']);
                        })
                        ->update($schedule);
            }
        } else {
            foreach ($schedules as $schedule) {
                $packet->schedule()->save(new PacketSchedule($schedule));
            }
        }

        return redirect()->route('admin.packets')->withSuccess('Penugasan berhasil dilakukan.');
    }

    public function schedulePage($packetId)
    {
        $api = request()->get('api', null);
        $paket = Packet::with([
                    'qualifications',
                    'attachments'
                ])->find($packetId);
        if ($paket) {
            $paket->instansi = $api->getAgency($paket->instansi_id);
        }
        $data['packet'] = $paket;

        if (!$data['packet'])
            return redirect()->route('admin.packets')->withError('Tugas tidak ditemukan.');

        return view('modules.admin.packets.schedule', $data);
    }

    public function update($packetId)
    {
        $packet = Packet::find($packetId);
        $data = request()->except(['lampiran', 'qualifications', 'interests']);
        $data['progress'] = request()->get('progress', 0);
        $data['anggaran'] = str_replace(".", "", $data['anggaran']);
        $qualifications = request()->get('qualifications');
        $interests = request()->get('interests', true);

        if ($packet && $packet->update($data)) {
            if ($qualifications) {
                $qualificationIds = $packet->qualifications()->pluck('id')->toArray();
                if (isset($qualifications['update'])) {
                    $deletedIds = [];
                    foreach ($qualificationIds as $id) {
                        if (isset($qualifications['update'][$id])) {
                            $packet->qualifications()
                                    ->where('id', $id)
                                    ->where('name', '!=', $qualifications['update'][$id])
                                    ->update(['name' => $qualifications['update'][$id]]);
                        } else {
                            $deletedIds[] = $id;
                        }
                    }
                    $packet->qualifications()->whereIn('id', $deletedIds)->delete();
                    unset($qualifications['update']);
                }
                if ($qualifications) {
                    foreach ($qualifications as $qualification) {
                        $packet->qualifications()->save(
                                new PacketQualification(['name' => $qualification])
                        );
                    }
                }
            }
            if ($interests) {
                $packet->interests()->sync($interests);
            }
            return redirect()->back()->withSuccess('Tugas berhasil disimpan');
        } else {
            return redirect()->back()->withErrors($packet->getErrors())->withError('Tugas tidak berhasil di simpan.');
        }
    }

    public function addAttachment($packetId)
    {
        $packet = Packet::find($packetId);
        $files = request()->file('lampiran');
        $invalidFiles = false;

        if (!$packet) {
            return redirect()->back()->withError('Tugas tidak ditemukan');
        }

        if (count($files)) {
            foreach ($files as $file) {
                $attachment = $this->attachTo($file);

                if ($attachment['status']) {
                    $packet->attachments()->attach($attachment['file']->id);
                } else {
                    if ($invalidFiles) {
                        $invalidFiles .= $attachment['filename'] . ' tidak berhasil disimpan.';
                    } else {
                        $invalidFiles = $attachment['filename'] . ' tidak berhasil disimpan.';
                    }
                }
            }

            return redirect()->back();
        }

        if ($invalidFiles) {
            return redirect()->back()->withError('Lampiran ' . $invalidFiles);
        }
    }

    public function approve()
    {
        $packetId = request()->get('packet_id');
        $rate = request()->get('rating');
        $packet = Packet::find($packetId);

        if (!$packet)
            return redirect()->route('admin.packets.ongoing')->withError('Tugas tidak ditemukan.');

        if ($packet->update([
                    'is_completed' => 1,
                    'progress' => 100,
                    'completed_at' => date('Y-m-d H:i:s')
                ])) {

            $rate['user_id'] = Auth::user()->id;
            $rating = new PacketRating($rate);
            $packet->rating()->save($rating);

            event(new PacketFinished($packet, $rate['review']));
            return redirect()->back()->withSuccess('Tugas berhasil disimpan.');
        } else {
            return redirect()->back()->withErrors($packet->getErrors())->withError('Tugas tidak berhasil di simpan.');
        }
    }

    public function cancel($packetId)
    {
        $packet = Packet::where('is_accepted', 0)->find($packetId);

        if (!$packet)
            return redirect()->route('admin.packets')->withError('Tugas tidak ditemukan.');

        if ($packet->delete()) {
            return redirect()->route('admin.packets')->withSuccess('Tugas telah berhasil dibatalkan.');
        } else {
            return redirect()->route('admin.packets')->withError('Tugas tidak berhasil dibatalkan.');
        }
    }

    public function negoPage($packetId, $userId)
    {
        $api = request()->get('api', null);
        $paket = Packet::with([
                    'attachments',
                    'negotiations' => function($q) use ($userId) {
                        $q->where('user_id', $userId);
                    }
                ])->find($packetId);


        if ($paket) {
            $paket->instansi = $api->getAgency($paket->instansi_id);
        }
        $data['packet'] = $paket;
        if (!$data['packet'])
            return redirect()->route('admin.packets')->withError('paket tidak ditemukan.');

        return view('modules.admin.packets.nego', $data);
    }

    public function nego($packetId, $userId)
    {
        $packet = Packet::find($packetId);
        $data = request()->only(['text']);
        $offer = str_replace(".", "", request()->get('offer'));
        $data['posted_by'] = Auth::user()->id;


        if (!$packet)
            return redirect()->back()->withError('Nego tidak berhasil dikirim.');

        // Save Comment
        $comment = new Comment($data);
        if (!$comment->save()) {
            return redirect()->back()->withError('Nego tidak berhasil dikirim');
        }

        $packet->negotiations()->attach($comment->id, ['user_id' => $userId, 'offer' => $offer]);
        $packet->pembayaran = $offer;
        $packet->save();

        return redirect()->back()->withSuccess('Nego berhasil dikirim.');
    }

    public function addComment($packetId)
    {
        $packet = Packet::find($packetId);
        $data = request()->only(['text']);
        $files = request()->file('lampiran');
        $data['posted_by'] = Auth::user()->id;
        $invalidFiles = false;

        if (!$packet)
            return redirect()->back()->withError('Pesan tidak berhasil dikirim.');

        // Save Comment
        $comment = new Comment($data);
        if (!$comment->save()) {
            return redirect()->back()->withError('Pesan tidak berhasil dikirim');
        }

        $packet->comments()->attach($comment->id);

        // Save Attachment
        if (count($files)) {
            foreach ($files as $file) {
                $attachment = $this->attachTo($file);

                if ($attachment['status']) {
                    $comment->attachments()->attach($attachment['file']->id);
                } else {
                    if ($invalidFiles) {
                        $invalidFiles .= $attachment['filename'] . ' tidak berhasil disimpan.';
                    } else {
                        $invalidFiles = $attachment['filename'] . ' tidak berhasil disimpan.';
                    }
                }
            }
        }

        if ($invalidFiles) {
            return redirect()->back()->withError('Pesan berhasil dikirim, tetapi lampiran ' . $invalidFiles);
        }

        return redirect()->back()->withSuccess('Pesan berhasil dikirim.');
    }

    private function attachTo($file)
    {
        $acceptedFiles = ['jpg', 'jpeg', 'png', 'bmp', 'pdf', 'docx', 'doc', 'ppt', 'pptx', 'zip', 'rar', 'xls', 'xlsx'];

        if (!in_array($file->extension(), $acceptedFiles))
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName
            ];

        $attachment = [
            'title' => $file->getClientOriginalName(),
            'size' => $file->getClientSize(),
            'mime' => $file->getMimeType(),
            'path' => $file->store('uploaded', 'public'),
            'short_url' => uniqid('exp' . Auth::user()->id),
            'owner_id' => Auth::user()->id
        ];

        $attachment = new Attachment($attachment);

        if ($attachment->save()) {
            return [
                'status' => true,
                'file' => $attachment
            ];
        } else {
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName
            ];
        }
    }

    public function paidsPage($packetId)
    {
        $paket = Packet::find($packetId);

        $data['packet'] = $paket;
        if (!$data['packet'])
            return redirect()->route('admin.packets')->withError('paket tidak ditemukan.');

        return view('modules.admin.packets.paids', $data);
    }

    public function paidPage($paidId)
    {
        $paid = PacketPaid::find($paidId);

        $data['paid'] = $paid;
        if (!$data['paid'])
            return redirect()->route('admin.packets')->withError('pembayaran tidak ditemukan.');

        return view('modules.admin.packets.paid', $data);
    }

    public function paid($paidId)
    {
        $paid = PacketPaid::find($paidId);
        $data = request()->except(['lampiran']);
        $data['nominal'] = str_replace(".", "", $data['nominal']);
        $files = request()->file('lampiran');
        $invalidFiles = false;

        if ($paid && $paid->update($data)) {
            if (count($files)) {
                foreach ($files as $file) {
                    $attachment = $this->attachTo($file);

                    if ($attachment['status']) {
                        $paid->attachments()->attach($attachment['file']->id);
                    } else {
                        if ($invalidFiles) {
                            $invalidFiles .= $attachment['filename'] . ' tidak berhasil disimpan.';
                        } else {
                            $invalidFiles = $attachment['filename'] . ' tidak berhasil disimpan.';
                        }
                    }
                }
            }

            if ($invalidFiles) {
                return redirect()->back()->withError('Pembayaran berhasil disimpan, tetapi lampiran ' . $invalidFiles);
            }
            return redirect()->route('admin.packets.paids.page', $paid->packet->id)->withSuccess('Pembayaran berhasil disimpan.');
        } else {
            return redirect()->back()->withError('Pembayaran tidak berhasil di simpan.');
        }
    }

    public function paidDetail($paidId)
    {
        $paid = PacketPaid::find($paidId);

        $data['paid'] = $paid;
        if (!$data['paid'])
            return redirect()->route('admin.packets.paids.page', $paid->packet->id)->withError('pembayaran tidak ditemukan.');

        return view('modules.admin.packets.paid_detail', $data);
    }

    //invitation
    public function invitations($packetId)
    {
        $packet = Packet::find($packetId);
        $invitations = $packet->invitations()->with('lists')->paginate();
        return view('modules.admin.packets.invitations.index', compact('invitations', 'packet'));
    }

    public function invitationShow($id)
    {
        $invitation = Invitation::with('lists')->find($id);
        $packetId = DB::table('p_packet_invitation')->where('invitation_id', $id)->first()->packet_id;
        $packet = Packet::find($packetId);

        if (!$invitation) {
            return redirect()->back()->withError('Data tidak ditemukan.');
        }

        return view('modules.admin.packets.invitations.view', compact('invitation', 'packet'));
    }

    public function invitationForm($packetId)
    {
        $packet = Packet::with(['schedule' => function($query) {
                        $query->where('t_packet_schedule.name', 'Pengumuman/Undangan');
                    }])->find($packetId);
        return view('modules.admin.packets.invitations.create', compact('packet'));
    }

    public function invitationCreate($packetId)
    {
        $packet = Packet::find($packetId);
        $packet->update(['is_publish' => true, 'publish_at' => date('Y-m-d H:i:s')]);

        $data = request()->except('invitation_lists');
        $data_lists = request()->get('invitation_lists', null);

        if (!count($data_lists)) {
            return redirect()->back()->withError('Daftar undangan tidak boleh kosong.');
        }

        $data['posted_by'] = Auth::id();
        $invitation = new Invitation($data);
        if ($invitation->save()) {
            $packet->invitations()->attach($invitation->id);
            foreach ($data_lists as $list) {
                $list = [
                    'name' => null,
                    'email' => $list,
                    'key' => base64_encode(time() . Auth::id() . "-" . route('expert.packets.offer', $packet->id))
                ];
                $invitation->lists()->save(new InvitationList($list));
            }

            event(new OfferEvent($invitation->lists));

            return redirect()->route('admin.packets.publish')->withSuccess('Pengumuman berhasil dibuat.');
        }

        return redirect()->back()->withErrors($invitation->getErrors())->withError('Pengumuman tidak berhasil dibuat.');
    }

    public function invitationEdit($id)
    {
        $invitation = Invitation::with('lists')->find($id);
        $packetId = DB::table('p_packet_invitation')->where('invitation_id', $id)->first()->packet_id;
        $packet = Packet::find($packetId);
        if (!$invitation) {
            return redirect()->back()->withError('Data tidak ditemukan.');
        }

        return view('modules.admin.packets.invitations.edit', compact('packet', 'invitation'));
    }

    public function invitationUpdate($id)
    {
        $data = request()->except('invitation_lists');
        $data_lists = request()->get('invitation_lists', null);

        if (!count($data_lists)) {
            return redirect()->back()->withError('Daftar undangan tidak boleh kosong.');
        }

        $data['posted_by'] = Auth::id();

        $invitation = Invitation::find($id);

        if (!$invitation) {
            return redirect()->back()->withError('Data tidak ditemukan.');
        }

        if (!$invitation->update($data)) {
            return redirect()->back()->withError('Data tidak berhasil di update');
        }

        $lists = InvitationList::where('invitation_id', $invitation->id)->get(['email']);

        if ($lists->count()) {
            $new_lists = array_reduce($lists->pluck('email')->toArray(), function($carry, $item) use ($data_lists) {
                if (!in_array($item, $data_lists))
                    return $item;
            });
        }

        // NEW LISTS
        $new_lists = InvitationList::whereNotIn('email', $data_lists)->where('invitation_id', $invitation->id)->get();

        if ($new_lists->count()) {
            foreach ($new_lists as $list) {
                $list = [
                    'name' => null,
                    'email' => $list,
                    'key' => str_random(64)
                ];

                $invitation->lists()->save(new InvitationList($list));
            }
        }

        return redirect()->route('admin.packet.invitations')->withSuccess('Undangan berhasil update.');
    }

    public function invitationDelete($id)
    {
        $invitation = Invitation::find($id);

        if (!$invitation) {
            return redirect()->back()->withError('Data tidak ditemukan.');
        }

        if (!$invitation->delete()) {
            return redirect()->back()->withError('Data tidak berhasil dihapus.');
        }

        return redirect()->back()->withSucces('Data berhasil dihapus');
    }

    public function invitationResend($id)
    {
        $email = request()->get('_email', null);

        if (!$email) {
            return redirect()->back()->withError('Email tidak ditemukan.');
        }

        $list = InvitationList::with('invitation')->where('email', $email)->where('invitation_id', $id)->first();

        if (!$list) {
            return redirect()->back()->withError('Email tidak ditemukan.');
        }

        $api = request()->get('api', null);
        $results = [];

        if (!$api)
            return false;

        $mailable = new OfferMailer($list);
        $api->sendEmail([
            'email_to' => $list->email,
            'subject' => $list->invitation->title,
            'content' => $mailable->build()
        ]);

        return redirect()->back()->withSuccess('Email telah dikirimkan.');
    }

}
