<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PKMS\Models\Interest;

class InterestController extends BaseController
{

    public function index()
    {
        $interests = Interest::paginate();
        return view('modules.admin.interests.index', compact('interests'));
    }

    public function create()
    {
    	return view('modules.admin.interests.create');
    }

    public function store()
    {
    	$data = request()->only(['name']);
    	$interest = new Interest($data);

    	if(! $interest->save())
    	{
    		return redirect()->back()->withErrors($interest->getErrors())->withError('Data minat tidak berhasil disimpan.');
    	}

    	return redirect()->route('admin.interests.index')->withSuccess('Data minat berhasil disimpan.');
    }

    public function edit($id)
    {
    	$interest = Interest::find($id);

    	if(!$interest)
    	{
    		return redirect()->back()->withError('Data minat tidak ditemukan.');
    	}

    	return view('modules.admin.interests.edit', compact('interest'));
    }

    public function update($id)
    {
    	$data = request()->only(['name']);
    	$interest = Interest::find($id);

    	if(!$interest)
    	{
    		return redirect()->back()->withError('Data minat tidak ditemukan.');
    	}

    	if(! $interest->update($data))
    	{
    		return redirect()->back()->withErrors($interest->getErrors())->withError('Data minat tidak berhasil disimpan.');
    	}

    	return redirect()->route('admin.interests.index')->withSuccess('Data minat berhasil disimpan.');
    }

    public function destroy($id)
    {
    	$interest = Interest::find($id);

    	if(!$interest)
    	{
    		return redirect()->back()->withError('Data minat tidak ditemukan.');
    	}

    	if(! $interest->delete())
    	{
    		return redirect()->back()->withErrors($interest->getErrors())->withError('Data minat tidak berhasil dihapus.');
    	}

    	return redirect()->route('admin.interests.index')->withSuccess('Data minat berhasil dihapus.');
    }
}