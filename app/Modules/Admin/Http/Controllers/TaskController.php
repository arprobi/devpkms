<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PKMS\Events\TaskPosted;
use PKMS\Events\TaskAssigned;
use PKMS\Events\TaskFinished;
use PKMS\Models\User;
use PKMS\Models\Task;
use PKMS\Models\Comment;
use PKMS\Models\Attachment;
use PKMS\Models\Rating;

class TaskController extends BaseController
{

    // Unassigned
    public function index()
    {
        $tasks = Task::with('postedby')->where('assigned_to', NULL)->where('is_assigned', 0)->paginate();
        return view('modules.admin.tasks.index', compact('tasks'));
    }

    // Create
    public function create()
    {
        $data = request()->only(['title', 'description']);
        $data['posted_by'] = Auth::user()->id;
        $files = request()->file('lampiran');
        $invalidFiles = false;

        $task = new Task($data);

        if ($task->save()) {

            // Save Attachment
            if (count($files)) {
                foreach ($files as $file) {
                    $attachment = $this->attachToTask($file);

                    if ($attachment['status']) {
                        $task->attachments()->attach($attachment['file']->id);
                    } else {
                        if ($invalidFiles) {
                            $invalidFiles .= $attachment['filename'] . ' tidak berhasil disimpan.';
                        } else {
                            $invalidFiles = $attachment['filename'] . ' tidak berhasil disimpan.';
                        }
                    }
                }
            }

            if ($invalidFiles) {
                return redirect()->back()->withError('Tugas berhasil disimpan, tetapi lampiran ' . $invalidFiles);
            }

            //event(new TaskPosted(Auth::user(), $task));

            return redirect()->back()->withSuccess('Tugas telah berhasil disimpan.');
        } else {
            return redirect()->back()->withError('Tugas tidak berhasil disimpan.');
        }
    }

    // on going
    public function onGoing()
    {
        $tasks = Task::with('postedby', 'assignedto')->where('is_accepted', 1)->where('is_completed', 0)->paginate();
        return view('modules.admin.tasks.ongoing', compact('tasks'));
    }

    // waiting approval
    public function waitingApproval()
    {
        $tasks = Task::with('postedby', 'assignedto')->where('is_accepted', 0)->where('is_assigned', 1)->paginate();
        return view('modules.admin.tasks.waiting', compact('tasks'));
    }

    // complete
    public function complete()
    {
        $tasks = Task::with('postedby', 'rating')->where('is_completed', 1)->paginate();
        return view('modules.admin.tasks.completed', compact('tasks'));
    }

    public function detail($taskId)
    {
        $data['task'] = Task::with([
                    'articles' => function($query) {
                        $query->with(['attachments', 'author']);
                    },
                    'attachments',
                    'comments' => function($query) {
                        $query->with(['attachments', 'author'])->orderBy('created_at', 'asc');
                    }
                ])->find($taskId);

        if (!$data['task'])
            return redirect()->route('admin.tasks')->withError('Tugas tidak ditemukan.');

        return view('modules.admin.tasks.detail', $data);
    }

    public function assign($taskId)
    {
        $task = Task::with('assignedto')->find($taskId);

        if (!$task)
            return redirect()->back()->withError('Tugas tidak ditemukan.');

        if ($task->is_accepted)
            return redirect()->back()->withError('Penugasan tidak berhasil. Tugas sudah diterima oleh pengguna dengan nama ', $task->assignedto->name . ' pada tanggal ' . $task->accepted_at);

        $userId = request()->get('user_id');

        if (!$userId)
            return redirect()->back()->withError('Tidak ada tenaga ahli yang dipilih.');

        $user = User::find($userId);

        if (!$user)
            return redirect()->back()->withError('Tenaga ahli tidak ditemukan.');

        if (!$task->update([
                    'assigned_to' => $user->id,
                    'assigned_at' => date('Y-m-d H:i:s'),
                    'assigned_by' => Auth::user()->id,
                    'is_assigned' => 1
                ]))
            return redirect()->back()->withError('Tugas tidak berhasil disimpan.');

        event(new TaskAssigned(Auth::user(), $task));

        return redirect()->back()->withSuccess('Penugasan berhasil dilakukan.');
    }

    public function assignPage($taskId)
    {
        $data['task'] = Task::with([
                    'assignedto' => function($query) {
                        $query->with(['task_history', 'my_tasks']);
                    },
                    'attachments'
                ])->find($taskId);

        $data['users'] = $users = User::with([
                    'task_history' => function($query) {
                        $query->where('action', 'refused');
                    }
                ])->where('is_admin', 0)
                ->where('is_verified', 1)
                ->get();

        if (!$data['task'])
            return redirect()->route('admin.tasks')->withError('Tugas tidak ditemukan.');

        return view('modules.admin.tasks.assign', $data);
    }

    public function update($taskId)
    {
        $task = Task::find($taskId);
        $data = [
            'title' => request()->get('title'),
            'description' => request()->get('description'),
            'progress' => request()->get('progress', 0),
        ];
        $files = request()->file('lampiran');
        $invalidFiles = false;

        if ($task && $task->update($data)) {
            // Save Attachment
            if (count($files)) {
                foreach ($files as $file) {
                    $attachment = $this->attachToTask($file);

                    if ($attachment['status']) {
                        $task->attachments()->attach($attachment['file']->id);
                    } else {
                        if ($invalidFiles) {
                            $invalidFiles .= $attachment['filename'] . ' tidak berhasil disimpan.';
                        } else {
                            $invalidFiles = $attachment['filename'] . ' tidak berhasil disimpan.';
                        }
                    }
                }
            }

            if ($invalidFiles) {
                return redirect()->back()->withError('Tugas berhasil disimpan, tetapi lampiran ' . $invalidFiles);
            }

            return redirect()->back()->withSuccess('Tugas berhasil disimpan');
        } else {
            return redirect()->back()->withErrors($task->getErrors())->withError('Tugas tidak berhasil di simpan.');
        }
    }

    public function addAttachment($taskId)
    {
        $task = Task::find($taskId);
        $files = request()->file('lampiran');
        $invalidFiles = false;

        if (!$task) {
            return redirect()->back()->withError('Tugas tidak ditemukan');
        }

        if (count($files)) {
            foreach ($files as $file) {
                $attachment = $this->attachToTask($file);

                if ($attachment['status']) {
                    $task->attachments()->attach($attachment['file']->id);
                } else {
                    if ($invalidFiles) {
                        $invalidFiles .= $attachment['filename'] . ' tidak berhasil disimpan.';
                    } else {
                        $invalidFiles = $attachment['filename'] . ' tidak berhasil disimpan.';
                    }
                }
            }

            return redirect()->back();
        }

        if ($invalidFiles) {
            return redirect()->back()->withError('Lampiran ' . $invalidFiles);
        }
    }

    public function approve()
    {
        $taskId = request()->get('task_id');
        $rate = request()->get('rating');
        $task = Task::with('articles')->find($taskId);

        if (!$task)
            return redirect()->route('admin.tasks')->withError('Tugas tidak ditemukan.');

        if ($task->update([
                    'is_completed' => 1,
                    'progress' => 100,
                    'completed_at' => date('Y-m-d H:i:s')
                ])) {
            if ($task->articles->count()) {
                $task->articles()->update(['is_published' => 1]);
            }

            $rate['user_id'] = Auth::user()->id;
            $rating = new Rating($rate);
            $task->rating()->save($rating);

            event(new TaskFinished(Auth::user(), $task, $rate['review']));
            return redirect()->back()->withSuccess('Tugas berhasil disimpan.');
        } else {
            return redirect()->back()->withErrors($task->getErrors())->withError('Tugas tidak berhasil di simpan.');
        }
    }

    public function cancel($taskId)
    {
        $task = Task::where('is_accepted', 0)->find($taskId);

        if (!$task)
            return redirect()->route('admin.tasks')->withError('Tugas tidak ditemukan.');

        if ($task->delete()) {
            return redirect()->route('admin.tasks')->withSuccess('Tugas telah berhasil dibatalkan.');
        } else {
            return redirect()->route('admin.tasks')->withError('Tugas tidak berhasil dibatalkan.');
        }
    }

    public function addComment($taskId)
    {
        $task = Task::find($taskId);
        $data = request()->only(['text']);
        $files = request()->file('lampiran');
        $data['posted_by'] = Auth::user()->id;
        $invalidFiles = false;

        if (!$task)
            return redirect()->back()->withError('Pesan tidak berhasil dikirim.');

        // Save Comment
        $comment = new Comment($data);
        if (!$comment->save()) {
            return redirect()->back()->withError('Pesan tidak berhasil dikirim');
        }

        $task->comments()->attach($comment->id);

        // Save Attachment
        if (count($files)) {
            foreach ($files as $file) {
                $attachment = $this->attachToComment($file);

                if ($attachment['status']) {
                    $comment->attachments()->attach($attachment['file']->id);
                } else {
                    if ($invalidFiles) {
                        $invalidFiles .= $attachment['filename'] . ' tidak berhasil disimpan.';
                    } else {
                        $invalidFiles = $attachment['filename'] . ' tidak berhasil disimpan.';
                    }
                }
            }
        }

        if ($invalidFiles) {
            return redirect()->back()->withError('Pesan berhasil dikirim, tetapi lampiran ' . $invalidFiles);
        }

        return redirect()->back()->withSuccess('Pesan berhasil dikirim.');
    }

    private function attachToComment($file)
    {
        $acceptedFiles = ['jpg', 'jpeg', 'png', 'bmp', 'pdf', 'docx', 'doc', 'ppt', 'pptx', 'zip', 'rar', 'xls', 'xlsx'];

        if (!in_array($file->extension(), $acceptedFiles))
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName
            ];

        $attachment = [
            'title' => $file->getClientOriginalName(),
            'size' => $file->getClientSize(),
            'mime' => $file->getMimeType(),
            'path' => $file->store('uploaded', 'public'),
            'short_url' => uniqid('exp' . Auth::user()->id),
            'owner_id' => Auth::user()->id
        ];

        $attachment = new Attachment($attachment);

        if ($attachment->save()) {
            return [
                'status' => true,
                'file' => $attachment
            ];
        } else {
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName
            ];
        }
    }

    private function attachToTask($file)
    {
        $acceptedFiles = ['jpg', 'jpeg', 'png', 'bmp', 'pdf', 'docx', 'doc', 'ppt', 'pptx', 'zip', 'rar', 'xls', 'xlsx'];

        if (!in_array($file->extension(), $acceptedFiles))
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName
            ];

        $attachment = [
            'title' => $file->getClientOriginalName(),
            'size' => $file->getClientSize(),
            'mime' => $file->getMimeType(),
            'path' => $file->store('uploaded', 'public'),
            'short_url' => uniqid('exp' . Auth::user()->id),
            'owner_id' => Auth::user()->id
        ];

        $attachment = new Attachment($attachment);

        if ($attachment->save()) {
            return [
                'status' => true,
                'file' => $attachment
            ];
        } else {
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName
            ];
        }
    }

}
