<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PKMS\Models\User;
use PKMS\Models\Profile;

class AdminController extends BaseController
{
    public function index()
    {
        $admins = User::where('is_admin', 1)->orderBy('created_at', 'DESC');
        $admins = $admins->paginate();
        
        return view('modules.admin.admins.index', compact('admins'));
    }

    public function create()
    {
        $create = 1;
        return view('modules.admin.admins.create', compact('create'));
    }

    public function store(Request $request)
    {
        $admin             = new User();
        $admin->name       = $request->input('name');
        $admin->username   = $request->input('username');
        $admin->email      = $request->input('email');
        $admin->password   = $request->input('password');
        $admin->is_admin   = 1;
        $admin->is_active  = 1;
        $admin->is_verified= 1;
        $admin->created_by = Auth::id();
        if ($admin->save()) {
            DB::table('t_profiles')->insert(
                [
                    'user_id'   => $admin->id,
                    'gender'    => $request->input('gender'),
                    'birth_place' => $request->input('birth_place'),
                    'birth_date'=> $request->input('birth_date'),
                    'address'   => $request->input('address'),
                    'phone'     => $request->input('phone'),
                    'mobile'    => $request->input('mobile')
                ]
            );
            
            return redirect()->route('admin.admins')->withSuccess('Admin sukses ditambahkan.');
        }

        return redirect()->back()->withError('Data admin gagal disimpan.');
    }

    public function detail($id)
    {
        $admin = User::with(['profile', 'experiences.attachments', 'educations.detail'])->find($id);

        if(!$admin)
            return redirect()->back()->withError('Pengguna tidak ditemukan.');

        return view('modules.admin.admins.detail', compact('admin'));
    }

    public function edit($id)
    {
        $admin = User::findOrFail($id);
        
        if ($admin->is_admin != 1) {
            return redirect()->back()->withError('User is not admin.');
        }
        $create  = 0;
        return view('modules.admin.admins.edit', compact('admin', 'create'));
    }

    public function update(Request $request, $id)
    {
        $admin             = User::findOrFail($id);
        $admin->username   = $request->input('username');
        $admin->name       = $request->input('name');
        
        if ($request->input('password')) {
            $admin->password   = $request->input('password');
        }
        
        $admin->is_admin    = 1;
        $admin->is_verified = $request->input('is_verified');
        $admin->is_active   = $request->input('is_active');
        
        if ($admin->update()) {
            return redirect()->route('admin.admins')->withSuccess('Admin berhasil update.');
        }

        return redirect()->route('admin.admins')->withError('Data admin gagal diupdate.');
    }

    public function delete($userId)
    {
        $user = User::find($userId);
        
        if ($user->hasProfile()) {
            $user->profile()->delete();
        }

        if(!$user)
        {
            return redirect()->route('admin.admins')->withError('Admin tidak ditemukan.');
        }
        
        if ($user->id == Auth::id()) {
            return redirect()->route('admin.admins')->withError('Tidak dapat menghapus data anda sendiri.');
        }

        if(!$user->delete())
        {
            return redirect()->back()->withErrors($user->getErrors())->withError('Admin tidak berhasil dihapus.');
        }

        return redirect()->route('admin.admins')->withSuccess('Admin berhasil dihapus.');
    }
}