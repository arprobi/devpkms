<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PKMS\Models\MasterArticleCode as Code;

class CodeController extends BaseController
{

    public function index()
    {
        $codes = Code::paginate();
        return view('modules.admin.codes.index', compact('codes'));
    }

    public function create()
    {
    	return view('modules.admin.codes.create');
    }

    public function store()
    {
    	$data = request()->only(['name']);
    	$code = new Code($data);

    	if(! $code->save())
    	{
    		return redirect()->back()->withErrors($code->getErrors())->withError('Data kode artikel tidak berhasil disimpan.');
    	}

    	return redirect()->route('admin.codes.index')->withSuccess('Data kode artikel berhasil disimpan.');
    }

    public function edit($id)
    {
    	$code = Code::find($id);

    	if(!$code)
    	{
    		return redirect()->back()->withError('Data kode artikel tidak ditemukan.');
    	}

    	return view('modules.admin.codes.edit', compact('code'));
    }

    public function update($id)
    {
    	$data = request()->only(['name']);
    	$code = Code::find($id);

    	if(!$code)
    	{
    		return redirect()->back()->withError('Data kode artikel tidak ditemukan.');
    	}

    	if(! $code->update($data))
    	{
    		return redirect()->back()->withErrors($code->getErrors())->withError('Data kode artikel tidak berhasil disimpan.');
    	}

    	return redirect()->route('admin.codes.index')->withSuccess('Data kode artikel berhasil disimpan.');
    }

    public function destroy($id)
    {
    	$code = Code::find($id);

    	if(!$code)
    	{
    		return redirect()->back()->withError('Data kode artikel tidak ditemukan.');
    	}

    	if(! $code->delete())
    	{
    		return redirect()->back()->withErrors($code->getErrors())->withError('Data kode artikel tidak berhasil dihapus.');
    	}

    	return redirect()->route('admin.codes.index')->withSuccess('Data kode artikel berhasil dihapus.');
    }
}