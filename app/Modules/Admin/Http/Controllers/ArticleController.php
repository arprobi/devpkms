<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PKMS\Models\Article;
use PKMS\Models\Attachment;

class ArticleController extends BaseController
{

    public function index()
    {
        $articles = Article::with(['tasks','author'])->paginate();
        return view('modules.admin.articles.index', compact('articles'));
    }

    public function view($articleId)
    {
        $article = Article::with('attachments')->find($articleId);

        if(!$article)
        {
            if(request()->ajax())
            {
                abort(404, 'Artikel tidak ditemukan');
            }
            else
            {
                return redirect()->back()->withError('Artikel tidak ditemukan.');
            }
        }

        if(request()->ajax())
        {
            return view('modules.admin.articles._view', compact('article'));
        }
        else
        {
            return view('modules.admin.articles.view', compact('article'));
        }
    }

    public function edit($articleId)
    {
        $article = Article::with('attachments','tasks')->find($articleId);

        if(!$article)
        {
            return redirect()->route('admin.articles')->withError('Artikel tidak ditemukan');
        }


        return view('modules.admin.articles.edit', compact('article'));
    }

    public function update($articleId)
    {
        $article = Article::with('attachments','tasks')->find($articleId);

        if(!$article)
        {
            return redirect()->route('admin.articles')->withError('Artikel tidak ditemukan');
        }

        $data = request()->except('lampiran');
        $files = request()->file('lampiran');
        $invalidFiles = false;

        if($article->update($data))
        {
            // Save Attachment
            if(count($files))
            {
                foreach($files as $file)
                {
                    $attachment = $this->attachToArticle($file);

                    if($attachment['status'])
                    {
                        $article->attachments()->attach($attachment['file']->id);
                    }
                    else
                    {
                        if($invalidFiles)
                        {
                            $invalidFiles .= $attachment['filename'] . ' tidak berhasil disimpan.';
                        }
                        else
                        {
                            $invalidFiles = $attachment['filename'] . ' tidak berhasil disimpan.';
                        }
                    }
                }
            }

            if($invalidFiles)
            {
                return request()->session()->setFlash('error','Artikel berhasil disimpan, tetapi lampiran '.$invalidFiles);
            }

            return redirect()->route('admin.tasks.detail', $article->tasks->first()->id)->withSuccess('Artikel berhasil disimpan.');
        }
        else
        {
            return redirect()->back()->withError('Artikel tidak berhasil disimpan.');
        }
    }

    private function attachToArticle($file)
    {
        $acceptedFiles = ['jpg', 'jpeg', 'png', 'bmp', 'pdf', 'docx', 'doc','ppt','pptx','zip','rar','xls','xlsx'];

        if(! in_array($file->extension(), $acceptedFiles))
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName
            ];

        $attachment = [
            'title' => $file->getClientOriginalName(),
            'size'  => $file->getClientSize(),
            'mime'  => $file->getMimeType(),
            'path'  => $file->store('uploaded', 'public'),
            'short_url' => uniqid('exp'.Auth::user()->id),
            'owner_id' => Auth::user()->id
        ];

        $attachment = new Attachment($attachment);

        if($attachment->save())
        {
            return [
                'status' => true,
                'file' => $attachment
            ];
        }
        else
        {
            return [
                'status' => false,
                'filename' => $file->getClientOriginalName
            ];
        }
    }
}
