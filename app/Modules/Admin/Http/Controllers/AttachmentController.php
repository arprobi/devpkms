<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PKMS\Models\Attachment;

class AttachmentController extends BaseController
{

    public function index() {}

    public function download($short_url)
    {
    	$attachment = Attachment::where('short_url', $short_url)->first();

    	if(!$attachment)
    		return redirect()->back()->withError('Lampiran tidak ditemukan.');

    	return response()->download(public_path('storage/' . $attachment->path), $attachment->title);
    }
    public function delete($short_url)
    {
    	$attachment = Attachment::where('short_url', $short_url)->first();

    	if(!$attachment)
    		return redirect()->back()->withError('Lampiran tidak ditemukan.');

    	if(!Storage::disk('public')->delete($attachment->path))
    		return redirect()->back()->withError('Lampiran tidak berhasil dihapus.');

    	if(!$attachment->delete())
    		return redirect()->back()->withError('Lampiran tidak berhasil dihapus.');

    	return redirect()->back()->withSuccess('Lampiran berhasil dihapus');
    }
}