<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PKMS\Models\Perpres;

class PerpresController extends BaseController
{

    public function index()
    {
        $perpres = Perpres::paginate();

        $query = request()->get('q', false);

        if ($query) {
            $perpres = Perpres::where('nama_perpres', 'like', "%$query%");
            $perpres = $perpres->paginate();
        }


        return view('modules.admin.perpres.index', compact('perpres'));
    }

    public function create()
    {
        return view('modules.admin.perpres.create');
    }

    public function store(Request $request)
    {
        $perpres = new Perpres();

        $perpres->nama_perpres = request()->nama_perpres;

        if ($perpres->save()) {
            return redirect()->route('admin.perpres')->withSuccess('Perpres berhasil di simpan.');
        }

        return redirect()->back()->withError('Perpres tidak berhasil di simpan.');
    }

    public function edit($id)
    {
        $perpres = Perpres::findOrFail($id);

        if ($perpres) {
            return view('modules.admin.perpres.edit', compact('perpres'));
        }

        return redirect()->back()->withError('Perpres tidak ditemukan.');
    }

    public function update($id)
    {
        $perpres = Perpres::findOrFail($id);

        if ($perpres) {
            $perpres->nama_perpres = request()->nama_perpres;
            
            if ($perpres->update()) {
                return redirect()->route('admin.perpres')->withSuccess('Perpres berhasil di simpan.');
            }
            
            return redirect()->back()->withError('Perpres gagal diubah.');
        
        }
        
        return redirect()->back()->withError('Perpres tidak ditemukan.');
    }

    public function delete($id)
    {
        $perpres = Perpres::findOrFail($id);
        if (!$perpres->pasals()->count()) {
            if ($perpres->delete()) {
                return redirect()->back()->withSuccess('Perpres berhasil di hapus.');
            }else{
                return redirect()->back()->withError('Perpres gagal di hapus.');
            }
        }
        
        return redirect()->back()->withError('Gagal, terdapat pasal pada perpres ini.');
    }

}
