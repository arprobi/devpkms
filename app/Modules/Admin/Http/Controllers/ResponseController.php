<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PKMS\Models\ResponseQuestion;
use PKMS\Models\ResponseChoice;

class ResponseController extends BaseController
{

    public function index()
    {
        $responses = ResponseQuestion::paginate();

        $query = request()->get('q', false);

        if ($query) {
            $responses = ResponseQuestion::where('question', 'like', "%$query%");
            $responses = $responses->paginate();
        }


        return view('modules.admin.responses.index', compact('responses'));
    }

    public function create()
    {
        return view('modules.admin.responses.create');
    }

    public function store(Request $request)
    {
        $response = new ResponseQuestion();
        $response->question      = request()->question;
        $response->is_selection  = request()->is_selection;

        if (request()->published) {
            $pool = ResponseQuestion::where('published', 1)->first();
            if ($pool) {
                $pool->published = 0;
                $pool->update();
            }
        }
            
        $response->published     = request()->published;

        if ($response->save()) {
            if (request()->is_selection == 1) {
                for ($i=0; $i < count(request()->choices); $i++) { 
                    $choice                         = new ResponseChoice();
                    $choice->choice                 = request()->choices[$i];
                    $choice->response_question_id   = $response->id;
                    $choice->save();
                }
            }

            return redirect()->route('admin.responses')->withSuccess('Pertanyaan berhasil di simpan.');
        }

        return redirect()->back()->withError('Pertanyaan gagal di simpan.');
    }

    public function edit($id)
    {
        $response = ResponseQuestion::findOrFail($id);

        if ($response) {
            return view('modules.admin.responses.edit', compact('response'));
        }

        return redirect()->back()->withError('Pertanyaan tidak ditemukan.');
    }

    public function update($id)
    {
        $response = ResponseQuestion::findOrFail($id);

        if ($response) {
            $response->question      = request()->question;
            $response->is_selection  = request()->is_selection;
            
            if (request()->published) {
                $pool = ResponseQuestion::where('published', 1)->first();
                if ($pool) {
                    $pool->published = 0;
                    $pool->update();
                }
            }
            
            $response->published     = request()->published;


            if ($response->update()) {
                if (request()->is_selection == 1) {
                    $response->choices()->delete();
                    for ($i=0; $i < count(request()->choices); $i++) { 
                        $choice                         = new ResponseChoice();
                        $choice->choice                 = request()->choices[$i];
                        $choice->response_question_id   = $response->id;
                        $choice->save();
                    }
                } else {
                    $response->choices()->delete();
                }
                return redirect()->route('admin.responses')->withSuccess('Pertanyaan berhasil di simpan.');
            }
            
            return redirect()->back()->withError('Pertanyaan gagal diubah.');
        
        }
        
        return redirect()->back()->withError('Pertanyaan tidak ditemukan.');
    }

    public function show($id)
    {
        $response = ResponseQuestion::findOrFail($id);

        if ($response) {
            return view('modules.admin.responses.show', compact('response'));
        }

        return redirect()->back()->withError('Pertanyaan tidak ditemukan.');
    }

    public function delete($id)
    {
        $response = ResponseQuestion::findOrFail($id);
        
        $response->choices()->delete();
        $response->answers()->delete();

        if ($response->delete()) {
            return redirect()->back()->withSuccess('Pertanyaan berhasil di hapus.');
        }

        return redirect()->back()->withError('Pertanyaan gagal di hapus.');
    }


    public function publish($id)
    {
        $action   = request()->get('action', false);
        $response = ResponseQuestion::findOrFail($id);
        if ($action == 'publish') {
            
            $pool = ResponseQuestion::where('published', 1)->first();
            if ($pool) {
                $pool->published = 0;
                $pool->update();
            }

            $response->published = 1;
            if($response->update()){
                return redirect()->route('admin.responses')->withSuccess('Pertanyaan berhasil di sembunyikan.');
            }else{
                return redirect()->route('admin.responses')->withError('Pertanyaan gagal di sembunyikan.');
            }
        } else {
            $pool = ResponseQuestion::where('published', 1)->first();
            if ($pool) {
                $pool->published = 0;
                $pool->update();
            }
            
            $response->published = 1;
            if($response->update()){
                return redirect()->route('admin.responses')->withSuccess('Pertanyaan berhasil di sembunyikan.');
            }else{
                return redirect()->route('admin.responses')->withError('Pertanyaan gagal di sembunyikan.');
            }
        }
    }

}
