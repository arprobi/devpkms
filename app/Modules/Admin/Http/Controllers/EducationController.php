<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PKMS\Models\MasterEducation as Education;

class EducationController extends BaseController
{

    public function index()
    {
        $educations = Education::paginate();
        return view('modules.admin.educations.index', compact('educations'));
    }

    public function create()
    {
    	return view('modules.admin.educations.create');
    }

    public function store()
    {
    	$data = request()->only(['code','name']);
    	$education = new Education($data);

    	if(! $education->save())
    	{
    		return redirect()->back()->withErrors($education->getErrors())->withError('Data edukasi tidak berhasil disimpan.');
    	}

    	return redirect()->route('admin.educations.index')->withSuccess('Data edukasi berhasil disimpan.');
    }

    public function edit($id)
    {
    	$education = Education::find($id);

    	if(!$education)
    	{
    		return redirect()->back()->withError('Data edukasi tidak ditemukan.');
    	}

    	return view('modules.admin.educations.edit', compact('education'));
    }

    public function update($id)
    {
    	$data = request()->only(['code','name']);
    	$education = Education::find($id);

    	if(!$education)
    	{
    		return redirect()->back()->withError('Data edukasi tidak ditemukan.');
    	}

    	if(! $education->update($data))
    	{
    		return redirect()->back()->withErrors($education->getErrors())->withError('Data edukasi tidak berhasil disimpan.');
    	}

    	return redirect()->route('admin.educations.index')->withSuccess('Data edukasi berhasil disimpan.');
    }

    public function destroy($id)
    {
    	$education = Education::find($id);

    	if(!$education)
    	{
    		return redirect()->back()->withError('Data edukasi tidak ditemukan.');
    	}

    	if(! $education->delete())
    	{
    		return redirect()->back()->withErrors($education->getErrors())->withError('Data edukasi tidak berhasil dihapus.');
    	}

    	return redirect()->route('admin.educations.index')->withSuccess('Data edukasi berhasil dihapus.');
    }
}