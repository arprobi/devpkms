<?php

namespace PKMS\Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PKMS\Models\Faq;
use PKMS\Models\FirebaseMobile;

class FaqController extends BaseController
{

    public function index()
    {
        $faqs = Faq::with('articlecode', 'articlesubcode')
                ->where('published', 1);
        $query = request()->get('q', false);
        if ($query) {
            $faqs = $faqs->where('title', 'like', "%$query%")->orWhere('tags', 'like', "%$query%");
        }
        $faqs = $faqs->orderBy('created_at', 'desc')->paginate();

        return view('modules.admin.faqs.index', compact('faqs'));
    }

    public function unpublish()
    {
        $faqs = Faq::where('published', 0)->orderBy('id', 'desc');
        $query = request()->get('q2', false);
        if ($query) {
            $faqs = $faqs->where('title', 'like', "%$query%")->orWhere('qa', 'like', "%$query%");
        }
        $faqs = $faqs->paginate();
        return view('modules.admin.faqs.unpublish', compact('faqs'));
    }

    public function form()
    {
        return view('modules.admin.faqs.create');
    }

    public function create()
    {
        $data = request()->except(['image', 'materi']);
        $file = request()->file('image');
        $acceptedFiles = ['jpg', 'jpeg', 'png', 'bmp'];
        
        $pdf  = request()->file('materi');
        $acceptedMtri  = ['pdf'];

        if (!request()->input('article_subcode')) {
            return redirect()->back()->withError('Subkodefikasi harus di input!.');
        }

        $faq = new Faq($data);
        $faq->author_id = Auth::id();
        if ($file) {
            if (!in_array($file->extension(), $acceptedFiles)) {
                return redirect()->back()->withError('Tipe file tidak didukung.');
            }

            $faq->image = $file->store('infografis', 'public');
        }

        if ($pdf) {
            if (!in_array($pdf->extension(), $acceptedMtri)) {
                return redirect()->back()->withError('Tipe file tidak didukung.');
            }

            $faq->materi = $pdf->store('materi', 'public');
        }


        if ($faq->save()) {
            if ($faq->published) {
                $type = ['type' => 4, 'detail_id' => $faq->id];
                $this->sendToAll($faq->title, $type);
            }
            return redirect()->route('admin.faqs')->withSuccess('FAQ berhasil di simpan.');
        }

        return redirect()->back()->withError('FAQ tidak berhasil di simpan.');
    }

    public function edit($id)
    {
        $faq = Faq::find($id);

        if (!$faq) {
            return redirect()->back()->withError('FAQ tidak ditemukam');
        }

        return view('modules.admin.faqs.edit', compact('faq'));
    }

    public function update($id)
    {
        $data = request()->except(['image', 'materi']);
        $file = request()->file('image');
        $acceptedFiles = ['jpg', 'jpeg', 'png', 'bmp'];

        $pdf  = request()->file('materi');
        $acceptedMtri  = ['pdf'];

        $faq = Faq::find($id);

        if (!$faq) {
            return redirect()->back()->withError('FAQ tidak ditemukam');
        }

        if ($file) {
            if (!in_array($file->extension(), $acceptedFiles)) {
                return redirect()->back()->withError('Tipe file tidak didukung.');
            }

            if ($faq->image) {
                $imgfile = Storage::disk('public')->url($faq->image);
                @fopen($imgfile, 'w');
                @fclose($imgfile);
            }

            $data['image'] = $file->store('infografis', 'public');
        }

        if ($pdf) {
            if (!in_array($pdf->extension(), $acceptedMtri)) {
                return redirect()->back()->withError('Tipe file tidak didukung.');
            }

            if ($faq->materi) {
                $pdffile = Storage::disk('public')->url($faq->materi);
                @fopen($pdffile, 'w');
                @fclose($pdffile);
            }

            $data['materi'] = $pdf->store('materi', 'public');
        }


        if ($faq->update($data)) {
            return redirect()->back()->withSuccess('FAQ berhasil di update.');
        }

        return redirect()->back()->withError('FAQ tidak berhasil di update.');
    }

    public function delete($id)
    {
        $faq = Faq::find($id);

        if (!$faq) {
            return redirect()->back()->withError('FAQ tidak ditemukam');
        }

        if ($faq->delete()) {
            return redirect()->back()->withSuccess('FAQ berhasil dihapus');
        }

        return redirect()->back()->withError('FAQ tidak berhasil dihapus');
    }

    public function sendToAll($message, $type = [])
    {
        $title  = 'Pengetahuan baru';
        $fcm    = new FirebaseMobile();
        $fcm->sendToAll($title, $message, $type, '');
    }

}
