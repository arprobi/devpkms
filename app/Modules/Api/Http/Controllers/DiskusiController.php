<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use DB;

use PKMS\Models\Answer;
use PKMS\Models\FcmToken;
use PKMS\Models\FirebaseMobile;
use PKMS\Models\Question;
use PKMS\Models\User;

class DiskusiController extends BaseController
{
	/**
	 * get Pasal
	 * @return Illuminate\Support\Collection
	 */
	public function getAll()
	{
		$offset = request()->get('offset', false) ? request()->get('offset', false) : 0;
		$limit 	= request()->get('limit', false) ? request()->get('limit', false) : 10;
		$offset = ($offset-1) * $limit;
		$data 	= Question::select('id', 'name', 'email', 'title', 'created_at')
					->offset($offset)
					->limit($limit)
					->orderBy('created_at', 'DESC')
					->get();

		if ($data->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data->take($limit)
			]);
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> []
			]);
		}

		return $result;
		
	}

	public function getByUser()
	{
		$user   = Auth::user();

		$offset = request()->get('offset', false) ? request()->get('offset', false) : 0;
		$limit 	= request()->get('limit', false) ? request()->get('limit', false) : 10;
		$offset = ($offset-1) * $limit;

		$data 	= Question::select('id', 'name', 'email', 'title', 'created_at')
					->where('email', $user->email)
					->orderBy('created_at', 'DESC')
					->offset($offset)
					->limit($limit)
					->get();

		$result = Response::json([
			'status'	=> 200,
			'message'	=> 'success',
			'data'		=> $data
		]);	

		return $result;
	}

	public function detail($id)
	{
		$data = Question::with('answers')->find($id);

		if ($data) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		}else{
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> []
			]);
		}

		return $result;
	}

	public function postDiskusi(Request $request)
	{
		if (Auth::user()) {
			$data = new Question();
			$data->name 	= Auth::user()->name;
			$data->email 	= Auth::user()->email;
			$data->title 	= request()->title;
			$data->content 	= request()->content;

			if ($data->save()) {
				// mention user in comment
				try {
					$type 		= ['type' => 1, 'detail_id' => $data->id];
					$userid 	= request()->userid;
					// send notif 
					$token		= FcmToken::whereIn('user_id', $userid)->where('status', 1)->pluck('token')->toArray();
					$title 		= Auth::user()->name. ' menyebut anda dalam diskusi';
					$message 	= request()->content;
					if (count($token)) {
						$fcm    = new FirebaseMobile();
						$fcm->sendToDevice($token, $title, $message, $userid, $type);
					}
				} catch (\Throwable $th) {
					//throw $th;
				}

				return $result = Response::json([
					'status'	=> 200,
					'message'	=> 'success',
					'data'		=> $data
				]);
			} else {
				return $result = Response::json([
					'status'	=> 500,
					'message'	=> 'error',
					'data'		=> []
				]);
			}
		}else {
			return $result = Response::json([
				'status'	=> 500,
				'message'	=> 'unauthorize',
				'data'		=> 'Silahkan login terlebih dahulu'
			]);
		}
	}
	public function jawabDiskusi($id)
	{
		$quest= Question::findOrFail($id);
		
		$type = ['type' => 1, 'detail_id' => $quest->id];

		if (Auth::user()) {
			$data = new Answer();
			$data->name 		= Auth::user()->name;
			$data->question_id 	= $quest->id;
			$data->email 		= Auth::user()->email;
			$data->content 		= request()->content;

			if ($data->save()) {
				// mention user in comment
				try {
					$userid 	= request()->userid;
					// send notif 
					$token		= FcmToken::whereIn('user_id', $userid)->where('status', 1)->pluck('token')->toArray();
					$title 		= Auth::user()->name. ' menyebut anda dalam diskusi';
					$message 	= request()->content;
					if (count($token)) {
						$fcm    = new FirebaseMobile();
						$fcm->sendToDevice($token, $title, $message, $userid, $type);
					}
				} catch (\Throwable $th) {
					//throw $th;
				}
				
				$this->sendNotification($quest, Auth::user()->name, request()->content, Auth::user()->email, $type);
				
				return $result = Response::json([
					'status'	=> 200,
					'message'	=> 'success',
					'data'		=> $data
				]);
			} else {
				return $result = Response::json([
					'status'	=> 500,
					'message'	=> 'error',
					'data'		=> []
				]);
			}
		}else {
			return $result = Response::json([
				'status'	=> 500,
				'message'	=> 'unauthorize',
				'data'		=> 'Silahkan login terlebih dahulu'
			]);
		}
	}

	public function getResponse($question_id)
	{
		$data = Answer::where('question_id', $question_id)->get();

		return $result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]); 
	}

	public function sendNotification($question, $user_name, $message, $except_mail, $type = [])
	{
		$email 	= $question->answers()->distinct('email')->pluck('email')->toArray();
		
		$user   = User::whereIn('email', $email)->whereNotIn('email', [$except_mail])->pluck('id')->toArray();

		$token  = FcmToken::whereIn('user_id', $user)->where('status', 1)->pluck('token')->toArray();
		
		$title 	= $user_name.' membalas diskusi';
		
		if (count($token)) {
			$fcm    = new FirebaseMobile();
			$fcm->sendToDevice($token, $title, $message, $user, $type);
		}

		return true;
	}
}