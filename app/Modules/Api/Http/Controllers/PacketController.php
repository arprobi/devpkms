<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PKMS\Models\Packet;

class PacketController extends BaseController
{

    public function getOffer($packetId, $userId)
    {
        $packet = Packet::find($packetId);
        $offers = [];
        
        $offer = $packet->offers()->where('user_id', $userId)->first();
        $offers[(string) $offer->offer] = "Rp." . number_format($offer->offer, 0, ",", ".");

        $negotiations = $packet->negotiations()->where('user_id', $userId)->get();

        if (isset($negotiations)) {
            foreach ($negotiations as $nego) {
                $offers[(string) $nego->pivot->offer] = "Rp." . number_format($nego->pivot->offer, 0, ",", ".");
            }
        }

        ksort($offers, SORT_NUMERIC);
        return $offers;
    }

}
