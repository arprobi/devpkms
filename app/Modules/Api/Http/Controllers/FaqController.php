<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use PKMS\Models\Faq;
use PKMS\Models\Komment;

class FaqController extends BaseController
{
	/**
	 * get all knowledge
	 * @return Illuminate\Support\Collection
	 */
	public function allFaq()
	{
		$offset 	= request()->get('offset', false) ? request()->get('offset', false) : 0;
		$limit 		= request()->get('limit', false) ? request()->get('limit', false) : 10;
		$terbaru 	= request()->get('terbaru', false) ? 1 : 0;

		$offset 	= ($offset-1) * $limit;

		$data = Faq::select('id', 'klasifikasi', 'title', 'faq', 'qa', 'answer', 'content_letter', 'video', 'materi', 'image', 'updated_at', 'viewer')
		->where('published', 1)
		->offset($offset)
		->limit($limit);

		if ($terbaru) {
			$data = $data->orderBy('updated_at', 'DESC')->get();
		}else {
			$data = $data->orderBy('viewer', 'DESC')->get();
		}

		if ($data->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> []
			]);
		}

		return $result;
		
	}
	
	/**
	 * get detail knowledge by id
	 * @return Illuminate\Support\Collection
	 */
	public function byId($id)
	{
		$data = Faq::with('author', 'articlecode', 'articlesubcode', 'comments')->findOrFail($id);

		if ($data->count()) {
			
			$data->viewer = $data->viewer + 1;
			$data->timestamps = false;
			$data->save();
			
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> []
			]);
		}

		return $result;
		
	}

	/**
	 * get all knowledge by code
	 * @return Illuminate\Support\Collection
	 */
	public function getByCode($id)
	{
		$offset = request()->get('offset', false) ? request()->get('offset', false) : 0;
		$limit = request()->get('limit', false) ? request()->get('limit', false) : 10;

		$offset 	= ($offset-1) * $limit;
		
		$data = Faq::select('id', 'klasifikasi', 'title', 'faq', 'qa', 'answer', 'content_letter', 'video', 'materi', 'image', 'updated_at', 'viewer')
		->where('article_code', $id)
		->where('published', 1)
		->offset($offset)
		->limit($limit)
		->orderBy('viewer', 'DESC')
		->get();

		if ($data->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> []
			]);
		}

		return $result;
		
	}

	/**
	 * get all knowledge by subcode
	 * @return Illuminate\Support\Collection
	 */
	public function getBySubcode($id)
	{
		$offset = request()->get('offset', false) ? request()->get('offset', false) : 0;
		$limit = request()->get('limit', false) ? request()->get('limit', false) : 10;

		$offset 	= ($offset-1) * $limit;

		$data = Faq::select('id', 'klasifikasi', 'title', 'faq', 'qa', 'answer', 'content_letter', 'video', 'materi', 'image', 'updated_at', 'viewer')
		->where('article_subcode', $id)
		->where('published', 1)
		->offset($offset)
		->limit($limit)
		->orderBy('viewer', 'DESC')
		->get();

		if ($data->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> []
			]);
		}

		return $result;
		
	}

	/**
	 * get all knowledge comment by id
	 * @return Illuminate\Support\Collection
	 */
	public function getKomentar($id)
	{
		$offset = request()->get('offset', false) ? request()->get('offset', false) : 0;
		$limit = request()->get('limit', false) ? request()->get('limit', false) : 10;

		$offset 	= ($offset-1) * $limit;

		$data = Komment::where('faq_id', $id)
				->offset($offset)
				->limit($limit)
				->orderBy('created_at', 'DESC')
				->get();
				
		if ($data->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> []
			]);
		}

		return $result;
	}
	
}