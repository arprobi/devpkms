<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use PKMS\Models\Highlight;
use PKMS\Models\HighlightComment;
use PKMS\Models\Pasal;

class HighlightController extends BaseController
{
	/**
	 * get all highlight pasal
	 * @return Illuminate\Support\Collection
	 */
	public function allHighlight(Request $request)
	{
		$offset 	= request()->get('offset', false) ? request()->get('offset', false) : 0;
		$limit 		= request()->get('limit', false) ? request()->get('limit', false) : 10;
		$terbaru 	= request()->get('terbaru', false) ? 1 : 0;

		$highlight = Highlight::offset($offset)->limit($limit)->orderBy('created_at', 'DESC')->get();

		return Response::json([
			'status'	=> 200,
			'message'	=> 'success',
			'data'		=> $highlight
		]);
	}

	/**
	 * post highlight pasal
	 * @return Illuminate\Support\Collection
	 */
	public function saveHighlight(Request $request)
	{
		$user  = Auth::user();
		
		// Request
		$pasal_id		= request()->pasal_id;
		$content		= request()->content;
		$highlight_start= request()->highlight_start;
		$highlight_end	= request()->highlight_end;
		$index_start	= request()->index_start;
		$index_end		= request()->index_end;


		// Process
		if (!$pasal = Pasal::findOrFail(request()->pasal_id)) {
			return Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> 'Pasal not found'
			]);
		} else {
			// check if keyword highlighted exist
			$chek_start = $this->checkHighlighted($highlight_start, $pasal->isi_pasal);
			$chek_end 	= $this->checkHighlighted($highlight_end, $pasal->isi_pasal);

			if ($chek_start && $chek_end) {
				// if exist save highlight
				$new_text 		= $this->getHighlightedPasal($highlight_start, $highlight_end, $index_start, $index_end, $pasal->highlighted);
				
				//inpt highlight	
				$hgt 			= new Highlight();
				$hgt->user_id 	= $user->id;
				$hgt->pasal_id 	= $pasal->id;
				$hgt->content 	= $content;
				$hgt->words 	= 'Sementara dulu';

				$newestText 		= $this->getHighlightedString($new_text, $pasal->id, 200);
				
				if ($hgt->save()) {
					$newestText 		= $this->getHighlightedString($new_text, $pasal->id, $hgt->id);
					// edit words with newest string
					$edthght 			= Highlight::findOrFail($hgt->id);
					$edthght->words 	= $newestText[0];
					$edthght->save();
					//update highlighted pasal attr
					$pasal->highlighted = $newestText[1];

					if ($pasal->save()) {
						return Response::json([
							'status'	=> 200,
							'message'	=> 'success',
							'data'		=> $pasal
						]);
					} else {
						Highlight::destroy($hgt->id);
						return Response::json([
							'status'	=> 500,
							'message'	=> 'error',
							'data'		=> 'Gagal menyimpan highlight!'
						]);
					}
				}

			}else{
				// if not exist
				return Response::json([
					'status'	=> 500,
					'message'	=> 'error',
					'data'		=> 'Tidak ada kata yang cocok!'
				]);
			}
		}
		
	}

	/**
	 * get detail highlight pasal by id
	 * @return Illuminate\Support\Collection
	 */
	public function detailHighlight($id='')
	{
		$highlight          = Highlight::findOrFail($id);
        $pasal              = Pasal::findOrFail($highlight->pasal_id);
        $highlight_tag      = '<highlighted hgltid="'. $highlight->id .'" pslid="'. $pasal->id .'">';
        
        $cut_start          = preg_split('#'.$highlight_tag.'#', $pasal->highlighted);
        $cut_end            = preg_split('#</highlighted>#', $cut_start[1]);

        $changed_words      = $highlight_tag.$cut_end[0].'</highlighted>';

        if ($changed_words != $highlight->words) {
            $highlight->words = $changed_words;
            $highlight->save();
        }
        
		$highlight = Highlight::where('id', $id)->with(['comments' => function($query) {
			$query->with('user')->get();
		}])->get();
		
		if ($highlight->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $highlight
			]);
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> 'Data not found'
			]);
		}

		return $result;
	}

	/**
	 * post comment highlight pasal
	 * @return Illuminate\Support\Collection
	 */
	public function commentSave()
	{
		$user  = Auth::user();
		$hight = Highlight::where('id', request()->highlight_id)->get();

		if (!$hight->count()) {
			return $result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> 'Highlight not found'
			]);
		}

		$comment 				= new HighlightComment();
		$comment->highlight_id 	= request()->highlight_id;
		$comment->user_id	 	= $user->id;
		$comment->comment	 	= request()->comment;

		if ($comment->save()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $comment
			]);
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error'
			]);
		}

		return $result;
	}

	/**
	 * get detail comment highlight pasal
	 * @return Illuminate\Support\Collection
	 */
	public function listComment($id='')
	{
		$comment = HighlightComment::where('highlight_id', $id)->get();
		
		if ($comment->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $comment
			]);
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> 'Data not found'
			]);
		}

		return $result;
	}

	/**
	 * delete comment highlight pasal
	 * @return Illuminate\Support\Collection
	 */
	public function deleteComment($id='')
	{
		$comment = HighlightComment::where('id', $id)->get();

		if ($comment->count()) {

			$delcomment = HighlightComment::findOrFail($id);

			if ($delcomment->user_id == Auth::user()->id) {
				if ($delcomment->delete()) {
					return $result = Response::json([
						'status'	=> 200,
						'message'	=> 'success',
						'data'		=> 'Comment deleted!'
					]);
				} else {
					return $result = Response::json([
						'status'	=> 500,
						'message'	=> 'error',
						'data'		=> 'Something wrong!'
					]);
				}
				
			}
			return $result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> 'Not allowed'
			]);
		} else {
			return $result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> 'Data not found'
			]);
		}

	}


	/**
	 * get list highlight by pasal id
	 * @return Illuminate\Support\Collection
	 */
	public function highlightPasal($id='')
	{
		$highlight = Highlight::where('pasal_id', $id)->get();
		
		if ($highlight) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $highlight
			]);
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> 'Data not found'
			]);
		}

		return $result;
	}


	// Check if keyword exist
	public function checkHighlighted($highlighted, $isi_pasal)
	{
		if (strpos($isi_pasal, $highlighted) === false) {
			return false;
		} else {
			return true;
		}
	}

	// Get highlighted for pasal highlighted attribute
	public function getHighlightedPasal($highlight_start, $highlight_end, $index_start, $index_end, $isi_pasal)
	{
		$start_sentences= '';
		$cut_start 		= preg_split('#'.$highlight_start.'#', $isi_pasal);
		$sisip_start	= '<temphighlight><span style="background-color: #b3ecff;">';
		$sisip_end		= '</span></temphighlight>';
		
		// sisipkan untuk start highlight (kata pertama)
		for ($i=0; $i < count($cut_start); $i++) { 
			if ($i == $index_start) {
				$start_sentences .= $cut_start[$i].$sisip_start.$highlight_start;
			}else{
				if (($i+1) < count($cut_start) ) {
					$start_sentences .= $cut_start[$i].$highlight_start;
				}else{
					$start_sentences .= $cut_start[$i];
				}
			}
		}

		$cut_end = preg_split('#'.$highlight_end.'#', $start_sentences);
		
		// sisipkan untuk end highlight (kata terakhir)
		$end_sentences = '';
		for ($i=0; $i < count($cut_end); $i++) { 
			if ($i == $index_end) {
				$end_sentences .= $cut_end[$i].$highlight_end.$sisip_end;
			}else{
				if (($i+1) < count($cut_end) ) {
					$end_sentences .= $cut_end[$i].$highlight_end;
				}else{
					$end_sentences .= $cut_end[$i];
				}
			}
		}

		return $end_sentences;
	}

	// Get highlighted string
	public function getHighlightedString($text, $pasal_id, $highlight_id)
	{
		$start_tag		= str_replace('<temphighlight>', '<highlighted hgltid="'.$highlight_id.'" pslid="'.$pasal_id.'">', $text);
		$end_tag		= str_replace('</temphighlight>', '</highlighted>', $start_tag);

		$pattern 		= '/<highlighted hgltid="'.$highlight_id.'" pslid="'.$pasal_id.'">(.*?)<\/highlighted>/';
		$filter		 	= preg_match($pattern, $end_tag, $matches);
		
		$result = [$matches[0], $end_tag];
		
		return $result;
	}



	// Dashboard
	public function loadMore($id)
	{
		$data = HighlightComment::where('highlight_id', $id)
								->limit(5)
								->offset(request()->offset)
								->with('user')
								->get();
		if ($data) {
			return $data;
		}else{
			return 0;
		}
	}
	
}