<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PKMS\Models\Province;
use PKMS\Models\City;
use PKMS\Models\District;
use PKMS\Models\Village;
use PKMS\Models\Faq;
use PKMS\Models\MasterArticleCode;
use PKMS\Models\MasterArticleSubcode;
use Terbilang;

class DataController extends BaseController
{

    /**
     * Get occupations filter
     * @return array
     */
    public function occupations()
    {
        $occupations = DB::table('t_experiences')->distinct();
        $name = request()->get('name', false);

        if ($name) {
            $occupations = $occupations->where('position', 'like', '%' . $name . '%');
        }

        $occupations = $occupations->get(['position']);

        return ($occupations->count()) ? $occupations->pluck(['position']) : [];
    }

    /**
     * Get interests filter
     * @return array
     */
    public function interests()
    {
        $interest = DB::table('m_interests')->distinct();
        $name = request()->get('name', false);

        if ($name) {
            $interest = $interest->where('name', 'like', '%' . $name . '%');
        }

        $interest = $interest->get(['name']);

        return ($interest->count()) ? $interest->pluck(['name']) : [];
    }

    /**
     * Get hobbies filter
     * @return array
     */
    public function hobbies()
    {
        $interest = DB::table('t_profiles')->distinct();
        $name = request()->get('name', false);

        if ($name) {
            $interest = $interest->where('hobby', 'like', '%' . $name . '%');
        }

        $interest = $interest->get(['hobby']);

        $hobbies = ($interest->count()) ? $interest->pluck(['hobby']) : [];
        $temp = [];

        foreach ($hobbies as $hobby) {
            $temp = array_merge(explode(',', $hobby), $temp);
        }

        return array_values(array_unique($temp));

        // return ($interest->count()) ? $interest->pluck(['hobby']) : [];
    }

    /**
     * get article code
     * @return Illuminate\Support\Collection
     */
    public function getArticleCode()
    {
        return MasterArticleCode::select(['id', 'name'])->get();
    }

    /**
     * get article sub code
     * @return Illuminate\Support\Collection
     */
    public function getArticleSubCode($id)
    {
        return MasterArticleSubcode::select(['id', 'name'])->where('code', $id)->get();
    }

    /**
     * Get city by province id
     * @param  $id
     * @return Illuminate\Support\Collection
     */
    public function getCityByProvinceId($id)
    {
        return City::where('province_id', $id)->get();
    }

    /**
     * Get district by city id
     * @param  $id
     * @return Illuminate\Support\Collection
     */
    public function getDistrictByCityId($id)
    {
        return District::where('city_id', $id)->get();
    }

    /**
     * Get village by district id
     * @param  $id
     * @return Illuminate\Support\Collection
     */
    public function getVillageByDistrictId($id)
    {
        return Village::where('district_id', $id)->get();
    }

    /**
     * Get all province or single by province id
     * @param  $id
     * @return Illuminate\Support\Collection
     */
    public function provinces($id = null)
    {
        if (!$id) {
            return Province::with('cities')->get(['code', 'name']);
        } else {
            return Province::with('cities')->where('code', $id)->first();
        }
    }

    /**
     * Get all city or single by city id
     * @param  $id
     * @return Illuminate\Support\Collection
     */
    public function cities($id = null)
    {
        if (!$id) {
            return City::with('districts')->get(['code', 'name']);
        } else {
            return City::with('districts')->where('code', $id)->first();
        }
    }

    /**
     * Get all district or single by district id
     * @param  $id
     * @return Illuminate\Support\Collection
     */
    public function districts($id = null)
    {
        if (!$id) {
            return District::with('villages')->get(['code', 'name']);
        } else {
            return District::with('villages')->where('code', $id)->first();
        }
    }

    /**
     * Get all village or single by village id
     * @param  $id
     * @return Illuminate\Support\Collection
     */
    public function villages($id = null)
    {
        if (!$id) {
            return Village::get(['code', 'name']);
        } else {
            return Village::where('code', $id)->first();
        }
    }

    public function faq(Request $request)
    {
        $qa = $request->input('qa');
        $konsultasi_id = $request->input('konsultasi_id', null);
        if (!$qa) {
            return "false";
        }
        if ($konsultasi_id) {
            $faq = Faq::where('konsultasi_id', $konsultasi_id)->first();
            if ($faq) {
                return $faq;
            }
        }
        $faq = Faq::create([
                    'konsultasi_id' => $konsultasi_id,
                    'qa' => $request->input('qa'),
                    'author_id' => 1,
                    'published' => 0
        ]);
        return $faq;
    }

    public function terbilang($number)
    {
        $number = str_replace(".", "", $number);
        return Terbilang::make($number, ' rupiah');
    }

}
