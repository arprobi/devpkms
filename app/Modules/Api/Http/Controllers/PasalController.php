<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use PKMS\Models\Pasal;
use PKMS\Models\Perpres;
use PKMS\Models\Kategori;

class PasalController extends BaseController
{
	/**
	 * get Pasal
	 * @return Illuminate\Support\Collection
	 */

	public function allPasal()
	{
		$offset = request()->get('offset', false) ? request()->get('offset', false) : 0;
		$limit = request()->get('limit', false) ? request()->get('limit', false) : 10;
		$offset = ($offset-1) * $limit;

		$data = Pasal::with('perpres', 'categories')
		->orderBy('perpres_id')
		->orderBy('order')
		->offset($offset)
		->limit($limit)
		->get();

		if ($data->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> []
			]);
		}

		return $result;
		
	}

	public function getPasal($pasalId = '')
	{
		$data = Pasal::with('perpres', 'categories')->where('id', $pasalId)->get();

		if ($data->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> ['message' => 'Data not found']
			]);
		}

		return $result;
		
	}

	public function getPasalByCategory($kategoriId='')
	{
		$data = Kategori::with(['pasals' => function($query){
			$offset = request()->get('offset', false) ? request()->get('offset', false) : 0;
			$limit = request()->get('limit', false) ? request()->get('limit', false) : 10;
			
			$query->with('perpres')
			->orderBy('order')
			->offset($offset)
			->limit($limit)
			->get();
		}])
		->where('id', $kategoriId)
		->get();

		if ($data->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> []
			]);
		}

		return $result;
	}
}