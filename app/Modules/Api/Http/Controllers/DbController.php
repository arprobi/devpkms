<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Response;

use PKMS\Models\Pasal;
use PKMS\Models\Highlight;

class DbController extends BaseController
{

    /**
     * Get db view
     * @return array
     */
    public function viewData()
    {
        $table  = request()->table;
        $func   = request()->func;

        $data   = DB::table($table)->{$func}();

        $result = Response::json([
            'status'    => 200,
            'message'   => 'success',
            'data'      => $data
        ]);

        return $result;
    }

    public function generate()
    {
        $generate   = request()->generate;
        $reset      = request()->reset ? true : false;
        $jumlah = 0;
        if ($generate == 'highlight_pasal' && !$reset) {
            $pasals = Pasal::all();
            foreach ($pasals as $pasal) {
                $psl = Pasal::findOrFail($pasal->id);
                $psl->highlighted = $psl->highlighted ? $psl->highlighted : $psl->isi_pasal;
                if ($psl->save()) {
                    $jumlah = $jumlah+1;
                }
            }

            return ['reset' => 'false', 'sukses' => $jumlah];
        }

        if ($generate == 'highlight_pasal' && $reset) {
            $pasals = Pasal::all();
            foreach ($pasals as $pasal) {
                $psl = Pasal::findOrFail($pasal->id);
                $psl->highlighted = $psl->isi_pasal;
                if ($psl->save()) {
                    $jumlah = $jumlah+1;
                }
            }

            return ['reset' => 'true', 'sukses' => $jumlah];
        }
    }

    public function remove()
    {
        $highlights = Highlight::select('id')->get();
        $total = 0;
        foreach($highlights as $highlight){
            $new        = Highlight::findOrFail($highlight->id);
            $new->words = str_replace('""', '"', $highlight->words);
            $total = $new->save() ? $total+1 : $total;
        }

        if ($total == $highlights->count()) {
            return 'success';
        } else {
            return 'ngga sukses';
        }
        
    }

}
