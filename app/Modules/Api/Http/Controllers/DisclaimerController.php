<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use File;
use Response;
use PKMS\Models\Disclaimer;

class DisclaimerController extends BaseController
{
	/**
	 * get Disclaimer by user
	 * @return Illuminate\Support\Collection
	 */
	public function view()
	{
		$data = Disclaimer::first();

		if ($data) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> []
			]);
		}

		return $result;
		
	}

	public function unduh()
	{
		$data = Disclaimer::first();
		return response()->download(storage_path("app/public/{$data->content}"));
	}
}