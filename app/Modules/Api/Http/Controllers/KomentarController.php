<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use PKMS\Models\Komment;
use PKMS\Models\FcmToken;
use PKMS\Models\FirebaseMobile;
use PKMS\Models\User;

class KomentarController extends BaseController
{
	/**
	 * get post komentar user
	 * @return Illuminate\Support\Collection
	 */
	public function store(Request $request)
	{
		$data 			= new Komment();
		$data->name 	= Auth::user()->name;
		$data->email 	= Auth::user()->email;
		$data->faq_id 	= request()->faq_id;
		$data->content 	= request()->content;
		
		// mention user in comment
		try {
			$type 		= ['type' => 2, 'detail_id' => request()->faq_id];
			$userid 	= request()->userid;
			// send notif 
			$token		= FcmToken::whereIn('user_id', $userid)->where('status', 1)->pluck('token')->toArray();
			$title 		= Auth::user()->name. ' menyebut anda dalam diskusi';
			$message 	= request()->content;
			if (count($token)) {
				$fcm    = new FirebaseMobile();
				$fcm->sendToDevice($token, $title, $message, $userid, $type);
			}
		} catch (\Throwable $th) {
			//throw $th;
		}
		
		$title 			= Auth::user()->name . ' menambahkan komentar';
		$type		 	= ['type' => 2, 'detail_id' => request()->faq_id];
		$this->sendNotification(request()->faq_id, $title, request()->content, Auth::user()->email, $type);

		if ($data->save()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> $data
			]);
		}

		return $result;
		
	}

	public function delete($id='')
	{
		$komentar = Komment::findOrFail($id);
		
		if (Auth::user()->email == $komentar->email) {
			if ($komentar->delete()) {
				$result = Response::json([
					'status'	=> 200,
					'message'	=> 'success',
					'data'		=> 'Comment deleted!'
				]);
			}else{
				$result = Response::json([
					'status'	=> 500,
					'message'	=> 'error',
					'data'		=> 'Internal service error!'
				]);
			}
		} else {
			$result = Response::json([
				'status'	=> 402,
				'message'	=> 'error',
				'data'		=> 'Not allowed!'
			]);
		}

		return $result;
	}

	public function sendNotification($faq_id, $title, $message, $except_user, $type = [])
	{
		$komentar 	= Komment::where('faq_id', $faq_id)->distinct('email')->pluck('email')->toArray();
		$user 		= User::whereIn('email', $komentar)->whereNotIn('email', [$except_user])->pluck('id')->toArray();
		$token		= FcmToken::whereIn('user_id', $user)->where('status', 1)->pluck('token')->toArray();

		if (count($token)) {
			$fcm    = new FirebaseMobile();
			$fcm->sendToDevice($token, $title, $message, $user, $type);
		}

		return true;
	}
	
}