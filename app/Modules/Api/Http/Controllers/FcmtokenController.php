<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Response;
use PKMS\Models\FcmToken;
use PKMS\Models\FirebaseMobile;
use PKMS\Models\User;

class FcmtokenController extends BaseController
{
	/**
	 * post user token
	 * @return Illuminate\Support\Collection
	 */
	public function store(Request $request)
	{
		$user = Auth::user();
		
		$fcm = FcmToken::firstOrNew(['user_id' => $user->id]);
		$fcm->token 	= request()->token;
		
		if ($fcm->save()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $fcm
			]);
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> []
			]);
		}

		return $result;
		
	}

	public function all()
	{
		$token = FcmToken::with('user')->get();
		return $token;
	}

	public function testing()
	{
		$data = DB::table('pkms_firebase_mobiles')
			->get();		
        return $data;
	}
	
}