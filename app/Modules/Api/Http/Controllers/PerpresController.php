<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use PKMS\Models\Perpres;
use PKMS\Models\Pasal;
use PKMS\Models\Kategori;

class PerpresController extends BaseController
{
	/**
	 * get Pasal
	 * @return Illuminate\Support\Collection
	 */
	public function getPasal($perpresId = '')
	{
		$offset = request()->get('offset', false) ? request()->get('offset', false) : 0;
		$limit 	= request()->get('limit', false) ? request()->get('limit', false) : 10;
		$offset = ($offset-1) * $limit;

		$data = Pasal::where('perpres_id', $perpresId)
				// ->limit($limit)
				// ->offset($offset)
				->orderBy('order', 'ASC')
				->get();

		if ($data->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> []
			]);
		}

		return $result;
		
	}

	/**
	 * get all perpres
	 * @return Illuminate\Support\Collection
	 */
	public function allPerpres()
	{
		$data = Perpres::all();

		if ($data->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> []
			]);
		}

		return $result;
		
	}

	/**
	 * get pasal by perpres for admin module
	 * @return Illuminate\Support\Collection
	 */
	public function listPasal($perpres_id)
	{
		$data = Pasal::where('perpres_id', $perpres_id)->select('id', 'nama_pasal', 'order')->orderBy('order', 'ASC')->get();

		return $data;
		
	}
}