<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PKMS\Models\User;
use PKMS\Models\Profile;
use PKMS\Models\SmsOTP;
use PKMS\Lib\Api;
use Validator;

use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;

class UserController extends BaseController
{

    public function login(Request $request)
    {
        if ($request->query('phone')) {
            $api        = new Api(env('SSO_API_SERVER'), env('SSO_API_KEY'));
            $validator  = Validator::make($request->all(), [
                'phonenumber' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->errors()], 422);
            }
            $profile = Profile::where('mobile', $request->input('phonenumber'))->first();
            if (!$profile) {
                $phone = substr_replace($request->input('phonenumber'), '+62', 0, 1);
                $profile = Profile::where('mobile', $phone)->first();
                if (!$profile) {
                    return response()->json(['status' => 403, 'message' => 'User not found with this phone number']);
                }
            }
            
            if (substr($profile->mobile, 0, 2) == '+6') {
                $phone = substr_replace($profile->mobile, '0', 0, 3);
            } else {
                $phone = $profile->mobile;
            }

            $otp            = new SmsOtp();
            $otp->user_id   = $profile->user_id;
            $otp->number    = $phone;
            $otp->otp_code  = substr(str_shuffle("0123456789"), 0, 5);
            $otp->sent_at   = Carbon::now()->format('Y-m-d H:i');
            $otp->expired_at= date('Y-m-d H:i',strtotime('+10 minutes',strtotime($otp->sent_at)));
            $otp->status    = 1;
            $message        = 'Kode OTP anda adalah '. $otp->otp_code . '. Harap tidak memberitahukan kepada seiapapun kode berikut.';

            if($otp->save()) {
                $sendotp = $this->sendSMS($phone, $message);
                return response()->json(['status' => 200, 'message' => 'Otp has sent to your number '.$otp->number]);
            }
            return response()->json(['status' => 500, 'message' => 'Oops, failed to send OTP, something went wrong']);

        } else {
            $api = new Api(env('SSO_API_SERVER'), env('SSO_API_KEY'));
            $validator = Validator::make($request->all(), [
                        'email' => 'required|email',
                        'password' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'message' => $validator->errors()], 422);
            }
            
            $useractive = User::where('email', request('email'))->get()->count();
            
            if ($useractive > 0) {
                if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
                    $userLogin  = Auth::user();
                    $token      = $userLogin->createToken('PKMSLKPP')->accessToken;
                    return response()->json(['status' => 'success', 'user' => $userLogin, 'token' => $token, 'from' => 'PKMS']);
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Email and Password not match', 'from' => 'PKMS' ], 409);
                }
            } else {
                try {
                    $loginPortal = $api->login(request('email'), request('password'));
                    $authUser   = $this->findOrCreateUser($loginPortal);
                    Auth::login($authUser, true);
                    $userLogin  = Auth::user();
                    $token      = $userLogin->createToken('PKMSLKPP')->accessToken;
                    return response()->json(['status' => 'success', 'user' => $userLogin, 'token' => $token, 'from' => 'Portal Deputi']);
                } catch (\Throwable $th) {
                    return response()->json(['status' => 'error', 'message' => 'Internal server error', 'from' => 'Portal Deputi'], 500);
                }
                
            }
        }
        
    }

    private function findOrCreateUser($userPortal)
    {
        $saveUser = User::where('email', '=', $userPortal->email)
                ->first();
        if (!$saveUser) {
            $saveUser = User::create([
                        'name' => $userPortal->name,
                        'username' => $userPortal->username,
                        'email' => $userPortal->email,
                        'password' => $userPortal->username,
                        'is_active' => 1
            ]);
            $saveUser->profile()->insert([
                'user_id' => $saveUser->id,
                // 'birth_place' => $userPortal->birthplace,
                // 'birth_date' => $userPortal->birthday,
                'mobile' => $userPortal->phone
            ]);
        }
        return $saveUser;
    }

    public function authOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'otp_code' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()], 422);
        }

        $otp = SmsOTP::where('number', $request->input('phone'))->where('otp_code', $request->input('otp_code'))->first();
        if ($otp) {
            $expiredAt  = Carbon::parse($otp->expired_at);
            $now        = Carbon::now();
            if ($now > $expiredAt) {
                return response()->json(['status' => 500, 'message' => 'Your otp is expired!']);
            }
            $authUser = User::findOrFail($otp->user_id);
            Auth::login($authUser, true);
            $userLogin = Auth::user();
            $token = $userLogin->createToken('PKMSLKPP')->accessToken;
            return response()->json(['user' => $userLogin, 'token' => $token]);
            
        } else {
            return response()->json(['status' => 403, 'message' => 'OTP code not found!']);
        }
        
    }

    public function resendOtp(Request $request)
    {
        $api        = new Api(env('OTP_API_URL'), env('OTP_USER_KEY'));
        $validator  = Validator::make($request->all(), [
            'phonenumber' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $validator->errors()], 422);
        }
        $profile = Profile::where('mobile', $request->input('phonenumber'))->first();
        if (!$profile) {
            $phone = substr_replace($request->input('phonenumber'), '+62', 0, 1);
            $profile = Profile::where('mobile', $phone)->first();
            if (!$profile) {
                return response()->json(['status' => 403, 'message' => 'User not found with this phone number']);
            }
        }
        
        if (substr($profile->mobile, 0, 2) == '+6') {
            $phone = substr_replace($profile->mobile, '0', 0, 3);
        } else {
            $phone = $profile->mobile;
        }

        $otp            = new SmsOtp();
        $otp->user_id   = $profile->user_id;
        $otp->number    = $phone;
        $otp->otp_code  = substr(str_shuffle("0123456789"), 0, 5);
        $otp->sent_at   = Carbon::now()->format('Y-m-d H:i');
        $otp->expired_at= date('Y-m-d H:i',strtotime('+5 minutes',strtotime($otp->sent_at)));
        $otp->status    = 1;
        
        $message        = 'Kode OTP anda adalah '. $otp->otp_code . '. Harap tidak memberitahukan kepada siapapun kode berikut.';
        if($otp->save()) {
            $sendotp = $this->sendSMS($phone, $message);
            return response()->json(['status' => 200, 'message' => 'Otp has sent to your number '.$otp->number]);
        }
        return response()->json(['status' => 500, 'message' => 'Oops, failed to send OTP, something went wrong']);
    }

    public function getUsers()
    {
        return User::select(['id', 'name', 'email', 'avatar'])->where('is_admin', 0)->get();
    }

    public function getUnverifiedUsers()
    {
        return User::select(['id', 'name', 'email', 'avatar'])->where('is_verified', 0)->where('is_admin', 0)->get();
    }

    public function getVerifiedUsers()
    {
        return User::select(['id', 'name', 'email', 'avatar'])->where('is_verified', 1)->where('is_admin', 0)->get();
    }

    public function getUserById($id)
    {
        return User::select(['id', 'name', 'email', 'avatar'])->find($id);
    }

    public function sendSMS($phone_number=0, $message='') {
        $client     = new Client();
        $serviceurl = env("OTP_API_URL", "http://hs-lkpp.zenziva.com/apps/smsapi.php") . '?userkey=' . env("OTP_USER_KEY", "kf7zqk9m8398xs4w92") . '&passkey=' . env("OTP_PASS_KEY", "9gfo7mu222la7l45ko") . '&nohp=' . $phone_number  . '&tipe=reguler' . '&pesan=' . $message;
        
        $response = $client->request('GET', $serviceurl);
        return $response;
    }

    public function delteProfileWithoutUser()
    {
        $profiles   = Profile::doesnthave('user')->get();
        $deletes    = Profile::doesnthave('user')->delete();
        return response()->json(['status' => 200, 'data' => $profiles, 'delete' => $deletes]);
    }

    public function search(Request $request)
    {
        $keyword = $request->input('keyword');

        $user    = User::where('username', 'like', '%'.$keyword.'%')->orWhere('name', 'like', '%'.$keyword.'%')->get();
        
        return response()->json([
            'status' => 200, 
            'message' => 'Search user data',
            'data' => $user
        ]);
    }

}
