<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use PKMS\Models\Usulan;
use PKMS\Models\User;

class UsulanController extends BaseController
{
	/**
	 * get usulan by user
	 * @return Illuminate\Support\Collection
	 */
	public function getUsulan()
	{
		$offset = request()->get('offset', false) ? request()->get('offset', false) : 0;
		$limit 	= request()->get('limit', false) ? request()->get('limit', false) : 10;
		$offset = ($offset-1) * $limit;
		$data 	= Usulan::with(['user' => function($query){
			$query->select('id', 'name')->get();
		}])
		->where('user_id', Auth::id())
		->offset($offset)
		->limit($limit)
		->orderBy('created_at', 'DESC')
		->get();

		if ($data->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> []
			]);
		}

		return $result;
		
	}

	public function all()
	{
		$offset = request()->get('offset', false) ? request()->get('offset', false) : 0;
		$limit 	= request()->get('limit', false) ? request()->get('limit', false) : 10;
		$offset = ($offset-1) * $limit;

		$data 	= Usulan::with(['user' => function($query){
			$query->select('id', 'name')->get();
		}])->where('status', 1)->orderBy('created_at', 'DESC')->offset($offset)->limit($limit)->get();

		$result = Response::json([
			'status'	=> 200,
			'message'	=> 'success',
			'data'		=> $data
		]);

		return $result;
	}

	public function postUsulan(Request $request)
	{
		$data = new Usulan();
		$data->user_id 	= Auth::id();
		$data->judul 	= request()->judul;
		$data->usulan 	= request()->usulan;
		$data->alasan 	= request()->alasan;
		$data->status 	= 0;
		if ($data->save()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> []
			]);
		}

		return $result;

	}

	public function detail($id = '')
	{
		$data = Usulan::with(['user' => function($query){
			$query->select('t_users.id', 't_users.username', 't_users.name')->get();
		}])->findOrFail($id);
		if ($data) {
			# code...
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> ['message' => 'Data not found']
			]);
		}

		return $result;
	}
}