<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use PKMS\Models\MasterArticleCode;
use PKMS\Models\MasterArticleSubcode;

class KategoriController extends BaseController
{
	/**
	 * get usulan by user
	 * @return Illuminate\Support\Collection
	 */
	public function allCode()
	{
		$offset = request()->get('offset', false) ? request()->get('offset', false) : 0;
		$limit = request()->get('limit', false) ? request()->get('limit', false) : 10;

		$data = MasterArticleCode::with('subcodes')
		->select('id', 'name')
		->offset($offset)
		->limit($limit)
		->get();

		if ($data->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> []
			]);
		}

		return $result;
		
	}
	
}