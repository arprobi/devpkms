<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PKMS\Models\Task;

class TaskController extends BaseController
{
	/**
	 * get unassigned task
	 * @return Illuminate\Support\Collection
	 */
	public function getUnassigned()
	{
		return Task::where('is_completed', 0)->where('is_assigned', 0)->get();
	}
}