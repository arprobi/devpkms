<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use File;
use Response;
use PKMS\Models\Bantuan;

class BantuanController extends BaseController
{
	/**
	 * get Bantuan by user
	 * @return Illuminate\Support\Collection
	 */
	public function all()
	{
		$often = Bantuan::where('often_asked', 0)->orderBy('created_at', 'desc')->get();
		$usual = Bantuan::where('often_asked', 1)->orderBy('created_at', 'desc')->get();

		
		$result = Response::json([
			'status'	=> 200,
			'message'	=> 'success',
			'data'		=> [
				'often_asked' 	=> $often,
				'other_help'	=> $usual
			]
		]);
		
		return $result;
		
	}
}