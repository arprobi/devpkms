<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use PKMS\Models\UserNotification;
use PKMS\Models\User;

class UserNotificationController extends BaseController
{
	/**
	 * get notification by user
	 * @return Illuminate\Support\Collection
	 */
	public function all()
	{
		$offset = request()->get('offset', false) ? request()->get('offset', false) : 0;
		$limit 	= request()->get('limit', false) ? request()->get('limit', false) : 10;
		$offset = ($offset-1) * $limit;
		$data 	= UserNotification::where('user_id', Auth::id())
		->offset($offset)
		->limit($limit)
		->get();

		if ($data->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		} else {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> []
			]);
		}

		return $result;
		
	}

	public function read($id)
	{
		if ($data = UserNotification::findOrFail($id)) {
			$data->read = 1;
			if ($data->update()) {
				$result = Response::json([
					'status'	=> 200,
					'message'	=> 'success',
					'data'		=> $data
				]);
			} else {
				$result = Response::json([
					'status'	=> 500,
					'message'	=> 'error',
					'data'		=> 'Terjadi kesalahan sistem'
				]);
			}
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> 'Data tidak ditemukan'
			]);
		}

		return $result;
	}

	public function delete($id)
	{
		if ($data = UserNotification::findOrFail($id)) {
			$data->read = 1;
			if ($data->delete()) {
				$result = Response::json([
					'status'	=> 200,
					'message'	=> 'success',
					'data'		=> 'Berihasil di hapus'
				]);
			} else {
				$result = Response::json([
					'status'	=> 500,
					'message'	=> 'error',
					'data'		=> 'Terjadi kesalahan sistem'
				]);
			}
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> 'Data tidak ditemukan'
			]);
		}

		return $result;
	}
}