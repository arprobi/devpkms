<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use PKMS\Models\User;

class BookmarkController extends BaseController
{
	/**
	 * get bookmark by user
	 * @return Illuminate\Support\Collection
	 */
	public function get()
	{
		$data =	User::where('id', Auth::id())->select('id');
		
		if (request()->faq) {
			$data =	$data->with(['knowledges' => function($query){
				$query->select('pkms_faqs.id', 'klasifikasi', 'title', 'faq', 'qa', 'answer', 'content_letter', 'video', 'materi', 'image', 'viewer', 'pkms_faqs.created_at', 'pkms_faqs.updated_at')->get();
			}])->get();
		}else if (request()->pasal) {
			$data =	$data->with(['pasals' => function($query){
				$query->select('pkms_pasals.id', 'nama_pasal', 'highlighted', 'pkms_pasals.created_at')->get();
			}])->get();
		}else if (request()->diskusi) {
			$data =	$data->with(['discuss' => function($query){
				$query->select('pkms_questions.id', 'title', 'name', 'pkms_questions.created_at')->get();
			}])->get();
		}else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> 'Tidak ada'
			]);
			return $result;	
		}

		$result = Response::json([
			'status'	=> 200,
			'message'	=> 'success',
			'data'		=> $data
		]);

		return $result;	
	}

	public function store(Request $request)
	{
		$user 			= User::findOrFail(Auth::id());

		if (request()->faq_id) {
			if (!$user->knowledges()->where('faq_id', request()->faq_id)->first()) {
				$user->knowledges()->attach([request()->faq_id]);
			}
		} else if (request()->pasal_id) {
			if (!$user->pasals()->where('pasal_id', request()->pasal_id)->first()) {
				$user->pasals()->attach([request()->pasal_id]);
			}
		} else if (request()->question_id) {
			if (!$user->discuss()->where('question_id', request()->question_id)->first()) {
				$user->discuss()->attach([request()->question_id]);
			}
		}else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> 'Tidak ada'
			]);
			return $result;	
		}

		$result = Response::json([
			'status'	=> 200,
			'message'	=> 'success',
			'data'		=> ['Sukses menandai']
		]);

		return $result;	
		
	}


	public function remove(Request $request)
	{
		$user 			= User::findOrFail(Auth::id());

		if (request()->faq_id) {
			$user->knowledges()->detach([request()->faq_id]);
		} else if (request()->pasal_id) {
			$user->pasals()->detach([request()->pasal_id]);
		} else if (request()->question_id) {
			$user->discuss()->detach([request()->question_id]);
		}else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> 'Tidak ada'
			]);
			return $result;	
		}

		$result = Response::json([
			'status'	=> 200,
			'message'	=> 'success',
			'data'		=> ['Sukses menghapus tanda']
		]);

		return $result;	
	}

	
}