<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use PKMS\Models\User;
use PKMS\Models\Usulan;
use PKMS\Models\Faq;

class LikeController extends BaseController
{
	/**
	 * Like or unlike knowledge
	 * @return Illuminate\Support\Collection
	 */
	public function like($id)
	{
		$user = Auth::user();
		if (Usulan::where('id', $id)->get()->count()) {
			$usulan = Usulan::findOrFail($id);
			if ($usulan->like) {
				return  $result = Response::json([
					'status'	=> 502,
					'message'	=> 'failed',
					'data'		=> 'Usulan telah di sukai'
				]);
			}
			if ($usulan->likes()->syncWithoutDetaching($user->id)) {
				$result = Response::json([
					'status'	=> 200,
					'message'	=> 'success',
					'data'		=> 'Berhasil di like'
				]);
			} else {
				$result = Response::json([
					'status'	=> 500,
					'message'	=> 'failed',
					'data'		=> 'Internal error'
				]);
			}
		}else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'failed',
				'data'		=> 'Usulan not found'
			]);
		}
		return $result;
	}

	public function unlike($id)
	{
		$user = Auth::user();
		if (Usulan::where('id', $id)->get()->count()) {
			$usulan = Usulan::findOrFail($id);
			if (!$usulan->like) {
				return  $result = Response::json([
					'status'	=> 502,
					'message'	=> 'failed',
					'data'		=> 'Usulan telah tidak di sukai'
				]);
			}
			if ($usulan->likes()->detach($user->id)) {
				$result = Response::json([
					'status'	=> 200,
					'message'	=> 'success',
					'data'		=> 'Berhasil unlike'
				]);
			}else {
				$result = Response::json([
					'status'	=> 500,
					'message'	=> 'failed',
					'data'		=> 'Internal error'
				]);
			}
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'failed',
				'data'		=> 'Usulan not found'
			]);
		}

		return $result;
	}

	public function slug()
	{
		$faqs = Faq::all();
		foreach ($faqs as $faq) {
			$slug = Faq::findOrFail($faq->id);
			$slug->slug = str_slug($slug->title, '-');
			$slug->save();
		}
	}
}