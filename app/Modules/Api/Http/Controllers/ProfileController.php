<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Response;
use PKMS\Models\Profile;
use PKMS\Models\User;

class ProfileController extends BaseController
{
	/**
	 * get profile user
	 * @return Illuminate\Support\Collection
	 */
	public function view()
	{
		$profile 	= Auth::user()->profile()->count();

		if ($profile) {
			$profile = Profile::where('user_id', Auth::id())->select('id', 'user_id', 'gender', 'birth_place', 'birth_date', 'phone', 'mobile')->first();
		} else {
			$profile = DB::table('t_profiles')
				->insert([
					'user_id' 		=> Auth::id()
				]);
			$profile = Profile::where('user_id', Auth::id())->select('id', 'user_id', 'gender', 'birth_place', 'birth_date', 'phone', 'mobile')->first();
		}
		
		return $result 	= Response::json([
			'status'	=> 200,
			'message'	=> 'success',
			'data'		=> $profile
		]);
	}

	public function update(Request $request)
	{
		if (Auth::user()) {
			if (Auth::user()->profile()->count()) {
				$profile = DB::table('t_profiles')
            			->where('user_id', Auth::id())
            			->update([
            				'gender' 		=> request()->gender, // m|f
							'birth_place' 	=> request()->birth_place,
							'birth_date' 	=> date('Y-m-d', strtotime(request()->birth_date)),
							'phone' 		=> request()->phone,
							'mobile' 		=> request()->mobile
            			]);
			
			} else {
				$profile = DB::table('t_profiles')
            			->insert([
            				'user_id' 		=> Auth::id(),
            				'gender' 		=> request()->gender, // m|f
							'birth_place' 	=> request()->birth_place,
							'birth_date' 	=> request()->birth_date,
							'phone' 		=> request()->phone,
							'mobile' 		=> request()->mobile
            			]);
			}

			$data = Profile::where('user_id', Auth::id())->select('id', 'user_id', 'gender', 'birth_place', 'birth_date', 'phone', 'mobile')->first();

			if ($data) {
				return $result 	= Response::json([
					'status'	=> 200,
					'message'	=> 'success',
					'data'		=> $data
				]);
			} else {
				return $result 	= Response::json([
					'status'	=> 500,
					'message'	=> 'error',
					'data'		=> []
				]);
			}
		} else {
			return $result 	= Response::json([
				'status'	=> 401,
				'message'	=> 'unauthorize',
				'data'		=> []
			]);
		}
	}
	
}