<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;
use DB;

use PKMS\Models\Search;
use PKMS\Models\Pasal;
use PKMS\Models\Faq;
use PKMS\Models\Question;

class SearchController extends BaseController
{
	/**
	 * get Pasal
	 * @return Illuminate\Support\Collection
	 */
	public function popSearch()
	{
		$data 	= Search::select('keyword')->orderBy('number', 'DESC')->get();
		$limit 	= request()->get('limit', false) ? request()->get('limit', false) : 10;

		if ($data->count()) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data->take($limit)
			]);
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> []
			]);
		}

		return $result;
		
	}

	public function search()
	{
		$perpres 	= request()->get('perpres', false);
		$knowledge 	= request()->get('knowledge', false);
		$diskusi 	= request()->get('diskusi', false);

		$offset 	= request()->get('offset', false) ? request()->get('offset', false) : 0;
		$limit 		= request()->get('limit', false) ? request()->get('limit', false) : 10;

		$offset 	= ($offset-1) * $limit;

		$data 		= '';

		if ($perpres && !$knowledge && !$diskusi) {
			$data = DB::table('pkms_pasals')
							->join('pkms_kategori_pasal', 'pkms_pasals.id' , '=', 'pkms_kategori_pasal.pasal_id')
							->join('pkms_kategoris', 'pkms_kategori_pasal.kategori_id' , '=', 'pkms_kategoris.id')
							->select('pkms_pasals.id', 'nama_pasal', 'isi_pasal', 'pkms_pasals.created_at', 'pkms_pasals.updated_at')
							->where('nama_pasal', 'LIKE', '%'.$perpres.'%')
							->orWhere('isi_pasal', 'LIKE', '%' .$perpres.'%')
							->orWhere('nama_kategori', 'LIKE', '%' .$perpres.'%')
							->offset($offset)
							->limit($limit)
							->get();
			$this->addKey($perpres);
		} 

		if (!$perpres && $knowledge && !$diskusi) {
			$data = Faq::select('id', 'klasifikasi', 'title', 'faq', 'qa', 'answer', 'content_letter', 'video', 'materi', 'image', 'updated_at', 'viewer')
							->where('title', 'LIKE', '%'.$knowledge.'%')
							->orWhere('tags', 'LIKE', '%' .$knowledge.'%')
							->offset($offset)
							->limit($limit)
							->orderBy('viewer', 'DESC')
							->get();
			$this->addKey($knowledge);
		}

		if (!$perpres && !$knowledge && $diskusi) {
			$data = Question::select('id', 'title', 'name', 'created_at', 'updated_at')
							->where('title', 'LIKE', '%'.$diskusi.'%')
							->orWhere('content', 'LIKE', '%' .$diskusi.'%')
							->offset($offset)
							->limit($limit)
							->get();
			$this->addKey($diskusi);
		}

		return Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
	}

	public function addKey($keyword)
	{
		$key = Search::where('keyword', strtolower($keyword))->first();

		if ($key) {
			$key->keyword 	= $keyword;
			$key->number  	= $key->number + 1;
		} else {
			$key 			= new Search();
			$key->keyword 	= strtolower($keyword);
			$key->number  	= 1;
		}

		$key->save();
	}
}