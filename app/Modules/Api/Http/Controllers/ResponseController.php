<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Response;

use PKMS\Models\ResponseQuestion;
use PKMS\Models\ResponseAnswer;

class ResponseController extends BaseController
{
	/**
	 * get Pasal
	 * @return Illuminate\Support\Collection
	 */
	public function getPooling()
	{
		$data 	= ResponseQuestion::where('published', 1)->with('choices')->first();
		if (!$data) {
			return Response::json([
				'status'	=> 500,
				'message'	=> 'failed',
				'data'		=> 'Tidak ada polling'
			]);
		}
		
		$exist 	= ResponseAnswer::where('response_question_id', $data->id)->where('user_id', Auth::id())->count();

		if ($exist) {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'failed',
				'data'		=> 'Anda sudah melakukan pooling'
			]);
		} else {
			if ($data) {
				$result = Response::json([
					'status'	=> 200,
					'message'	=> 'success',
					'data'		=> $data
				]);
			} else {
				$result = Response::json([
					'status'	=> 200,
					'message'	=> 'error',
					'data'		=> ['Tidak ada pooling tersedia :)']
				]);
			}
		}


		return $result;
		
	}

	public function postPooling(Request $request)
	{
		$pool = ResponseQuestion::findOrFail(request()->pool_id);
		
		if ($pool) {
			$exist = ResponseAnswer::where('response_question_id', $pool->id)->where('user_id', Auth::id())->count();

			if ($exist) {
				return Response::json([
					'status'	=> 500,
					'message'	=> 'failed',
					'data'		=> 'Anda sudah melakukan pooling'
				]);
			} else {
				$data 						= new ResponseAnswer();
				$data->response_question_id = $pool->id;
				$data->user_id 				= Auth::id();
				$data->is_selection 		= $pool->is_selection;
				$data->answer 				= request()->answer;
				

				if ($data->save()) {
					return Response::json([
						'status'	=> 200,
						'message'	=> 'success',
						'data'		=> $data
					]);
				}else {
					return Response::json([
						'status'	=> 500,
						'message'	=> 'error',
						'data'		=> []
					]);
				}
			}

		}

		return Response::json([
			'status'	=> 500,
			'message'	=> 'error',
			'data'		=> []
		]);
	}

	public function loadMore($id)
	{
		$data = ResponseAnswer::where('response_question_id', $id)
								->limit(5)
								->offset(request()->offset)
								->with('user')
								->get();
		if ($data) {
			return $data;
		}else{
			return 0;
		}
	}
}