<?php

namespace PKMS\Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use DB;
use File;
use Response;
use PKMS\Models\User;
use PKMS\Models\Profile;

class NotifikasiController extends BaseController
{
	/**
	 * Post on/off notifikasi mobile apps
	 * @return Illuminate\Support\Collection
	 */
	public function store(Request $request)
	{
		$user 			= Auth::user();

		if ($user) {
			if ($user->profile()->update(['notify' => request()->status])) {
				$result = Response::json([
					'status'	=> 200,
					'message'	=> 'success',
					'data'		=> $user->profile
				]);

				DB::table('pkms_fcm_tokens')->where('user_id', Auth::id())->update(['status' => request()->status]);

			} else {
				$result = Response::json([
					'status'	=> 500,
					'message'	=> 'error',
					'data'		=> []
				]);
			}
			
		} else {
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> 'User not found'
			]);
		}

		return $result;
		
	}

	public function get()
	{
		$user   	= Auth::user();

		if (!$user->profile) {
			DB::table('t_profiles')->insert([
			    [
			    	'user_id' => $user->id,
			    	'notify'  => 1,
				]
			]);
		}
		$data 		= Profile::where('user_id', $user->id)->select('notify')->first();


		if ($data) {
			$result = Response::json([
				'status'	=> 200,
				'message'	=> 'success',
				'data'		=> $data
			]);
		}else{
			$result = Response::json([
				'status'	=> 500,
				'message'	=> 'error',
				'data'		=> 'User not found'
			]);
		}
		return $result;
	}
}