<?php

namespace App\Modules\Core\Contracts;


interface RepositoryInterface
{
	public function all($category = []);
	public function findById($id);
	public function create($data);
	public function update($id, $data);
	public function delete($id);
}