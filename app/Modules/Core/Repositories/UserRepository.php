<?php

namespace App\Modules\Core\Repositories;

use App\Models\User;
use App\Modules\Core\Contracts\RepositoryInterface;

class UserRepository implements RepositoryInterface
{
	public function all($category = [])
	{

	}

	public function findById($id)
	{

	}

	public function create($data)
	{

	}

	public function update($id, $data)
	{

	}

	public function delete($id)
	{

	}
}