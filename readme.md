# PKMS

PKMS adalah

## Expert List

Expert list adalah......

# SSO API REFERENCES

## EXAMPLE
    $params = array(
        'user_id'=>1
    );
    $this->ssoclient->get_user($params);

### get_login_url
Params:

- redirect `string`


### get_redirect_url
Params:


### get_user
Params:

- user_id `int` *default: current user*


### get_users_by_ids
Params:

- user_ids `int[]`


### get_users_by_broker
Params:

- broker_id `string` *default: current broker*
- start `int` *default: 0*
- rows `int` *default: 10*
- search `string` *default: ""*
- sort_by `string` *default: name*
- order `string` *asc|desc*
- klasifikasi_id `int` *default: false*

### get_users
Params:

- start `int` *default: 0*
- rows `int` *default: 10*
- search `string` *default: ""*
- sort_by `string` *default: name*
- order `string` *asc|desc*
- klasifikasi_id `int` *default: false*


### send_sms
Params:

- phone_number `string|string[]`
- message `string`
- send_at `string` *format: Y-m-d H:i:s, default: current datetime*


### send_sms_to_klasifikasi_user
Params:

- klasifikasi_id `int`
- message `string`
- send_at `string` *format: Y-m-d H:i:s, default: current datetime*


### get_histories_sms
Params:

- broker_id `string` *default: current broker*
- start `int` *default: 0*
- rows `int` *default: 10*
- search `string` *default: ""*
- sort_by `string` *default: created_at*
- order `string` *asc|desc*


### send_email
Params:

- email_to `string|string[]`
- subject `string`
- content `string`
- send_at `string` *format: Y-m-d H:i:s, default: current datetime*


### send_email_to_klasifikasi_user
Params:

- klasifikasi_id `int`
- subject `string`
- content `string`
- send_at `string` *format: Y-m-d H:i:s, default: current datetime*


### get_histories_email
Params:

- broker_id `string` *default: current broker*
- start `int` *default: 0*
- rows `int` *default: 10*
- search `string` *default: ""*
- sort_by `string` *default: created_at*
- order `string` *asc|desc*


### get_klasifikasi_user
Params:

- klasifikasi_id `int`


### get_klasifikasis_user
params:


### get_klasifikasis_user_by_ids
Params:

- klasifikasi_ids `int[]`