<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHighlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pkms_highlights', function (Blueprint $table) {
            $table->increments('id');
            $table->text('words')->nullable();
            $table->integer('user_id')->unisgned();
            $table->integer('pasal_id')->unisgned();
            $table->timestamps();

            // $table->foreign('user_id')->references('id')->on('t_users');
            // $table->foreign('pasal_id')->references('id')->on('pkms_pasals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pkms_highlights');
    }
}
