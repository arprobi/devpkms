<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnHighlightedPkmsPasalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pkms_pasals', function (Blueprint $table) {
            $table->dropColumn('highlighted');
        });

        Schema::table('pkms_pasals', function (Blueprint $table) {
            $table->longText('highlighted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pkms_pasals', function (Blueprint $table) {
            //
        });
    }
}
