<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponseChoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pkms_response_choices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('choice');
            $table->integer('response_question_id')->unsigned();
            $table->timestamps();

            $table->foreign('response_question_id')->references('id')->on('pkms_response_questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pkms_response_choices');
    }
}
