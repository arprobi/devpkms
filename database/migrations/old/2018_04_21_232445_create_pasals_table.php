<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pkms_pasals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_pasal');
            $table->text('penjelasan');
            $table->text('isi_pasal');
            $table->integer('perpres_id')->unsigned();
            $table->integer('order');
            $table->timestamps();

            $table->foreign('perpres_id')->references('id')->on('pkms_perpres');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pkms_pasals');
    }
}
