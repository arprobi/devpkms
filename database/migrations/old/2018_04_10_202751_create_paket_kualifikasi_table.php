<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaketKualifikasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_packet_qualifications', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('packet_id');
            $table->string('name');
             $table->timestamps();
             
            $table->foreign('packet_id')->references('id')->on('t_packets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packet_qualifications');
    }
}
