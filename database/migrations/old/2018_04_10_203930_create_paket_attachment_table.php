<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaketAttachmentTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_attachment_packet', function (Blueprint $table) {
            $table->unsignedInteger('packet_id');
            $table->unsignedInteger('attachment_id');

            $table->foreign('packet_id')->references('id')->on('t_packets')->onDelete('cascade');
            $table->foreign('attachment_id')->references('id')->on('t_attachments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
