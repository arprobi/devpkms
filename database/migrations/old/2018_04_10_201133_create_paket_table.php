<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_packets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('description');
            $table->unsignedInteger('instansi_id');
            $table->string('satuan_kerja');
            $table->integer('tahun_anggaran');
            $table->string('kode_anggaran',11);
            $table->integer('anggaran');
            $table->integer('jangka_waktu');
            $table->enum('interval_waktu', ['hari', 'minggu', 'bulan', 'tahun']);
            $table->enum('jenis_pengadaan', ['jasa konsultasi', 'jasa lainnya']);
            $table->enum('jenis_penyedia', ['perorangan']);
            $table->string('ppk_nama');
            $table->string('ppk_nip');
            $table->string('ppk_jabatan');
            $table->string('pejabat_nama');
            $table->string('pejabat_nip');
            $table->string('pejabat_jabatan');
            $table->unsignedInteger('province_code');
            $table->unsignedInteger('city_code');
            $table->longText('detail_lokasi');
            $table->enum('cara_pembayaran',['bulanan','termin','sekaligus']);
            $table->string('metode_pengadaan');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packets');
    }
}
