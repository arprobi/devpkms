<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumUsulansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pkms_usulans', function (Blueprint $table) {
            $table->string('judul')->after('user_id')->nullable();
            $table->text('alasan')->after('usulan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pkms_usulans', function (Blueprint $table) {
            $table->dropColumn('judul');
            $table->dropColumn('alasan');
        });
    }
}
