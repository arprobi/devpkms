<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasalKategoriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pkms_kategori_pasal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pasal_id')->unsigned();
            $table->integer('kategori_id')->unsigned();
            $table->timestamps();

            $table->foreign('pasal_id')->references('id')->on('pkms_pasals');
            $table->foreign('kategori_id')->references('id')->on('pkms_kategoris');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pkms_kategori_pasal');
    }
}
