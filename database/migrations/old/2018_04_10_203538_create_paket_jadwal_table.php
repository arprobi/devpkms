<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaketJadwalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_packet_schedule', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('packet_id');
            $table->string('name');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            
            $table->foreign('packet_id')->references('id')->on('t_packets')->onDelete('cascade');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packet_schedule');
    }
}
