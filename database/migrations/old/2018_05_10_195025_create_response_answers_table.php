<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponseAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pkms_response_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('response_question_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->boolean('is_selection');
            $table->text('answer');
            $table->timestamps();

            $table->foreign('response_question_id')->references('id')->on('pkms_response_questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pkms_response_answers');
    }
}
