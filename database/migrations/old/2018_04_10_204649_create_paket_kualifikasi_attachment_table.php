<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaketKualifikasiAttachmentTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_attachment_packet_qualification', function (Blueprint $table) {
            $table->unsignedInteger('packet_qualification_id');
            $table->unsignedInteger('attachment_id');

//            $table->foreign('packet_qualification_id')->references('id')->on('t_packet_qualifications')->onDelete('cascade');
//            $table->foreign('attachment_id')->references('id')->on('t_attachments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
