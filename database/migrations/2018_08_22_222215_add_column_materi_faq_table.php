<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMateriFaqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pkms_faqs', function (Blueprint $table) {
            $table->integer('klasifikasi')->after('id')->default(1);
            $table->text('materi')->after('video')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pkms_faqs', function (Blueprint $table) {
            $table->dropColumn('klasifikasi');
            $table->dropColumn('materi');
        });
    }
}
