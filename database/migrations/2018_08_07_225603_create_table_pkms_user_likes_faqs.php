<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePkmsUserLikesFaqs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pkms_user_like_usulans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('usulan_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('t_users');
            $table->foreign('usulan_id')->references('id')->on('pkms_usulans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pkms_user_like_usulans');
    }
}
