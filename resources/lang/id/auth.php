<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Kombinasi Email, Username atau Telepon dengan Password tidak cocok.',
    'throttle' => 'Terlalu banyak percobaaan login. Coba lagi setelah :seconds detik.',

];
