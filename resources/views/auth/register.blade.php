@extends('layouts.login')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1 class="lead">Registrasi Tenaga Ahli</h1>
            <div class="panel panel-default">
                <div class="panel-heading">Pendaftaran Akun</div>
                {!! Former::open()->action(route('register')) !!}
                <div class="panel-body">
                    {!! Former::text('name')->label('Nama')->placeholder('Nama')->required() !!}
                    {!! Former::text('username')->label('Username')->placeholder('Username')->required() !!}
                    {!! Former::email('email')->placeholder('Alamat Email')->required() !!}
                    {!! Former::password('password')->placeholder('Password')->required() !!}
                    {!! Former::password('password_confirmation')->placeholder('Ketik ulang password')->required() !!}

                    {!! Recaptcha::render() !!}
                </div>
                <div class="panel-footer">
                    <a href="/" class="btn btn-default">Kembali</a>
                    <div class="pull-right">
                        <input id="reset" type="reset" class="btn btn-default" value="Hapus">
                        <button class="btn btn-primary">Daftar</button>
                    </div>
                </div>
                {!! Former::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
