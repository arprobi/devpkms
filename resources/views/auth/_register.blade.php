@extends('layouts.login')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1 class="lead">Registrasi Tenaga Ahli</h1>
            <div class="panel panel-default">
                <div class="panel-heading">Akun</div>
                {!! Former::open_for_files()->action(route('register')) !!}
                <div class="panel-body">
                    {!! Former::email('email')->placeholder('Alamat Email')->required() !!}
                    {!! Former::password('password')->placeholder('Password')->required() !!}
                    {!! Former::password('password_confirmation')->placeholder('Ketik ulang password')->required() !!}
                </div>
                <div class="panel-heading">Profil</div>
                <div class="panel-body">
                    {!! Former::small_text('name')->label('Nama Lengkap')->placeholder('Nama Lengkap')->required() !!}
                    {!! Former::radios('Jenis Kelamin')->radios([
                        'Laki-Laki' => ['name' => 'gender', 'value' => 'm'],
                        'Perempuan' => ['name' => 'gender', 'value' => 'f']
                    ])->check('m')->required() !!}
                    <div class="row">
                        <div class="col-md-6">
                            {!! Former::text('birth_place')->label('Tempat Lahir')->placeholder('Tempat Lahir')->required() !!}
                        </div>
                        <div class="col-md-6">
                            {!! Former::text('birth_date_view')->label('Tanggal Lahir')->placeholder('Tanggal Lahir')->required() !!}
                            {!! Former::hidden('birth_date')->id('birth_date') !!}
                        </div>
                    </div>
                    {!! Former::textarea('address')->label('Alamat')->rows(3)->placeholder('Alamat') !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    {!! Former::text('rt')->label('RT')->placeholder('No RT') !!}
                                </div>
                                <div class="col-md-6">
                                    {!! Former::text('rw')->label('RW')->placeholder('No RW') !!}
                                </div>
                            </div>
                            {!! Former::text('kode_pos')->label('Kode Pos')->placeholder('Kode Pos') !!}
                            {!! Former::text('phone')->label('No Telepon')->placeholder('No Telepon') !!}
                            {!! Former::text('mobile')->label('No Handphone')->placeholder('No Handphone') !!}
                        </div>
                        <div class="col-md-6">
                            {!! Former::select('province_code')->label('Propinsi')->placeholder('Pilih')->fromQuery(PKMS\Models\Province::get(),'name','code')->required() !!}
                            {!! Former::select('city_code')->label('Kabupaten/Kota')->placeholder('Pilih')->disabled()->required() !!}
                            {!! Former::select('district_code')->label('Kecamatan')->placeholder('Pilih')->disabled()->required() !!}
                            {!! Former::select('village_code')->label('Kelurahan/Desa')->placeholder('Pilih')->disabled()->required() !!}
                        </div>
                    </div>
                    {!! Former::select('interest[]')->label('minat')->fromQuery(PKMS\Models\Interest::get(),'name')->multiple() !!}
                </div>
                <div class="panel-heading">
                    Pengalaman
                    <div class="pull-right">
                        <a href="javascript:;" id="btn-add" class="btn btn-primary btn-sm btn-circle-sm"><i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="exp-warning alert alert-warning">Anda belum memasukan pengalaman kerja.</div>
                    <div class="exp-box">
                        
                    </div>
                </div>
                <div class="panel-footer">
                    <a href="/" class="btn btn-default">Kembali</a>
                    <div class="pull-right">
                        <input id="reset" type="reset" class="btn btn-default" value="Hapus">
                        <button class="btn btn-primary">Daftar</button>
                    </div>
                </div>
                {!! Former::close() !!}
            </div>
        </div>
    </div>
</div>

<script id="entry-template" type="text/x-handlebars-template">
<div class="exp-item list-group">
    <div class="list-group-item">
        <div class="pull-right" style="padding-bottom: 1em">
            <a href="javascript:;" class="btn btn-delete btn-default btn-circle-sm"><i class="fa fa-times"></i></a>
        </div>
        <div class="form-group">
            <input name="exp[@{{x}}][title]" type="text" placeholder="Perusahaan / Proyek" class="form-control">
        </div>
        <div class="row">
            <div class="col-md-6">
                <h5>Dari</h5>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group"><select class="form-control" id="exp[@{{x}}][start][month]" name="exp[@{{x}}][start][month]"><option value="" disabled="disabled" selected="selected">Bulan</option><option value="01">Januari</option><option value="02">Februari</option><option value="03">Maret</option><option value="04">April</option><option value="05">Mei</option><option value="06">Juni</option><option value="07">Juli</option><option value="08">Agustus</option><option value="09">September</option><option value="10">Oktober</option><option value="11">November</option><option value="12">Desember</option></select></div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group"><select class="form-control" id="exp[@{{x}}][start][year]" name="exp[@{{x}}][start][year]"><option value="" disabled="disabled" selected="selected">Tahun</option><option value="1970">1970</option><option value="1971">1971</option><option value="1972">1972</option><option value="1973">1973</option><option value="1974">1974</option><option value="1975">1975</option><option value="1976">1976</option><option value="1977">1977</option><option value="1978">1978</option><option value="1979">1979</option><option value="1980">1980</option><option value="1981">1981</option><option value="1982">1982</option><option value="1983">1983</option><option value="1984">1984</option><option value="1985">1985</option><option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option></select></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h5>Hingga</h5>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group"><select class="form-control" id="exp[@{{x}}][until][month]" name="exp[@{{x}}][until][month]"><option value="" disabled="disabled" selected="selected">Bulan</option><option value="01">Januari</option><option value="02">Februari</option><option value="03">Maret</option><option value="04">April</option><option value="05">Mei</option><option value="06">Juni</option><option value="07">Juli</option><option value="08">Agustus</option><option value="09">September</option><option value="10">Oktober</option><option value="11">November</option><option value="12">Desember</option></select></div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group"><select class="form-control" id="exp[@{{x}}][until][year]" name="exp[@{{x}}][until][year]"><option value="" disabled="disabled" selected="selected">Tahun</option><option value="1970">1970</option><option value="1971">1971</option><option value="1972">1972</option><option value="1973">1973</option><option value="1974">1974</option><option value="1975">1975</option><option value="1976">1976</option><option value="1977">1977</option><option value="1978">1978</option><option value="1979">1979</option><option value="1980">1980</option><option value="1981">1981</option><option value="1982">1982</option><option value="1983">1983</option><option value="1984">1984</option><option value="1985">1985</option><option value="1986">1986</option><option value="1987">1987</option><option value="1988">1988</option><option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option><option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option></select></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <textarea name="exp[@{{x}}][description]" rows="5" class="form-control" placeholder="Deskripsi"></textarea>
        </div>
        <div class="form-group">
            <label>Lampiran</label>
            <input name="exp[@{{x}}][lampiran]" type="file" multiple="" class="form-control">
        </div>
    </div>
</div>
</script>
@endsection