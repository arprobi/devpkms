@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <h1 class="lead" align="center">Selamat Datang <i class="fa fa-smile-o"></i></h1>
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                {!! Former::open()->action(route('login')) !!}
                <div class="panel-body">
                    {!! Former::text('email')->label('Email/Username/Telepon')->placeholder('Alamat Email, Username atau Nomor Telepon(08XXX...)')->required() !!}
                    {!! Former::password('password')->placeholder('Password')->required() !!}

                    @if (env("APP_ENV")=="production")
                    <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                        {!! Recaptcha::render() !!}
                        @if ($errors->has('g-recaptcha-response'))
                        <span class="help-block">
                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                        </span>
                        @endif
                    </div>
                    @endif

                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                            </label>
                        </div>
                    </div>


                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary">
                        Login
                    </button>

                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        Forgot Your Password?
                    </a>
                </div>
                {!! Former::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
