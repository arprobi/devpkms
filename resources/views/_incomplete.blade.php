@if(request()->session()->get('incomplete', false))
    <div class="alert alert-warning">
        <i class="fa fa-exclamation-circle"></i> {!! request()->session()->get('incomplete', false) !!}
    </div>
@endif
