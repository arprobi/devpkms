@extends('layouts.app')

@section('content')

<div id="educations-page" class="container">

    @include('_incomplete')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-graduation"></i> Edukasi</div>
        @if($user->educations->count())
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Institusi</th>
                            <th>Jurusan / Bidang keahlian</th>
                            <th>Nilai</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user->educations()->orderBy('education_id','asc')->get() as $education)
                            <tr>
                                <td>{{$education->detail->code}} - {{$education->name}}</td>
                                <td>{{$education->major}}</td>
                                <td>{{$education->gpa}}</td>
                                <td>
                                    {!! Former::open(route('expert.educations.delete', $education->id))->method('delete') !!}
                                        <a href="{{route('expert.educations.edit', $education->id)}}" class="btn btn-default">Edit</a>
                                        <button class="btn btn-delete btn-danger">Hapus</button>
                                    {!! Former::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="panel-body">
                <div class="alert alert-warning">
                    Anda belum memasukan data riwayat pendidikan.
                </div>
            </div>
        @endif
        <div class="panel-footer">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-education">
                <i class="fa fa-fw fa-plus"></i> Tambah riwayat pendidikan
            </button>
        </div>
    </div>
</div>

{{-- Modal edukasi  --}}
<div id="modal-education" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Riwayat pendidikan</h4>
            </div>
            {!! Former::open(route('expert.educations.create')) !!}
                <div class="modal-body">
                    {!! Former::select('education_id')->label('Tingkat Pendidikan')->placeholder('Pilih')->fromQuery(PKMS\Models\MasterEducation::get(), 'name', 'id')->required() !!}
                    {!! Former::text('name')->label('Nama Institusi Pendidikan')->placeholder('Nama Institusi Pendidikan')->required() !!}
                    {!! Former::text('major')->label('Nama Jurusan / Bidang Keahlian')->placeholder('Nama Jurusan / Bidang Keahlian (Opsional)') !!}
                    {!! Former::text('gpa')->label('Nilai / Peringkat Kelulusan / GPA')->placeholder('Nilai (Opsional)') !!}
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-primary">Simpan</button>
                </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>
@endsection
