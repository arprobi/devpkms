@component('mail::message')
# Hi {{ $data['name'] }} {{ $data['email'] }}

{{ $data['content'] }}

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent
