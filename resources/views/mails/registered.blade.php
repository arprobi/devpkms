@component('mail::message')
# Selamat Datang {{ $user->name }}

Lengkapi profil, riwayat pendidikan, riwayat pekerjaan dan data dukung lainnya agar segera dapat dilakukan verifikasi tenaga ahli oleh Admin pengelola aplikasi.

@component('mail::button', ['url' => route('expert.dashboard')])
Dashboard
@endcomponent

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent
