@component('mail::message')
# Hi {{ $list->name }} {{ $list->email }}

{{ $list->invitation->description }}

@component('mail::button', ['url' => route('main.invited', $list->key)])
Mulai
@endcomponent

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent
