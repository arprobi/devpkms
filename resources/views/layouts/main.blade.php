<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="author" content="LKPP" />
        <meta name="prefix" content="{{env('APP_PREFIX', '')}}">
        <meta name="robots" content="index,follow"/>
        <meta name="googlebot-news" content="index,follow"/>
        <meta name="googlebot" content="index,follow"/>
        <meta name="language" content="id" />
        <meta name="geo.country" content="id" />
        <meta http-equiv="content-language" content="In-Id" />
        <meta name="geo.placename" content="Indonesia" />
        <meta name="mobile-web-app-capable" content="yes"/>
        <meta name="platform" content="desktop">

        @yield('meta')
        
        <!-- Fonts -->
        <link href="//fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="/fav.png" rel="shortcut icon"/>
        <link rel="stylesheet" href="{{asset('/pkms/assets/includes/font-awesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('/pkms/assets/includes/bootstrap/css/bootstrap.css')}}">
        @yield('styles')
        <link rel="stylesheet" href="{{asset('/pkms/assets/css/style.css')}}">
    </head>
    <body>

        @yield('content')
        @include('modules.main._footer')

        @yield('scripts')
    </body>
</html>
