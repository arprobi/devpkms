@php

$cache = Cache::remember('user_'. Auth::user()->id, 30, function () {

return [
'average_rating' => Auth::user()->averageRating()->first(),
'unread_message' => Auth::user()->inbox()->where('read', 0)->get()
];

});

@endphp

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="prefix" content="{{env('APP_PREFIX', '')}}">
        <title>{{ config('app.name', 'Laravel') }} - Control Panel</title>

        <!-- Styles -->
        <link href="{{asset('/css/app.css') }}" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
        <!-- Scripts -->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
        <script>
            window.Laravel = {!! json_encode([
                    'csrfToken' => csrf_token(),
            ]) !!}
            ;
            
        </script>
    </head>
    <body style="padding-top: 70px">
        <div id="app">
            <nav class="navbar navbar-inverse navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">

                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/admin') }}">
                            {{ config('app.name', 'Laravel') }} - Control Panel
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="{{route('admin.messages')}}"><i class="fa fa-fw fa-envelope-o"></i>
                                    Inbox &nbsp;
                                    @if($cache['unread_message']->count())
                                    <span class="badge badge-danger">{{$cache['unread_message']->count()}}</span>
                                    @endif
                                </a>
                            </li>
                            <!-- Authentication Links -->
                            @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                            @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="{{route('admin.profile')}}"><i class="fa fa-fw icon-user"></i> Profil</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                            <i class="fa fa-fw icon-logout"></i> Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2 sidebar">
                        <ul class="nav nav-sidebar">
                            <li class="with-border"><a href="javascript:;">Master Data</a></li>
                            <li><a href="{{route('admin.educations.index')}}"><i class="fa fa-fw icon-graduation"></i> Edukasi</a></li>
                            <li><a href="{{route('admin.interests.index')}}"><i class="fa fa-fw icon-tag"></i> Bidang Kompetensi</a></li>
                            <li><a href="{{route('admin.codes.index')}}"><i class="fa fa-fw icon-list"></i> Kodefikasi</a></li>
                            <li><a href="{{route('admin.subcodes.index')}}"><i class="fa fa-fw icon-list"></i> Sub-Kodefikasi</a></li>
                            <li><a href="{{route('admin.kategories')}}"><i class="fa fa-fw fa-tags"></i> Perpres Tags</a></li>
                            <li><a href="{{route('admin.disclaimer')}}"><i class="fa fa-fw fa-pencil-square-o"></i> Disclaimer</a></li>
                            <li><a href="{{route('admin.bantuans')}}"><i class="fa fa-fw fa-question"></i> Bantuan</a></li>
                            <li class="separator"></li>
                            <li class="with-border"><a href="javascript:;">PKMS</a></li>
                            <li><a href="{{route('admin.faqs')}}"><i class="fa fa-fw fa-book"></i> F.A.Q</a></li>
                            <li><a href="{{route('admin.questions')}}"><i class="fa fa-fw icon-question"></i> Pertanyaan</a></li>
                            <li><a href="{{route('admin.usulans')}}"><i class="fa fa-fw fa-hand-paper-o"></i> Usulan</a></li>
                            <li><a href="{{route('admin.perpres')}}"><i class="fa fa-fw fa-institution"></i> Perpres</a></li>
                            <li><a href="{{route('admin.pasals')}}"><i class="fa fa-fw fa-gavel"></i> Perpres Pasal</a></li>
                            <li><a href="{{route('admin.firebase')}}"><i class="fa fa-fw fa-bullhorn"></i> Broadcast Message</a></li>
                            <li><a href="{{route('admin.responses')}}"><i class="fa fa-fw fa-question-circle"></i> Polling</a></li>
                            <li><a href="{{route('admin.highlights')}}"><i class="fa fa-fw fa-bookmark"></i> Highlight Pasal</a></li>
                            <li class="separator"></li>
                            <li class="with-border"><a href="javascript:;">List of Expert</a></li>
                            <li><a href="{{route('admin.experts')}}"><i class="fa fa-fw icon-people"></i> Tenaga Ahli</a></li>
                            <li><a href="{{route('admin.unverified_list')}}"><i class="fa fa-fw icon-user-following"></i> Verifikasi</a></li>
                            <li><a href="{{route('admin.tasks')}}"><i class="fa fa-fw fa-tasks"></i> Tugas</a></li>
                            <li><a href="{{route('admin.articles')}}"><i class="fa fa-fw icon-book-open"></i> Artikel</a></li>
                            <li class="separator"></li>
                            <li class="with-border"><a href="javascript:;">Job Seeker</a></li>
                            <li><a href="{{route('admin.packets')}}"><i class="fa fa-fw fa-barcode"></i> Paket</a></li>
                            <li class="separator"></li>
                            <li class="with-border"><a href="javascript:;">System</a></li>
                            <li><a href="{{route('admin.users')}}"><i class="fa fa-fw icon-people"></i> Pengguna</a></li>
                            <li><a href="{{route('admin.admins')}}"><i class="fa fa-fw icon-key"></i> Admin</a></li>
                            <li><a href="{{route('admin.invitations')}}"><i class="fa fa-fw icon-envelope "></i> Invitations</a></li>
                        </ul>
                    </div>
                    
                    <div class="col-md-10 col-md-offset-2">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

        <!-- Scripts -->
        <script src="{{ asset('/js/app.js') }}"></script>
        @yield('script')
    </body>
</html>
