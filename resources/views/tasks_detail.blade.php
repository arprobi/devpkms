@extends('layouts.app')

@section('content')
<div id="task-detail-page" class="container">
    <div class="row">
        <div class="col-md-2">
            @include('_box_profile')
        </div>

        <div class="col-md-10">

            @include('_incomplete')

            {{-- Breadcrumb  --}}
            <ol class="breadcrumb">
                <li><a href="{{route('expert.tasks')}}"><i class="fa fa-fw fa-tasks"></i> Tugas</a></li>
                <li class="active">Detail - ID : {{$task->id}}, Title: {{$task->title}}</li>
            </ol>

            {{-- Task Detail --}}
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-fw fa-info"></i> Tugas</div>

                @if($task->is_accepted)
                    {!! Former::populate($task) !!}
                    {!! Former::open(route('expert.tasks.update', $task->id)) !!}
                        <div class="panel-body">
                            {!! Former::text('title')->label('Judul')->disabled() !!}
                            {!! Former::textarea('description')->label('Deskripsi')->rows(5)->disabled() !!}

                            @if(!$task->is_completed)
                                {!! Former::number('progress')->max(100)->min(0)->append('%') !!}
                            @else
                                {!! Former::number('progress')->max(100)->min(0)->append('%')->disabled() !!}

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Tgl Mulai Pelaksanaan</label>
                                            <div class="form-control">{{$task->accepted_at}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Tgl Selesai Pelaksanaan</label>
                                            <div class="form-control">{{$task->completed_at}}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Status</label>
                                    <div class="form-control"><i class="fa fa-fw fa-check text-success"></i> Telah selesai</div>
                                </div>
                            @endif
                        </div>

                        @if(!$task->is_completed)
                            <div class="panel-footer">
                                <button class="btn btn-success">Update Progress</button>
                            </div>
                        @endif
                    {!! Former::close() !!}
                @else
                    {!! Former::populate($task) !!}
                    {!! Former::open()->method('get') !!}
                        <div class="panel-body">
                            {!! Former::text('title')->label('Judul')->disabled() !!}
                            {!! Former::textarea('description')->label('Deskripsi')->rows(5)->disabled() !!}
                        </div>
                    {!! Former::close() !!}

                    <div class="panel-footer">
                        <a href="{{route('expert.tasks.accept', $task->id)}}" class="btn btn-accept btn-default"><i class="fa fa-check text-success"></i> Terima</a>
                        <a href="{{route('expert.tasks.deny', $task->id)}}" class="btn btn-deny btn-default"><i class="fa fa-times text-danger"></i> Tolak</a>
                    </div>
                @endif
            </div>

            {{-- Related Articles  --}}
            @if($task->is_accepted)
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa icon-docs"></i> Artikel terkait</div>
                    @if($task->articles->count())
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Penulis</th>
                                        <th>Revisi</th>
                                        <th>Update terakhir</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($task->articles as $article)
                                        <tr>
                                            <td><a href="{{route('expert.articles.view', $article->id)}}" data-toggle="modal" data-target="#modal-view-article">{{$article->title}}</a></td>
                                            <td>{{$article->author->name}}</td>
                                            <td>{{$artile->revision or '-'}}</td>
                                            <td>{{$article->updated_at or $article->created_at}}</td>
                                            <td>{{$article->is_published ? 'Terbit' : 'Belum terbit'}}</td>
                                            <td>
                                                @if(!$task->is_completed)
                                                    {!! Former::open(route('expert.articles.delete', $article->id))->method('delete') !!}
                                                        <a href="{{route('expert.articles.edit', $article->id)}}" class="btn btn-sm btn-default">Edit</a>
                                                        <button class="btn btn-sm btn-danger btn-delete">Hapus</button>
                                                    {!! Former::close() !!}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="panel-body">
                            <p>Tidak ada artikel terkait.</p>
                        </div>
                    @endif
                    <div class="panel-footer">
                        <a href="{{route('expert.tasks.add_article', $task->id)}}" class="btn btn-default"><i class="fa fa-fw fa-plus"></i> Buat Artikel</a>
                    </div>
                </div>

                {{-- Related Comments  --}}
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-fw fa-envelope-o"></i> Pesan terkait</div>
                    <div class="panel-body">
                        @if($task->comments->count())
                            <div class="media-lists comments">
                                @foreach($task->comments as $comment)
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="javascript:;">
                                                <img class="media-object img-circle" height="64px" width="64px" src="{{$comment->author->avatar ? secure_asset('storage/'. $comment->author->avatar) : secure_asset('/img/default_avatar.jpg') }}">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">{{$comment->author->name}} <small>{{$comment->updated_at or $comment->created_at}}</small></h4>
                                            <p>{{$comment->text}}</p>
                                            @if($comment->attachments->count())
                                                <ul class="list-inline">
                                                    @foreach($comment->attachments as $attachment)
                                                        <li><a href="{{secure_asset('storage/'.$attachment->path)}}" target="_blank" class="btn btn-sm btn-default"><i class="fa fa-fw fa-paperclip"></i> {{$attachment->title}}</a></li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <p>Tidak ada pesan terkait.</p>
                        @endif
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Pesan baru</div>
                    {!! Former::open_for_files(route('expert.tasks.add_comment', $task->id)) !!}
                        <div class="panel-body">
                            {!! Former::textarea('text')->label('Pesan')->placeholder('pesan')->rows(5)->required() !!}
                            {!! Former::file('lampiran[]')->label('Lampiran')->multiple()->accept('.jpg, .jpeg, .png, .bmp, .pdf, .docx, .doc, .ppt, .pptx, .zip, .rar, .xls, .xlsx') !!}
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-success"><i class="fa fa-fw fa-send"></i> Kirim pesan</button>
                        </div>
                    {!! Former::close() !!}
                </div>
            @endif
        </div>
    </div>
</div>

{{-- Modal Article --}}
@if($task->is_accepted)

    {{-- Modal View Article  --}}
    <div id="modal-view-article" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content"></div>
        </div>
    </div>
@endif

@endsection
