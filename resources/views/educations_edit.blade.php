@extends('layouts.app')

@section('content')

<div id="educations-page" class="container">

    @include('_incomplete')

    <div class="panel panel-default">
        <div class="panel-heading">Edit Edukasi</div>
        {!! Former::populate($education) !!}
        {!! Former::open(route('expert.educations.update', $education_id)) !!}
            <div class="panel-body">
                {!! Former::select('education_id')->label('Tingkat Pendidikan')->placeholder('Pilih')->fromQuery(PKMS\Models\MasterEducation::get(), 'name', 'id')->required() !!}
                {!! Former::text('name')->label('Nama Institusi Pendidikan')->placeholder('Nama Institusi Pendidikan')->required() !!}
                {!! Former::text('major')->label('Nama Jurusan / Bidang Keahlian')->placeholder('Nama Jurusan / Bidang Keahlian (Opsional)') !!}
                {!! Former::text('gpa')->label('Nilai / Peringkat Kelulusan / GPA')->placeholder('Nilai (Opsional)') !!}
            </div>

            <div class="panel-footer">
                <button class="btn btn-primary">Simpan</button>
                <a href="{{route('expert.educations')}}" class="btn btn-default">Kembali</a>
            </div>
        {!! Former::close() !!}
    </div>
</div>

@endsection
