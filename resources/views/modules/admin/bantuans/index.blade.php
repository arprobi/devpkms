@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">BANTUAN</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-filter"></i> Filters</div>
        <div class="panel-body">
            <div class="pull-left visible-md visible-lg">
                {!! Former::open_inline()->method('get') !!}

                {!! Former::select('often_asked')->class('form-control')
                    ->label('Kategori')
                    ->options(['' => 'Tampilkan semua', '0' => 'Bantuan lainnya', '1' => 'Sering ditanyakan'])
                    ->style('min-width:270px;') !!}

                {!! Former::text('question')->class('form-control')->label('Question')->placeholder('Question')->style('min-width:270px;') !!}

                {!! Former::text('answer')->class('form-control')->label('Answer')->placeholder('Answer')->style('min-width:270px;') !!}

                <button class="btn btn-success"><i class="fa fa-fw fa-filter"></i> Filter</button>
                {!! Former::close() !!}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Daftar Bantuan</div>
        <div class="panel-body">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                    @if($bantuans->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Question</th>
                                    <th>Answer</th>
                                    <th>Jenis Bantuan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($bantuans as $bantuan)
                                <tr>
                                    <td>{!!$bantuan->question!!}</td>
                                    <td>{!!str_limit($bantuan->answer, 150)!!}</td>
                                    <td>{!!$bantuan->kategori!!}</td>
                                    <td>
                                        {!! Former::open(route('admin.bantuans.delete', $bantuan->id))->method('delete') !!}
                                        <a href="{{route('admin.bantuans.edit', $bantuan->id)}}" class="btn btn-sm btn-default">Edit</a>
                                        <button class="btn btn-sm btn-delete btn-danger">Hapus</button>
                                        {!! Former::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-left">
                        <a href="{{route('admin.bantuans.create')}}" class="btn btn-success">Tambah data</a>
                    </div>
                    <div class="pull-right">
                        {{$bantuans->appends(request()->all())->links()}}
                    </div>
                    @else
                    <div class="pull-left">
                        <a href="{{route('admin.bantuans.create')}}" class="btn btn-success">Tambah data</a>
                    </div>
                    <div class="pull-right">
                        Bantuan tidak ditemukan.
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection