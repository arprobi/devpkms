@extends('layouts.admin')

@section('content')
<div id="bantuan-create-page">
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.bantuans')}}">Bantuan</a></li>
        <li class="active">{{ $create ? 'Tambah' : 'Ubah' }} data</li>
    </ol>

    @include('modules.admin._alerts')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-graduation"></i> Master Bantuan</div>

        @if($create)
        {!! Former::open_for_files(route('admin.bantuans.store'))->id('form-input') !!}
        @else
        {!! Former::populate($bantuan) !!}
        {!! Former::open_for_files(route('admin.bantuans.update', $bantuan->id))->id('form-input') !!}
        @endif
        <div class="panel-body">
            
            {!! Former::select('often_asked')
                    ->label('Jenis Bantuan')
                    ->options([
                        '' => 'Pilih kategori',
                        '0' => 'Bantuan lainnya',
                        '1' => 'Sering ditanyakan'
                    ])->required() !!}

            {!! Former::textarea('question')->addClass('summernote')->label('Question')->placeholder('Question') !!}

            {!! Former::textarea('answer')->addClass('summernote')->label('Answer')->placeholder('Answer')->style('min-width:270px;') !!}
            
        </div>

        <div class="panel-footer">
            <button type="button" class="btn btn-success" id="simpan">Simpan</button>
            <a href="{{route('admin.bantuans')}}" class="btn btn-default">Kembali ke index</a>
        </div>

        {!! Former::close() !!}

    </div>
</div>
@endsection

@section('script')

<script type="text/javascript">
    $(document).ready(function() {
        $('#simpan').click(function() {
            if (!$('select[name=often_asked]').val() || !$('textarea[name=question]').val() || !$('textarea[name=answer]').val()) {
                alert('Harap mengisi semua input yang tersedia');
            } else {
                $('#form-input').submit();
            }
        });
    });
</script>

@endsection