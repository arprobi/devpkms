@if(request()->session()->get('error'))
    <div class="alert alert-danger">
        {!! request()->session()->get('error') !!}
    </div>
@endif

@if(request()->session()->get('success'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
        </button>
        {!! request()->session()->get('success') !!}
    </div>
@endif