@extends('layouts.admin')

@section('content')
<div id="pemberitahuan-page">
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.firebase')}}">Broadcast Message</a></li>
        <li class="active">Tambah data</li>
    </ol>

    @include('modules.admin._alerts')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-graduation"></i> Master Broadcast Message</div>

        {!! Former::open(route('admin.firebase.store')) !!}

        <div class="panel-body">
            {!! Former::select('destination')->label('Kontak')
                ->options([0 => 'Semua Pengguna (Admin & Pengguna)', 1 => 'Admin PKMS', 2 => 'Pengguna umum'])
                ->placeholder('Pilih kontak')
                ->required() !!}
            {{-- <div class="row">
                <div class="col-md-4">
                    {!! Former::radios('jadwalkan')->radios([
                        'Ya'    => ['name' => 'jadwalkan', 'value' => '0', 'checked'],
                        'Tidak' => ['name' => 'jadwalkan', 'value' => '1']
                        ])->label('Kirim Sekarang') !!}
                </div>
                <div class="col-md-8" id="waktu" style="display: none;">
                    {!! Former::text('sending_time')->label('Tanggal pengiriman')->placeholder('Tanggal pengiriman') !!}
                </div>
            </div> --}}
            {!! Former::text('title')->label('Judul')->placeholder('Judul')->required() !!}
            {!! Former::textarea('message')->label('Pesan')->placeholder('Pesan')->addRow('15')->required() !!}
        </div>

        <div class="panel-footer">
            <button class="btn btn-success">Simpan</button>
            <a href="{{route('admin.firebase')}}" class="btn btn-default pull-right">Kembali ke index</a>
        </div>

        {!! Former::close() !!}

    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('input:radio').change(function(){
            var value = $("form input[type='radio']:checked").val();
            if (value == 1) {
                $('#waktu').show();
            } else {
                $('#waktu').hide();
            }
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $('#sending_time').datetimepicker({
            locale: 'id',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            defaultDate: new Date(),
            minDate:new Date(),
            format:'YYYY-MM-DD HH:00:00',
        });
    });
</script>
@endsection