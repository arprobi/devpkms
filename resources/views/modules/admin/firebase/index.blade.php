@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">PEMBERITAHUAN/BROADCAST MESSAGE</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading">Daftar Pesan Broadcast</div>
        <div class="panel-body">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                    <p>
                        <form method="get">
                            <div class="row">
                                <div class="col-xs-11">
                                    <input name="q" type="text" class="form-control" placeholder="Pencarian">
                                </div>
                                <div class="col-xs-1">
                                    <button class="btn btn-md btn-success">Cari</button>
                                </div>
                            </div>
                        </form>
                    </p>
                    @if($firebase->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Message</th>
                                    <th>Tujuan</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($firebase as $fire)
                                <tr>
                                    <td>{{$fire->title}}</td>
                                    <td>{{$fire->message}}</td>
                                    @if ($fire->user_id == 0)
                                        <td>Semua</td>
                                    @elseif ($fire->user_id == 1)
                                        <td>Admin PKMS</td>
                                    @else
                                        <td>Pengguna PKMS</td>
                                    @endif
                                    <td>{!!$fire->status!!}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-left">
                        <a href="{{route('admin.firebase.create')}}" class="btn btn-success">Kirim Pesan</a>
                    </div>
                    <div class="pull-right">
                        {{$firebase->appends(request()->all())->links()}}
                    </div>
                    @else
                    <div class="pull-left">
                        <a href="{{route('admin.firebase.create')}}" class="btn btn-success">Kirim Pesan</a>
                    </div>
                    <div class="pull-right">
                        Pemberitahuan tidak ditemukan.
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection