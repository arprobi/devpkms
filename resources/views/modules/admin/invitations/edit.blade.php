@extends('layouts.admin')

@section('content')
<div id="invitation-page">
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-envelope"></i> Invitations</div>
        {!! Former::populate($invitation) !!}
        {!! Former::open(route('admin.invitations.update', $invitation->id)) !!}
            <div class="panel-body">
                {!! Former::text('title')->label('Subject')->placeholder('Subject')->required() !!}
                {!! Former::textarea('description')->label('Deskripsi')->placeholder('Deskripsi')->rows(4) !!}
                <div class="row">
                    <div class="col-md-6">
                        {!! Former::text('from')->data_type('date')->data_date_end_date('0d')->label('Mulai')->placeholder('Mulai')->required() !!}
                    </div>
                    <div class="col-md-6">
                        {!! Former::text('until')->data_type('date')->data_date_end_date('0d')->label('Hingga')->placeholder('Hingga (Kosongkan jika perlu)') !!}
                    </div>
                </div>
                {!! Former::select('invitation_lists[]')->id('lists')->label('Daftar Undangan')->options(array_pluck($invitation->lists, 'email'),array_pluck($invitation->lists, 'email'), true)->help('Tuliskan alamat email yang ingin anda undang bergabung.')->multiple() !!}
            </div>
            <div class="panel-footer">
                <button class="btn btn-default">Simpan</button>
                <a href="{{route('admin.invitations')}}" class="btn btn-default">Kembali</a>
            </div>
        {!! Former::close() !!}
    </div>
</div>

@endsection