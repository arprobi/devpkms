@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">DISCLAIMER</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading">Disclaimer</div>
        <div class="panel-body">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                    @if($disclaimer->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Dibuat Pada</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($disclaimer as $disclaim)
                                <tr>
                                    <td>{{$disclaim->title}}</td>
                                    <td>{{$disclaim->created_at}}</td>
                                    <td>
                                        <a href="{{route('admin.disclaimer.edit', $disclaim->id)}}" class="btn btn-sm btn-default">Edit</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                    <div class="pull-left">
                        <a href="{{route('admin.disclaimer.create')}}" class="btn btn-success">Tambah data</a>
                    </div>
                    <div class="pull-right">
                        Tag tidak ditemukan.
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection