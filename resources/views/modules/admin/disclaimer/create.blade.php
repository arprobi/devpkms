@extends('layouts.admin')

@section('content')
<div id="disclaimer-create-page">
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.disclaimer')}}">Disclaimer</a></li>
        <li class="active">{{ $create ? 'Tambah' : 'Ubah' }} data</li>
    </ol>

    @include('modules.admin._alerts')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-graduation"></i> Master Disclaimer</div>

        @if($create)
        {!! Former::open_for_files(route('admin.disclaimer.store')) !!}
        @else
        {!! Former::populate($disclaimer) !!}
        {!! Former::open_for_files(route('admin.disclaimer.update', $disclaimer->id)) !!}
        @endif
        <div class="panel-body">
            {!! Former::text('title')->label('Judul')->placeholder('Judul')->required() !!}
            
            {!! Former::textarea('content')->addClass('summernote')->label('Isi')->required() !!}
            
            {!! Former::radios('version')->radios([
                'Ya'    => ['name' => 'version', 'value' => '1'],
                'Tidak' => ['name' => 'version', 'value' => '0', 'checked']
                ])->label('Ubah Versi') !!}
        </div>


        <div class="panel-footer">
            <button class="btn btn-success">Simpan</button>
            <a href="{{route('admin.disclaimer')}}" class="btn btn-default pull-right">Kembali ke index</a>
        </div>

        {!! Former::close() !!}

    </div>
</div>
@endsection