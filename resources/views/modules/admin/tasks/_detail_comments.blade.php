{{-- Related Comments  --}}
<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-fw fa-envelope-o"></i> Pesan terkait</div>
    <div class="panel-body">
        @if($task->comments->count())
            <div class="media-lists comments">
                @foreach($task->comments as $comment)
                    <div class="media">
                        <div class="media-left">
                            <a href="javascript:;">
                                <img class="media-object img-circle" height="64px" width="64px" src="{{$comment->author->avatar ? secure_asset('storage/'. $comment->author->avatar) : secure_asset('/img/default_avatar.jpg') }}">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{{$comment->author->name}} <small>{{$comment->updated_at or $comment->created_at}}</small></h4>
                            <p>{{$comment->text}}</p>
                            @if($comment->attachments->count())
                                <ul class="list-inline">
                                    @foreach($comment->attachments as $attachment)
                                        <li><a href="{{route('admin.attachments.download', $attachment->short_url)}}" target="_blank" class="btn btn-sm btn-default"><i class="fa fa-fw fa-paperclip"></i> {{$attachment->title}}</a></li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <p>Tidak ada pesan terkait.</p>
        @endif
    </div>
</div>

@if(!$task->is_completed)
    <div class="panel panel-default">
        <div class="panel-heading">Pesan baru</div>
        {!! Former::open_for_files(route('admin.tasks.add_comment', $task->id)) !!}
            <div class="panel-body">
                {!! Former::textarea('text')->label('Pesan')->placeholder('pesan')->rows(5)->required() !!}
                {!! Former::file('lampiran[]')->label('Lampiran')->multiple()->help('tipe: .jpg, .jpeg, .png, .bmp, .pdf, .docx, .doc, .ppt, .pptx, .zip, .rar, .xls, .xlsx, Ukuran file: 8MB') !!}
            </div>
            <div class="panel-footer">
                <button class="btn btn-success"><i class="fa fa-fw fa-send"></i> Kirim pesan</button>
            </div>
        {!! Former::close() !!}
    </div>
@endif