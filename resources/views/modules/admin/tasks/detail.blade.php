@extends('layouts.admin')

@section('content')
<div id="admin-task-detail-page">

    {{-- Breadcrumb  --}}
    <ol class="breadcrumb">
        <li><a href="{{route('admin.tasks')}}"><i class="fa fa-fw fa-tasks"></i> Tugas</a></li>
        <li class="active">Detail - ID : {{$task->id}}, Title: {{$task->title}}</li>
    </ol>

    {{-- Task Detail --}}
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-info"></i> Tugas</div>

        @if($task->is_accepted)
            @include('modules.admin.tasks._detail_accepted')
        @else
            @include('modules.admin.tasks._detail_waiting')
        @endif
    </div>

    {{-- Attachment --}}
    @include('modules.admin.tasks._detail_attachment')

    {{-- Related Articles  --}}
    @if($task->is_accepted)
        @include('modules.admin.tasks._detail_articles')

        @include('modules.admin.tasks._detail_comments')

    @endif
</div>

{{-- Modals --}}
@if($task->is_accepted)
    @include('modules.admin.tasks._detail_modals')
@endif

@endsection
