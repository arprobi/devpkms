{{-- Modal View Article  --}}
<div id="modal-view-article" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content"></div>
    </div>
</div>

{{-- Modal Approve Task  --}}
<div id="modal-approve-task" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Approve tugas</h4>
            </div>
            {!! Former::open(route('admin.tasks.approve')) !!}
                <div class="modal-body">
                    <div id="approve-task" class="form-group">
                        <label>Tugas</label>
                        <div class="form-control"></div>
                    </div>
                    {!! Former::hidden('task_id')->id('task_id')->required() !!}
                    {!! Former::select('rating[rating]')->label('Rating')->options([
                        '1' => 'Buruk Sekali',
                        '2' => 'Buruk',
                        '3' => 'Cukup',
                        '4' => 'Baik',
                        '5' => 'Baik Sekali'
                    ])->required() !!}
                    {!! Former::textarea('rating[review]')->label('Komentar') !!}
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-primary">Simpan</button>
                </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>