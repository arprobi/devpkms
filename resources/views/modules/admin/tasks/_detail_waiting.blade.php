{!! Former::populate($task) !!}
{!! Former::open_for_files(route('admin.tasks.update', $task->id)) !!}
    <div class="panel-body">
        {!! Former::text('title')->label('Judul')->required() !!}
        {!! Former::textarea('description')->label('Deskripsi')->rows(5) !!}
        {{-- {!! Former::file('lampiran[]')->label('Lampiran')->help('ukuran maksimum 10MB untuk tiap lampiran')->multiple() !!} --}}
    </div>
    <div class="panel-footer">
        <button class="btn btn-default">Update</button>
        <a href="{{route('admin.tasks.cancel', $task->id)}}" class="btn btn-cancel btn-danger">Batalkan</a>
    </div>
{!! Former::close() !!}