{!! Former::populate($task) !!}
{!! Former::open(route('admin.tasks.update', $task->id)) !!}
    <div class="panel-body">
        {!! Former::text('title')->label('Judul')->required() !!}
        {!! Former::textarea('description')->label('Deskripsi')->rows(5) !!}

        @if(!$task->is_completed)
            {!! Former::number('progress')->max(100)->min(0)->append('%') !!}
        @else
            {!! Former::number('progress')->max(100)->min(0)->append('%')->disabled() !!}

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Tgl Mulai Pelaksanaan</label>
                        <div class="form-control">{{$task->accepted_at}}</div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Tgl Selesai Pelaksanaan</label>
                        <div class="form-control">{{$task->completed_at}}</div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label>Status</label>
                <div class="form-control"><i class="fa fa-fw fa-check text-success"></i> Telah selesai</div>
            </div>
        @endif
    </div>

    @if(!$task->is_completed)
        <div class="panel-footer">
            <button class="btn btn-default">Update</button>
            <div class="pull-right">
                <a href="javascript:;" data-task-id="{{$task->id}}" data-task-title="{{$task->title}}" data-toggle="modal" data-target="#modal-approve-task" class="btn btn-approve btn-sm btn-success">Approve Tugas</a>
            </div>
        </div>
    @endif
{!! Former::close() !!}