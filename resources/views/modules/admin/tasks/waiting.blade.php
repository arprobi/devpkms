@extends('layouts.admin')

@section('content')
<div id="task-unassigned-list-page">
    <ol class="breadcrumb flat">
        <li class="active"><a href="{{route('admin.tasks')}}">Tugas</a></li>
        <li class="active">Menunggu persetujuan</li>
    </ol>

    <div class="panel panel-default">
        <div class="panel-heading no-border-bottom">Tugas</div>
        <div class="panel-heading with-tab">
            <ul class="nav nav-tabs hidden-xs" role="tablist">
                <li role="presentation"><a href="{{route('admin.tasks')}}">Belum ditugaskan</a></li>
                <li role="presentation" class="active"><a href="{{route('admin.tasks.waiting_approval')}}">Menunggu Persetujuan</a></li>
                <li role="presentation"><a href="{{route('admin.tasks.ongoing')}}">Sedang Berjalan</a></li>
                <li role="presentation"><a href="{{route('admin.tasks.complete')}}">Selesai</a></li>
            </ul>
            <ul class="nav nav-tabs visible-xs" role="tablist">
                <li role="presentation" class="active dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-controls="tab-dropdown" aria-expanded="false">Pilih <span class="caret"></span></a>
                    <ul class="dropdown-menu" id="tab-dropdown">
                        <li role="presentation"><a href="{{route('admin.tasks')}}">Belum ditugaskan</a></li>
                        <li role="presentation"><a href="{{route('admin.tasks.waiting_approval')}}">Menunggu Persetujuan</a></li>
                        <li role="presentation"><a href="{{route('admin.tasks.ongoing')}}">Sedang Berjalan</a></li>
                        <li role="presentation"><a href="{{route('admin.tasks.complete')}}">Selesai</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        @if($tasks->count())
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Judul</th>
                            <th>Deskripsi</th>
                            <th>Diposting pada tgl</th>
                            <th>Diposting oleh</th>
                            <th>Ditugaskan kepada</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td>{{$task->id}}</td>
                                <td><a href="{{route('admin.tasks.detail', $task->id)}}">{{$task->title}}</a></td>
                                <td>{{str_limit($task->description, 50)}}</td>
                                <td>{{$task->created_at}}</td>
                                <td><a href="{{route('admin.experts.detail', $task->postedby->id)}}">{{$task->postedby->name}}</a></td>
                                <td><a href="{{route('admin.experts.detail', $task->assignedto->id)}}">{{$task->assignedto->name}}</a></td>
                                <td>
                                    <a href="{{route('admin.tasks.detail', $task->id)}}" class="btn btn-sm btn-default">Detil</a>
                                    <a href="{{route('admin.tasks.assign.page', $task->id)}}" class="btn btn-sm btn-default">Penugasan</a>
                                    {{-- <a href="javascript:;" class="btn btn-assign btn-sm btn-default" data-task-id="{{$task->id}}" data-title="{{$task->title}}" data-desc="{{$task->description}}" data-toggle="modal" data-target="#modal-assign-task">Penugasan</a> --}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="panel-footer">{!! $tasks->links() !!}</div>
        @else
            <div class="panel-body">
                Tidak ada tugas.
            </div>
        @endif

    </div>

    <a href="javascript:;" data-toggle="modal" data-target="#modal-new-task" class="btn btn-success">Buat tugas baru</a>
</div>


{{-- Modal New Task  --}}
<div id="modal-new-task" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tugas baru</h4>
            </div>
            {!! Former::open(route('admin.tasks.create'))->method('post') !!}
                <div class="modal-body">
                    {!! Former::text('title')->label('Judul')->placeholder('Judul')->required() !!}
                    {!! Former::textarea('description')->label('Deskripsi')->placeholder('Deskripsi tugas')->required() !!}
                    {!! Former::file('lampiran[]')->label('Lampiran')->help('ukuran maksimum 10MB untuk tiap lampiran')->multiple() !!}
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-primary">Simpan</button>
                </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>
@endsection
