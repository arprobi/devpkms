@extends('layouts.admin')

@section('content')
<div id="admin-task-assign-page">
    <ol class="breadcrumb flat">
        <li class="active"><a href="{{route('admin.tasks')}}">Tugas</a></li>
        <li class="active">Detail - ID : {{$task->id}}, Title: {{$task->title}}</li>
    </ol>

    {{-- Task Detail --}}
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-info"></i> Tugas</div>
        <div class="panel-body">
            <div class="form-group">
                <label>Judul</label>
                <div class="form-control">{{$task->title}}</div>
            </div>
            <div class="form-group">
                <label>Deskripsi</label>
                <div class="form-control textarea">{{$task->description}}</div>
            </div>
            <div class="form-group">
                <label>Lampiran</label>
                @if($task->attachments->count())
                    <ul class="list-inline">
                        @foreach($task->attachments as $attachment)
                            <li><a href="{{secure_asset('storage/'. $attachment->path)}}" target="_blank" class="btn btn-sm btn-default">{{$attachment->title}}</a></li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
        @if($task->is_assigned)
            <div class="panel-heading">Ditugaskan Kepada</div>
            <div class="panel-body">
                <div class="media">
                    <div class="media-left">
                        <img src="{{ $task->assignedto->avatar ? secure_asset('storage/'.$task->assignedto->avatar) : secure_asset('/img/default_avatar.jpg') }}" alt="Avatar" width="64px" height="64px">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">
                            <a href="{{route('admin.experts.detail', $task->assignedto->id)}}" target="_blank">{{$task->assignedto->name}}</a> <code>{{$task->assignedto->email}}</code>
                        </h4>
                        @if(!$task->is_accepted)
                            <p>
                                <span class="label label-danger">Menunggu Konfirmasi</span>
                            </p>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered table-condensed">
                                <thead>
                                    <tr>
                                        <th>Belum diterima</th>
                                        <th>Ditolak</th>
                                        <th>Berjalan</th>
                                        <th>Selesai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td align="center">{{$task->assignedto->tasks->where('is_accepted',0)->count()}} Tugas</td>
                                        <td align="center">{{$task->assignedto->task_history->count()}} Tugas</td>
                                        <td align="center">{{$task->assignedto->tasks->where('is_completed',0)->count()}} Tugas</td>
                                        <td align="center">{{$task->assignedto->tasks->where('is_completed',1)->count()}} Tugas</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Daftar tenaga ahli</div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th colspan="2">Nama</th>
                        <th class="text-center">Belum diterima</th>
                        <th class="text-center">Ditolak</th>
                        <th class="text-center">Berjalan</th>
                        <th class="text-center">Selesai</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td width="64">
                                <img src="{{ $user->avatar ? secure_asset('storage/'.$user->avatar) : secure_asset('/img/default_avatar.jpg') }}" alt="Avatar" width="64px" height="64px">
                            </td>
                            <td>
                                <a href="{{route('admin.experts.detail', $user->id)}}" target="_blank">{{$user->name}}</a> <code>{{$user->email}}</code>
                            </td>
                            <td align="center">{{$user->tasks->where('is_accepted',0)->count()}} Tugas</td>
                            <td align="center">{{$user->task_history->count()}} Tugas</td>
                            <td align="center">{{$user->tasks->where('is_completed',0)->count()}} Tugas</td>
                            <td align="center">{{$user->tasks->where('is_completed',1)->count()}} Tugas</td>
                            <td>
                                {!! Former::open()  !!}
                                    {!! Former::hidden('user_id')->value($user->id) !!}
                                    <button class="btn btn-assign btn-sm btn-default">Penugasan</button>
                                {!! Former::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
