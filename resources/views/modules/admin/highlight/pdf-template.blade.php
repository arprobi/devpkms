<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <style type="text/css">
            
        </style>
        <title>{{ strip_tags($highlight->words) }}</title>
    </head>
    <body>
        <page size="A4">
            <div style="margin: 10px;">
                <table style="margin-top: 10px; margin-bottom: 40px; border-bottom: 2px solid black;">
                    <tr>
                        <td>
                            <img style="width: 200px;" src="{{ asset('/img/new_lkpp.jpg') }}">
                        </td>
                        <td style="text-align: center;">
                            <h2>Lembaga Kebijakan Pengadaan Barang/Jasa Pemerintah (LKPP)</h2>
                            <p>Procurement Knowledge Management System</p>
                        </td>
                    </tr>
                </table>

                <table style="margin: 10px; margin-bottom: 30px;">
                    <tr>
                        <td style="font-weight: bold; width: 30%">Perpres</td>
                        <td style="width: 10%;">:</td>
                        <td>{{ $highlight->pasal->perpres->nama_perpres }}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; width: 30%">Pasal</td>
                        <td style="width: 10%;">:</td>
                        <td>{{ strip_tags($highlight->pasal->nama_pasal) }}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; width: 30%">Kata</td>
                        <td style="width: 10%;">:</td>
                        <td>{{ strip_tags($highlight->words) }}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; width: 30%">Penjelasan</td>
                        <td style="width: 10%;">:</td>
                        <td>{{ $highlight->content }}</td>
                    </tr>
                </table>

                <h4 style="margin-top: 10px; margin-bottom: 20px; margin-left: 10px;">Tabel Diskusi Admin</h4>

                @if($highlight->comments->count())
                <table style="border-collapse: collapse; margin-right: 10px; margin-left: 10px;">
                    <thead>
                        <tr style="border: 1px solid black;">
                            <th style="border: 1px solid black; width: 30%;">USER</th>
                            <th style="border: 1px solid black; widht: 20%">EMAIL</th>
                            <th style="border: 1px solid black; widht: 30%">DISKUSI</th>
                            <th style="border: 1px solid black; widht: 20%">TANGGAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($highlight->comments as $comment)
                        <tr style="border: 1px solid black;">
                            <td style="border:1px solid black; width: 20%">{{$comment->user->name}}</td>
                            <td style="border:1px solid black; width: 20%">{{$comment->user->email}}</td>
                            <td style="border:1px solid black; width: 35%">{{$comment->comment}}</td>
                            <td style="border:1px solid black; width: 15%">{{$comment->created_at}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                    <p>Tidak ada diskusi</p>
                @endif
            </div>
        </page>
    </body>
</html>