@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">DETAIL HIGHLIGHT</li>
    </ol>

    <div class="panel panel-default">
        <div class="panel-heading">
            <p>
                <i class="fa fa-fw fa-gavel"> </i> Highlight {{strip_tags($highlight->pasal->nama_pasal)}}
                @if($highlight->comments->count())
                <a href="{{route('admin.highlights.export', $highlight->id)}}" class="btn btn-sm btn-info pull-right btn-exprt"><i class="fa fa-fw fa-file-pdf-o"></i> Eksport PDF</a>
                @endif
            </p>
        </div>
        <div class="panel-body">
            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <label>Kata di Sorot</label>
                    <div class="form-control-static">{{ strip_tags($highlight->words) }}</div>
                </div>
                <div class="form-group">
                    <label>Nama Pengguna</label>
                    <div class="form-control-static">{{$highlight->user->name}}</div>
                </div>
                <div class="form-group">
                    <label>Perpres</label>
                    <div class="form-control-static">{{$highlight->pasal->perpres->nama_perpres}}</div>
                </div>
                <div class="form-group">
                    <label>Nama Pasal</label>
                    <div class="form-control-static">{{strip_tags($highlight->pasal->nama_pasal)}}</div>
                </div>
                <div class="form-group">
                    <label>Tanggal</label>
                    <div class="form-control-static">{{date_format($highlight->created_at,"d-m-Y H:i:s")}}</div>
                </div>
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
                <div class="form-group">
                    <label>Penjelasan</label>
                    <div class="form-control-static">{{$highlight->content}}</div>
                </div>
                @if($highlight->comments->count())
                <div class="form-group">
                    <label>Diskusi</label>
                    <div class="form-control-static">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Nama User</th>
                                        <th>Jawaban</th>
                                    </tr>
                                </thead>
                                <tbody id="list-comment">
                                    @foreach($highlight->comments->take(5) as $comment)
                                    <tr>
                                        <td>{{ $comment->user->name }}</td>
                                        <td>{{ $comment->comment }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <a href="#" id="next-comment" data-offset="5" data-id="{{$highlight->id}}">Selanjutnya</a>

                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {

        // Loadmore btn
        $('#next-comment').on('click', function() {
            var offset  = $(this).data('offset');
            var id      = $(this).data('id');
            
            $(this).data('offset', offset + 5);

            $.ajax({
                url: '/api/comment/highlight/'+id,
                type: 'GET',
                data: {offset: offset},
            })
            .done(function(response) {
                if (response.length) {
                    $.each(response, function(index, val) {
                        $('#list-comment').append('<tr><td>'+ val.user.name +'</td><td>'+ val.comment +'</td></tr>');
                    });
                }else {
                    $('#next-comment').hide();
                }
            });
            
        });

    });
</script>
@endsection