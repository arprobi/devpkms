@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">HIGHLIGHT PASAL</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-filter"></i> Filters</div>
        <div class="panel-body">
            <div class="pull-left visible-md visible-lg">
                {!! Former::open_inline()->method('get') !!}

                {!! Former::select('perpres')->class('form-control')->label('Perpres')->options(['' => 'Pilih perpres'])->fromQuery(PKMS\Models\Perpres::get(), 'nama_perpres', 'id')->style('min-width:270px;') !!}

                <button class="btn btn-success"><i class="fa fa-fw fa-filter"></i> Filter</button>
                {!! Former::close() !!}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-list"></i> Daftar highlight</div>
        <div class="panel-body">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                    <p>
                        <form method="get">
                            <div class="row">
                                <div class="col-xs-11">
                                    <input name="q" type="text" class="form-control" placeholder="Pencarian" value="{{$query}}">
                                </div>
                                <div class="col-xs-1">
                                    <button class="btn btn-md btn-success">Cari</button>
                                </div>
                            </div>
                        </form>
                    </p>
                    @if($highlights->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Pasal</th>
                                    <th>User</th>
                                    <th>Kata Highlight</th>
                                    <th>Penjelasan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($highlights as $highlight)
                                <tr>
                                    <td>{!!strip_tags($highlight->pasal->nama_pasal)!!}</td>
                                    <td>{{$highlight->user->name}}</td>
                                    <td>{{ strip_tags($highlight->words) }}</td>
                                    <td>{!!$highlight->content!!}</td>
                                    <td>
                                        <a href="{{route('admin.highlights.show', $highlight->id)}}" data-id="{{$highlight->id}}" class="btn btn-sm btn-info">Detail</a>
                                        <button data-url="{{route('admin.highlights.delete', $highlight->id)}}" data-id="{{$highlight->id}}" class="btn btn-sm btn-danger btn-hapus">Hapus</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-right">
                        {{$highlights->appends(request()->all())->links()}}
                    </div>
                    @else
                    <div class="pull-right">
                        Pemberitahuan tidak ditemukan.
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection