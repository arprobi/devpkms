@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.codes.index')}}">Sub Kodefikasi</a></li>
        <li class="active">Tambah data</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-graduation"></i> Master Kodefikasi Artikel</div>

        {!! Former::open(route('admin.subcodes.store')) !!}

        <div class="panel-body">
            {!! Former::select('code')->label('Kode')->fromQuery(PKMS\Models\MasterArticleCode::get(), 'name', 'id')->required() !!}
            {!! Former::text('name')->label('Sub-Kode')->placeholder('Nama Sub-Kode')->required() !!}
        </div>

        <div class="panel-footer">
            <button class="btn btn-success">Simpan</button>
            <a href="{{route('admin.subcodes.index')}}" class="btn btn-default">Kembali ke index</a>
        </div>

        {!! Former::close() !!}

    </div>
</div>
@endsection
