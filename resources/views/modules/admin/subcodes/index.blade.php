@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">Sub Kodefikasi</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-graduation"></i> Master Sub-Kodefikasi Artikel</div>

        @if($codes->count())
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Sub-Kode</th>
                            <th width="200"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($codes as $code)
                            <tr>
                                <td>{{$code->articlecode->name}}</td>
                                <td>{{$code->name}}</td>
                                <td align="right">
                                    {!! Former::open(route('admin.subcodes.destroy', $code->id))->method('delete') !!}
                                        <a href="{{route('admin.subcodes.edit', $code->id)}}" class="btn btn-sm btn-default">Edit</a>
                                        <button class="btn btn-sm btn-delete btn-danger">Hapus</button>
                                    {!! Former::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="panel-footer">
                    <div class="pull-left">
                        <a href="{{route('admin.subcodes.create')}}" class="btn btn-success">Tambah data</a>
                    </div>
                    <div class="pull-right">
                        {!! $codes->links() !!}
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        @else
            <div class="panel-body">
                Belum ada data.
            </div>
        @endif
    </div>
</div>
@endsection
