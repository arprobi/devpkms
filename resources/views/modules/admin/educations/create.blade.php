@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.educations.index')}}">Edukasi</a></li>
        <li class="active">Tambah data</li>
    </ol>

    @include('modules.admin._alerts')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-graduation"></i> Master Edukasi</div>

        {!! Former::open(route('admin.educations.store')) !!}

        <div class="panel-body">
            {!! Former::text('code')->label('Kode')->placeholder('Kode. Contoh : SD, SMP, SMA, S1, S2, dst..')->required() !!}
            {!! Former::text('name')->label('Jenjang Pendidikan')->placeholder('Jenjang Pendidikan. Contoh : Sekolah Dasar, dst...')->required() !!}
        </div>

        <div class="panel-footer">
            <button class="btn btn-success">Simpan</button>
            <a href="{{route('admin.educations.index')}}" class="btn btn-default">Kembali ke index</a>
        </div>

        {!! Former::close() !!}

    </div>
</div>
@endsection
