@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">Edukasi</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-graduation"></i> Master Edukasi</div>

        @if($educations->count())
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th width="200"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($educations as $education)
                            <tr>
                                <td>{{$education->code}}</td>
                                <td>{{$education->name}}</td>
                                <td align="right">
                                    {!! Former::open(route('admin.educations.destroy', $education->id))->method('delete') !!}
                                        <a href="{{route('admin.educations.edit', $education->id)}}" class="btn btn-sm btn-default">Edit</a>
                                        <button class="btn btn-sm btn-delete btn-danger">Hapus</button>
                                    {!! Former::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="panel-footer">
                    <div class="pull-left">
                        <a href="{{route('admin.educations.create')}}" class="btn btn-success">Tambah data</a>
                    </div>
                    <div class="pull-right">
                        {!! $educations->links() !!}
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        @else
            <div class="panel-body">
                Belum ada master edukasi.
            </div>
        @endif
    </div>
</div>
@endsection
