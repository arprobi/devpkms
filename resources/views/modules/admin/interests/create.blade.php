@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.interests.index')}}">Bidang Kompetensi</a></li>
        <li class="active">Tambah data</li>
    </ol>

    @include('modules.admin._alerts')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-graduation"></i> Master Bidang Kompetensi</div>

        {!! Former::open(route('admin.interests.store')) !!}

        <div class="panel-body">
            {!! Former::text('name')->label('Bidang Kompetensi')->placeholder('Bidang Kompetensi')->required() !!}
        </div>

        <div class="panel-footer">
            <button class="btn btn-success">Simpan</button>
            <a href="{{route('admin.interests.index')}}" class="btn btn-default">Kembali ke index</a>
        </div>

        {!! Former::close() !!}

    </div>
</div>
@endsection
