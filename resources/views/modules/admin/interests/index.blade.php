@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">Bidang Kompetensi</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-graduation"></i> Master Bidang Kompetensi</div>

        @if($interests->count())
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Bidang Kompetensi</th>
                            <th width="200"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($interests as $interest)
                            <tr>
                                <td>{{$interest->name}}</td>
                                <td align="right">
                                    {!! Former::open(route('admin.interests.destroy', $interest->id))->method('delete') !!}
                                        <a href="{{route('admin.interests.edit', $interest->id)}}" class="btn btn-sm btn-default">Edit</a>
                                        <button class="btn btn-sm btn-delete btn-danger">Hapus</button>
                                    {!! Former::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="panel-footer">
                    <div class="pull-left">
                        <a href="{{route('admin.interests.create')}}" class="btn btn-success">Tambah data</a>
                    </div>
                    <div class="pull-right">
                        {!! $interests->links() !!}
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        @else
            <div class="panel-body">
                Belum ada master minat.
            </div>
        @endif
    </div>
</div>
@endsection
