@extends('layouts.admin')

@section('content')
<div id="admin-packet-detail-page">

    {{-- Breadcrumb  --}}
    <ol class="breadcrumb">
        <li><a href="{{route('admin.packets')}}"><i class="fa fa-fw fa-packets"></i> Paket</a></li>
        <li class="active">Detail - ID : {{$packet->id}}, Title: {{$packet->title}}</li>
    </ol>

    @include('modules.admin._alerts')

    {{-- packet Detail --}}
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-info"></i> Paket</div>
        @include('modules.admin.packets._detail_static')
    </div>
    {{-- Attachment --}}
    @include('modules.admin.packets._detail_attachment')
    @include('modules.admin.packets._detail_nego')

</div>


@endsection
