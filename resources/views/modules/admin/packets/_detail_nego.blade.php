{{-- Related Comments  --}}
<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-fw fa-envelope-o"></i> Pesan terkait</div>
    <div class="panel-body">
        @if($packet->negotiations->count())
        <div class="media-lists comments">

            @foreach($packet->negotiations as $comment)
            <div class="media">
                <div class="media-left">
                    <a href="javascript:;">
                        <img class="media-object img-circle" height="64px" width="64px" src="{{$comment->author->avatar ? secure_asset('storage/'. $comment->author->avatar) : secure_asset('/img/default_avatar.jpg') }}">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">{{$comment->author->name}} <small>{{$comment->updated_at or $comment->created_at}}</small></h4>
                    <p>{{$comment->text}}</p>
                    <b>Harga Nego : Rp. {{ number_format($comment->pivot->offer,0,',','.') }} ( {{Terbilang::make($comment->pivot->offer, ' rupiah')}})</b>
                </div>
            </div>
            @endforeach
        </div>
        @else
        <p>Tidak ada pesan nego terkait.</p>
        @endif
    </div>
</div>

@if(!$packet->is_accepted)
<div class="panel panel-default">
    <div class="panel-heading">Negosiasi</div>
    {!! Former::open_for_files() !!}

    <div class="panel-body">
        {!! Former::text('offer')->class('form-control number')->label('Harga Nego')->required() !!}
        <div class='form-group'>
            <label>Terbilang</label>
            <p class='form-control-static' id='terbilang'>Nol Rupiah</p>
        </div>
        {!! Former::textarea('text')->label('Pesan')->placeholder('pesan')->rows(5)->required() !!}
    </div>
    <div class="panel-footer">
        <button class="btn btn-success"><i class="fa fa-fw fa-send"></i> Kirim pesan</button>
    </div>
    {!! Former::close() !!}
</div>
@endif