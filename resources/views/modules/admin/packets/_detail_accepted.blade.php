{!! Former::populate($packet) !!}
{!! Former::open(route('admin.packets.update', $packet->id)) !!}
<div class="panel-body">
    <div class="row">
        <div class="col-md-6">
            {!! Former::text('title')->label('Nama Paket Pengerjaan')->required() !!}
            {!! Former::select('instansi_id')->options($agencies)->placeholder('Pilih')->label('Instansi')->required() !!}
            {!! Former::select('tahun_anggaran')->label('Tahun Anggaran')->placeholder('Pilih')->options(array_combine(range(date('Y')+2, date('Y')-2),range(date('Y')+2, date('Y')-2)))->required() !!}
            {!! Former::text('anggaran')->class('form-control number')->label('Anggaran')->required() !!}
            <div class='form-group'>
                <label>Terbilang</label>
                <p class='form-control-static' id='terbilang'>{{ Terbilang::make($packet->anggaran, ' Rupiah') }}</p>
            </div>
            {!! Former::select('jenis_pengadaan')->options(['jasa konsultasi' => 'Jasa Konsultasi','jasa lainnya'=>'Jasa Lainnya'])->label('Jenis Pengadaan')->required() !!}    
        </div>
        <div class="col-md-6">
            {!! Former::textarea('description')->label('Lingkup Pengerjaan Paket')->rows(5) !!}
            {!! Former::text('satuan_kerja')->label('Satuan Kerja')->required() !!}
            {!! Former::text('kode_anggaran')->label('Kode Anggaran')->required() !!}
            <div class="row">
                <div class="col-sm-6">
                    {!! Former::select('jangka_waktu')->options(array_combine(range(1,31),range(1,31)))->disabled(); !!}
                </div>
                <div class="col-sm-6">
                    {!! Former::select('interval_waktu')->options(['hari' => 'Hari','minggu' => 'Minggu','bulan'=>'Bulan','tahun'=>'Tahun'])->disabled(); !!}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    {!! Former::select('cara_pembayaran')->options(['sekaligus' => 'Sekaligus','bulanan' => 'Bulanan','termin'=>'Termin'])->label('Cara Pembayaran')->disabled(); !!}
                </div>
                <div class="col-sm-6">
                    {!! Former::text('termin')->id('termin')->disabled(); !!}
                </div>
            </div>
        </div>
    </div>
</div>
<div class='panel-body'>
    <div class="row">
        <div class="col-sm-6        ">
            <h4>PPK</h4>
            {!! Former::text('ppk_nama')->label('Nama')->required() !!}
            {!! Former::text('ppk_nip')->label('NIP')->required() !!}
            {!! Former::text('ppk_jabatan')->label('Jabatan')->required() !!}
        </div>
        <div class="col-sm-6">
            <h4>Pejabat Pengadaan</h4>
            {!! Former::text('pejabat_nama')->label('Nama')->required() !!}
            {!! Former::text('pejabat_nip')->label('NIP')->required() !!}
            {!! Former::text('pejabat_jabatan')->label('Jabatan')->required() !!}
        </div>
    </div>                        
    <div class="row">
        <h4>Lokasi Pengadaan</h4>
        <div class="col-sm-6">
            {!! Former::select('province_code')->label('Propinsi')->placeholder('Pilih')->fromQuery(PKMS\Models\Province::get(),'name','code')->required() !!}
            {!! Former::select('city_code')->label('Kabupaten/Kota')->placeholder('Pilih')->disabled()->required() !!}
        </div>
        <div class="col-sm-6">
            {!! Former::textarea('detail_lokasi')->label('Detail Lokasi')->rows(5) !!}
        </div>
    </div>
</div>
<div class='panel-body'>
    <div class="row controls">
        <div class="col-sm-6 form">
            <h4>Syarat & Bidang Kompetensi</h4>
            @if($packet->qualifications->count())
            @foreach($packet->qualifications as $qualification)
            <div class="entry input-group">
                <input class="form-control" name="qualifications[update][{{$qualification->id}}]" value="{{ $qualification->name }}" type="text" placeholder="Syarat" />
                <span class="input-group-btn">
                    @if($loop->last)
                    <button class="btn btn-success btn-add" type="button">
                        <span class="glyphicon glyphicon-plus"></span>
                    </button>
                    @else
                    <button class="btn btn-danger btn-remove" type="button">
                        <span class="glyphicon glyphicon-minus"></span>
                    </button>
                    @endif
                </span>
            </div>
            @endforeach
            @endif
        </div>
        <div class="col-sm-6">
            {!! Former::select('interests')->name('interests[]')->label('Bidang Kompetensi')->fromQuery(PKMS\Models\Interest::get(),'name','id')->multiple() !!}
            {!! Former::text('metode_pengadaan')->label('Metode Pengadaan')->value('Pengadaan Langsung')->readonly() !!}
            {!! Former::text('jenis_penyedia')->label('Jenis Penyedia')->value('Perorangan')->readonly() !!}    
        </div>
    </div>
</div>    
<div class="panel-body">
    @if(!$packet->is_completed)
    {!! Former::number('progress')->max(100)->min(0)->append('%') !!}
    @else
    {!! Former::number('progress')->max(100)->min(0)->append('%')->disabled() !!}

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Tgl Mulai Pelaksanaan</label>
                <div class="form-control">{{$packet->accepted_at}}</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Tgl Selesai Pelaksanaan</label>
                <div class="form-control">{{$packet->completed_at}}</div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label>Status</label>
        <div class="form-control"><i class="fa fa-fw fa-check text-success"></i> Telah selesai</div>
    </div>
    @endif
</div>

@if(!$packet->is_completed)
<div class="panel-footer">
    <button class="btn btn-default">Update</button>
    <div class="pull-right">
        <a href="javascript:;" data-packet-id="{{$packet->id}}" data-packet-title="{{$packet->title}}" data-toggle="modal" data-target="#modal-approve-packet" class="btn btn-approve btn-sm btn-success">Approve Paket</a>
    </div>
</div>
@endif
{!! Former::close() !!}