@extends('layouts.admin')

@section('content')
<div id="packet-unassigned-list-page">
    <ol class="breadcrumb flat">
        <li class="active"><a href="{{route('admin.packets')}}">Paket</a></li>
        <li class="active">Sedang Berjalan</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading no-border-bottom">Paket</div>
        <div class="panel-heading with-tab">
            <ul class="nav nav-tabs hidden-xs" role="tablist">
                <li role="presentation"><a href="{{route('admin.packets')}}">Paket Baru</a></li>
                <li role="presentation"><a href="{{route('admin.packets.publish')}}">Sudah Diumumkan/Evaluasi</a></li>
                <li role="presentation"><a href="{{route('admin.packets.waiting_approval')}}">Menunggu Persetujuan</a></li>
                <li role="presentation" class="active"><a href="{{route('admin.packets.ongoing')}}">Sedang Berjalan</a></li>
                <li role="presentation"><a href="{{route('admin.packets.complete')}}">Selesai</a></li>
            </ul>
            <ul class="nav nav-tabs visible-xs" role="tablist">
                <li role="presentation" class="active dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-controls="tab-dropdown" aria-expanded="false">Pilih <span class="caret"></span></a>
                    <ul class="dropdown-menu" id="tab-dropdown">
                        <li role="presentation"><a href="{{route('admin.packets')}}">Paket Baru</a></li>
                        <li role="presentation"><a href="{{route('admin.packets.publish')}}">Sudah Diumumkan/Evaluasi</a></li>
                        <li role="presentation"><a href="{{route('admin.packets.waiting_approval')}}">Menunggu Persetujuan</a></li>
                        <li role="presentation"><a href="{{route('admin.packets.ongoing')}}">Sedang Berjalan</a></li>
                        <li role="presentation"><a href="{{route('admin.packets.complete')}}">Selesai</a></li>
                    </ul>
                </li>
            </ul>
        </div>

        @if($packets->count())
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Paket</th>
                        <th>Deskripsi</th>
                        <th>Progress</th>
                        <th>Diposting pada tgl</th>
                        <th>Diposting Oleh</th>
                        <th>Dikerjakan Oleh</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($packets as $packet)
                    <tr>
                        <td>{{$packet->id}}</td>
                        <td><a href="{{route('admin.packets.detail', $packet->id)}}">{{$packet->title}}</a></td>
                        <td>{{str_limit($packet->description, 50)}}</td>
                        <td>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="{{$packet->progress}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$packet->progress}}%;">{{$packet->progress}}%</div>
                            </div>
                        </td>
                        <td>{{$packet->created_at}}</td>
                        <td><a href="{{route('admin.experts.detail', $packet->postedby->id)}}">{{$packet->postedby->name}}</a></td>
                        <td><a href="{{route('admin.experts.detail', $packet->assignedto->id)}}">{{$packet->assignedto->name}}</a></td>
                        <td>
                            <a href="{{route('admin.packets.detail', $packet->id)}}" class="btn btn-sm btn-default">Approve Paket</a>
                            <a href="{{route('admin.packets.paids.page', $packet->id)}}" class="btn btn-sm btn-default">Pembayaran Paket</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="panel-footer">{!! $packets->links() !!}</div>
        @else
        <div class="panel-body">
            Tidak ada Paket.
        </div>
        @endif

    </div>
    <a href="{{route('admin.packets.add')}}" class="btn btn-success">Buat Paket baru</a>
</div>
{{-- Modal Approve packet  --}}
<div id="modal-approve-packet" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Approve Paket</h4>
            </div>
            {!! Former::open(route('admin.packets.approve')) !!}
            <div class="modal-body">
                <div id="approve-packet" class="form-group">
                    <label>Paket</label>
                    <div class="form-control"></div>
                </div>
                {!! Former::hidden('packet_id')->id('packet_id')->required() !!}
                {!! Former::select('rating[rating]')->label('Rating')->options([
                '1' => 'Buruk Sekali',
                '2' => 'Buruk',
                '3' => 'Cukup',
                '4' => 'Baik',
                '5' => 'Baik Sekali'
                ])->required() !!}
                {!! Former::textarea('rating[review]')->label('Komentar') !!}
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button class="btn btn-primary">Simpan</button>
            </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>
@endsection
