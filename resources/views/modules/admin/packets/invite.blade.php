@extends('layouts.admin')

@section('content')
<div id="admin-packet-assign-page">
    <ol class="breadcrumb flat">
        <li class="active"><a href="{{route('admin.packets')}}">Paket</a></li>
        <li class="active">Detail - ID : {{$packet->id}}, Title: {{$packet->title}}</li>
    </ol>

   @include('modules.admin._alerts')

    {{-- packet Detail --}}
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-info"></i> Paket</div>
        @include('modules.admin.packets._detail_static')
        
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Daftar tenaga ahli yang sesuai dengan kualifikasi</div>

        <table class="table" data-url="{{ route('datatable.packet.user', $packet->id) }}">
            <thead>
                <tr>
                    <th width="200px" colspan="2" rowspan="2" class="text-center text-middle">Nama</th>
                    <th width="150px" rowspan="2" class="text-center text-middle">Bidang Kompetensi</th>
                    <th colspan="4" class="text-center text-middle">Tugas</th>
                    <th rowspan="2" class="text-center text-middle">Aksi</th>
                </tr>
                <tr>
                    <th class="text-center">Belum diterima</th>
                    <th class="text-center">Ditolak</th>
                    <th class="text-center">Berjalan</th>
                    <th class="text-center">Selesai</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td width="64">
                        <img src="{{ $user->avatar ? secure_asset('storage/'.$user->avatar) : secure_asset('/img/default_avatar.jpg') }}" alt="Avatar" width="64px" height="64px">
                    </td>
                    <td>
                        <a href="{{route('admin.experts.detail', $user->id)}}" target="_blank">{{$user->name}}</a> <code>{{$user->email}}</code>
                    </td>
                    <td>
                        @foreach($user->interests as $interest)
                        <span class="label label-primary">{{$interest->name}}</span>
                        @endforeach
                    </td>
                    <td align="center">{{$user->packets->where('is_accepted',0)->count()}} Paket</td>
                    <td align="center">{{$user->packet_history->count()}} Paket</td>
                    <td align="center">{{$user->packets->where('is_completed',0)->count()}} Paket</td>
                    <td align="center">{{$user->packets->where('is_completed',1)->count()}} Paket</td>
                    <td>
                        @if(in_array($user->id, $userInvited))
                        Sudah diundang
                        @else
                        {!! Former::open()  !!}
                        {!! Former::hidden('user_id')->value($user->id) !!}
                        <button class="btn btn-assign btn-sm btn-default">Undang</button>
                        {!! Former::close() !!}
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    @endsection
