@extends('layouts.admin')

@section('content')
<div id="admin-packet-assign-page">
    <ol class="breadcrumb flat">
        <li class="active"><a href="{{route('admin.packets')}}">Paket</a></li>
        <li class="active">Detail - ID : {{$packet->id}}, Title: {{$packet->title}}</li>
    </ol>

    @include('modules.admin._alerts')

    {{-- packet Detail --}}
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-info"></i> Paket</div>
        @include('modules.admin.packets._detail_static')
        @if($packet->is_assigned)
        <div class="panel-heading">Dikerjakan Oleh</div>
        <div class="panel-body">
            <div class="media">
                <div class="media-left">
                    <img src="{{ $packet->assignedto->avatar ? secure_asset('storage/'.$packet->assignedto->avatar) : secure_asset('/img/default_avatar.jpg') }}" alt="Avatar" width="64px" height="64px">
                </div>
                <div class="media-body">
                    <h4 class="media-heading">
                        <a href="{{route('admin.experts.detail', $packet->assignedto->id)}}" target="_blank">{{$packet->assignedto->name}}</a> <code>{{$packet->assignedto->email}}</code>
                    </h4>
                    @if(!$packet->is_accepted)
                    <p>
                        <span class="label label-danger">Menunggu Konfirmasi</span>
                    </p>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed">
                            <thead>
                                <tr>
                                    <th>Belum diterima</th>
                                    <th>Ditolak</th>
                                    <th>Berjalan</th>
                                    <th>Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="center">{{$packet->assignedto->packets->where('is_accepted',0)->count()}} Paket</td>
                                    <td align="center">{{$packet->assignedto->packet_history->count()}} Paket</td>
                                    <td align="center">{{$packet->assignedto->packets->where('is_completed',0)->count()}} Paket</td>
                                    <td align="center">{{$packet->assignedto->packets->where('is_completed',1)->count()}} Paket</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Daftar penawaran</div>
        <div class="panel-body">
            {!! Former::populate($packet) !!}
            {!! Former::open()->id('filter') !!}
            {!! Former::select('interests')->id('interest')->name('interests[]')->label('Bidang Kompetensi')->fromQuery(PKMS\Models\Interest::get(),'name','id')->multiple() !!}

            {!! Former::close() !!}

            <table class="table" id="datatables" data-url="{{ route('datatable.packet.user.evaluated', $packet->id) }}">
                <thead>
                    <tr>
                        <th width="64"></th>
                        <th align="center">Nama</th>
                        <th width="200" align="center">Bidang Kompetensi</th>
                        <th width="100" align="center">Penawaran</th>
                        <th width="100" align="center">Nego</th>
                        <th width="200"></th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>
{{-- Modal Approve packet  --}}
<div id="modal-assign-packet" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Pilih Penyedia</h4>
            </div>
            {!! Former::open() !!}
            <div class="modal-body">
                <h3>Apakah anda yakin memilih <span id='user-name'></span> sebagai pemenang ?
                    <br><small id='info-assign'>Silahkan pilih harga pembayaran</small>
                </h3>
                <div id="approve-packet" class="form-group">
                    <label>Paket</label>
                    <div class="form-control"></div>
                </div>
                {!! Former::hidden('user_id')->id('user_id')->required() !!}
                {!! Former::hidden('packet_id')->id('packet_id')->required() !!}
                {!! Former::select('pembayaran')->id('pembayaran')->label('Pembayaran yang disetujui')->required() !!}
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                <button class="btn btn-primary">Simpan</button>
            </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>

@endsection
