@if($packet->articles->count())
<div class="panel panel-default">
    <div class="panel-heading"><i class="fa icon-docs"></i> Artikel terkait</div>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Penulis</th>
                    <th>Revisi</th>
                    <th>Update terakhir</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($packet->articles as $article)
                    <tr>
                        <td><a href="{{route('admin.articles.view', $article->id)}}" data-toggle="modal" data-target="#modal-view-article">{{$article->title}}</a></td>
                        <td>{{$article->author->name}}</td>
                        <td>{{$article->revision or '-'}}</td>
                        <td>{{$article->updated_at or $article->created_at}}</td>
                        <td>{{$article->is_published ? 'Terbit' : 'Belum terbit'}}</td>
                        <td>
                            @if(!$packet->is_completed)
                                <div class="btn-group btn-group-sm">
                                    <a href="{{route('admin.articles.edit', $article->id)}}" class="btn btn-default">Edit</a>
                                </div>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endif