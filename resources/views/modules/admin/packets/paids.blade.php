@extends('layouts.admin')

@section('content')
<div id="admin-packet-schedule-page">
    <ol class="breadcrumb flat">
        <li class="active"><a href="{{route('admin.packets')}}">Paket</a></li>
        <li class="active">Detail - ID : {{$packet->id}}, Title: {{$packet->title}}</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Paket</label>
                        <div class="form-control">{{$packet->title}}</div>
                    </div>
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <div class="form-control textarea" style="height: 102px;">{{$packet->description}}</div>
                    </div>
                    <div class="form-group">
                        <label>Kode Anggaran</label>
                        <div class="form-control">{{$packet->kode_anggaran}}</div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Pembayaran</label>
                        <div class="form-control">Rp. {{ number_format($packet->pembayaran,0,',','.') }}</div>
                    </div>
                    <div class='form-group'>
                        <label>Terbilang</label>
                        <p class='form-control-static'>{{ Terbilang::make($packet->pembayaran, ' rupiah') }}</p>
                    </div>
                    <div class="form-group">
                        <label>Jangka Waktu</label>
                        <div class="form-control">{{$packet->jangka_waktu}} {{$packet->interval_waktu}}</div>
                    </div>
                    <div class="form-group">
                        <label>Cara Pembayaran</label>
                        <div class="form-control">{{$packet->cara_pembayaran}}
                            @if($packet->cara_pembayaran == 'termin')
                            {{ $packet->termin }} Kali
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Detail Pembayaran</div>
        @if($packet->paid->count())
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Dibayar Tanggal</th>
                        <th>Dibayar Sebesar</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $sisa = $packet->pembayaran;
                    @endphp
                    @foreach($packet->paid as $paid)
                    <tr>
                        <td><a href="{{ route('admin.packets.paid.detail', $paid->id ) }}">{{ $paid->name }}</a></td>
                        <td>{{ ($paid->paid_date)?$paid->paid_date:"belum dibayar" }}</a></td>
                        <td>Rp. {{ number_format($paid->nominal,0,',','.') }} </td>
                        <td>
                            @if($paid->nominal == "")
                            <a href="{{ route('admin.packets.paid', $paid->id) }}" class='btn btn-sm btn-default'>Bayar</a>
                            @endif
                        </td>
                    </tr>
                    @php
                    $sisa = $sisa - $paid->nominal;
                    @endphp
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2">Sisa Pembayaran</td>
                        <td>Rp. {{ number_format($sisa,0,',','.') }} </td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="panel-footer">

        </div>
        @else
        <div class="panel-body">
            Tidak ada Pembayaran Paket.
        </div>
        @endif
    </div>

</div>
@endsection
