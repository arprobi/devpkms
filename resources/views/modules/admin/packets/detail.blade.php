@extends('layouts.admin')

@section('content')
<div id="admin-packet-detail-page">

    {{-- Breadcrumb  --}}
    <ol class="breadcrumb">
        <li><a href="{{route('admin.packets')}}"><i class="fa fa-fw fa-packets"></i> Paket</a></li>
        <li class="active">Detail - ID : {{$packet->id}}, Title: {{$packet->title}}</li>
    </ol>
    
    @include('modules.admin._alerts')

    {{-- packet Detail --}}
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-info"></i> Paket</div>

        @if($packet->is_accepted)
            @include('modules.admin.packets._detail_accepted')
        @else
            @include('modules.admin.packets._detail_waiting')
        @endif
    </div>

    {{-- Attachment --}}
    @include('modules.admin.packets._detail_attachment')

    {{-- Related Articles  --}}
    @if($packet->is_accepted)
        @include('modules.admin.packets._detail_comments')

    @endif
</div>

{{-- Modals --}}
@if($packet->is_accepted)
    @include('modules.admin.packets._detail_modals')
@endif

@endsection
