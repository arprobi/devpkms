@extends('layouts.admin')

@section('content')

<div id="admin-packet-add-page">

    {{-- Breadcrumb  --}}
    <ol class="breadcrumb">
        <li><a href="{{route('admin.packets')}}"><i class="fa fa-fw fa-packets"></i> Paket</a></li>
    </ol>

    @include('modules.admin._alerts')

    {{-- packet Detail --}}
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-info"></i> Paket</div>
        {!! Former::open_for_files(route('admin.packets.create')) !!}
        <div class="panel-body"> 

            <div class="wizardform">
                <div class="navbar">
                    <div class="navbar-inner">
                        <div class="container">
                            <ul>
                                <li><a href="#tab1" data-toggle="tab">Paket</a></li>
                                <li><a href="#tab2" data-toggle="tab">PPK & Pejabat Pengadaan</a></li>
                                <li><a href="#tab3" data-toggle="tab">Lokasi Pengadaan</a></li>
                                <li><a href="#tab4" data-toggle="tab">Syarat & Bidang Kompetensi</a></li>
                                <li><a href="#tab5" data-toggle="tab">Lampiran</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane" id="tab1">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Former::text('title')->label('Nama Paket Pengerjaan')->value($usulan)->required() !!}
                                {!! Former::select('instansi_id')->options($agencies)->placeholder('Pilih')->label('Instansi')->required() !!}
                                {!! Former::select('tahun_anggaran')->label('Tahun Anggaran')->placeholder('Pilih')->options(array_combine(range(date('Y')+2, date('Y')-2),range(date('Y')+2, date('Y')-2)))->required() !!}
                                {!! Former::text('anggaran')->class('form-control number')->label('Anggaran')->required() !!}
                                <div class='form-group'>
                                    <label>Terbilang</label>
                                    <p class='form-control-static' id='terbilang'>Nol Rupiah</p>
                                </div>
                                {!! Former::select('jenis_pengadaan')->options(['jasa konsultasi' => 'Jasa Konsultasi','jasa lainnya'=>'Jasa Lainnya'])->label('Jenis Pengadaan')->required() !!}
                            </div>
                            <div class="col-md-6">
                                {!! Former::textarea('description')->label('Lingkup Pengerjaan Paket')->rows(5) !!}
                                {!! Former::text('satuan_kerja')->label('Satuan Kerja')->required() !!}
                                {!! Former::text('kode_anggaran')->label('Kode Anggaran')->required() !!}
                                <div class="row">
                                    <div class="col-sm-6">
                                        {!! Former::select('jangka_waktu')->options(array_combine(range(1,31),range(1,31)))->required() !!}
                                    </div>
                                    <div class="col-sm-6">
                                        {!! Former::select('interval_waktu')->options(['hari' => 'Hari','minggu' => 'Minggu','bulan'=>'Bulan','tahun'=>'Tahun'])->required() !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        {!! Former::select('cara_pembayaran')->options(['sekaligus' => 'Sekaligus','bulanan' => 'Bulanan','termin'=>'Termin'])->label('Cara Pembayaran')->required() !!}
                                    </div>
                                    <div class="col-sm-6">
                                        {!! Former::text('termin')->id('termin')->disabled(); !!}
                                    </div>
                                </div>
                                <small>*Setelah Data Tersimpan Jangka Waktu, Interval, Cara Pembayaran & Jumlah Termin tidak dapat dirubah.</small>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <div class="row">
                            <div class="col-sm-6">
                                <h4>PPK</h4>
                                {!! Former::text('ppk_nama')->label('Nama')->required() !!}
                                {!! Former::text('ppk_nip')->label('NIP')->required() !!}
                                {!! Former::text('ppk_jabatan')->label('Jabatan')->required() !!}
                            </div>
                            <div class="col-sm-6">
                                <h4>Pejabat Pengadaan</h4>
                                {!! Former::text('pejabat_nama')->label('Nama')->required() !!}
                                {!! Former::text('pejabat_nip')->label('NIP')->required() !!}
                                {!! Former::text('pejabat_jabatan')->label('Jabatan')->required() !!}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab3">
                        <div class="row">
                            <div class="col-sm-6">
                                {!! Former::select('province_code')->label('Propinsi')->placeholder('Pilih')->fromQuery(PKMS\Models\Province::get(),'name','code')->required() !!}
                                {!! Former::select('city_code')->label('Kabupaten/Kota')->placeholder('Pilih')->disabled()->required() !!}
                            </div>
                            <div class="col-sm-6">
                                {!! Former::textarea('detail_lokasi')->label('Detail Lokasi')->rows(5) !!}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab4">
                        <div class="row controls">
                            <div class="col-sm-6 form">
                                <div class="entry input-group">
                                    <input class="form-control" name="qualifications[]" type="text" placeholder="Syarat" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-success btn-add" type="button">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                {!! Former::select('interests')->name('interests[]')->label('Bidang Kompetensi')->fromQuery(PKMS\Models\Interest::get(),'name','id')->multiple() !!}
                                {!! Former::text('metode_pengadaan')->label('Metode Pengadaan')->value('Pengadaan Langsung')->readonly() !!}
                                {!! Former::text('jenis_penyedia')->label('Jenis Penyedia')->value('Perorangan')->readonly() !!}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab5">
                        {!! Former::file('lampiran[]')->label('Lampiran')->help('ukuran maksimum 10MB untuk tiap lampiran')->multiple() !!}
                    </div>
                </div>
                <ul class="pager wizard">
                    <li class="previous"><a href="javascript:;">Sebelumnya</a></li>
                    <li class="next"><a href="javascript:;">Selanjutnya</a></li>
                    <li class="finish"><button type="submit" class="btn btn-success">Simpan</button></li>
                </ul>
            </div>
        </div>
        <div class="panel-footer">
            <a href="" class="btn btn-cancel btn-danger">Batalkan</a>
        </div>
        {!! Former::close() !!}
    </div>
</div>
@endsection
