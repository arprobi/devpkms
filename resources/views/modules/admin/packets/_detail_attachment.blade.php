<div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-paperclip"></i> Lampiran</div>
    @if($packet->attachments->count())
    <div class="panel-body">
        <ul class="list-inline">
            @foreach($packet->attachments as $attachment)
            <li>
                <div class="btn-group btn-group-sm">
                    <a href="{{route('admin.attachments.download', $attachment->short_url)}}" target="_blank" class="btn btn-default">{{$attachment->title}}</a>
                    <a href="{{route('admin.attachments.delete', $attachment->short_url)}}" class="btn btn-default" data-confirm data-message="Hapus lampiran ini?"><i class="fa fa-times text-danger"></i></a>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
    @endif
    @if(!$packet->is_completed)
    {!! Former::open_for_files(route('admin.packets.add_attachment', $packet->id)) !!}
    <div class="panel-body">
        {!! Former::file('lampiran[]')->label('Lampiran')->help('ukuran maksimum 10MB untuk tiap lampiran')->multiple() !!}
    </div>
    <div class="panel-footer">
        <button class="btn btn-success"><i class="fa fa-fw fa-upload"></i> Upload</button>
    </div>
    {!! Former::close() !!}
    @endif
</div>