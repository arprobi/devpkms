@extends('layouts.admin')

@section('content')
<div id="invitation-page">
    <ol class="breadcrumb">
        <li><a href="{{route('admin.packets')}}"><i class="fa fa-fw fa-packets"></i> Paket</a></li>
        <li class="active">Detail - ID : {{$packet->id}}, Title: {{$packet->title}}</li>
    </ol>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-envelope"></i> Invitations</div>
        <div class="panel-body">
            <div class="form-group">
                <label>Judul</label>
                <div class="form-control-static">{{$invitation->title}}</div>
            </div>
            <div class="form-group">
                <label>Deskripsi</label>
                <div class="form-control-static">{{$invitation->description}}</div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Mulai</label>
                        <div class="form-control-static">{{$invitation->from}}</div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Hingga</label>
                        <div class="form-control-static">{{$invitation->until}}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nama / Email</th>
                        <th>Key</th>
                        <th>Terdaftar</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($invitation->lists as $list)
                    <tr>
                        <td>{{$list->name}} {{$list->email}}</td>
                        <td><code>{{$list->key}}</code></td>
                        <td>{{$list->isRegistered() ? 'Terdaftar' : 'Belum terdaftar'}}</td>
                        <td>
                            {!! Former::open(route('admin.packets.invitations.resend', $invitation->id))  !!}
                            {!! Former::hidden('_email')->value($list->email)->required() !!}
                            <button class="btn btn-default"><i class="fa fa-fw fa-envelope-o"></i> Kirim Ulang</button>
                            {!! Former::close() !!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="panel-footer">
            <a href="{{route('admin.packets.invitations',$packet->id)}}" class="btn btn-default">Kembali</a>
        </div>
    </div>
</div>

@endsection