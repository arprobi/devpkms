@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb">
        <li><a href="{{route('admin.packets')}}"><i class="fa fa-fw fa-packets"></i> Paket</a></li>
        <li class="active">Detail - ID : {{$packet->id}}, Title: {{$packet->title}}</li>
    </ol>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-envelope"></i> Invitations</div>
        @if($invitations->count())
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Judul</th>
                            <th width="300px">Deskripsi</th>
                            <th>Masa Berlaku</th>
                            <th>Daftar undangan</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($invitations as $inv)
                            <tr>
                                <td>{{$inv->title}}</td>
                                <td>{{$inv->description}}</td>
                                <td>{{$inv->from}} - {{$inv->until}}</td>
                                <td>
                                    <ul class="list-unstyled">
                                        @foreach($inv->lists as $list)
                                            <li><code>{{$list->email}}</code></li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>
                                    {!! Former::open(route('admin.invitations.delete', $inv->id))->method('delete') !!}
                                        <a href="{{route('admin.packets.invitations.edit', $inv->id)}}" class="btn btn-default">Edit</a>
                                        <a href="{{route('admin.packets.invitations.show', $inv->id)}}" class="btn btn-default">Detil</a>
                                        <button class="btn btn-default btn-delete">Delete</button>
                                    {!! Former::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="panel-body">
                Belum ada undangan.
            </div>
        @endif
        <div class="panel-footer">
            <a href="{{route('admin.packets.invitations.new',$packet->id)}}" class="btn btn-default">Buat pengumuman baru</a>
        </div>
    </div>
</div>

@endsection