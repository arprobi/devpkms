@extends('layouts.admin')

@section('content')
<div id="invitation-page">
    <ol class="breadcrumb">
        <li><a href="{{route('admin.packets')}}"><i class="fa fa-fw fa-packets"></i> Paket</a></li>
        <li class="active">Detail - ID : {{$packet->id}}, Title: {{$packet->title}}</li>
    </ol>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-envelope"></i> Umumkan Paket</div>
        {!! Former::open(route('admin.packets.invitations.create', $packet->id)) !!}
        <div class="panel-body">
            {!! Former::text('title')->label('Subject')->value('Pengumuman Pekerjaan Paket '.$packet->name)->placeholder('Subject')->required() !!}
            {!! Former::textarea('description')->label('Deskripsi')->value($packet->description)->placeholder('Deskripsi')->rows(4) !!}
            <div class="row">
                <div class="col-md-6">
                    {!! Former::text('from')->data_type('date')->data_date_end_date('0d')->value(date('d F Y', strtotime($packet->schedule[0]->start_date)))->label('Mulai')->placeholder('Mulai')->required() !!}
                </div>
                <div class="col-md-6">
                    {!! Former::text('until')->data_type('date')->value(date('d F Y', strtotime($packet->schedule[0]->end_date)))->label('Hingga')->placeholder('Hingga (Kosongkan jika perlu)') !!}
                </div>
            </div>
            {!! Former::select('invitation_lists[]')->id('lists')->label('Daftar Email')->help('Tuliskan alamat email yang ingin anda infokan tentang paket.')->multiple() !!}
        </div>
        <div class="panel-footer">
            <button class="btn btn-default">Kirim & Umumkan</button>
            <a href="{{route('admin.packets.publishing', $packet->id)}}" class="btn btn-default">Umumkan/Publish</a>
            <a href="{{route('admin.packets')}}" class="btn btn-default">Kembali</a>
        </div>
        {!! Former::close() !!}
    </div>
</div>

@endsection