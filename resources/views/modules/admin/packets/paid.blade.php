@extends('layouts.admin')

@section('content')
<div id="admin-packet-paid-page">
    <ol class="breadcrumb flat">
        <li class="active"><a href="{{route('admin.packets')}}">Paket</a></li>
        <li class="active">Detail - ID : {{$paid->packet->id}}, Title: {{$paid->packet->title}}</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        {!! Former::populate($paid) !!}
        {!! Former::open_for_files(route('admin.packets.paid', $paid->id)) !!}
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    {!! Former::text('name')->label('Nama')->required() !!}
                    {!! Former::textarea('description')->label('Deskripsi Pembayaran')->rows(5) !!}
                    {!! Former::text('paid_date')->label('Dibayar pada')->data_type('date')->placeholder('Dibayar Pada')->required() !!}
                    {!! Former::text('nominal')->class('form-control number')->label('Nominal Pembayaran')->required() !!}
                    <div class='form-group'>
                        <label>Terbilang</label>
                        <p class='form-control-static' id='terbilang'>Nol Rupiah</p>
                    </div>
                    {!! Former::file('lampiran[]')->label('Lampiran')->help('ukuran maksimum 10MB untuk tiap lampiran')->multiple() !!}
                </div>
                <div class='col-md-6'>
                    <div class="form-group">
                        <label>Kode Anggaran</label>
                        <div class="form-control">{{$paid->packet->kode_anggaran}}</div>
                    </div>
                    <div class="form-group">
                        <label>Total Pembayaran</label>
                        <div class="form-control">Rp. {{ number_format($paid->packet->pembayaran,0,',','.') }}</div>
                    </div>
                    <div class='form-group'>
                        <label>Terbilang</label>
                        <p class='form-control-static'>{{ Terbilang::make($paid->packet->pembayaran, ' rupiah') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <button class="btn btn-success">Update</button>
            <a href="{{route('admin.packets.paids.page', $paid->packet->id)}}" class="btn btn-cancel btn-danger">Batalkan</a>
        </div>
        {!! Former::close() !!}
    </div>
</div>
@endsection
