@extends('layouts.admin')

@section('content')
<div id="admin-packet-schedule-page">
    <ol class="breadcrumb flat">
        <li class="active"><a href="{{route('admin.packets')}}">Paket</a></li>
        <li class="active">Detail - ID : {{$packet->id}}, Title: {{$packet->title}}</li>
    </ol>
    
    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-calendar"></i> Jadwal Pelaksanaan</div>
        {!! Former::open(route('admin.packets.schedule', $packet->id)) !!}
        <div class="panel-body">
            <table class='table table-bordered'>
                <tr>
                    <th rowspan="2" class='text-center' width='50%'>Kegiatan</th>
                    <th colspan="2" class='text-center'>Tanggal & Waktu</th>
                </tr>
                <tr>
                    <th class='text-center'>Waktu Mulai</th>
                    <th class='text-center'>Waktu Akhir</th>
                </tr>
                @if($packet->schedule->count())
                @foreach($packet->schedule as $schedule)
                <tr>
                    <td>
                        {!! Former::text('schedule[update]['.$schedule->id.'][name]')->label('')->value($schedule->name)->readonly() !!}
                    </td>
                    <td>
                        {!! Former::text('schedule[update]['.$schedule->id.'][start_date]')->value($schedule->start_date)->label('')->data_type('datetime')->required() !!}
                    </td>
                    <td>
                        {!! Former::text('schedule[update]['.$schedule->id.'][end_date]')->value($schedule->end_date)->label('')->data_type('datetime')->required() !!}
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td>
                        {!! Former::text('schedule[1][name]')->label('')->value('Pengumuman/Undangan')->readonly() !!}
                    </td>
                    <td>
                        {!! Former::text('schedule[1][start_date]')->label('')->data_type('datetime')->required() !!}
                    </td>
                    <td>
                        {!! Former::text('schedule[1][end_date]')->label('')->data_type('datetime')->required() !!}
                    </td>
                </tr>
                <tr>
                    <td>
                        {!! Former::text('schedule[2][name]')->label('')->value('Pemasukan Dokumen Penawaran')->readonly() !!}
                    </td>
                    <td>
                        {!! Former::text('schedule[2][start_date]')->label('')->data_type('datetime')->required() !!}
                    </td>
                    <td>
                        {!! Former::text('schedule[2][end_date]')->label('')->data_type('datetime')->required() !!}
                    </td>
                </tr>
                <tr>
                    <td>
                        {!! Former::text('schedule[3][name]')->label('')->value('Pembukaan Dokumen Penawaran, Evaluasi Penawaran, Klarifikasi Teknis dan Negosiasi Harga')->readonly() !!}
                    </td>
                    <td>
                        {!! Former::text('schedule[3][start_date]')->label('')->data_type('datetime')->required() !!}
                    </td>
                    <td>
                        {!! Former::text('schedule[3][end_date]')->label('')->data_type('datetime')->required() !!}
                    </td>
                </tr>
                <tr>
                    <td>
                        {!! Former::text('schedule[4][name]')->label('')->value('Penetapan dan Pengumuman Pemenang')->readonly() !!}
                    </td>
                    <td>
                        {!! Former::text('schedule[4][start_date]')->label('')->data_type('datetime')->required() !!}
                    </td>
                    <td>
                        {!! Former::text('schedule[4][end_date]')->label('')->data_type('datetime')->required() !!}
                    </td>
                </tr>
                @endif
            </table>
        </div>
        <div class="panel-footer">
            <button class="btn btn-success"><i class="fa fa-fw fa-save"></i> Simpan</button>
        </div>
        {!! Former::close() !!}
    </div>


    {{-- packet Detail --}}
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-info"></i> Paket</div>
        @include('modules.admin.packets._detail_static')
    </div>
</div>
@endsection
