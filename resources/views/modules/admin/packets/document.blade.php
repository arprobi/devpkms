@extends('layouts.admin')

@section('content')
<div id="expert-detail-page">

    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.packets.assign.page', $packet->id)}}">Dokumen Penawaran Paket {{$packet->title}}</a></li>
        <li class="active">{{$user->name}}</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="row">
        <div class="col-md-2">
            <div align="center">
                <img src="{{ $user->avatar ? secure_asset('storage/'.$user->avatar) : secure_asset('img/default_avatar.jpg') }}" alt="avatar" width="128px" height="128px" class="img img-thumbnail img-circle img-responsive margin-bottom-10">
            </div>
        </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                {{-- Profile  --}}
                <div class="panel-heading"><i class="fa fa-fw icon-user"></i> Profil</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>Nama</label>
                        <div class="form-control">{{$user->name}}</div>
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <div class="form-control">{{$user->email}}</div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                {{-- Profile  --}}
                <div class="panel-heading"><i class="fa fa-fw fa-files-o"></i> Lampiran</div>
                <div class="panel-body">
                    @foreach($packet->qualifications as $qualification)
                    @php
                    $attachment = $qualification->attachments()->where('owner_id', $user->id)->first();
                    @endphp
                    <div class="form-group">
                        <label>{{ $qualification->name }}</label>
                        <br/>
                        <div class="btn-group btn-group-sm">
                            <a href="{{route('admin.attachments.download', $attachment->short_url)}}" target="_blank" class="btn btn-default">{{$attachment->title}}</a>
                        </div>
                    </div>
                    @endforeach

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
