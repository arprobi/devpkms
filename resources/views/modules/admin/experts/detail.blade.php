@extends('layouts.admin')

@section('content')
<div id="expert-detail-page">

    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.experts')}}">Tenaga Ahli</a></li>
        <li class="active">{{$user->name}}</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="row">
        <div class="col-md-2">
            <div align="center">
                <img src="{{ $user->avatar ? secure_asset('storage/'.$user->avatar) : secure_asset('img/default_avatar.jpg') }}" alt="avatar" width="128px" height="128px" class="img img-thumbnail img-circle img-responsive margin-bottom-10">
            </div>
        </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                {{-- Profile  --}}
                <div class="panel-heading"><i class="fa fa-fw icon-user"></i> Profil</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>Nama</label>
                        <div class="form-control">{{$user->name}}</div>
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <div class="form-control">{{$user->email}}</div>
                    </div>

                    {{-- Profile  --}}
                    @if($user->profile)
                        <div class="form-group">
                            <label>Jenis kelamin</label>
                            <div class="form-control">{{$user->profile->gender == 'm' ? 'Laki-laki' : 'Perempuan'}}</div>
                        </div>

                        <div class="form-group">
                            <label>Tempat/Tanggal Lahir</label>
                            <div class="form-control">{{$user->profile->birth_place}}, {{$user->profile->birth_date}}</div>
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <div class="form-control">{{$user->profile->address}}, Kel. {{$user->profile->village->name}}, Kec. {{$user->profile->district->name}} - {{$user->profile->city->name}}, {{$user->profile->province->name}}</div>
                        </div>

                        <div class="form-group">
                            <label>Kode Pos</label>
                            <div class="form-control">{{$user->profile->zip_code}}</div>
                        </div>

                        <div class="form-group">
                            <label>Telephone</label>
                            <div class="form-control">{{$user->profile->phone}}</div>
                        </div>

                        <div class="form-group">
                            <label>Handphone</label>
                            <div class="form-control">{{$user->profile->mobile}}</div>
                        </div>
                    @else
                        <div class="alert alert-danger">Pengguna ini belum mengisi data profil.</div>
                    @endif
                </div>

                {{-- Education  --}}
                <div class="panel-heading"><i class="fa fa-fw icon-doc"></i> Edukasi</div>
                @if($user->educations->count())
                    <div class="panel-body">
                        <dl class="list-unstyled">
                        @foreach($user->educations as $education)
                            <dt>{{$education->detail->code}} {{$education->name}}</dt>
                            <dd>{{$education->major}}</dd>
                            <dd>{{$education->gpa}}</dd>
                        @endforeach
                        </dl>
                    </div>
                @else
                    <div class="panel-body">
                        <div class="alert alert-danger">Pengguna ini belum mengisi data edukasi.</div>
                    </div>
                @endif

                {{-- Experience  --}}
                <div class="panel-heading"><i class="fa fa-fw icon-briefcase"></i> Pengalaman Kerja</div>
                @if($user->experiences->count())
                    <div class="panel-body">
                        <dl class="list-unstyled">
                        @foreach($user->experiences as $experience)
                            <dt>{{$experience->title}} ({{$experience->from}} hingga {{$experience->to}})</dt>
                            <dd>{{$experience->position}}</dd>
                            <dd>{{$experience->desc}}</dd>
                            @if($experience->attachments->count())
                                <dd>
                                    <ul class="list-inline">
                                        @foreach($experience->attachments as $attachment)
                                            <li><a href="{{secure_asset('storage/'.$attachment->path)}}" target="_blank" class="btn btn-default btn-sm">{{$attachment->title}}</a></li>
                                        @endforeach
                                    </ul>
                                </dd>
                            @endif
                        @endforeach
                        </dl>
                    </div>
                @else
                    <div class="panel-body">
                        <div class="alert alert-danger">Pengguna ini belum mengisi data pengalaman kerja.</div>
                    </div>
                @endif

                <div class="panel-footer">
                    @if(!$user->is_verified)
                        @if($user->isComplete())
                            {!! Former::open(route('admin.experts.verify', $user->id)) !!}
                                <button class="btn btn-sm btn-success btn-verify">Verifikasi</button>
                            {!! Former::close() !!}
                        @else
                            <a href="{{url()->previous()}}" class="btn btn-default">Kembali</a>
                            <code>Belum memenuhi syarat untuk dilakukan verifikasi.</code>
                        @endif
                    @else

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
