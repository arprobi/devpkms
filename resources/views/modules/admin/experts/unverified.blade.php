@extends('layouts.admin')

@php
    $filters = request()->get('filters', false);
    $hobbies = isset($filters['hobbies']) ? array_combine($filters['hobbies'], $filters['hobbies']) : [];
    $interests = isset($filters['interests']) ? array_combine($filters['interests'], $filters['interests']) : [];
    $occupations = isset($filters['occupations']) ? array_combine($filters['occupations'], $filters['occupations']) : [];
@endphp

@section('content')
<div id="unverified-list-page">

    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.experts')}}">Tenaga Ahli</a></li>
        <li class="active">Verifikasi</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-filter"></i> Filters</div>
        <div class="panel-body">
            <div class="pull-right visible-md visible-lg">
                {!! Former::open_inline()->method('get') !!}
                {!! Former::select('filters[occupations][]')->class('form-control filter_occupations')->label('')->options($occupations,array_keys($occupations))->style('min-width:200px;')->multiple() !!}
                {!! Former::select('filters[interests][]')->class('form-control filter_interests')->label('')->options($interests,array_keys($interests))->style('min-width:150px;')->multiple() !!}
                {!! Former::select('filters[hobbies][]')->class('form-control filter_hobbies')->label('')->options($hobbies,$hobbies)->style('min-width:150px;')->multiple() !!}
                {!! Former::text('filters[name]')->class('form-control filter_name')->label('')->placeholder('Nama tenaga ahli') !!}
                <button class="btn btn-success"><i class="fa fa-fw fa-filter"></i> Filter</button>
                {!! Former::close() !!}
            </div>
            <div class="visible-sm visible-xs">
                {!! Former::open()->method('get') !!}
                {!! Former::select('filters[occupations][]')->class('form-control filter_occupations')->label('')->options($occupations,array_keys($occupations))->style('min-width:200px;')->multiple() !!}
                {!! Former::select('filters[interests][]')->class('form-control filter_interests')->label('')->options($interests,array_keys($interests))->style('min-width:125px;width:100%')->multiple() !!}
                {!! Former::select('filters[hobbies][]')->class('form-control filter_hobbies')->label('')->options($hobbies,$hobbies)->style('min-width:125px;width:100%')->multiple() !!}
                {!! Former::text('filters[name]')->class('form-control filter_name')->label('')->placeholder('Nama tenaga ahli') !!}
                <button class="btn btn-block btn-success"><i class="fa fa-fw fa-search"></i> Cari</button>
                {!! Former::close() !!}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Daftar tenaga ahli belum terverifikasi</div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th colspan="2">Nama</th>
                        <th>Edukasi</th>
                        <th>Pengalaman</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data['users'] as $user)
                        <tr>
                            <td width="64">
                                <img src="{{ $user->avatar ? secure_asset('storage/'.$user->avatar) : secure_asset('/img/default_avatar.jpg') }}" alt="Avatar" width="64px" height="64px">
                            </td>
                            <td>
                                <a href="{{route('admin.experts.detail', $user->id)}}" target="_blank">{{$user->name}}</a> <code>{{$user->email}}</code>
                            </td>
                            <td>
                                @if($user->educations->count())
                                    <ul class="list-unstyled">
                                        @foreach($user->educations as $education)
                                            <li>{{$education->detail->code}} - {{$education->name}}</li>
                                        @endforeach
                                    </ul>
                                @else
                                    N/A
                                @endif
                            </td>
                            <td>
                                @if($user->experiences->count())
                                    <ul class="list-unstyled">
                                        @foreach($user->experiences as $experience)
                                            <li>
                                                {{$experience->position}} - {{$experience->title}}
                                                <br>
                                                <code>(Dari: {{$experience->from}}, Hingga: {{$experience->to}})</code>
                                            </li>
                                        @endforeach
                                    </ul>
                                @else
                                    N/A
                                @endif
                            </td>
                            <td>
                                @if($user->isComplete())
                                    <a href="{{route('admin.experts.detail', $user->id)}}" class="btn btn-success">Verifikasi</a>
                                @else
                                    <a href="javascript:;" class="btn btn-sm btn-default btn-reminder"
                                        data-id="{{$user->id}}"
                                        data-name="{{$user->name}}"
                                        data-toggle="modal"
                                        data-target="#modal-new-message">
                                        Kirim Pesan
                                    </a>
                                    <br>
                                    <code>Belum memenuhi syarat untuk dilakukan verifikasi.</code>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

{{-- Modal New Message  --}}
<div id="modal-new-message" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Kirim pesan</h4>
            </div>
            {!! Former::open_for_files(route('admin.messages.send'))->method('post') !!}
                <div class="modal-body">
                    {!! Former::hidden('to')->id('to')->required() !!}
                    <div class="form-group">
                        <label>Ke</label>
                        <div id="user-name" class="form-control"></div>
                    </div>
                    {!! Former::text('subject')->label('Subjek')->placeholder('Subjek pesan')->value('Peringatan prihal kelengkapan data.')->required()  !!}
                    {!! Former::textarea('message')->label('Pesan')->rows(6)->placeholder('Pesan')->required() !!}
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-primary">Simpan</button>
                </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>
@endsection
