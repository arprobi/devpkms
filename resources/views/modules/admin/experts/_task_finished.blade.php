@if($user->tasks->count())
    @foreach($user->tasks as $task)
        <dl>
            <dt>
                <a href="{{route('admin.tasks.detail', $task->id)}}">{{$task->title}}</a>
                @for($i = 0; $i < $task->rating->rating; $i++)
                    <i class="fa fa-fw fa-star bg-yellow"></i>
                @endfor
            </dt>
            <dd>{{$task->description}}</dd>
        </dl>
    @endforeach
@else
    Tidak ada tugas.
@endif