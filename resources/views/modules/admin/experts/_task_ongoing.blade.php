@if($user->tasks->count())
    @foreach($user->tasks as $task)
        <dl>
            <dt>
                <a href="{{route('admin.tasks.detail', $task->id)}}">{{$task->title}}</a>
                {{$task->progress}} %
            </dt>
            <dd>{{$task->description}}</dd>
        </dl>
    @endforeach
@else
    Tidak ada tugas.
@endif