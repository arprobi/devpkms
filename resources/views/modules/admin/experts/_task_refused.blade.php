@if($user->task_history->count())
    @foreach($user->task_history as $history)
        <dl>
            <dt>
                <a href="{{route('admin.tasks.detail', $history->task->id)}}">{{$history->task->title}}</a>
            </dt>
            <dd>{{$history->task->description}}</dd>
            <dt>Ditolak dengan alasan</dt>
            <dd>{{$history->text or 'tidak ada alasan.'}}</dd>
        </dl>
    @endforeach
@else
    Tidak ada tugas.
@endif