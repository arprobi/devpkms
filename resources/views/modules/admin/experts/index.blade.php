@extends('layouts.admin')

@section('content')

@php
    $filters = request()->get('filters', false);
    $hobbies = isset($filters['hobbies']) ? array_combine($filters['hobbies'], $filters['hobbies']) : [];
    $interests = isset($filters['interests']) ? array_combine($filters['interests'], $filters['interests']) : [];
    $occupations = isset($filters['occupations']) ? array_combine($filters['occupations'], $filters['occupations']) : [];
@endphp

<div id="expert-list-page">
    <ol class="breadcrumb flat">
        <li class="active">Tenaga Ahli</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-filter"></i> Filters</div>
        <div class="panel-body">
            <div class="pull-right visible-md visible-lg">
                {!! Former::open_inline()->method('get') !!}
                {!! Former::select('filters[occupations][]')->class('form-control filter_occupations')->label('')->options($occupations,array_keys($occupations))->style('min-width:200px;')->multiple() !!}
                {!! Former::select('filters[interests][]')->class('form-control filter_interests')->label('')->options($interests,array_keys($interests))->style('min-width:150px;')->multiple() !!}
                {!! Former::select('filters[hobbies][]')->class('form-control filter_hobbies')->label('')->options($hobbies,$hobbies)->style('min-width:150px;')->multiple() !!}
                {!! Former::select('filters[rating]')->class('form-control filter_rating')->label('')->options([0,1,2,3,4,5])->style('min-width:150px') !!}
                {!! Former::text('filters[name]')->class('form-control filter_name')->label('')->placeholder('Nama tenaga ahli') !!}
                <button class="btn btn-success"><i class="fa fa-fw fa-filter"></i> Filter</button>
                {!! Former::close() !!}
            </div>
            <div class="visible-sm visible-xs">
                {!! Former::open()->method('get') !!}
                {!! Former::select('filters[occupations][]')->class('form-control filter_occupations')->label('')->options($occupations,array_keys($occupations))->style('min-width:200px;')->multiple() !!}
                {!! Former::select('filters[interests][]')->class('form-control filter_interests')->label('')->options($interests,array_keys($interests))->style('min-width:125px;width:100%')->multiple() !!}
                {!! Former::select('filters[hobbies][]')->class('form-control filter_hobbies')->label('')->options($hobbies,$hobbies)->style('min-width:125px;width:100%')->multiple() !!}
                {!! Former::select('filters[rating]')->class('form-control filter_rating')->label('')->options([0,1,2,3,4,5])->style('min-width:125px') !!}
                {!! Former::text('filters[name]')->class('form-control filter_name')->label('')->placeholder('Nama tenaga ahli') !!}
                <button class="btn btn-block btn-success"><i class="fa fa-fw fa-search"></i> Cari</button>
                {!! Former::close() !!}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">Daftar Tenaga Ahli</div>
        @if($users->count())
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="200px" colspan="2" rowspan="2" class="text-center text-middle">Nama</th>
                            <th width="150px" rowspan="2" class="text-center text-middle">Bidang Kompetensi</th>
                            <th width="125" rowspan="2" class="text-center text-middle">Rating</th>
                            <th colspan="4" class="text-center text-middle">Tugas</th>
                            <th rowspan="2" class="text-center text-middle">Aksi</th>
                        </tr>
                        <tr>
                            <th class="text-center">Belum diterima</th>
                            <th class="text-center">Ditolak</th>
                            <th class="text-center">Berjalan</th>
                            <th class="text-center">Selesai</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td width="64">
                                    <img src="{{ $user->avatar ? secure_asset('storage/'.$user->avatar) : secure_asset('img/default_avatar.jpg') }}" alt="avatar" width="64px" height="64px">
                                </td>
                                <td>
                                    <a href="{{route('admin.experts.detail', $user->id)}}" target="_blank">{{$user->name}}</a> <code>{{$user->email}}</code>
                                </td>
                                <td>
                                    @foreach($user->interests as $interest)
                                        <span class="label label-primary">{{$interest->name}}</span>
                                    @endforeach
                                </td>
                                <td class="text-center">
                                    @if($user->ratings->avg('rating'))
                                        @php
                                            $avg_rating = $user->ratings->avg('rating');
                                            $star = 0;
                                        @endphp

                                        @for($i = 0; $i < floor($avg_rating); $i++, $star++)
                                            <i class="fa fa-star bg-yellow"></i>
                                        @endfor

                                        @if(fmod($avg_rating, 1))
                                            @php
                                                $star--;
                                            @endphp
                                            <i class="fa fa-star-half-empty bg-yellow"></i>
                                        @endif

                                        @for($i = 1; $i < $star; $i++)
                                            <i class="fa fa-star-o bg-yellow"></i>
                                        @endfor

                                    @else
                                        @for($i = 0; $i < 5; $i++)
                                            <i class="fa fa-star-o bg-yellow"></i>
                                        @endfor
                                    @endif
                                </td>
                                <td align="center">
                                    <a href="javascript:;" data-toggle="bootbox"
                                        data-remote="{{route('admin.experts.task_waiting', $user->id)}}"
                                        data-title="Tugas yang belum diterima"
                                        data-target="#modal-detail-task">{{$user->tasks->where('is_accepted',0)->count()}} Tugas</a>
                                </td>
                                <td align="center">
                                    <a href="javascript:;" data-toggle="bootbox"
                                        data-remote="{{route('admin.experts.task_refused', $user->id)}}"
                                        data-title="Tugas yang ditolak"
                                        data-target="#modal-detail-task">{{$user->task_history->count()}} Tugas</a>
                                </td>
                                <td align="center">
                                    <a href="javascript:;" data-toggle="bootbox"
                                        data-remote="{{route('admin.experts.task_ongoing', $user->id)}}"
                                        data-title="Tugas yang sedang berjalan"
                                        data-target="#modal-detail-task">{{$user->tasks->where('is_completed',0)->count()}} Tugas</a>
                                </td>
                                <td align="center">
                                    <a href="javascript:;" data-toggle="bootbox"
                                        data-remote="{{route('admin.experts.task_finished', $user->id)}}"
                                        data-title="Tugas yang telah selesai"
                                        data-target="#modal-detail-task">{{$user->tasks->where('is_completed',1)->count()}} Tugas</a>
                                </td>
                                <td align="center">
                                    <a href="javascript:;" class="btn btn-assign btn-sm btn-default" data-user-id="{{$user->id}}" data-username="{{$user->name}}" data-toggle="modal" data-target="#modal-assign-task">Penugasan</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="panel-footer" align="right">
                {!! $users->appends(request()->only(['filters']))->links() !!}
            </div>
        @else
            <div class="panel-body">Tenaga ahli tidak ditemukan.</div>
        @endif
    </div>
</div>

{{-- Modal Assign Task  --}}
<div id="modal-assign-task" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Penugasan</h4>
            </div>
            {!! Former::open() !!}
                <div class="modal-body">
                    <div id="assigned-user" class="form-group">
                        <label>Tenaga Ahli</label>
                        <div class="form-control"></div>
                    </div>
                    {!! Former::select('task_id[]')->id('task_id')->label('Tugas')->multiple()->required() !!}
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-primary">Simpan</button>
                </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>

@endsection
