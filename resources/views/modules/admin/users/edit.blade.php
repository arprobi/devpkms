@extends('layouts.admin')

@section('content')
<div id="expert-detail-page">
    {!! Former::populate($user) !!}
    {!! Former::open(route('admin.users.update', $user->id)) !!}

        <div class="row">
            <div class="col-md-2">
                <div align="center">
                    <img src="{{ $user->avatar ? secure_asset('storage/'.$user->avatar) : secure_asset('img/default_avatar.jpg') }}" alt="avatar" width="128px" height="128px" class="img img-thumbnail img-circle img-responsive margin-bottom-10">
                </div>
            </div>
            <div class="col-md-10">

                <div class="panel panel-default">
                    <div class="panel-heading">Edit user</div>

                        <div class="panel-body">
                            {!! Former::text('name')->label('Nama lengkap')->placeholder('Nama Lengkap')->required() !!}
                            {!! Former::email('email')->label('Email')->placeholder('Alamat Email')->required() !!}

                            {!! Former::radios('Admin')->radios([
                                'Ya' => ['name' => 'is_admin', 'value' => '1'],
                                'Tidak' => ['name' => 'is_admin', 'value' => '0']
                            ])->required() !!}

                            {!! Former::radios('Terverifikasi')->radios([
                                'Ya' => ['name' => 'is_verified', 'value' => '1'],
                                'Tidak' => ['name' => 'is_verified', 'value' => '0']
                            ])->required() !!}

                            {!! Former::radios('Aktif')->radios([
                                'Ya' => ['name' => 'is_active', 'value' => '1'],
                                'Tidak' => ['name' => 'is_active', 'value' => '0']
                            ])->required() !!}
                        </div>

                        <div class="panel-footer">
                            <button class="btn btn-success">Simpan</button>
                            <div class="pull-right">
                                <a href="{{route('admin.users.reset_password', $user->id)}}" class="btn btn-default">Reset Password</a>
                            </div>
                        </div>

                </div>

            </div>
        </div>
    {!! Former::close() !!}
</div>
@endsection
