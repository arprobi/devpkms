@extends('layouts.admin')

@section('content')
<div id="expert-list-page">
    <div class="panel panel-default">
        <div class="panel-heading">Daftar Tenaga Ahli</div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="2" class="text-center text-middle">Nama</th>
                        <th>Phone Number</th>
                        <th width="100px" class="text-center text-middle">Tipe</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td width="64">
                                <img src="{{ $user->avatar ? secure_asset('storage/'.$user->avatar) : secure_asset('img/default_avatar.jpg') }}" alt="avatar" width="64px" height="64px">
                            </td>
                            <td><a href="{{route('admin.users.detail', $user->id)}}" target="_blank">{{$user->name}}</a> <code>{{$user->email}}</code></td>
                            <td>{{ $user->profile ? $user->profile->mobile : '' }}</td>
                            <td align="center">{{$user->is_admin ? 'Admin' : 'Tenaga Ahli'}}</td>
                            <td align="right">
                                {!! Former::open(route('admin.users.delete', $user->id))->method('delete') !!}
                                    <a href="{{route('admin.users.edit', $user->id)}}" class="btn btn-default btn-sm">Edit</a>
                                    <button class="btn btn-delete btn-sm btn-danger">Hapus</button>
                                {!! Former::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="panel-footer" align="right">
            {!! $users->links() !!}
        </div>
    </div>
</div>

{{-- Modal Assign Task  --}}
<div id="modal-assign-task" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Penugasan</h4>
            </div>
            {!! Former::open() !!}
                <div class="modal-body">
                    <div id="assigned-user" class="form-group">
                        <label>Tenaga Ahli</label>
                        <div class="form-control"></div>
                    </div>
                    {!! Former::select('task_id[]')->id('task_id')->label('Tugas')->multiple()->required() !!}
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-primary">Simpan</button>
                </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>
@endsection
