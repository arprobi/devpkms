@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">F.A.Q</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading">Daftar FAQ</div>
        <div class="panel-body">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" ><a href="{{route('admin.faqs')}}">Published</a></li>
                <li role="presentation" class="active"><a href="{{route('admin.faqs.unpublish')}}">Unpublished</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" >
                    <p>
                        <form method="get">
                            <div class="row">
                                <div class="col-xs-11">
                                    <input name="q2" type="text" class="form-control" placeholder="Pencarian">
                                </div>
                                <div class="col-xs-1">
                                    <button class="btn btn-md btn-success">Cari</button>
                                </div>
                            </div>
                        </form>
                    </p>
                    @if($faqs->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Question & Answer</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($faqs as $faq)
                                <tr>
                                    <td>{{strip_tags($faq->qa)}}</td>
                                    <td>{{ check_status($faq) }}</td>
                                    <td>
                                        {!! Former::open(route('admin.faqs.delete', $faq->id))->method('delete') !!}
                                        <a href="{{route('admin.faqs.edit', $faq->id)}}" class="btn btn-xs btn-link">Edit</a>
                                        <button class="btn btn-xs btn-link"><span class="text-danger">Hapus</span></button>
                                        {!! Former::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-right">
                        {{$faqs->appends(request()->all())->links()}}
                    </div>
                    @else
                    FAQ tidak ditemukan.
                    @endif
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <a href="{{route('admin.faqs.new')}}" class="btn btn-default">Tambah data</a>
        </div>
    </div>
</div>
@endsection