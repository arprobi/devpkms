@extends('layouts.admin')

@section('content')
<div id="faq-create-page">
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.faqs')}}">F.A.Q</a></li>
        <li class="active">Tambah data</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading">Tambah data</div>
        {!! Former::open_for_files()->id('form-faq') !!}
        <div class="panel-body">
            {!! Former::radios('klasifikasi')->radios([
                'Infografis'    => ['1' => 'klasifikasi', 'value' => '1'],
                'Studi kasus'   => ['2' => 'klasifikasi', 'value' => '2'],
                'Video'         => ['3' => 'klasifikasi', 'value' => '3'],
                'Materi paparan'=> ['4' => 'klasifikasi', 'value' => '4'],
                ])->check('1')->required() !!}
            {!! Former::select('article_code')->label('Kodefikasi')->fromQuery(PKMS\Models\MasterArticleCode::get(),'name','id')->required() !!}
            {!! Former::select('article_subcode')->label('Subkodefikasi')->fromQuery(PKMS\Models\MasterArticleSubcode::where('code', 1)->get(),'name','id')->required() !!}
            {!! Former::text('title')->label('Judul')->placeholder('judul tulisan')->required() !!}
            {!! Former::file('image')->label('Infografis')->accept('.jpg, .jpeg, .png, .bmp')->help('tipe: .jpg, .jpeg, .png, .bmp, Ukuran file: 8MB') !!}
            <div class="image-placeholder" style="margin-bottom: 10px"><img class="img img-responsive img-thumbnail" src="" alt=""></div>
            {!! Former::file('materi')->label('File pdf')->id('materi')->accept('.pdf')->help('tipe: .pdf, Ukuran file: 8MB') !!}
            {!! Former::text('video')->label('ID Video')->placeholder('ID Video (ex: ?v=XUrr8Ay__lE) *Lihat di url') !!}
            {!! Former::textarea('faq')->addClass('summernote')->label('Dasar Hukum')->placeholder('dasar hukum') !!}
            {!! Former::textarea('qa')->addClass('summernote')->label('Tanya Jawab')->placeholder('konten') !!}
            {!! Former::textarea('answer')->addClass('summernote')->label('Jawaban')->placeholder('jawaban') !!}
            {!! Former::textarea('content_letter')->addClass('summernote')->label('Isi Surat')->placeholder('isi surat') !!}
            {!! Former::select('tags[]')->id('tags')->label('Tags')->multiple() !!}
            {!! Former::radios('terbit')->radios([
            'Ya'    => ['name' => 'published', 'value' => '1'],
            'Tidak' => ['name' => 'published', 'value' => '0']
            ])->required() !!}
        </div>
        <div class="panel-footer">
            <button type="button" class="btn btn-primary" id="simpan">Simpan</button>
            <a href="{{route('admin.faqs')}}" class="btn btn-default pull-right">Kembali ke halaman index</a>
        </div>
        {!! Former::close() !!}
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">  
    $(document).ready(function() {
        // onchange action
        var video   = $('#video'); //id video
        var faq     = $('#faq'); //dasar hukum
        var qa      = $('#qa'); //tanya jawab
        var letter  = $('#content_letter'); //isi surat
        var materi  = $('#materi') //materi pdf
        var answer  = $('#answer') //answer

        video.parent().hide();
        qa.parent().hide();
        faq.parent().show();
        materi.parent().hide();
        letter.parent().hide();
        answer.parent().hide();

        $('input[name=klasifikasi]').on('change', function(){
            if (this.value == 1) {
                //artikel
                video.parent().hide();
                qa.parent().hide();
                materi.parent().hide();
                letter.parent().hide();
                answer.parent().hide();
                //show
                faq.parent().show();
                $('.form-group label[for=faq]').text('Dasar Hukum').prop('required', true)
            } else if(this.value == 2) {
                //studi kasus
                video.parent().hide();
                faq.parent().hide();
                materi.parent().hide();
                letter.parent().hide();
                //show
                qa.parent().show();
                answer.parent().show();
                $('.form-group label[for=qa]').text('Pertanyaan').prop('required', true)
            } else if(this.value == 3) {
                //video
                qa.parent().hide();
                materi.parent().hide();
                letter.parent().hide();
                answer.parent().hide();
                //show
                faq.parent().show();
                video.parent().show();
                $('.form-group label[for=faq]').text('Deskripsi Singkat').prop('required', true)
            }else{
                //materi paparan
                materi.parent().show();
                faq.parent().show();
                //hide
                $('.form-group label[for=faq]').text('Deskripsi Singkat').prop('required', true)
                qa.parent().hide();
                answer.parent().hide();
                video.parent().hide();
                letter.parent().hide();
            }
        })


        // validation
        $('#simpan').on('click', function () {
            var selected = $('input[name=klasifikasi]:checked').val();

            if (selected == 1) {
                if (!faq.val()) {
                    swal({
                        title: 'Oops!',
                        text: 'Dasar hukum harus diisi!',
                        type: 'error'
                    });
                    faq.focus();
                } else {
                    $('#form-faq').submit();
                }
            }

            if (selected == 2) {
                if (!qa.val()) {
                    swal({
                        title: 'Oops!',
                        text: 'Pertanyaan harus diisi!',
                        type: 'error'
                    });
                    qa.focus();
                } else if(!answer.val()) {
                    swal({
                        title: 'Oops!',
                        text: 'Jawaban harus diisi!',
                        type: 'error'
                    });
                    letter.focus();
                } else {
                    $('#form-faq').submit();
                }
            }

            if (selected == 3) {
                if (!video.val()) {
                    swal({
                        title: 'Oops!',
                        text: 'ID Video harus diisi!',
                        type: 'error'
                    });
                    video.focus();
                } else {
                    $('#form-faq').submit();
                }
            }

            if (selected == 4) {
                if (!materi.val()) {
                    swal({
                        title: 'Oops!',
                        text: 'Materi harus diisi!',
                        type: 'error'
                    });
                    materi.focus();
                } else if(!faq.val()) {
                    swal({
                        title: 'Oops!',
                        text: 'Deskripsi singkat harus diisi!',
                        type: 'error'
                    });
                    faq.focus();
                } else {
                    $('#form-faq').submit();
                }
            }
        });
    });
</script>
@endsection