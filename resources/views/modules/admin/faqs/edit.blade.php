@extends('layouts.admin')

@php
$tags = explode(',', $faq->tags);
@endphp

@section('content')
<div id="faq-create-page">
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.faqs')}}">F.A.Q</a></li>
        <li class="active">Edit data</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading">Edit data</div>
        {!! Former::populate($faq) !!}
        {!! Former::open_for_files()->id('form-faq') !!}
        <div class="panel-body">
            {!! Former::radios('klasifikasi')->radios([
                'Infografis'    => ['1' => 'klasifikasi', 'value' => '1'],
                'Studi kasus'   => ['2' => 'klasifikasi', 'value' => '2'],
                'Video'         => ['3' => 'klasifikasi', 'value' => '3'],
                'Materi paparan'=> ['4' => 'klasifikasi', 'value' => '4'],
                ])->required() !!}

            {!! Former::select('article_code')->label('Kodefikasi')->fromQuery(PKMS\Models\MasterArticleCode::get(),'name','id')->required() !!}
            {!! Former::select('article_subcode')->label('Subkodefikasi')->fromQuery(PKMS\Models\MasterArticleSubcode::where('code', $faq->article_code)->get(),'name','id')->required() !!}
            {!! Former::text('title')->label('Judul')->placeholder('judul tulisan')->required() !!}
            {!! Former::file('image')->label('Infografis')->accept('.jpg, .jpeg, .png, .bmp')->help('tipe: .jpg, .jpeg, .png, .bmp, Ukuran file: 8MB') !!}
            <div class="image-placeholder" style="margin-bottom: 10px"><img class="img img-responsive img-thumbnail" src="{{ $faq->image ? secure_asset('storage/'. $faq->image) : '' }}" alt=""></div>
            {!! Former::file('materi')->label('File pdf')->id('materi')->accept('.pdf')->help('tipe: .pdf, Ukuran file: 8MB') !!}
            {!! Former::text('video')->label('ID Video')->placeholder('ID Video (ex: ?v=XUrr8Ay__lE) *Lihat di url') !!}
            {!! Former::textarea('faq')->addClass('summernote')->label('Dasar Hukum')->placeholder('dasar hukum') !!}
            {!! Former::textarea('qa')->addClass('summernote')->label('Tanya Jawab')->placeholder('konten') !!}
            {!! Former::textarea('answer')->addClass('summernote')->label('Jawaban')->placeholder('pertanyaan') !!}
            {!! Former::textarea('content_letter')->addClass('summernote')->label('Isi Surat')->placeholder('isi surat') !!}
            {!! Former::select('tags[]')->id('tags')->label('Tags')->options($tags, $tags, true)->placeholder('tags')->multiple() !!}
            {!! Former::radios('terbit')->radios([
            'Ya'    => ['name' => 'published', 'value' => '1'],
            'Tidak' => ['name' => 'published', 'value' => '0']
            ])->required() !!}
        </div>
        <div class="panel-footer">
            <button type="button" class="btn btn-primary" id="simpan">Simpan</button>
            <a href="{{route('admin.faqs')}}" class="btn btn-default pull-right">Kembali ke halaman index</a>
        </div>
        {!! Former::close() !!}
        <input type="hidden" name="filemateri" id="filemateri" value="{{$faq->materi}}">
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">  
    $(document).ready(function() {
        // onchange action
        var video   = $('#video'); //id video
        var faq     = $('#faq'); //dasar hukum
        var qa      = $('#qa'); //tanya jawab
        var letter  = $('#content_letter'); //isi surat
        var materi  = $('#materi') //materi pdf
        var answer  = $('#answer') //answer

        var diselect=  $('input[name=klasifikasi]:checked').val();

        if (diselect == 1) {
            //artikel
            video.parent().hide();
            qa.parent().hide();
            materi.parent().hide();
            letter.parent().hide();
            answer.parent().hide();
            //show
            faq.parent().show();
            $('.form-group label[for=faq]').text('Dasar Hukum').prop('required', true)
        } else if(diselect == 2) {
            //studi kasus
            video.parent().hide();
            faq.parent().hide();
            materi.parent().hide();
            //show
            answer.parent().show();
            qa.parent().show();
            letter.parent().show();
            $('.form-group label[for=qa]').text('Pertanyaan').prop('required', true)
        } else if(diselect == 3) {
            //video
            qa.parent().hide();
            materi.parent().hide();
            letter.parent().hide();
            answer.parent().hide();
            //show
            faq.parent().show();
            $('.form-group label[for=faq]').text('Deskripsi Singkat').prop('required', true)
            video.parent().show();
        }else{
            //materi paparan
            materi.parent().show();
            faq.parent().show();
            letter.parent().hide();
            answer.parent().hide();
            //hide
            $('.form-group label[for=faq]').text('Deskripsi Singkat').prop('required', true)
            qa.parent().hide();
            video.parent().hide();
        }

        $('input[name=klasifikasi]').on('change', function(){
            if (this.value == 1) {
                //artikel
                video.parent().hide();
                qa.parent().hide();
                materi.parent().hide();
                letter.parent().hide();
                answer.parent().hide();
                //show
                faq.parent().show();
                $('.form-group label[for=faq]').text('Dasar Hukum').prop('required', true)
            } else if(this.value == 2) {
                //studi kasus
                video.parent().hide();
                faq.parent().hide();
                materi.parent().hide();
                //show
                answer.parent().show();
                qa.parent().show();
                letter.parent().show();
                $('.form-group label[for=qa]').text('Pertanyaan').prop('required', true)
            } else if(this.value == 3) {
                //video
                qa.parent().hide();
                materi.parent().hide();
                letter.parent().hide();
                answer.parent().hide();
                //show
                faq.parent().show();
                $('.form-group label[for=faq]').text('Deskripsi Singkat').prop('required', true)
                video.parent().show();
            }else{
                //materi paparan
                materi.parent().show();
                faq.parent().show();
                answer.parent().hide();
                //hide
                $('.form-group label[for=faq]').text('Deskripsi Singkat').prop('required', true)
                qa.parent().hide();
                video.parent().hide();
                letter.parent().hide();
            }
        })


        // validation
        $('#simpan').on('click', function () {
            var selected = $('input[name=klasifikasi]:checked').val();

            if (selected == 1) {
                if (!faq.val()) {
                    swal({
                        title: 'Oops!',
                        text: 'Dasar hukum harus diisi!',
                        type: 'error'
                    });
                    faq.focus();
                } else {
                    $('#form-faq').submit();
                }
            }

            if (selected == 2) {
                if (!qa.val()) {
                    swal({
                        title: 'Oops!',
                        text: 'Tanya jawab harus diisi!',
                        type: 'error'
                    });
                    qa.focus();
                } else if(!letter.val()) {
                    swal({
                        title: 'Oops!',
                        text: 'Isi surat harus diisi!',
                        type: 'error'
                    });
                    letter.focus();
                } else if(!answer.val()) {
                    swal({
                        title: 'Oops!',
                        text: 'Isi surat harus diisi!',
                        type: 'error'
                    });
                    letter.focus();
                } else {
                    $('#form-faq').submit();
                }
            }

            if (selected == 3) {
                if (!video.val()) {
                    swal({
                        title: 'Oops!',
                        text: 'ID Video harus diisi!',
                        type: 'error'
                    });
                    video.focus();
                } else {
                    $('#form-faq').submit();
                }
            }

            if (selected == 4) {
                if (!materi.val() && !$('#filemateri').val()) {
                    swal({
                        title: 'Oops!',
                        text: 'Materi harus diisi!',
                        type: 'error'
                    });
                    materi.focus();
                } else if(!faq.val()) {
                    swal({
                        title: 'Oops!',
                        text: 'Deskripsi singkat harus diisi!',
                        type: 'error'
                    });
                    faq.focus();
                } else {
                    $('#form-faq').submit();
                }
            }
        });
    });
</script>
@endsection