@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.perpres')}}">Perpres</a></li>
        <li class="active">Tambah data</li>
    </ol>

    @include('modules.admin._alerts')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-graduation"></i> Master Perpres</div>

        {!! Former::open(route('admin.perpres.store')) !!}

        <div class="panel-body">
            {!! Former::text('nama_perpres')->label('Nama Perpres')->placeholder('Nama Perpres')->required() !!}
        </div>

        <div class="panel-footer">
            <button class="btn btn-success">Simpan</button>
            <a href="{{route('admin.perpres')}}" class="btn btn-default pull-right">Kembali ke index</a>
        </div>

        {!! Former::close() !!}

    </div>
</div>
@endsection