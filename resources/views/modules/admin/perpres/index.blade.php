@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">PERPRES</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading">Daftar Perpres</div>
        <div class="panel-body">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                    <p>
                        <form method="get">
                            <div class="row">
                                <div class="col-xs-11">
                                    <input name="q" type="text" class="form-control" placeholder="Pencarian">
                                </div>
                                <div class="col-xs-1">
                                    <button class="btn btn-md btn-success">Cari</button>
                                </div>
                            </div>
                        </form>
                    </p>
                    @if($perpres->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Nama Perpres</th>
                                    <th>Dibuat Pada</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($perpres as $perp)
                                <tr>
                                    <td>{{$perp->nama_perpres}}</td>
                                    <td>{{$perp->created_at}}</td>
                                    <td>
                                        {!! Former::open(route('admin.perpres.delete', $perp->id))->method('delete') !!}
                                        <a href="{{route('admin.perpres.edit', $perp->id)}}" class="btn btn-sm btn-default">Edit</a>
                                        <button class="btn btn-sm btn-delete btn-danger">Hapus</button>
                                        {!! Former::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-left">
                        <a href="{{route('admin.perpres.create')}}" class="btn btn-success">Tambah data</a>
                    </div>
                    <div class="pull-right">
                        {{$perpres->appends(request()->all())->links()}}
                    </div>
                    @else
                    <div class="pull-left">
                        <a href="{{route('admin.perpres.create')}}" class="btn btn-success">Tambah data</a>
                    </div>
                    <div class="pull-right">
                        Perpres tidak ditemukan.
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection