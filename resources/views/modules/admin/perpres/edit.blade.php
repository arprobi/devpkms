@extends('layouts.admin')

@section('content')
<div id="faq-create-page">
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.perpres')}}">PERPRES</a></li>
        <li class="active">Edit data</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading">Edit data</div>
        {!! Former::populate($perpres) !!}
        {!! Former::open(route('admin.perpres.update', $perpres->id))->method('post') !!}
        <div class="panel-body">
            {!! Former::text('nama_perpres')->label('Nama Perpres')->placeholder('nama perpres')->required() !!}
        </div>
        <div class="panel-footer">
            <button class="btn btn-primary">Simpan</button>
            <a href="{{route('admin.perpres')}}" class="btn btn-default pull-right">Kembali ke halaman index</a>
        </div>
        {!! Former::close() !!}
    </div>
</div>
@endsection