@extends('layouts.admin')

@section('content')

<div id="questions">
	<ol class="breadcrumb flat">
		<li><a href="{{route('admin.questions')}}">Pertanyaan</a></li>
		<li class="active">{{$question->title}}</li>
	</ol>

	@include('modules.admin._alerts')

	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">Pertanyaan</div>
				<div class="panel-body">

					<div class="form-group">
						<label>Pengirim</label>
						<div class="form-control-static">{{$question->name}} <code>{{$question->email}}</code></div>
					</div>

					<div class="form-group">
						<label>Judul</label>
						<div class="form-control-static">{{$question->title}}</div>
					</div>

					<div class="form-group">
						<label>Waktu</label>
						<div class="form-control-static">{{$question->created_at}}</div>
					</div>

					<div class="form-group">
						<label>Pertanyaan</label>
						<div class="form-control-static">{{$question->content}}</div>
					</div>

					<hr>

					<div class="form-group">
						<label>Jawaban</label>
						@if($question->answers->count())
							<ul class="media-list">
								@foreach($question->answers as $answer)
									<li class="media">
										<div class="media-left">
											<img src="{{ "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $answer->email ) ) ) . "?s=48" }}" alt="avatar" class="img-circle">
										</div>
										<div class="media-body">
											<h5 class="media-heding">{{$answer->name}} <code>{{$answer->email}}</code></h5>
											<p>{{$answer->content}}</p>
										</div>
									</li>
								@endforeach
							</ul>
						@else
							<div class="form-control-static">Belum ada jawaban</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">Jawab pertanyaan</div>
				{!! Former::open(route('admin.questions.answer', $question->id)) !!}
					<div class="panel-body">
						{!! Former::textarea('content')->label('')->placeholder('Jawaban')->rows(4)->required() !!}
					</div>
					<div class="panel-footer">
						<button class="btn btn-primary">Simpan</button>
						<a href="{{route('admin.questions')}}" class="btn btn-default">Kembali</a>
					</div>
				{!! Former::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection