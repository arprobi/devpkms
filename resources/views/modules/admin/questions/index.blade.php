@extends('layouts.admin')

@section('content')

<div id="questions">
	<ol class="breadcrumb flat">
		<li class="active">Pertanyaan</li>
	</ol>

	@include('modules.admin._alerts')

	<div class="panel panel-default">
		<div class="panel-heading">
			Daftar pertanyaan
		</div>
		@if($questions->count())
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>Nama</th>
							<th>Email</th>
							<th>Subject</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($questions as $question)
							<tr>
								<td>{{$question->name}}</td>
								<td>{{$question->email}}</td>
								<td>{{$question->title}}</td>
								<td class="text-right"><a href="{{route('admin.questions.detail', $question->id)}}" class="btn btn-default btn-sm">Detil</a></td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="panel-footer">
				{!! $questions->links() !!}
			</div>
		@endif
	</div>
</div>
@endsection