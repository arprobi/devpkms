@extends('layouts.admin')

@section('content')
<div id="faq-create-page">
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.kategories')}}">KATEGORI</a></li>
        <li class="active">Edit data</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading">Edit data</div>
        {!! Former::populate($kategori) !!}
        {!! Former::open(route('admin.kategories.update', $kategori->id))->method('post') !!}
        <div class="panel-body">
            {!! Former::text('nama_kategori')->label('Nama kategori')->placeholder('nama kategori')->required() !!}
        </div>
        <div class="panel-footer">
            <button class="btn btn-primary">Simpan</button>
            <a href="{{route('admin.kategories')}}" class="btn btn-default pull-right">Kembali ke halaman index</a>
        </div>
        {!! Former::close() !!}
    </div>
</div>
@endsection