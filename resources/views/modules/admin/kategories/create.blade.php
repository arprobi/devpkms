@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.kategories')}}">Tag</a></li>
        <li class="active">Tambah data</li>
    </ol>

    @include('modules.admin._alerts')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-graduation"></i> Master Tag</div>

        {!! Former::open(route('admin.kategories.store')) !!}

        <div class="panel-body">
            {!! Former::text('nama_kategori')->label('Nama Tag')->placeholder('Nama Tag')->required() !!}
        </div>

        <div class="panel-footer">
            <button class="btn btn-success">Simpan</button>
            <a href="{{route('admin.kategories')}}" class="btn btn-default pull-right">Kembali ke index</a>
        </div>

        {!! Former::close() !!}

    </div>
</div>
@endsection