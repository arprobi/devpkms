@extends('layouts.admin')

@section('content')
<div id="expert-detail-page">
    {!! Former::populate($admin) !!}
    {!! Former::open(route('admin.admins.update', $admin->id)) !!}

        <div class="row">
            <div class="col-md-2">
                <div align="center">
                    <img src="{{ $admin->avatar ? secure_asset('storage/'.$admin->avatar) : secure_asset('img/default_avatar.jpg') }}" alt="avatar" width="128px" height="128px" class="img img-thumbnail img-circle img-responsive margin-bottom-10">
                </div>
            </div>
            <div class="col-md-10">

                <div class="panel panel-default">
                    <div class="panel-heading">Edit Admin</div>

                        <div class="panel-body">
                            {!! Former::text('name')->label('Nama lengkap')->placeholder('Nama Lengkap')->required() !!}
                            {!! Former::text('username')->label('Username')->placeholder('Username')->required() !!}
                            {!! Former::email('email')->label('Email')->placeholder('Alamat Email')->required() !!}
                            {!! Former::password('password')->label('Password')->placeholder('Change password') !!}

                            {!! Former::radios('Terverifikasi')->radios([
                                'Ya' => ['name' => 'is_verified', 'value' => '1'],
                                'Tidak' => ['name' => 'is_verified', 'value' => '0']
                            ])->required() !!}

                            {!! Former::radios('Aktif')->radios([
                                'Ya' => ['name' => 'is_active', 'value' => '1'],
                                'Tidak' => ['name' => 'is_active', 'value' => '0']
                            ])->required() !!}
                        </div>

                        <div class="panel-footer">
                            <button class="btn btn-success">Simpan</button>
                        </div>

                </div>

            </div>
        </div>
    {!! Former::close() !!}
</div>
@endsection
