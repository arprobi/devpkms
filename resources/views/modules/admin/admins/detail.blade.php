@extends('layouts.admin')

@section('content')
<div id="expert-detail-page">
    {!! Former::populate($admin) !!}
    {!! Former::open() !!}

        <div class="row">
            <div class="col-md-2">
                <div align="center">
                    <img src="{{ $admin->avatar ? secure_asset('storage/'.$admin->avatar) : secure_asset('img/default_avatar.jpg') }}" alt="avatar" width="128px" height="128px" class="img img-thumbnail img-circle img-responsive margin-bottom-10">
                </div>
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    {{-- Profile  --}}
                    <div class="panel-heading"><i class="fa fa-fw icon-user"></i> Profil</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label>Nama</label>
                            <div class="form-control">{{$admin->name}}</div>
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <div class="form-control">{{$admin->email}}</div>
                        </div>

                        {{-- Profile  --}}
                        @if($admin->profile)
                            <div class="form-group">
                                <label>Jenis kelamin</label>
                                <div class="form-control">{{$admin->profile->gender == 'm' ? 'Laki-laki' : 'Perempuan'}}</div>
                            </div>

                            <div class="form-group">
                                <label>Tempat/Tanggal Lahir</label>
                                <div class="form-control">{{$admin->profile->birth_place}}, {{$admin->profile->birth_date}}</div>
                            </div>

                            <div class="form-group">
                                <label>Alamat</label>
                                <div class="form-control">{{$admin->profile->address}}</div>
                            </div>

                            <div class="form-group">
                                <label>Kode Pos</label>
                                <div class="form-control">{{$admin->profile->zip_code}}</div>
                            </div>

                            <div class="form-group">
                                <label>Telephone</label>
                                <div class="form-control">{{$admin->profile->phone}}</div>
                            </div>

                            <div class="form-group">
                                <label>Handphone</label>
                                <div class="form-control">{{$admin->profile->mobile}}</div>
                            </div>
                        @else
                            Pengguna ini belum mengisi data profil.
                        @endif
                    </div>

                    {{-- Education  --}}
                    <div class="panel-heading"><i class="fa fa-fw icon-doc"></i> Edukasi</div>
                    @if($admin->educations->count())
                        <div class="panel-body">
                            <dl class="list-unstyled">
                            @foreach($admin->educations as $education)
                                <dt>{{$education->detail->code}} {{$education->name}}</dt>
                                <dd>{{$education->major}}</dd>
                                <dd>{{$education->gpa}}</dd>
                            @endforeach
                            </dl>
                        </div>
                    @else
                        <div class="panel-body">
                            Pengguna ini belum mengisi data edukasi.
                        </div>
                    @endif

                    {{-- Experience  --}}
                    <div class="panel-heading"><i class="fa fa-fw icon-briefcase"></i> Pengalaman Kerja</div>
                    @if($admin->experiences->count())
                        <div class="panel-body">
                            <dl class="list-unstyled">
                            @foreach($admin->experiences as $experience)
                                <dt>{{$experience->title}} ({{$experience->from}} hingga {{$experience->to}})</dt>
                                <dd>{{$experience->position}}</dd>
                                <dd>{{$experience->desc}}</dd>
                                @if($experience->attachments->count())
                                    <dd>
                                        <ul class="list-inline">
                                            @foreach($experience->attachments as $attachment)
                                                <li><a href="{{secure_asset('storage/'.$attachment->path)}}" target="_blank" class="btn btn-default btn-sm">{{$attachment->title}}</a></li>
                                            @endforeach
                                        </ul>
                                    </dd>
                                @endif
                            @endforeach
                            </dl>
                        </div>
                    @else
                        <div class="panel-body">
                            Pengguna ini belum mengisi data pengalaman kerja.
                        </div>
                    @endif

                    <div class="panel-footer">
                        @if(!$admin->is_verified)
                            @if($admin->isComplete())
                                <button class="btn btn-success">Verifikasi</button>
                            @else
                                <a href="{{url()->previous()}}" class="btn btn-default">Kembali</a>
                            @endif
                        @else

                        @endif
                    </div>
                </div>
            </div>
        </div>

    {!! Former::close() !!}
</div>
@endsection
