@extends('layouts.admin')

@section('content')
<div id="bantuan-create-page">
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.admins')}}">Admin</a></li>
        <li class="active">{{ $create ? 'Tambah' : 'Ubah' }} data</li>
    </ol>

    @include('modules.admin._alerts')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-graduation"></i> Form Admin</div>

        @if($create)
        {!! Former::open_for_files(route('admin.admins.store'))->id('form-input') !!}
        @else
        {!! Former::populate($admin) !!}
        {!! Former::open_for_files(route('admin.admins.update', $admin->id))->id('form-input') !!}
        @endif
        <div class="panel-body">
            
            {!! Former::text('name')->label('Nama')->placeholder('Nama')->required() !!}
            {!! Former::select('gender')->label('Jenis Kelamin')->options(['m' => 'Laki-laki', 'f' => 'Perempuan'])->required() !!}
            {!! Former::text('username')->label('Username')->placeholder('Nama')->required() !!}
            {!! Former::email('email')->label('Email')->placeholder('Nama')->required() !!}
            {!! Former::password('password')->label('Password')->placeholder('Password')->required() !!}
            {!! Former::text('mobile')->label('Nomor HP')->placeholder('Nomor HP')->required() !!}
            {!! Former::text('birth_place')->label('Tempat Lahir')->placeholder('Tempat Lahir') !!}
            {!! Former::text('birth_date')->label('Tanggal lahir')->placeholder('Tanggal lahir') !!}
            {!! Former::text('phone')->label('Nomor Telepon')->placeholder('Telepon') !!}
            {!! Former::textarea('address')->label('Alamat')->placeholder('Alamat') !!}
        </div>

        <div class="panel-footer">
            <button class="btn btn-success">Simpan</button>
            <a href="{{route('admin.admins')}}" class="btn btn-default">Kembali ke index</a>
        </div>

        {!! Former::close() !!}

    </div>
</div>
@endsection

@section('script')

<script type="text/javascript">
    $(function () {
        $('#birth_date').datetimepicker({
            locale: 'id',
            defaultDate: new Date(),
            minDate:new Date(),
            format:'YYYY-MM-DD',
        });
    });
</script>

@endsection