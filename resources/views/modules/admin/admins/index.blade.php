@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">ADMIN</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-filter"></i> Filters</div>
        <div class="panel-body">
            <div class="pull-left visible-md visible-lg">
                {!! Former::open_inline()->method('get') !!}

                {!! Former::text('query')->class('form-control')->label('Query')->placeholder('Query') !!}

                <button class="btn btn-success"><i class="fa fa-fw fa-filter"></i> Filter</button>
                
                {!! Former::close() !!}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Daftar Admin</div>
        <div class="panel-body">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                    @if($admins->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th colspan="2" class="text-center text-middle">Nama</th>
                                    <th>Phone Number</th>
                                    <th class="text-center text-middle">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($admins as $admin)
                                <tr>
                                    <td width="64">
                                        <img src="{{ $admin->avatar ? secure_asset('storage/'.$admin->avatar) : secure_asset('img/default_avatar.jpg') }}" alt="avatar" width="64px" height="64px">
                                    </td>
                                    <td>
                                        <a href="{{route('admin.admins.detail', $admin->id)}}" target="_blank">{{$admin->name}}</a> <code>{{$admin->email}}</code>
                                    </td>
                                    <td>
                                        {{ $admin->profile ? $admin->profile->mobile : '' }}
                                    </td>
                                    <td align="center">
                                        {!! Former::open(route('admin.admins.delete', $admin->id))->method('delete') !!}
                                            <a href="{{route('admin.admins.edit', $admin->id)}}" class="btn btn-default btn-sm">Edit</a>
                                            <button class="btn btn-delete btn-sm btn-danger">Hapus</button>
                                        {!! Former::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-left">
                        <a href="{{route('admin.admins.create')}}" class="btn btn-success">Tambah data</a>
                    </div>
                    <div class="pull-right">
                        {{$admins->appends(request()->all())->links()}}
                    </div>
                    @else
                    <div class="pull-left">
                        <a href="{{route('admin.admins.create')}}" class="btn btn-success">Tambah data</a>
                    </div>
                    <div class="pull-right">
                        Bantuan tidak ditemukan.
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection