@extends('layouts.admin')

@section('content')
<div>
    .{{-- Breadcrumb  --}}
    <ol class="breadcrumb">
        <li><a href="{{route('admin.messages')}}"><i class="fa fa-fw fa-envelope"></i> Inbox</a></li>
        <li class="active">Detail</li>
    </ol>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="media">
                <div class="media-left">
                    <img src="{{ $message->fromUser->avatar ? secure_asset('storage/'.$message->fromUser->avatar) : secure_asset('img/default_avatar.jpg') }}" alt="avatar" width="64px" height="64px" class="img-circle">
                </div>
                <div class="media-body">
                    <h4 class="media-heading"> {{$message->subject or 'No Subject'}} </h4>
                    <p>
                        <span class="label label-success">Dari : {{$message->fromUser->name}}</span>
                        <span class="label label-success">Untuk : {{$message->toUser->name}}</span>
                        <span class="label label-primary">{{$message->created_at->diffForHumans()}}</span>
                    </p>
                    <p>{{$message->message}}</p>
                </div>
            </div>
        </div>
    </div>

    @if($message->replies->count())
        @php $replyIndex = 1; @endphp
        @foreach($message->replies as $reply)
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        @if($replyIndex % 2)
                            <div class="media-body">
                                <h4 class="media-heading"> {{$reply->subject or 'No Subject'}} </h4>
                                <p>
                                    <span class="label label-success">Dari : {{$reply->fromUser->name}}</span>
                                    <span class="label label-success">Untuk : {{$reply->toUser->name}}</span>
                                    <span class="label label-primary">{{$reply->created_at->diffForHumans()}}</span>
                                </p>
                                <p>{{$reply->message}}</p>
                            </div>
                            <div class="media-right">
                                <img src="{{ $reply->fromUser->avatar ? secure_asset('storage/'.$reply->fromUser->avatar) : secure_asset('img/default_avatar.jpg') }}" alt="avatar" width="64px" height="64px" class="img-circle">
                            </div>
                        @else
                            <div class="media-left">
                                <img src="{{ $reply->fromUser->avatar ? secure_asset('storage/'.$reply->fromUser->avatar) : secure_asset('img/default_avatar.jpg') }}" alt="avatar" width="64px" height="64px" class="img-circle">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading"> {{$reply->subject or 'No Subject'}} </h4>
                                <p>
                                    <span class="label label-success">Dari : {{$reply->fromUser->name}}</span>
                                    <span class="label label-success">Untuk : {{$reply->toUser->name}}</span>
                                    <span class="label label-primary">{{$reply->created_at->diffForHumans()}}</span>
                                </p>
                                <p>{{$reply->message}}</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @php $replyIndex++; @endphp
        @endforeach
    @endif

    <div class="panel panel-default">
        {!! Former::open(route('admin.messages.detail', $message->id)) !!}
        <div class="panel-body">
            {!! Former::textarea('message')->label('Balas pesan')->rows(4)->placeholder('Pesan')->required() !!}
        </div>
        <div class="panel-footer">
            <button class="btn btn-sm btn-success">Kirim</button>
        </div>
        {!! Former::close() !!}
    </div>
</div>
@endsection
