@extends('layouts.admin')

@section('content')
<div>
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-envelope"></i> Inbox</div>

        @if($messages->count())
            @foreach($messages as $message)
                <div class="panel-body">
                    <div class="media">
                        <div class="media-left">
                            <img src="{{ $message->fromUser->avatar ? secure_asset('storage/'.$message->fromUser->avatar) : secure_asset('img/default_avatar.jpg') }}" alt="avatar" width="64px" height="64px" class="img-circle">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">
                                @if(($message->to == Auth::id() && $message->read == 0) || ( $message->replies->where('to', Auth::id())->where('read', 0)->count() ))
                                        <i class="fa fa-circle text-primary"></i>
                                    @else
                                        <i class="fa fa-circle-o text-primary"></i>
                                    @endif
                                <a href="{{route('admin.messages.detail', $message->id)}}">
                                    {{$message->subject or 'No Subject'}}
                                </a>
                            </h4>
                            <p>
                                <span class="label label-success">Dari : {{$message->fromUser->name}}</span>
                                <span class="label label-success">Untuk : {{$message->toUser->name}}</span>
                                <span class="label label-primary">{{$message->created_at->diffForHumans()}}</span>
                            </p>
                            <p>{{str_limit($message->message, 100)}}</p>

                            @if($message->replies->count())
                                <code>Memiliki {{$message->replies->count()}} pesan balasan</code>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="panel-body">
                Anda tidak memiliki pesan masuk
            </div>
        @endif

        <div class="panel-footer">
            <a href="javascript:;" class="btn btn-sm btn-default" data-toggle="modal" data-target="#modal-new-message">Buat pesan baru</a>
            {{$messages->links()}}
        </div>
    </div>
</div>

{{-- Modal New Message  --}}
<div id="modal-new-message" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Pesan baru</h4>
            </div>
            {!! Former::open_for_files(route('admin.messages.send'))->method('post') !!}
                <div class="modal-body">
                    {!! Former::select('to')->label('Ke')->fromQuery(PKMS\Models\User::where('id', '!=', Auth::id())->get(), 'name', 'id')->required() !!}
                    {!! Former::text('subject')->label('Subjek')->placeholder('Subjek pesan')->required()  !!}
                    {!! Former::textarea('message')->label('Pesan')->rows(6)->placeholder('Pesan')->required() !!}
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-primary">Simpan</button>
                </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>

@endsection