@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">DETAIL PASAL</li>
    </ol>

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-gavel"> </i> Pasal</div>
        <div class="panel-body">
            <div class="form-group">
                <label>Perpres</label>
                <div class="form-control-static">{{$pasal->perpres->nama_perpres}}</div>
            </div>
            <div class="form-group">
                <label>Judul/Nama Pasal</label>
                <div class="form-control-static">{{$pasal->nama_pasal}}</div>
            </div>
            <div class="form-group">
                <label>Isi Pasal</label>
                <div class="form-control-static">{!!$pasal->isi_pasal!!}</div>
            </div>
            <div class="form-group">
                <label>Penjelasan</label>
                <div class="form-control-static">{!!$pasal->penjelasan!!}</div>
            </div>
            <div class="form-group">
                <label>Tag</label>
                <div class="form-control-static">{!!$pasal->kategori!!}</div>
            </div>
            <div class="form-group">
                <label>Dibuat Tanggal</label>
                <div class="form-control-static">{!!$pasal->created_at!!}</div>
            </div>
            <div class="form-group">
                <label>Terakhir diubah</label>
                <div class="form-control-static">{!!$pasal->updated_at!!}</div>
            </div>
            <div class="form-group">
                <div class="form-control-static">
                    <a href="{{route('admin.pasals.edit', $pasal->id)}}" class="btn btn-sm btn-primary">Edit</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection