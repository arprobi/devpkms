@extends('layouts.admin')

@section('content')
<div id="pasal-create-page">
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.pasals')}}">Pasal</a></li>
        <li class="active">Tambah data</li>
    </ol>

    @include('modules.admin._alerts')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-graduation"></i> Master Pasal</div>

        {!! Former::open(route('admin.pasals.store')) !!}

        <div class="panel-body">
            {!! Former::textarea('nama_pasal')->addClass('summernote')->label('Nama Pasal / Judul')->placeholder('Nama Pasal')->required() !!}

            {!! Former::select('perpres_id')->label('Perpres')->placeholder('Perpres')->fromQuery(PKMS\Models\Perpres::get(), 'nama_perpres', 'id')->required() !!}

            {!! Former::textarea('isi_pasal')->addClass('summernote')->label('Isi')->placeholder('Isi')->rows(10)->required() !!}
            
            {!! Former::textarea('penjelasan')->addClass('summernote')->label('Penjelasan')->placeholder('Penjelasn') !!}

            {!! Former::select('kategori')->label('Tags')->addId('kategori')->options(['' => 'Pilih tags'])->fromQuery(PKMS\Models\Kategori::get(), 'nama_kategori', 'id')->placeholder('Tags')->required() !!}

            {!! Former::hidden('kategori_id')->addId('kategori_id')->value('') !!}

            <ul class="list-inline kategori" id="list-kategori">
                <!-- Dynamic list -->
            </ul>

            <div class="row">
                <div class="col-md-3">
                    {!! Former::radios('posisi')->radios([
                        'Di awal'   => ['name' => 'posisi', 'value' => '0'],
                        'Setelah'   => ['name' => 'posisi', 'value' => '1'],
                        'Di akhir'  => ['name' => 'posisi', 'value' => '2', 'checked']
                        ])->label('Posisi Pasal') !!}
                </div>
                <div class="col-md-9" id="posisi_pasal" style="display: none;">
                    {!! Former::select('pasal')->label('Setelah Pasal')->placeholder('Setelah pasal') !!}
                </div>
            </div>

        </div>

        <div class="panel-footer">
            <button class="btn btn-success">Simpan</button>
            <a href="{{route('admin.pasals')}}" class="btn btn-default pull-right">Kembali ke index</a>
        </div>

        {!! Former::close() !!}

    </div>
</div>
@endsection

@section('script')
<!-- Script untuk pilih multiple tags/kategori -->
<script type="text/javascript">
    $(document).ready(function() {

        var kategori = $('#kategori');
        var selected = '';
        kategori.on('select', function() 
        {
            selected = $("#kategori option:selected").text();
            addList(selected, kategori.val());
        });

        var addList = function(text, val)
        {
            var html = '<li style="margin-bottom: 4px;" class="list_'+val+'">'+
                            '<div class="btn-group btn-group-sm">'+
                                '<a class="btn btn-default">'+ text +'</a>'+ 
                                '<a class="btn btn-default del-list"><i class="fa fa-times text-danger"></i></a>'+ 
                                '<input type="hidden" name="kategori_id[]" value="'+ val +'">'+ 
                            '</div>'+
                        '<li>';
            
            if (!$('.list_'+val).length) {
                $('#list-kategori').append(html);
            }
            clearLi();
        }

        $(document).on('click', '.del-list', function (e) {
            $(this).parent().parent().remove();
            clearLi();
        });

        var clearLi = function()
        {
            $('#list-kategori li:empty').remove();
        }

    });
</script>

<!-- Script untuk posisi pasal -->
<script type="text/javascript">
    $(document).ready(function() {
        $('input:radio').change(function(){
            var value = $("form input[type='radio']:checked").val();
            if (value == 1) {
                $('#posisi_pasal').show();
            } else {
                $('#posisi_pasal').hide();
            }
        });


        $('#perpres_id').on('change', function() {
            var perpres_id = $(this).val();
            getPasal(perpres_id);
        });

        var getPasal = function(perpres_id) {
            $.ajax({
                url: '/api/perpres/'+perpres_id,
                type: 'GET'
            })
            .done(function(resp) {
                $('.added').remove();
                $.each(resp, function(index, val) {
                    $('#pasal').append('<option value="'+val.id+'" class="added">'+val.nama_pasal+'</option>');
                });
            })
            .fail(function(resp) {
                console.log(resp);
            });
            
        }
    });
</script>
@endsection