@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">PASAL</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-filter"></i> Filters</div>
        <div class="panel-body">
            <div class="pull-left visible-md visible-lg">
                {!! Former::open_inline()->method('get') !!}

                {!! Former::select('perpres')->class('form-control')->label('Perpres')->options(['' => 'Pilih perpres'])->fromQuery(PKMS\Models\Perpres::get(), 'nama_perpres', 'id')->style('min-width:270px;') !!}

                {!! Former::text('nama_pasal')->class('form-control')->label('Nama Pasal')->placeholder('Nama pasal')->style('min-width:270px;') !!}

                {!! Former::text('isi_pasal')->class('form-control')->label('Isi Pasal')->placeholder('Isi pasal')->style('min-width:270px;') !!}

                <button class="btn btn-success"><i class="fa fa-fw fa-filter"></i> Filter</button>
                {!! Former::close() !!}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Daftar Pasal</div>
        <div class="panel-body">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                    @if($pasals->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Nama Pasal</th>
                                    <th>Isi Pasal</th>
                                    <th>Tags</th>
                                    <th>Dibuat Pada</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pasals as $pasal)
                                <tr>
                                    <td>{{str_limit(strip_tags($pasal->nama_pasal), 50)}}</td>
                                    <td>{{str_limit(strip_tags($pasal->isi_pasal), 50)}}</td>
                                    <td>{!!$pasal->kategori!!}</td>
                                    <td>{{$pasal->created_at}}</td>
                                    <td>
                                        {!! Former::open(route('admin.pasals.delete', $pasal->id))->method('delete') !!}
                                        <a href="{{route('admin.pasals.edit', $pasal->id)}}" class="btn btn-sm btn-default">Edit</a>
                                        <a href="{{route('admin.pasals.show', $pasal->id)}}" class="btn btn-sm btn-info">Lihat</a>
                                        <button class="btn btn-sm btn-delete btn-danger">Hapus</button>
                                        {!! Former::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-left">
                        <a href="{{route('admin.pasals.create')}}" class="btn btn-success">Tambah data</a>
                    </div>
                    <div class="pull-right">
                        {{$pasals->appends(request()->all())->links()}}
                    </div>
                    @else
                    <div class="pull-left">
                        <a href="{{route('admin.pasals.create')}}" class="btn btn-success">Tambah data</a>
                    </div>
                    <div class="pull-right">
                        Pasal tidak ditemukan.
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection