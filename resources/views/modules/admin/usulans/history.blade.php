@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">USULAN</li>
    </ol>

    @include('modules.admin._alerts')
    
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-fw fa-filter"></i> Filters</div>
        <div class="panel-body">
            <div class="pull-left visible-md visible-lg">
                {!! Former::open_inline()->method('get') !!}
                {!! Former::select('status')
                    ->class('form-control')
                    ->label('')
                    ->options([
                        '' => 'Semua',
                        '1' => 'Diproses',
                        '3' => 'Ditolak'])
                    ->style('min-width:250px;') !!}

                {!! Former::text('usulan')->class('form-control')->label('')->placeholder('Isi usulan')->style('min-width:250px;') !!}
                <button class="btn btn-success"><i class="fa fa-fw fa-filter"></i> Filter</button>
                {!! Former::close() !!}
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Daftar Usulan</div>
        <div class="panel-body">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="{{route('admin.usulans')}}">New</a></li>
                <li role="presentation" class="active"><a href="{{route('admin.usulans.history')}}">History</a></li>
            </ul>
            <div class="tab-content" style="padding-top: 15px;">
                <div role="tabpanel" class="tab-pane active">
                    @if($usulans->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Nama Pengusul</th>
                                    <th>Usulan</th>
                                    <th>Status</th>
                                    <th>Diusulkan Pada</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($usulans as $usulan)
                                <tr>
                                    <td>{{$usulan->user->name}}</td>
                                    <td>{{$usulan->usulan}}</td>
                                    <td>{!!$usulan->status_usulan!!}</td>
                                    <td>{{$usulan->created_at}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-right">
                        {{$usulans->appends(request()->all())->links()}}
                    </div>
                    @else
                    Usulan tidak ditemukan.
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection