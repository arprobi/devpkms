@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">USULAN</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading">Daftar Usulan</div>
        <div class="panel-body">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="{{route('admin.usulans')}}">New</a></li>
                <li role="presentation"><a href="{{route('admin.usulans.history')}}">History</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                    <p>
                        <form method="get">
                            <div class="row">
                                <div class="col-xs-11">
                                    <input name="q" type="text" class="form-control" placeholder="Pencarian">
                                </div>
                                <div class="col-xs-1">
                                    <button class="btn btn-md btn-success">Cari</button>
                                </div>
                            </div>
                        </form>
                    </p>
                    @if($usulans->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Nama Pengusul</th>
                                    <th>Usulan</th>
                                    <th>Status</th>
                                    <th>Diusulkan Pada</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($usulans as $usulan)
                                <tr>
                                    <td>{{$usulan->user->name}}</td>
                                    <td>{{$usulan->usulan}}</td>
                                    <td>{!!$usulan->status_usulan!!}</td>
                                    <td>{{$usulan->created_at}}</td>
                                    <td>
                                    <button data-url="{{route('admin.usulans.process', $usulan->id)}}" class="btn btn-xs btn-default btn-process" data-id="{{$usulan->id}}" data-judul="{{$usulan->judul}}">Proses</button>
                                        <button data-url="{{route('admin.usulans.delete', $usulan->id)}}" class="btn btn-xs btn-danger btn-hapus" data-id="{{$usulan->id}}">Tolak</button>
                                        {{-- {!! Former::open(route('admin.usulans.delete', $usulan->id))->method('delete') !!}
                                        <a href="{{route('admin.usulans.process', $usulan->id)}}" class="btn btn-xs btn-default">Proses</a>
                                        <button class="btn btn-xs btn-delete btn-danger">Tolak</button>
                                        {!! Former::close() !!} --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-right">
                        {{$usulans->appends(request()->all())->links()}}
                    </div>
                    @else
                    Usulan tidak ditemukan.
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function() {
        $('.btn-hapus').on('click', function() {
            swal({
                title: 'Anda yakin menolak usulan?',
                text: "Data usulan akan di pindah pada history!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, tolak!',
                cancelButtonText: 'Tidak, batalkan!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    deleteItem($(this).data('id'));
                } else {
                    swal(
                        'Batal!',
                        'Tolak data usulan dibatalkan',
                        'error'
                    );
                }
            })
        });

        $('.btn-process').on('click', function() {
            swal({
                title: 'Anda yakin ingin menerima usulan ini?',
                text: "Data akan tersimpan pada menu history!",
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    swal({
                        title: 'Apakah anda ingin membuat paket?',
                        text: "Anda akan di arahkan ke halaman paket!",
                        type: 'question',
                        showCancelButton: true,
                        confirmButtonText: 'Ya',
                        cancelButtonText: 'Tidak!',
                        reverseButtons: true
                    }).then((result) => {
                        if (result.value) {
                            processItem($(this).data('id'), true, $(this).data('judul'));
                        } else {
                            processItem($(this).data('id'), false, $(this).data('judul'));
                        }
                    })
                } else {
                    swal(
                        'Batal!',
                        'Penerimaan usulan dibatalkan',
                        'error'
                    );
                }
            })
        });

        var deleteItem = function (id) {
            $.ajax({
                url: 'usulans/delete/'+ id,
                type: 'GET'
            }).done(function(result) {
                swal(
                    'Usulan Ditolak!',
                    'Data berhasil di pindahkan di menu history.',
                    'success'
                ).then((result) => {
                    location.reload();
                });
            }).fail(function (err) {
                swal(
                    'Gagal!',
                    'Terjadi kesalahan, coba lagi!',
                    'error'
                );  
            });
        }

        var processItem = function (id, create=false, judul='') {
            $.ajax({
                url: 'usulans/process/'+ id,
                type: 'GET'
            }).done(function(result) {
                swal(
                    'Usulan Diproses!',
                    'Data berhasil di pindahkan di menu history.',
                    'success'
                ).then((result) => {
                    const url = "{{route('admin.packets.add')}}?usulan=" + judul;
                    if (create) {
                        window.location.href = url;
                    }else{
                        location.reload();
                    }
                });
            }).fail(function (err) {
                swal(
                    'Gagal!',
                    'Terjadi kesalahan, coba lagi!',
                    'error'
                );  
            });
        }
    });
</script>    
@endsection