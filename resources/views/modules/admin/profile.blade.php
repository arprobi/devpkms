@extends('layouts.admin')

@php
    $user = Auth::user();
    $profile = $user->profile;
    $hobby = ($profile) ? $profile->hobby : [];
@endphp

@section('content')
<div id="profile-page">

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-lock"></i> Akun</div>

        {!! Former::populate($user) !!}
        {!! Former::open(route('admin.profile.update_account')) !!}
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3" align="center">
                        <img src="{{ Auth::user()->avatar ? secure_asset('storage/'.Auth::user()->avatar) : secure_asset('img/default_avatar.jpg') }}" alt="avatar" width="128px" class="img img-circle img-responsive img-thumbnail">
                        <div class="margin-top-10">
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-avatar"><i class="fa fa-fw fa-cloud-upload"></i> Unggah</button>
                        </div>
                    </div>
                    <div class="col-md-9">
                        {{-- Nama --}}
                        {!! Former::text('name')->label('Nama Lengkap')->placeholder('Nama lengkap')->required() !!}

                        {{-- Nama --}}
                        {!! Former::text('username')->label('Username')->placeholder('Username')->required() !!}

                        {{-- Email --}}
                        {!! Former::email('email')->label('Email')->placeholder('Alamat Email')->required() !!}

                        {{-- Interests --}}
                        {!! Former::select('interests')->name('interests[]')->label('Bidang Kompetensi')->fromQuery(PKMS\Models\Interest::get(),'name','id')->multiple() !!}

                        <div class="form-group">
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-password">Ganti password</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-9 col-md-offset-3">
                        <button class="btn btn-success">Simpan</button>
                    </div>
                </div>
            </div>
        {!! Former::close() !!}
    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-user"></i> Profile</div>

        {!! Former::populate($profile) !!}
        {!! Former::open(route('admin.profile.save'))->method('post') !!}

            <div class="panel-body">
                {{-- Tempat / Tanggal Lahir --}}
                <div class="row">
                    <div class="col-md-6">{!! Former::text('birth_place')->label('Tempat Kelahiran')->placeholder('Birth Place') !!}</div>
                    <div class="col-md-6">{!! Former::text('birth_date')->label('Tanggal Lahir')->data_type('date')->placeholder('Birth Date')->required() !!}</div>
                </div>

                {{-- Jenis Kelamin --}}
                {!! Former::radios('Jenis Kelamin')->radios([
                    'Laki-Laki' => ['name' => 'gender', 'value' => 'm'],
                    'Perempuan' => ['name' => 'gender', 'value' => 'f']
                ])->check('m')->required() !!}

                {{-- Telepon --}}
                <div class="row">
                    <div class="col-md-6">{!! Former::text('phone')->label('Telepon')->placeholder('No Telepon')->required() !!}</div>
                    <div class="col-md-6">{!! Former::text('mobile')->label('HP')->placeholder('No HP')->required() !!}</div>
                </div>

                {{-- Alamat --}}
                @if($profile)
                    <div class="row">
                        <div class="col-md-3">
                            {!! Former::select('province_code')->label('Propinsi')->placeholder('Pilih')->fromQuery(PKMS\Models\Province::get(),'name','code')->required() !!}
                        </div>
                        <div class="col-md-3">
                            {!! Former::select('city_code')->label('Kabupaten/Kota')->placeholder('Pilih')->fromQuery(PKMS\Models\City::where('province_id', $profile->province_code)->get(),'name','code')->required() !!}
                        </div>
                        <div class="col-md-3">
                            {!! Former::select('district_code')->label('Kecamatan')->placeholder('Pilih')->fromQuery(PKMS\Models\District::where('city_id', $profile->city_code)->get(),'name','code')->required() !!}
                        </div>
                        <div class="col-md-3">
                            {!! Former::select('village_code')->label('Kelurahan/Desa')->placeholder('Pilih')->fromQuery(PKMS\Models\Village::where('district_id', $profile->district_code)->get(),'name','code')->required() !!}
                        </div>
                    </div>
                @else
                    <div class="row">
                        <div class="col-md-3">
                            {!! Former::select('province_code')->label('Propinsi')->placeholder('Pilih')->fromQuery(PKMS\Models\Province::get(),'name','code')->required() !!}
                        </div>
                        <div class="col-md-3">
                            {!! Former::select('city_code')->label('Kabupaten/Kota')->placeholder('Pilih')->disabled()->required() !!}
                        </div>
                        <div class="col-md-3">
                            {!! Former::select('district_code')->label('Kecamatan')->placeholder('Pilih')->disabled()->required() !!}
                        </div>
                        <div class="col-md-3">
                            {!! Former::select('village_code')->label('Kelurahan/Desa')->placeholder('Pilih')->disabled()->required() !!}
                        </div>
                    </div>
                @endif
                {!! Former::textarea('address')->label('Alamat')->rows(3)->placeholder('Alamat')->required() !!}
                {!! Former::text('zip_code')->label('Kode Pos')->placeholder('Kode pos')->required() !!}
                {!! Former::select('hobby[]')->id('hobby')->label('Hobi')->options($hobby, $hobby, true)->placeholder('Sepakbola, Tenis, Otomotif, dsb.')->multiple() !!}
            </div>

            <div class="panel-footer">
                <button class="btn btn-success">Simpan</button>
            </div>
        {!! Former::close() !!}

    </div>
</div>


{{-- Modal Avatar  --}}
<div id="modal-avatar" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Unggah Avatar</h4>
            </div>
            {!! Former::open_for_files(route('admin.profile.upload_avatar')) !!}
                <div class="modal-body">
                    {!! Former::file('avatar')->accept('.jpg, .jpeg, .png, .bmp')->help('tipe: .jpg, .jpeg, .png, .bmp, Ukuran file: 8MB')->required() !!}
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-primary">Simpan</button>
                </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>

{{-- Modal Password Change --}}
<div id="modal-password" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ganti Password</h4>
            </div>
            {!! Former::open(route('admin.profile.update_password')) !!}
                <div class="modal-body">
                    {!! Former::password('old_password')->label('Password Lama')->placeholder('Password Lama')->required() !!}
                    {!! Former::password('password')->label('Password Baru')->placeholder('Password Baru')->required() !!}
                    {!! Former::password('password_confirmation')->label('Konfirmasi Password')->placeholder('Konfirmasi Password')->required() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-primary">Simpan</button>
                </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>

@endsection
