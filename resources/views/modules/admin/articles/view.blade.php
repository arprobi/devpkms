@extends('layouts.admin')

@section('content')

<div id="articles-page">
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.articles')}}">Artikel</a></li>
        <li class="active">{{$article->title}}</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-notebook"></i> {{$article->title}}</div>
        <div class="panel-body">
            {!! $article->content !!}
        </div>
        @if($article->attachments->count())
            <div class="panel-body">
                <p class="lead">Lampiran</p>
                <ul class="list-inline">
                    @foreach($article->attachments as $attachment)
                        <li>
                            <a href="{{route('admin.attachments.download', $attachment->short_url)}}" target="_blank" class="btn btn-default btn-sm">{{$attachment->title}}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="panel-footer">
            <a href="{{route('admin.articles')}}" class="btn btn-default">Kembali</a>
        </div>
    </div>

</div>

@endsection
