@extends('layouts.admin')

@section('content')
<div id="article-edit-page">
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.articles')}}">Artikel</a></li>
        <li class="active">Edit data</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading">Edit artikel</div>
        {!! Former::populate($article) !!}
        {!! Former::open_for_files(route('admin.articles.update', $article->id)) !!}
            <div class="panel-body">
                {!! Former::select('article_code')->label('Kodefikasi')->fromQuery(PKMS\Models\MasterArticleCode::get(),'name','id')->required() !!}
                {!! Former::select('article_subcode')->label('Subkodefikasi')->fromQuery(PKMS\Models\MasterArticleSubcode::where('code', $article->article_code)->get(),'name','id')->required() !!}
                {!! Former::text('title')->label('Judul')->placeholder('judul tulisan')->required() !!}
                {!! Former::textarea('content')->label('Konten')->placeholder('konten')->required() !!}
                {!! Former::number('revision')->label('Revisi ke')->min(0)->placeholder('No Revisi') !!}
            </div>
            <div class="panel-heading"><i class="fa fa-paperclip"></i> Lampiran</div>
            <div class="panel-body">
                {!! Former::file('lampiran[]')->label('Lampiran')->multiple()
                    ->accept('.jpg, .jpeg, .png, .bmp, .pdf, .docx, .doc, .ppt, .pptx, .zip, .rar, .xls, .xlsx')
                    ->help('tipe: .jpg, .jpeg, .png, .bmp, .pdf, .docx, .doc, .ppt, .pptx, .zip, .rar, .xls, .xlsx, Ukuran file: 8MB') !!}

                @if($article->attachments->count())
                    <ul class="list-inline">
                        @foreach($article->attachments as $attachment)
                            <li>
                                <div class="btn-group btn-group-sm">
                                    <a href="{{route('admin.attachments.download', $attachment->short_url)}}" target="_blank" class="btn btn-default">{{$attachment->title}}</a>
                                    <a href="{{route('admin.attachments.delete', $attachment->short_url)}}" class="btn btn-default" data-confirm data-message="Hapus lampiran ini?"><i class="fa fa-times text-danger"></i></a>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
            <div class="panel-footer">
                <a href="{{route('admin.tasks.detail', $article->tasks->first()->id)}}" class="btn btn-default">Kembali</a>
                <button class="btn btn-success">Simpan</button>
            </div>
        {!! Former::close() !!}
    </div>
</div>
@endsection
