@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">Artikel</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-docs"></i> Daftar artikel</div>

        @if($articles->count())
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Judul</th>
                            <th>Penulis</th>
                            <th>Revisi</th>
                            <th>Status</th>
                            <th>Tugas terkait</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($articles as $article)
                            <tr>
                                <td><a href="{{route('admin.articles.view', $article->id)}}">{{$article->title}}</a></td>
                                <td>{{$article->author->name}}</td>
                                <td>{{$article->revision}}</td>
                                <td>{{$article->is_published ? 'Terbit' : 'Draft'}}</td>
                                <td>
                                    @if($article->tasks->count())
                                        <ul class="list-unstyled">
                                            @foreach($article->tasks as $task)
                                                <li><a href="{{ route('admin.tasks.detail', $task->id) }}">{{$task->title}}</a></li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('admin.articles.edit', $article->id)}}" class="btn btn-sm btn-default">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="panel-footer">
                    {!! $articles->links() !!}
                </div>
            </div>
        @else
            <div class="panel-body">
                Belum ada artikel.
            </div>
        @endif
    </div>
</div>
@endsection
