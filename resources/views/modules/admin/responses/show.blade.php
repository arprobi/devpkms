@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">DETAIL POLLING</li>
    </ol>

    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-fw fa-gavel"> </i> Polling
            <a href="{{route('admin.responses.edit', $response->id)}}" class="btn btn-sm btn-primary pull-right">Edit</a>
        </div>
        <div class="panel-body">
            <div class="col-md-4 col-sm-4 col-xs-4">
                <div class="form-group">
                    <label>Pertanyaan</label>
                    <div class="form-control-static">{{$response->question}}</div>
                </div>
                <div class="form-group">
                    <label>Jenis Pertanyaan</label>
                    <div class="form-control-static">{{$response->jenis}}</div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                @if($response->is_selection == 1)
                <div class="form-group">
                    <label>Pilihan Jaswaban</label>
                    <div class="form-control-static">
                        <ul>
                            @foreach($response->choices as $choice)
                            <li>
                                {{$choice->choice}}
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                @endif
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <div class="form-group">
                    <label>Dibuat Tanggal</label>
                    <div class="form-control-static">{!!$response->created_at!!}</div>
                </div>
                <div class="form-group">
                    <label>Terakhir diubah</label>
                    <div class="form-control-static">{!!$response->updated_at!!}</div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    @if($response->is_selection)
                        <label>Hasil Polling</label>
                        <div class="form-control-static">
                            <ul>
                                @foreach($response->choices as $choice)
                                <li>
                                    <div class="col-sm-6">
                                        {{$choice->choice}}
                                    </div>
                                    <div class="col-sm-6">
                                        <span>{{ $choice->answers->count() }} pemilih</span>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div> 
                    @endif
                    @if($response->answers->count())
                    <label>Jawaban user</label>
                    <div class="form-control-static">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Nama User</th>
                                        <th>Jawaban</th>
                                    </tr>
                                </thead>
                                <tbody id="list-answer">
                                    @foreach($response->answers->take(5) as $answer)
                                    <tr>
                                        <td>{{$answer->user->name}}</td>
                                        @if($answer->is_selection)
                                        <td>{{$answer->choice_answer}}</td>
                                        @else
                                        <td>{{$answer->answer}}</td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <a href="#" id="next-answer" data-offset="5" data-id="{{$response->id}}">Selanjutnya</a>

                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#next-answer').on('click', function() {
            var offset  = $(this).data('offset');
            var id      = $(this).data('id');
            
            $(this).data('offset', offset + 5);

            $.ajax({
                url: '/api/pooling/answer/'+id,
                type: 'GET',
                data: {offset: offset},
            })
            .done(function(response) {
                if (response.length) {
                    $.each(response, function(index, val) {
                        if (val.is_selection) {
                            $('#list-answer').append('<tr><td>'+ val.user.name +'</td><td>'+ val.choice_answer +'</td></tr>');
                        } else {
                            $('#list-answer').append('<tr><td>'+ val.user.name +'</td><td>'+ val.answer +'</td></tr>');
                        }
                    });
                }else {
                    $('#next-answer').hide();
                }
            });
            
        });
    });
</script>
@endsection