@extends('layouts.admin')

@section('content')
<div id="pasal-create-page">
    <ol class="breadcrumb flat">
        <li><a href="{{route('admin.responses')}}">Polling</a></li>
        <li class="active">Tambah data</li>
    </ol>

    @include('modules.admin._alerts')
    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-graduation"></i> Master Polling</div>

        {!! Former::open(route('admin.responses.store'))->id('form-pooling') !!}

        <div class="panel-body">
            {!! Former::text('question')->label('Pertanyaan')->placeholder('Pertanyaan')->required() !!}

            {!! Former::radios('is_selection')->radios([
                'Isian'   => ['name' => 'is_selection', 'value' => '0', 'checked'],
                'Pilihan' => ['name' => 'is_selection', 'value' => '1']
                ])->label('Jenis Pertanyaan') !!}

            <div class="row" id="div-pilihan" style="display: none; padding-bottom: 20px;">
                <div class="col-md-6">
                    {!! Former::text('pilihan')->label('Opsi Jawaban')->placeholder('Opsi Jawaban') !!}
                    <button type="button" class="btn btn-primary btn-sm" id="add_choice">Tambahkan</button>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Opsi Jawaban</label>
                        <ul class="kategori" id="list-pilihan">
                            <!-- Dynamic list -->
                        </ul>
                    </div>
                </div>
            </div>

            {!! Former::radios('published')->radios([
                'Ya'    => ['name' => 'published', 'value' => '1'],
                'Tidak' => ['name' => 'published', 'value' => '0', 'checked']
                ])->label('Publikasikan') !!}

        </div>

        <div class="panel-footer">
            <button class="btn btn-success" type="button" id="save">Simpan</button>
            <a href="{{route('admin.responses')}}" class="btn btn-default pull-right">Kembali ke index</a>
        </div>

        {!! Former::close() !!}

    </div>
</div>
@endsection

@section('script')
<style type="text/css">
    .kategori {
        padding-top: 10px;
        margin-left: -26px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {

        $('#add_choice').on('click', function() 
        {
            var isi = $("#pilihan").val();
            if (isi) {
                addList(isi);
            }else{
                swal({
                    title: 'Oops!',
                    text: 'Pilihan jawaban harus di isi!',
                    type: 'warning'
                });
            }
        });

        var addList = function(val)
        {
            var html = '<li style="margin-bottom: 4px;">'+
                            '<div class="btn-group btn-group-sm">'+
                                '<a class="btn btn-default">'+ val +'</a>'+ 
                                '<a class="btn btn-default del-list"><i class="fa fa-times text-danger"></i></a>'+ 
                                '<input type="hidden" name="choices[]" value="'+ val +'">'+ 
                            '</div>'+
                        '<li>';
            
            
            $('#list-pilihan').append(html);
            $("#pilihan").val('')
            clearLi();
        }

        $(document).on('click', '.del-list', function (e) {
            $(this).parent().parent().remove();
            clearLi();
        });

        var clearLi = function()
        {
            $('#list-pilihan li:empty').remove();
        }


        $('input:radio').change(function(){
            var value = $("form input[type='radio']:checked").val();
            if (value == 1) {
                $('#div-pilihan').show();
            } else {
                $('#div-pilihan').hide();
            }
        });

        $('#save').click(function() {
            var pilihan     = $("input[name=is_selection]:checked").val();
            var selected    = $('ul.kategori li').length;
            if (pilihan == 1) {
                if (selected > 0) {
                    $('#form-pooling').submit();
                }else {
                    swal({
                        title: 'Oops!',
                        text: 'Pilihan jawaban harus di isi!',
                        type: 'warning'
                    });
                }
            }else {
                $('#form-pooling').submit();
            }
        });

    });
</script>
@endsection