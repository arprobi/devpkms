@extends('layouts.admin')

@section('content')
<div>
    <ol class="breadcrumb flat">
        <li class="active">POOLING</li>
    </ol>

    @include('modules.admin._alerts')

    <div class="panel panel-default">
        <div class="panel-heading">Daftar Polling</div>
        <div class="panel-body">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                    <p>
                        <form method="get">
                            <div class="row">
                                <div class="col-xs-11">
                                    <input name="q" type="text" class="form-control" placeholder="Pencarian">
                                </div>
                                <div class="col-xs-1">
                                    <button class="btn btn-md btn-success">Cari</button>
                                </div>
                            </div>
                        </form>
                    </p>
                    @if($responses->count())
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Polling</th>
                                    <th>Jawaban Polling</th>
                                    <th>Dibuat Pada</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($responses as $response)
                                <tr>
                                    <td>{{$response->question}}</td>
                                    <td>{!!$response->jenis!!}</td>
                                    <td>{{$response->created_at}}</td>
                                    <td>
                                        @if($response->published == 0)
                                            <a href="{{route('admin.responses.publish', $response->id)}}?action=unpublish" class="text text-danger">Unpublished <i class="fa fa-times"></i></a>
                                        @else
                                            <a href="{{route('admin.responses.publish', $response->id)}}?action=publish" class="text text-primary">Published <i class="fa fa-check"></i></a>
                                        @endif
                                    </td>
                                    <td>
                                        {!! Former::open(route('admin.responses.delete', $response->id))->method('delete') !!}
                                        <a href="{{route('admin.responses.edit', $response->id)}}" class="btn btn-sm btn-default">Edit</a>
                                        <a href="{{route('admin.responses.show', $response->id)}}" class="btn btn-sm btn-info">Lihat</a>
                                        <button class="btn btn-sm btn-delete btn-danger">Hapus</button>
                                        {!! Former::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-left">
                        <a href="{{route('admin.responses.create')}}" class="btn btn-success">Tambah data</a>
                    </div>
                    <div class="pull-right">
                        {{$responses->appends(request()->all())->links()}}
                    </div>
                    @else
                    <div class="pull-left">
                        <a href="{{route('admin.responses.create')}}" class="btn btn-success">Tambah data</a>
                    </div>
                    <div class="pull-right">
                        Pasal tidak ditemukan.
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection