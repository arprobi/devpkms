@extends('layouts.app')

@section('content')

<div id="experiences-page" class="container">

    @include('_incomplete')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-briefcase"></i> Pengalaman Kerja</div>
        @if($user->experiences->count())
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Project / Perusahaan</th>
                            <th>Posisi</th>
                            <th>Deskripsi</th>
                            <th>Dari</th>
                            <th>Hingga</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user->experiences as $experience)
                            <tr>
                                <td>{{$experience->title}}</td>
                                <td>{{$experience->position}}</td>
                                <td>{{$experience->description}}</td>
                                <td>{{$experience->from}}</td>
                                <td>{{$experience->to}}</td>
                                <td>
                                    {!! Former::open(route('expert.experiences.delete', $experience->id))->method('delete') !!}
                                        <a href="{{route('expert.experiences.edit', $experience->id)}}" class="btn btn-default">Edit</a>
                                        <button class="btn btn-delete btn-danger">Hapus</button>
                                    {!! Former::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <div class="panel-body">
                <div class="alert alert-warning">Anda belum mengisi pengalaman kerja.</div>
            </div>
        @endif

        <div class="panel-footer">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-experience">
                <i class="fa fa-fw fa-plus"></i> Tambah riwayat pengalaman kerja
            </button>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Tugas</div>

        @if($user->my_tasks->count())
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Judul</th>
                            <th width="300">Deskripsi</th>
                            <th>Progress</th>
                            <th>Status</th>
                            <th>Rating</th>
                            <th>Ditugaskan Oleh</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($user->my_tasks as $task)
                            <tr>
                                <td>{{$task->assigned_at}}</td>
                                <td><a href="javascript:;">{{$task->title}}</a></td>
                                <td>{{str_limit($task->description, 125)}}</td>
                                <td>{{$task->progress}} %</td>
                                <td>
                                    @if($task->is_assigned)
                                        {{$task->is_canceled ? 'Dibatalkan' : $task->is_completed && $task->progress == 100 ? 'Selesai' : 'Belum selesai' }}
                                    @else
                                        Butuh persetujuan
                                    @endif
                                </td>
                                <td>
                                    @if($task->is_completed && $task->rating)
                                        @for($i = 0; $i < $task->rating->rating; $i++)
                                            <i class="fa fa-star bg-yellow"></i>
                                        @endfor
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>{{$task->assignedby->name}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        @else
            <div class="panel-body">
                <div class="alert alert-warning">Anda belum pernah mendapatkan tugas.</div>
            </div>
        @endif
    </div>
</div>

{{-- Modal experience  --}}
<div id="modal-experience" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Riwayat pengalaman kerja</h4>
            </div>
            {!! Former::open_for_files(route('expert.experiences.create')) !!}
                <div class="modal-body">
                    {!! Former::text('title')->label('Proyek / Perusahaan')->placeholder('Nama proyek / perusahaan')->required() !!}
                    {!! Former::text('position')->label('Posisi / Jabatan')->placeholder('Nama posisi / jabatan')->required() !!}
                    {!! Former::textarea('description')->label('Deskripsi pekerjaan')->placeholder('Deskripsi pekerjaan')->rows(3) !!}
                    <div class="row">
                        <div class="col-md-6">
                            {!! Former::text('from')->data_type('date')->data_date_end_date('0d')->label('Dari')->placeholder('Mulai bekerja')->required() !!}
                        </div>
                        <div class="col-md-6">
                            {!! Former::text('to')->data_type('date')->data_date_end_date('0d')->label('Hingga')->placeholder('Hingga (Kosongkan jika masih bekerja)') !!}
                        </div>
                    </div>

                    {!! Former::file('lampiran[]')->accept('.pdf, .docx, .doc')->multiple()->help('Tipe File : .doc, .docx, pdf. Max Size: 2MB') !!}
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button class="btn btn-primary">Simpan</button>
                </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>
@endsection
