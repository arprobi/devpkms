@extends('layouts.app')

@section('content')

<div id="experiences-page" class="container">

    @include('_incomplete')

    <div class="panel panel-default">
        <div class="panel-heading"><i class="fa icon-briefcase"></i> Edit Pengalaman Kerja</div>
        {!! Former::populate($experience) !!}
        {!! Former::open_for_files(route('expert.experiences.update', $experience->id)) !!}
            <div class="panel-body">
                {!! Former::text('title')->label('Proyek / Perusahaan')->placeholder('Nama proyek / perusahaan')->required() !!}
                {!! Former::text('position')->label('Posisi / Jabatan')->placeholder('Nama posisi / jabatan')->required() !!}
                {!! Former::textarea('description')->label('Deskripsi pekerjaan')->placeholder('Deskripsi pekerjaan')->rows(3) !!}
                <div class="row">
                    <div class="col-md-6">
                        {!! Former::text('from')->data_type('date')->data_date_end_date('0d')->label('Dari')->placeholder('Mulai bekerja')->required() !!}
                    </div>
                    <div class="col-md-6">
                        {!! Former::text('to')->data_type('date')->data_date_end_date('0d')->label('Hingga')->placeholder('Hingga (Kosongkan jika masih bekerja)') !!}
                    </div>
                </div>

                {!! Former::file('lampiran[]')->accept('.pdf, .docx, .doc')->multiple()->help('Tipe File : .doc, .docx, pdf. Max Size: 2MB') !!}

                @if($experience->attachments->count())
                    <hr>
                    <ul class="list-inline">
                        @foreach($experience->attachments as $attachment)
                            <li>
                                <div class="btn-group btn-group-sm">
                                    <a href="{{secure_asset('storage/'. $attachment->path)}}" target="_blank" class="btn btn-default"><i class="fa icon-doc"></i> {{$attachment->title}}</a>
                                    <a href="{{route('expert.attachments.delete', $attachment->short_url)}}" class="btn btn-default"><i class="fa fa-times"></i></a>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
            <div class="panel-footer">
                <a href="{{route('expert.experiences')}}" class="btn btn-default">Kembali</a>
                <button class="btn btn-primary">Simpan</button>
            </div>
        {!! Former::close() !!}
    </div>

</div>

@endsection
