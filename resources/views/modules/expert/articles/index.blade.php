@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @include('_box_profile')
        </div>
        <div class="col-md-10">
            @include('_incomplete')
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa icon-docs"></i> Daftar artikel</div>

                @if($articles->count())
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Judul</th>
                                    <th>Penulis</th>
                                    <th>Revisi</th>
                                    <th>Status</th>
                                    <th>Tugas terkait</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($articles as $article)
                                    <tr>
                                        <td><a href="{{route('expert.articles.view', $article->id)}}">{{$article->title}}</a></td>
                                        <td>{{$article->author->name}}</td>
                                        <td>{{$article->revision}}</td>
                                        <td>{{$article->is_published ? 'Terbit' : 'Draft'}}</td>
                                        <td>
                                            @if($article->tasks->count())
                                                <ul class="list-unstyled">
                                                    @foreach($article->tasks as $task)
                                                        <li><a href="{{ route('expert.tasks.detail', $task->id) }}">{{$task->title}}</a></li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </td>
                                        <td>
                                            @if(!$article->is_published)
                                                {!! Former::open(route('expert.articles.delete', $article->id))->method('delete') !!}
                                                    <a href="{{route('expert.articles.edit', $article->id)}}" class="btn btn-sm btn-default">Edit</a>
                                                    <button class="btn btn-sm btn-danger btn-delete">Hapus</button>
                                                {!! Former::close() !!}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="panel-footer">
                            {!! $articles->links() !!}
                        </div>
                    </div>
                @else
                    <div class="panel-body">
                        Belum ada artikel.
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
