@extends('layouts.app')

@section('content')
<div id="article-edit-page" class="container">
    <div class="row">
        <div class="col-md-2">
            @include('_box_profile')
        </div>
        <div class="col-md-10">
            @include('_incomplete')
            <div class="panel panel-default">
                <div class="panel-heading">Edit artikel</div>
                {!! Former::populate($article) !!}
                {!! Former::open_for_files(route('expert.articles.update', $article->id)) !!}
                    <div class="panel-body">
                        {!! Former::select('article_code')->label('Kodefikasi')->fromQuery(PKMS\Models\MasterArticleCode::get(),'name','id')->required() !!}
                        {!! Former::select('article_subcode')->label('Subkodefikasi')->fromQuery(PKMS\Models\MasterArticleSubcode::where('code', $article->article_code)->get(),'name','id')->required() !!}
                        {!! Former::text('title')->label('Judul')->placeholder('judul tulisan')->required() !!}
                        {!! Former::textarea('content')->label('Konten')->placeholder('konten')->required() !!}
                        {!! Former::number('revision')->label('Revisi ke')->min(0)->placeholder('No Revisi') !!}
                    </div>
                    <div class="panel-heading"><i class="fa fa-paperclip"></i> Lampiran</div>
                    <div class="panel-body">
                        {!! Former::file('lampiran[]')->label('Lampiran')->multiple()->accept('.jpg, .jpeg, .png, .bmp, .pdf, .docx, .doc, .ppt, .pptx, .zip, .rar, .xls, .xlsx') !!}

                        @if($article->attachments->count())
                            <ul class="list-inline">
                                @foreach($article->attachments as $attachment)
                                    <li>
                                        <div class="btn-group btn-group-sm">
                                            <a href="{{route('expert.attachments.download', $attachment->short_url)}}" target="_blank" class="btn btn-default btn-sm">{{$attachment->title}}</a>
                                            <a href="{{route('expert.attachments.delete', $attachment->short_url)}}" class="btn btn-default" data-confirm data-message="Hapus lampiran ini?"><i class="fa fa-times text-danger"></i></a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    <div class="panel-footer">
                        <a href="{{route('expert.tasks.detail', $article->tasks->first()->id)}}" class="btn btn-default">Kembali</a>
                        <button class="btn btn-success">Simpan</button>
                    </div>
                {!! Former::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
