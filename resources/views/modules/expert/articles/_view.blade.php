
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{$article->title}}</h4>
</div>
<div class="modal-body">
    {!! $article->content !!}

    <hr>
    <p class="lead">Lampiran</p>
    <ul class="list-inline">
        @foreach($article->attachments as $attachment)
            <li>
                <a href="{{route('expert.attachments.download', $attachment->short_url)}}" target="_blank" class="btn btn-default btn-sm">{{$attachment->title}}</a>
            </li>
        @endforeach
    </ul>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
</div>
