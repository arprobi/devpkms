@extends('layouts.app')

@section('content')

<div id="articles-page" class="container">

    @include('_incomplete')

    <div class="row">
        <div class="col-md-2">
            @include('_box_profile')
        </div>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa icon-notebook"></i> {{$article->title}}</div>
                <div class="panel-body">
                    {!! $article->content !!}
                </div>
                @if($article->attachments->count())
                    <div class="panel-body">
                        <p class="lead">Lampiran</p>
                        <ul class="list-inline">
                            @foreach($article->attachments as $attachment)
                                <li>
                                    <a href="{{route('expert.attachments.download', $attachment->short_url)}}" target="_blank" class="btn btn-default btn-sm">{{$attachment->title}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection
