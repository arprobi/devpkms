@extends('layouts.app')

@section('content')
<div id="packet-detail-page" class="container">
    <div class="row">
        <div class="col-md-2">
            @include('_box_profile')
        </div>

        <div class="col-md-10">

            @include('_incomplete')

            {{-- Breadcrumb  --}}
            <ol class="breadcrumb">
                <li><a href="{{route('expert.packets')}}"><i class="fa fa-fw fa-packets"></i> Paket</a></li>
                <li class="active">Detail - ID : {{$packet->id}}, Title: {{$packet->title}}</li>
            </ol>

            {{-- packet Detail --}}
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-fw fa-info"></i> Paket</div>

                @if($packet->is_accepted)
                    {!! Former::populate($packet) !!}
                    {!! Former::open(route('expert.packets.update', $packet->id)) !!}
                        <div class="panel-body">
                            {!! Former::text('title')->label('Judul')->disabled() !!}
                            {!! Former::textarea('description')->label('Deskripsi')->rows(5)->disabled() !!}

                            @if(!$packet->is_completed)
                                {!! Former::number('progress')->max(100)->min(0)->append('%') !!}
                            @else
                                {!! Former::number('progress')->max(100)->min(0)->append('%')->disabled() !!}

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Tgl Mulai Pelaksanaan</label>
                                            <div class="form-control">{{$packet->accepted_at}}</div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Tgl Selesai Pelaksanaan</label>
                                            <div class="form-control">{{$packet->completed_at}}</div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Status</label>
                                    <div class="form-control"><i class="fa fa-fw fa-check text-success"></i> Telah selesai</div>
                                </div>
                            @endif
                        </div>

                        @if(!$packet->is_completed)
                            <div class="panel-footer">
                                <button class="btn btn-success">Update Progress</button>
                            </div>
                        @endif
                    {!! Former::close() !!}
                @else
                    {!! Former::populate($packet) !!}
                    {!! Former::open()->method('get') !!}
                        <div class="panel-body">
                            {!! Former::text('title')->label('Judul')->disabled() !!}
                            {!! Former::textarea('description')->label('Deskripsi')->rows(5)->disabled() !!}
                        </div>
                    {!! Former::close() !!}

                    <div class="panel-footer">
                        <a href="{{route('expert.packets.accept', $packet->id)}}" class="btn btn-accept btn-default"><i class="fa fa-check text-success"></i> Terima</a>
                        <a href="{{route('expert.packets.deny', $packet->id)}}" class="btn btn-deny btn-default"><i class="fa fa-times text-danger"></i> Tolak</a>
                    </div>
                @endif
            </div>

            @if($packet->is_accepted)

                {{-- Related Comments  --}}
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa fa-fw fa-envelope-o"></i> Pesan terkait</div>
                    <div class="panel-body">
                        @if($packet->comments->count())
                            <div class="media-lists comments">
                                @foreach($packet->comments as $comment)
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="javascript:;">
                                                <img class="media-object img-circle" height="64px" width="64px" src="{{$comment->author->avatar ? secure_asset('storage/'. $comment->author->avatar) : secure_asset('/img/default_avatar.jpg') }}">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">{{$comment->author->name}} <small>{{$comment->updated_at or $comment->created_at}}</small></h4>
                                            <p>{{$comment->text}}</p>
                                            @if($comment->attachments->count())
                                                <ul class="list-inline">
                                                    @foreach($comment->attachments as $attachment)
                                                        <li><a href="{{secure_asset('storage/'.$attachment->path)}}" target="_blank" class="btn btn-sm btn-default"><i class="fa fa-fw fa-paperclip"></i> {{$attachment->title}}</a></li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <p>Tidak ada pesan terkait.</p>
                        @endif
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Pesan baru</div>
                    {!! Former::open_for_files(route('expert.packets.add_comment', $packet->id)) !!}
                        <div class="panel-body">
                            {!! Former::textarea('text')->label('Pesan')->placeholder('pesan')->rows(5)->required() !!}
                            {!! Former::file('lampiran[]')->label('Lampiran')->multiple()->accept('.jpg, .jpeg, .png, .bmp, .pdf, .docx, .doc, .ppt, .pptx, .zip, .rar, .xls, .xlsx') !!}
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-success"><i class="fa fa-fw fa-send"></i> Kirim pesan</button>
                        </div>
                    {!! Former::close() !!}
                </div>
            @endif
        </div>
    </div>
</div>

{{-- Modal Article --}}
@if($packet->is_accepted)

    {{-- Modal View Article  --}}
    <div id="modal-view-article" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content"></div>
        </div>
    </div>
@endif

@endsection
