<div class="panel-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Paket</label>
                <div class="form-control">{{$packet->title}}</div>
            </div>
            <div class="form-group">
                <label>Instansi</label>
                <div class="form-control">{{ $packet->instansi->name }}</div>
            </div>
            <div class="form-group">
                <label>Tahun Anggaran</label>
                <div class="form-control">{{$packet->tahun_anggaran}}</div>
            </div>
            <div class="form-group">
                <label>Anggaran</label>
                <div class="form-control">Rp. {{ number_format($packet->anggaran,0,',','.') }}</div>
            </div>
            <div class='form-group'>
                <label>Terbilang</label>
                <p class='form-control-static'>{{ Terbilang::make($packet->anggaran, ' Rupiah') }}</p>
            </div>
            <div class="form-group">
                <label>Jenis Pengadaan</label>
                <div class="form-control">{{ ucwords($packet->jenis_pengadaan) }}</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Deskripsi</label>
                <div class="form-control textarea" style="height: 102px;">{{$packet->description}}</div>
            </div>
            <div class="form-group">
                <label>Satuan_Kerja</label>
                <div class="form-control">{{$packet->satuan_kerja}}</div>
            </div>
            <div class="form-group">
                <label>Kode Anggaran</label>
                <div class="form-control">{{$packet->kode_anggaran}}</div>
            </div>
            <div class="form-group">
                <label>Jangka Waktu</label>
                <div class="form-control">{{$packet->jangka_waktu}} {{$packet->interval_waktu}}</div>
            </div>
            <div class="form-group">
                <label>Jenis_Penyedia</label>
                <div class="form-control">{{$packet->jenis_penyedia}}</div>
            </div>
        </div>
    </div>
</div>
<div class='panel-body'>
    <div class="row">
        <div class="col-sm-6">
            <h4>PPK</h4>
            <div class="form-group">
                <label>Nama</label>
                <div class="form-control">{{$packet->ppk_nama}}</div>
            </div>
            <div class="form-group">
                <label>NIP</label>
                <div class="form-control">{{$packet->ppk_nip}}</div>
            </div>
            <div class="form-group">
                <label>Jabatan</label>
                <div class="form-control">{{$packet->ppk_jabatan}}</div>
            </div>
        </div>
        <div class="col-sm-6">
            <h4>Pejabat Pengadaan</h4>
            <div class="form-group">
                <label>Nama</label>
                <div class="form-control">{{$packet->pejabat_nama}}</div>
            </div>
            <div class="form-group">
                <label>NIP</label>
                <div class="form-control">{{$packet->pejabat_nip}}</div>
            </div>
            <div class="form-group">
                <label>Jabatan</label>
                <div class="form-control">{{$packet->pejabat_jabatan}}</div>
            </div>
        </div>
    </div>
    <div class="row">
        <h4>Lokasi Pengadaan</h4>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Propinsi</label>
                <div class="form-control">{{$packet->province->name}}</div>
            </div>
            <div class="form-group">
                <label>Kabupaten/Kota</label>
                <div class="form-control">{{$packet->city->name}}</div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group textarea">
                <label>Detail Lokasi</label>
                <div class="form-control">{{$packet->detail_lokasi}}</div>
            </div>
        </div>
    </div>
</div>
<div class='panel-body'>
    <div class="row">
        <div class="col-sm-6">
            <h4>Syarat Kualifikasi</h4>
            @if($packet->qualifications->count())
            <ol>
                @foreach($packet->qualifications as $qualification)
                <li>{{ $qualification->name }}</li>
                @endforeach
            </ol>
            @endif

        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Metode Pengadaan</label>
                <div class="form-control">{{$packet->metode_pengadaan}}</div>
            </div>
            <div class="form-group">
                <label>Cara Pembayaran</label>
                <div class="form-control">{{$packet->cara_pembayaran}}</div>
            </div>
        </div>
    </div>
</div>