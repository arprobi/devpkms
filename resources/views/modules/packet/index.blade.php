@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @include('_box_profile')
        </div>

        <div class="col-md-10">

            @include('_incomplete')
            <ul class="nav nav-tabs hidden-xs" role="tablist" style="margin-bottom: 15px;">
                <li role="presentation" class="active"><a href="{{route('expert.packets')}}">Dashboard</a></li>
                <li role="presentation"><a href="{{route('expert.packets.new')}}">Paket Baru</a></li>
                <li role="presentation"><a href="{{route('expert.packets.follow')}}">Paket yang diikuti</a></li>
                <li role="presentation"><a href="{{route('expert.packets.ongoing')}}">Paket yang berjalan</a></li>
            </ul>
            <ul class="nav nav-tabs visible-xs" role="tablist" style="margin-bottom: 15px;">
                <li role="presentation" class="active dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-controls="tab-dropdown" aria-expanded="false">Pilih <span class="caret"></span></a>
                    <ul class="dropdown-menu" id="tab-dropdown">
                        <li role="presentation" class="active"><a href="{{route('expert.packets')}}">Dashboard</a></li>
                        <li role="presentation"><a href="{{route('expert.packets.new')}}">Paket Baru</a></li>
                        <li role="presentation"><a href="{{route('expert.packets.follow')}}">Paket yang diikuti</a></li>
                        <li role="presentation"><a href="{{route('expert.packets.ongoing')}}">Paket yang berjalan</a></li>
                    </ul>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa fa-fw fa-info"></i> Undangan Paket</div>

                        @if($invite_packets->count())
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="175">Judul</th>
                                        <th width="250">Deskripsi</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($invite_packets as $packet)
                                    <tr>
                                        <td><a href="{{route('expert.packets.detail', $packet->id)}}">{{$packet->title}}</a></td>
                                        <td>{{str_limit($packet->description, 100)}}</td>
                                        <td align="right">
                                            @if($packet->offers()->where('user_id',Auth::user()->id)->count())
                                            Sudah melakukan penawaran
                                            @else
                                            <div class="btn-group btn-group-sm">
                                                <a href="{{route('expert.packets.offer', $packet->id)}}" class="btn btn-default"><i class="fa fa-money"></i> Penawaran</a>
                                            </div>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <div class="panel-body">
                            <p>Tidak ada undangan baru.</p>
                        </div>
                        @endif
                    </div>

                    {{-- Tugas Baru  --}}
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa fa-fw fa-info"></i> Paket yang dimenangkan</div>

                        @if($new_packets->count())
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="120">Tgl Terima</th>
                                        <th width="175">Judul</th>
                                        <th width="250">Deskripsi</th>
                                        <th width="150">Anggaran Disetujui</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($new_packets as $packet)
                                    <tr>
                                        <td>{{$packet->assigned_at}}</td>
                                        <td><a href="{{route('expert.packets.detail', $packet->id)}}">{{$packet->title}}</a></td>
                                        <td>{{str_limit($packet->description, 100)}}</td>
                                        <td>Rp. {{ number_format($packet->pembayaran,0,',','.') }}</td>
                                        <td align="right">
                                            <div class="btn-group btn-group-sm">
                                                <a href="{{route('expert.packets.accept', $packet->id)}}" class="btn btn-accept btn-default"><i class="fa fa-check text-success"></i> Terima</a>
                                                <a href="{{route('expert.packets.deny', $packet->id)}}" class="btn btn-deny btn-default"><i class="fa fa-times text-danger"></i> Tolak</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <div class="panel-body">
                            <p>Tidak ada penugasan paket baru.</p>
                        </div>
                        @endif
                    </div>

                    {{-- Tugas Selesai --}}
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa fa-fw fa-check text-success"></i> Paket selesai</div>
                        @if($completed_packets->count())
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="120">Tgl Penugasan</th>
                                        <th width="175">Judul</th>
                                        <th width="250"Deskripsi</th>
                                        <th>Rating</th>
                                        <th>Review</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($completed_packets as $packet)
                                    <tr>
                                        <td>{{$packet->accepted_at}}</td>
                                        <td><a href="{{route('expert.packets.detail', $packet->id)}}">{{$packet->title}}</a></td>
                                        <td>{{str_limit($packet->description, 100)}}</td>
                                        <td>
                                            @for($i = 0; $i < floor($packet->rating->rating); $i++)
                                            <i class="fa fa-star bg-yellow"></i>
                                            @endfor

                                            @if(fmod($packet->rating->rating, 1))
                                            <i class="fa fa-star-half bg-yellow"></i>
                                            @endif
                                        </td>
                                        <td>{{$packet->rating->review or '-'}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <div class="panel-body">
                            <p>Belum ada tugas yang selesai.</p>
                        </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
