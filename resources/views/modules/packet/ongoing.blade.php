@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @include('_box_profile')
        </div>

        <div class="col-md-10">

            @include('_incomplete')

            <ul class="nav nav-tabs hidden-xs" role="tablist" style="margin-bottom: 15px;">
                <li role="presentation"><a href="{{route('expert.packets')}}">Dashboard</a></li>
                <li role="presentation"><a href="{{route('expert.packets.new')}}">Paket Baru</a></li>
                <li role="presentation"><a href="{{route('expert.packets.follow')}}">Paket yang diikuti</a></li>
                <li role="presentation" class="active"><a href="{{route('expert.packets.ongoing')}}">Paket yang berjalan</a></li>
            </ul>
            <ul class="nav nav-tabs visible-xs" role="tablist" style="margin-bottom: 15px;">
                <li role="presentation" class="active dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-controls="tab-dropdown" aria-expanded="false">Pilih <span class="caret"></span></a>
                    <ul class="dropdown-menu" id="tab-dropdown">
                        <li role="presentation"><a href="{{route('expert.packets')}}">Dashboard</a></li>
                        <li role="presentation"><a href="{{route('expert.packets.new')}}">Paket Baru</a></li>
                        <li role="presentation"><a href="{{route('expert.packets.follow')}}">Paket yang diikuti</a></li>
                        <li role="presentation" class="active"><a href="{{route('expert.packets.ongoing')}}">Paket yang berjalan</a></li>
                    </ul>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                    {{-- Tugas Berjalan  --}}
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa fa-fw fa-columns"></i> Paket berjalan</div>

                        @if($packets->count())
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="120">Tgl Penugasan</th>
                                        <th width="175">Judul</th>
                                        <th width="250">Deskripsi</th>
                                        <th>Progress</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($packets as $packet)
                                    <tr>
                                        <td>{{$packet->accepted_at}}</td>
                                        <td><a href="{{route('expert.packets.detail', $packet->id)}}">{{$packet->title}}</a></td>
                                        <td>{{str_limit($packet->description, 100)}}</td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{$packet->progress}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$packet->progress}}%;">{{$packet->progress}}%</div>
                                            </div>
                                        </td>
                                        <td>
                                            {{ ($packet->progress >= 100) ? 'Menunggu approval' : 'Belum selesai' }}
                                        </td>
                                        <td>
                                            <a href="{{ route('expert.packets.paids.page', $packet->id)}}" class="btn btn-sm btn-default">Informasi Pembayaran</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="panel-footer">{!! $packets->links() !!}</div>
                        @else
                        <div class="panel-body">
                            <p>Belum ada paket yang sedang berjalan.</p>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
