@extends('layouts.app')

@section('content')
<div id="packet-detail-page" class="container">
    <div class="row">
        <div class="col-md-2">
            @include('_box_profile')
        </div>

        <div class="col-md-10">
            @include('_incomplete')
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-fw fa-info"></i> Paket</div>
                @include('modules.admin.packets._detail_static')
            </div>
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-money"></i> Penawaran</div>
                {!! Former::open_for_files(route('expert.packets.offer', $packet->id)) !!}
                <div class="panel-body">
                    {!! Former::text('offer')->class('form-control number')->label('Penawaran Harga')->required() !!}
                    <h4>Lampiran Dokumen Penawaran </h4>
                    <small>* 'jpg', 'jpeg', 'pdf', 'docx', 'doc', 'ppt', 'pptx', 'zip', 'rar', 'xls', 'xlsx'</small>
                    @if($packet->qualifications->count())
                    @foreach($packet->qualifications as $qualification)
                    {!! Former::file('lampiran['.$qualification->id.']')->label($qualification->name)->help('ukuran maksimum 10MB') !!}
                    @endforeach
                    @endif
                    <hr>
                    <h3>Mengikuti Paket</h3>
                    <div class="text-center"><h4>PAKTA INTEGRITAS</h4></div>
                    <ol>
                        <li> tidak akan melakukan praktek Korupsi, Kolusi, dan Nepotisme (KKN);</li>
                        <li>akan melaporkan kepada pihak berwajib/berwenang apabila mengetahui ada indikasi KKN dalam proses pengadaan ini;</li>
                        <li>akan mengikuti proses pengadaan secara bersih, transparan, dan profesional untuk memberikan hasil kerja terbaik sesuai ketentuan peraturan perundang-undangan;</li>
                        <li>apabila melanggar hal-hal yang dinyatakan dalam PAKTA INTEGRITAS ini, saya bersedia dikenakan sanksi moral, sanksi administratif, serta dituntut ganti rugi dan pidana sesuai dengan ketentuan peraturan perundang-undangan yang berlaku.</li>
                    </ol>
                </div>
                <div class="panel-footer">
                    <button class="btn btn-success"> Setuju & Ikut Paket</button>
                    <button class="btn btn-danger"> Tidak Setuju</button>
                </div>
                {!! Former::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
