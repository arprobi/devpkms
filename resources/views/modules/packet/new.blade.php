@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @include('_box_profile')
        </div>

        <div class="col-md-10">

            @include('_incomplete')
            <ul class="nav nav-tabs hidden-xs" role="tablist" style="margin-bottom: 15px;">
                <li role="presentation"><a href="{{route('expert.packets')}}">Dashboard</a></li>
                <li role="presentation" class="active"><a href="{{route('expert.packets.new')}}">Paket Baru</a></li>
                <li role="presentation"><a href="{{route('expert.packets.follow')}}">Paket yang diikuti</a></li>
                <li role="presentation"><a href="{{route('expert.packets.ongoing')}}">Paket yang berjalan</a></li>
            </ul>
            <ul class="nav nav-tabs visible-xs" role="tablist" style="margin-bottom: 15px;">
                <li role="presentation" class="active dropdown">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-controls="tab-dropdown" aria-expanded="false">Pilih <span class="caret"></span></a>
                    <ul class="dropdown-menu" id="tab-dropdown">
                        <li role="presentation"><a href="{{route('expert.packets')}}">Dashboard</a></li>
                        <li role="presentation" class="active"><a href="{{route('expert.packets.new')}}">Paket Baru</a></li>
                        <li role="presentation"><a href="{{route('expert.packets.follow')}}">Paket yang diikuti</a></li>
                        <li role="presentation"><a href="{{route('expert.packets.ongoing')}}">Paket yang berjalan</a></li>
                    </ul>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa fa-fw fa-info"></i> Paket Baru</div>
                        @if($packets->count())
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th width="175">Judul</th>
                                        <th width="250">Deskripsi</th>
                                        <th width="120">Anggaran</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($packets as $packet)
                                    <tr>
                                        <td><a href="{{route('expert.packets.detail', $packet->id)}}">{{$packet->title}}</a></td>
                                        <td>{{str_limit($packet->description, 100)}}</td>
                                        <td>Rp. {{ number_format($packet->anggaran,0,',','.') }}</td>
                                        <td align="right">
                                            <div class="btn-group btn-group-sm">
                                                <a href="{{route('expert.packets.offer', $packet->id)}}" class="btn btn-default"><i class="fa fa-money"></i> Penawaran</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="panel-footer">{!! $packets->links() !!}</div>
                        @else
                        <div class="panel-body">
                            <p>Tidak ada paket baru.</p>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
