@extends('layouts.app')

@section('content')
<div id="packet-detail-page" class="container">
    <div class="row">
        <div class="col-md-2">
            @include('_box_profile')
        </div>

        <div class="col-md-10">

            @include('_incomplete')

            {{-- Breadcrumb  --}}
            <ol class="breadcrumb">
                <li><a href="{{route('expert.packets')}}"><i class="fa fa-fw fa-packets"></i> Paket</a></li>
                <li class="active">Detail - ID : {{$paid->packet->id}}, Title: {{$paid->packet->title}}</li>
            </ol>

            {{-- packet Detail --}}

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama</label>
                                <div class="form-control">{{$paid->name}}</div>
                            </div>
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <div class="form-control textarea">{{ $paid->description }}</div>
                            </div>
                            <div class="form-group">
                                <label>Dibayar Tanggal</label>
                                <div class="form-control">{{$paid->paid_date}}</div>
                            </div>
                            <div class="form-group">
                                <label>Nominal</label>
                                <div class="form-control">Rp. {{ number_format($paid->nominal,0,',','.') }}</div>
                            </div>
                            <div class='form-group'>
                                <label>Terbilang</label>
                                <p class='form-control-static'>{{ Terbilang::make($paid->nominal, ' Rupiah') }}</p>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <label>Kode Anggaran</label>
                                <div class="form-control">{{$paid->packet->kode_anggaran}}</div>
                            </div>
                            <div class="form-group">
                                <label>Total Pembayaran</label>
                                <div class="form-control">Rp. {{ number_format($paid->packet->pembayaran,0,',','.') }}</div>
                            </div>
                            <div class='form-group'>
                                <label>Terbilang</label>
                                <p class='form-control-static'>{{ Terbilang::make($paid->packet->pembayaran, ' rupiah') }}</p>
                            </div>
                            <div class='form-group'>
                                <label>Lampiran</label>
                                @if($paid->attachments->count())
                                <ul class="list-inline">
                                    @foreach($paid->attachments as $attachment)
                                    <li>
                                        <div class="btn-group btn-group-sm">
                                            <a href="{{route('admin.attachments.download', $attachment->short_url)}}" target="_blank" class="btn btn-default">{{$attachment->title}}</a>
                                            <a href="{{route('admin.attachments.delete', $attachment->short_url)}}" class="btn btn-default" data-confirm data-message="Hapus lampiran ini?"><i class="fa fa-times text-danger"></i></a>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                                @endif
                            </div>

                        </div>
                    </div>

                </div>
                <div class="panel-footer">
                    <a href="{{route('expert.packets.paids.page', $paid->packet->id)}}" class="btn btn-danger">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
