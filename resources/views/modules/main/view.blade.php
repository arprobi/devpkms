@extends('layouts.main')

@section('meta')
	<meta name="description" content="{{$faq->title}}">
    <meta name="keywords" content="{{$faq->tags}}, lkpp, hukum sanggah, pengadaan barang dan jasa pemerintah, hukum sanggah">
    <meta property="og:type" content="article" />
	<meta property="og:image" content="{{ url($faq->original_img) }}" />
	<meta property="og:title" content="{{$faq->title}}" />
	<meta property="og:description" content="{{ str_limit(strip_tags($faq->faq), 250) }}">
	<meta property="og:url" content="{{ url()->current() }}" />
	<meta property="og:site_name" content="{{ url()->current() }}" />

	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:title" content="{{$faq->title}}" />
	<meta name="twitter:description" content="{{ str_limit(strip_tags($faq->faq), 250) }}" />
	<meta name="twitter:image:src" content="{{ url($faq->original_img) }}" />

	<link rel="canonical" href="{{ url()->current() }}" />

	<title>{{$faq->title}}</title>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{asset('pkms/assets/css/pkms/detail.css')}}">
@endsection

@section('content')
	<div class="flex-center position-ref full-height">
		@include('modules.main._header')

		<div class="container" style="margin-top: 20px">
			<ol class="breadcrumb" style="text-align: left">
				<li><a href="{{route('main.search', ['code' => $faq->articlecode->id])}}" class="help-block">{{$faq->articlecode->name}}</a></li>
				<li><a href="{{route('main.search', ['subcode' => $faq->articlesubcode->id])}}">{{$faq->articlesubcode->name}}</a></li>
	        </ol>

	        <article id="content">
	        	<header>
	        		<h1 class="title text-center">{{$faq->title}}</h1>
	        	</header>

	        	@if($faq->image)
	        		<div class="page-header"><h3>Infografis</h3></div>
	        		<p><img class="img-responsive" src="{{ asset('images/original/' . $faq->image) }}" alt="Infografis"></p>
	        	@endif

	        	<div class="page-header"><h3>Dasar Hukum</h3></div>
	        	<p>{!! $faq->faq !!}</p>
	        	<div class="page-header"><h3>Tanya Jawab</h3></div>
	        	<p>{!! $faq->qa !!}</p>

	        	@if($faq->tags)
	                <p>
	                    @php $labels = explode(',', $faq->tags); @endphp
	                    @foreach($labels as $label)
	                        <a href="{{route('main.search', array_merge(request()->all(), ['tags' => $label]) )}}" class="btn btn-xs btn-primary">{{$label}}</a>
	                    @endforeach
	                </p>
                @endif
	        </article>

	        <hr>
			<h4 class="comment-header text-center">Komentar</h4>

			@if(request()->get('user_portal', false))
				{!! Former::open(route('main.comment', $faq->id)) !!}
				{!! Former::textarea('content')->id('comment-content')->label('')->rows(4)->placeholder('Berikan komentar anda tentang artikel ini')->required() !!}
				<button class="btn btn-primary"><i class="fa fa-send"></i> Kirim komentar</button>
				{!! Former::close() !!}
			@else
				<div class="form-group">
					<textarea class="form-control" rows="4" placeholder="Berikan komentar anda tentang artikel ini" disabled></textarea>
					<span class="help-block">Login  terlebih dulu untuk dapat memberikan komentar</span>
				</div>
				<div class="form-group">
					<a href="{{request()->get('sso_login_url', route('login'))}}" class="btn btn-primary">Login</a>
				</div>
			@endif


	        @if($faq->comments->count())
	        	<hr>
	        	<ul class="media-list">
		        	@foreach($faq->comments as $comment)
						<li class="media">
							<div class="media-left"><img src="{{ "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $comment->email ) ) ) . "?s=48" }}" alt="avatar" class="img-circle"></div>
							<div class="media-body">
								<h5 class="media-heading">{{$comment->name}} <small>{{$comment->created_at}}</small></h5>
								<p>{{$comment->content}}</p>
							</div>
						</li>
		        	@endforeach
	        	</ul>
	        @else
	        	<div class="alert alert-default text-center"><i class="fa fa-info-circle"></i> Belum ada komentar</div>
	        @endif

			@if($suggestions->count())
		        <div id="suggestion">
		        	<div class="page-header"><h4>Suggestion</h4></div>
		        	<div class="media-list">
			        	@foreach($suggestions as $suggestion)
			        		<div class="media" style="padding-bottom: 14px; border-bottom: 1px dashed #ccc">
			        			<div class="media-body">
			        				<h4 class="media-heading">
			        					<a href="{{route('main.detail', ['kategori' => str_replace(' ', '', strtolower($suggestion->nm_klasifikasi)), 'id' => $suggestion->id, 'slug' => $suggestion->slug])}}">{{$suggestion->title}}</a>
			        				</h4>
			        				<span class="media-heading">
			        					<a href="{{route('main.search', ['code' => $suggestion->articlecode->id])}}">{{$suggestion->articlecode->name}}</a>
			        					&gt;
			        					<a href="{{route('main.search', ['subcode' => $suggestion->articlesubcode->id])}}">{{$suggestion->articlesubcode->name}}</a>
			        				</span>
			        				<p>{{ str_limit(strip_tags($suggestion->faq), 255) }} <a href="{{route('main.detail', ['kategori' => str_replace(' ', '', strtolower($suggestion->nm_klasifikasi)), 'id' => $suggestion->id, 'slug' => $suggestion->slug])}}">lebih lanjut</a></p>

			        				@if($suggestion->tags)
						                <p>
						                    @php $labels = explode(',', $suggestion->tags); @endphp
						                    @foreach($labels as $label)
						                        <a href="{{route('main.search', array_merge(request()->all(), ['tags' => $label]) )}}" class="btn btn-xs btn-primary">{{$label}}</a>
						                    @endforeach
						                </p>
					                @endif
			        			</div>
							</div>
			        	@endforeach
		        	</div>
		        </div>
	        @endif
        </div>

	</div>
@endsection