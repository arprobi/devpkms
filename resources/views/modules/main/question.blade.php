@extends('layouts.main')

@section('meta')
    <meta name="description" content="PKMS Expert Management System">
    <meta name="keywords" content="pkms,pemerintah, pengadaan barang dan jasa, perpres 16 tahun 2018, dasar hukum, lembaga pemerintah, pengetahuan, infografis, studi kasus, materi paparan, lkpp, hukum sanggah">

    <meta property="og:type" content="article" />
    <meta property="og:image" content="{{ asset('/img/new_lkpp.jpg') }}" />
    <meta property="og:title" content="PKMS Expert Management System" />
    <meta property="og:description" content="Procurement Knowledge Management System (PKMS) merupakan sebuah aplikasi dimana anda dapat membaca berbagai pengetahuan mengenai pengadaan barang/jasa pemerintah Indonesia. Pengetahuan yang ada pada aplikasi ini didasarkan pada Perpres 16 Tahun 2018 serta beberapa sumber hukum lain.">
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:site_name" content="{{ url()->current() }}" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="PKMS Expert Management System" />
    <meta name="twitter:description" content="Procurement Knowledge Management System (PKMS) merupakan sebuah aplikasi dimana anda dapat membaca berbagai pengetahuan mengenai pengadaan barang/jasa pemerintah Indonesia. Pengetahuan yang ada pada aplikasi ini didasarkan pada Perpres 16 Tahun 2018 serta beberapa sumber hukum lain." />
    <meta name="twitter:image:src" content="{{ asset('/img/new_lkpp.jpg') }}" />

    <link rel="canonical" href="{{ url()->current() }}" />

    <title>PKMS Expert Management System</title>
@endsection

@section('styles')
	<link rel="stylesheet" href="{{ asset('pkms/assets/css/pkms/pertanyaan/main.css') }}">
@endsection

@section('scripts')
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="{{asset('js/main.js')}}"></script>
@endsection

@php
	$counter = 0;
@endphp

@section('content')
	<div class="flex-center position-ref full-height">
        @include('modules.main._header')

		<div class="container">
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="panel">
	        			<div class="panel-heading"><p class="lead">Pertanyaan</p></div>
	        			<div class="panel-body">
	        				<div id="widget-list-pertanyaan">
	        					<div class="list-group" id="list-pertanyaan">
	        						@foreach($questions as $question)
	        							<a href="javascript:;" class="list-group-item {{$counter ? '' : 'active'}}" data-id="{{$question->id}}">
	        								<div class="subject">{{$question->title}}</div>
	        								<div class="user">
	        									<strong>{{$question->name}}</strong>
	        									{{$question->created_at}}
	        								</div>
	        								<div class="text">{{$question->content}}</div>
	        							</a>
	        							@php $counter++ @endphp
	        						@endforeach
	        					</div>
	        					<div class="button-toolbar">
	        						<div class="pull-left">
	        							<div class="btn-group">{{$questions->total()}} pertanyaan</div>
	        						</div>
	        						<div class="pull-right">
        							{{$questions->links()}}
	        						</div>
	        						<div class="clear-fix"></div>
	        					</div>
	        				</div>
	        			</div>
	        		</div>
	        		<div class="panel panel-default">
	        			<div class="panel-heading">Kirim pertanyaan anda</div>
	        			{!! Former::open() !!}
	        				<div class="panel-body">
	        					{!! Former::text('name')->label('Nama')->placeholder('Nama')->required() !!}
	        					{!! Former::email('email')->label('Email')->placeholder('Email')->required() !!}
	        					{!! Former::text('title')->label('Subjek')->placeholder('Subjek pertanyaan')->required() !!}
	        					{!! Former::textarea('content')->label('Detail pertanyaan')->rows(6)->required() !!}

	        					<div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
		                            {!! Recaptcha::render() !!}
		                            @if ($errors->has('g-recaptcha-response'))
		                                <span class="help-block">
		                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
		                                </span>
		                            @endif
		                        </div>
	        				</div>
	        				<div class="panel-footer" align="text-right">
	        					<button class="btn btn-success"><i class="fa fa-fw fa-send"></i> Submit</button>
	        				</div>
        				{!! Former::close() !!}
	        		</div>
	        	</div>
	        	<div class="col-md-6">
					<div class="panel">
						<div class="panel-heading"><p class="lead">Detail Pertanyaan</p></div>
						<div class="panel-body">
							<div class="hide loading text-center">
								<i class="fa fa-circle-o-notch fa-spin fa-5x fa-fw"></i>
							</div>
							<div class="result-detail"></div>
						</div>
					</div>
	        	</div>
	        </div>
        </div>
    </div>
@endsection