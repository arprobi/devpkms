@php
	$captcha = str_random(5);
@endphp

<div class="form-group">
	<label>Pengirim</label>
	<div class="form-control-static">{{$question->name}}</div>
</div>

<div class="form-group">
	<label>Judul</label>
	<div class="form-control-static">{{$question->title}}</div>
</div>

<div class="form-group">
	<label>Waktu</label>
	<div class="form-control-static">{{$question->created_at}}</div>
</div>

<div class="form-group">
	<label>Pertanyaan</label>
	<div class="form-control-static" style="height: 10em; overflow-y: scroll;">{{$question->content}}</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Jawaban</div>
	<div class="panel-body">
		<ul class="media-list">
			@if($question->answers->count())
				@foreach($question->answers as $answer)
					<li class="media">
						<div class="media-left">
							<div class="media-left"><img src="{{ "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $answer->email ) ) ) . "?s=48" }}" alt="avatar" class="img-circle"></div>
						</div>
						<div class="media-body">
							<p>
								<strong>{{$answer->name}}</strong><code>{{$answer->email}}</code>
								<br>
								<small>{{$answer->created_at}}</small>
							</p>
							<p>{{$answer->content}}</p>
						</div>
					</li>
				@endforeach
			@else
				<li class="list-group-item">Belum ada jawaban</li>
			@endif
		</ul>
	</div>
	<div class="panel-heading">Kirim tanggapan</div>
	{!! Former::open(route('main.postAnswer', $question->id)) !!}
		<div class="panel-body">
			{!! Former::text('name')->label('Nama')->placeholder('Nama')->required() !!}
			{!! Former::email('email')->label('Email')->placeholder('Email')->required() !!}
			{!! Former::textarea('content')->label('Tanggapan')->rows(6)->required() !!}
			{!! Former::hidden('captcha')->value($captcha) !!}

			<p class="captcha text-center">
				{{$captcha}}
			</p>

			{!! Former::text('captcha_confirmation')->label('captcha')->placeholder('Salin kode diatas..')->required() !!}
		</div>
		<div class="panel-footer">
			<button class="btn btn-default">Kirim</button>
		</div>
	{!! Former::close() !!}
</div>