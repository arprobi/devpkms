@extends('layouts.main')

@section('meta')
    <meta name="description" content="PKMS Expert Management System">
    <meta name="keywords" content="pkms,pemerintah, pengadaan barang dan jasa, perpres 16 tahun 2018, dasar hukum, lembaga pemerintah, pengetahuan, infografis, studi kasus, materi paparan, lkpp, hukum sanggah">

    <meta property="og:type" content="article" />
    <meta property="og:image" content="{{ asset('/img/new_lkpp.jpg') }}" />
    <meta property="og:title" content="PKMS Expert Management System" />
    <meta property="og:description" content="Procurement Knowledge Management System (PKMS) merupakan sebuah aplikasi dimana anda dapat membaca berbagai pengetahuan mengenai pengadaan barang/jasa pemerintah Indonesia. Pengetahuan yang ada pada aplikasi ini didasarkan pada Perpres 16 Tahun 2018 serta beberapa sumber hukum lain.">
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:site_name" content="{{ url()->current() }}" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="PKMS Expert Management System" />
    <meta name="twitter:description" content="Procurement Knowledge Management System (PKMS) merupakan sebuah aplikasi dimana anda dapat membaca berbagai pengetahuan mengenai pengadaan barang/jasa pemerintah Indonesia. Pengetahuan yang ada pada aplikasi ini didasarkan pada Perpres 16 Tahun 2018 serta beberapa sumber hukum lain." />
    <meta name="twitter:image:src" content="{{ asset('/img/new_lkpp.jpg') }}" />

    <link rel="canonical" href="{{ url()->current() }}" />

    <title>{{ $disclaimer->title }}</title>
@endsection

@section('content')
    <div class="container" style="margin-top: 50px; margin-bottom: 100px;">
        <h1 class="display-4" style="margin-bottom: 30px;">{{ $disclaimer->title }}</h1>

        <div class="flex-center position-ref full-height" style="font-size: 16px;">
            {!! $disclaimer->content !!}
        </div>
    </div>
@endsection