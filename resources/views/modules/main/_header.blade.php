<nav class="navbar navbar-inverse navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ url('/')}}"><span class="hidden-xs">Procurement Knowledge Management System</span><span class="visible-xs">PKMS</span></a>
        </div>
    </div>
</nav>