@extends('layouts.main')

@section('meta')
    <meta name="description" content="PKMS Expert Management System">
    <meta name="keywords" content="pkms,pemerintah, pengadaan barang dan jasa, perpres 16 tahun 2018, dasar hukum, lembaga pemerintah, pengetahuan, infografis, studi kasus, materi paparan, lkpp, hukum sanggah">

    <meta property="og:type" content="article" />
    <meta property="og:image" content="{{ asset('/img/new_lkpp.jpg') }}" />
    <meta property="og:title" content="PKMS Expert Management System" />
    <meta property="og:description" content="Procurement Knowledge Management System (PKMS) merupakan sebuah aplikasi dimana anda dapat membaca berbagai pengetahuan mengenai pengadaan barang/jasa pemerintah Indonesia. Pengetahuan yang ada pada aplikasi ini didasarkan pada Perpres 16 Tahun 2018 serta beberapa sumber hukum lain.">
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:site_name" content="{{ url()->current() }}" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="PKMS Expert Management System" />
    <meta name="twitter:description" content="Procurement Knowledge Management System (PKMS) merupakan sebuah aplikasi dimana anda dapat membaca berbagai pengetahuan mengenai pengadaan barang/jasa pemerintah Indonesia. Pengetahuan yang ada pada aplikasi ini didasarkan pada Perpres 16 Tahun 2018 serta beberapa sumber hukum lain." />
    <meta name="twitter:image:src" content="{{ asset('/img/new_lkpp.jpg') }}" />

    <link rel="canonical" href="{{ url()->current() }}" />

    <title>PKMS Expert Management System</title>
@endsection

@php
    $kodefikasi = PKMS\Models\MasterArticleCode::with('subcodes')->get();
@endphp

@section('styles')
    <link rel="stylesheet" href="{{asset('pkms/assets/css/pkms/search.css')}}">
@endsection

@section('content')
    <div class="flex-center position-ref full-height">
        @include('modules.main._header')

        <div class="jumbotron" style="background: #f9f9f9">
            <div class="inner">
                <div class="container">
                    <ol class="breadcrumb" style="text-align: left">
                        @if($codes)
                            <li><a href="{{route('main.search', ['code', $codes->id])}}">{{$codes->name}}</a></li>
                        @endif

                        @if($subcodes)
                            <li><a href="{{route('main.search', ['code' => $subcodes->articlecode->id])}}">{{$subcodes->articlecode->name}}</a></li>
                            <li><a href="{{route('main.search', ['subcode' => $subcodes->id])}}">{{$subcodes->name}}</a></li>
                        @endif
                    </ol>
                    <form action="{{route('main.search')}}" method="get" id="search">
                        <div class="input-group input-group-lg">
                            <input type="text" name="q" class="form-control" placeholder="Masukan kata kunci..." autofocus="" value="{{request()->get('q','')}}">
                            <span class="input-group-btn">
                                <button class="btn btn-primary"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    @if(request()->get('tags', false))
                    <p class="text-left">
                        @php $labels = explode(',', request()->get('tags', false)); @endphp
                        @foreach($labels as $label)
                            @php

                                $links = ($labels) ? implode(',', array_except($labels, array_search($label, $labels)) ) : null;
                            @endphp
                            <span class="btn-group btn-group-xs">
                                <a href="javascript:;" class="btn btn-primary">{{$label}}</a>
                                <a href="{{route('main.search', array_merge(request()->except('tags'), ['tags' => $links]) )}}" class="btn btn-primary btn-delete"><i class="fa fa-times"></i></a>
                            </span>
                        @endforeach
                    </p>
                    @endif
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    @if($kodefikasi->count())
                        <div class="page-header no-padding"><h4>Kodefikasi</h4></div>
                        @foreach($kodefikasi as $code)
                            <div class="radio kodefikasi">
                                <label>
                                    <a href="{{route('main.search', [ 'code' => $code->id])}}">
                                        <input type="radio" name="radios" @php echo request()->get('code',false) == $code->id ? 'checked' : '' @endphp>
                                        {{$code->name}}
                                    </a>
                                </label>
                            </div>
                            @if($code->subcodes->count())
                                <div class="sub-kodefikasi">
                                @foreach($code->subcodes as $subcode)
                                    <div class="radio kodefikasi">
                                        <label>
                                            <a href="{{route('main.search', ['subcode' => $subcode->id ])}}">
                                                <input type="radio" name="radios" @php echo request()->get('subcode',false) == $subcode->id ? 'checked' : '' @endphp>
                                                {{$subcode->name}}
                                            </a>
                                        </label>
                                    </div>
                                @endforeach
                                </div>
                            @endif
                        @endforeach
                    @else
                        &nbsp;
                    @endif
                </div>
                <div class="col-md-9" style="border-left: 1px solid #eee">
                    <div class="page-header no-padding">
                        <h4>Hasil pencarian</h4>
                    </div>
                    @if($faqs->count())
                        @foreach($faqs as $faq)
                            <div class="list-group" id="list-article">
                                <div class="list-group-item">
                                    <h4><a href="{{route('main.detail', ['kategori' => str_replace(' ', '', strtolower($faq->nm_klasifikasi)), 'id' => $faq->id, 'slug' => $faq->slug])}}">{{$faq->title}}</a></h4>
                                    <p class="text">
                                        {{ str_limit(strip_tags($faq->faq), 400) }} <a href="{{route('main.detail', ['kategori' => str_replace(' ', '', strtolower($faq->nm_klasifikasi)), 'id' => $faq->id, 'slug' => $faq->slug])}}">lebih lanjut.</a>
                                    </p>
                                    <p class="meta">
                                        <a href="{{route('main.search', ['code' => $faq->articlecode->id])}}">{{$faq->articlecode->name}}</a>
                                        &gt;
                                        <a href="{{route('main.search', ['subcode' => $faq->articlesubcode->id])}}">{{$faq->articlesubcode->name}}</a>
                                    </p>
                                    @if($faq->tags)
                                    <p>
                                        @php
                                            $labels = explode(',', $faq->tags);
                                        @endphp
                                        @foreach($labels as $label)
                                            @php
                                                $tags   = request()->get('tags', false) ? explode(',', request()->get('tags')) : [];
                                                $links  = in_array($label, $tags) ? implode(',', $tags) : implode(',',array_merge($tags, [$label]));
                                            @endphp
                                            <a href="{{route('main.search', array_merge(request()->except('tags'), ['tags' => $links]) ) }}" class="btn btn-xs btn-primary">{{$label}}</a>
                                        @endforeach
                                    </p>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                        <div class="pull-right">{{$faqs->appends(request()->all())->links()}}</div>
                        <div class="clearfix"></div>
                    @else
                        <p><i class="fa fa-info-circle"></i> Pencarian nihil.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection