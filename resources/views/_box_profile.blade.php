@php

$cache = Cache::remember('user_'. Auth::user()->id, 30, function () {
    return [
        'average_rating' => Auth::user()->averageRating()->first(),
        'unread_message' => Auth::user()->inbox()->where('read', 0)->get()
    ];
});

@endphp

<div class="panel panel-default visible-md visible-lg">
    <div class="panel-body" align="center">
        <img src="{{ Auth::user()->avatar ? secure_asset('storage/'.Auth::user()->avatar) : secure_asset('img/default_avatar.jpg') }}" alt="avatar" width="128px" height="128px" class="img img-circle img-responsive img-thumbnail">
    </div>
    <table class="table">
        <tr>
            <th colspan="3" class="text-center">
                <a href="{{route('expert.profile')}}">{{Auth::user()->name}}</a>
            </th>
        </tr>

        @if(!Auth::user()->is_admin)
            <tr>
                <td colspan="3" class="text-center">
                    @if($cache['average_rating'])
                        @php
                            $avg_rating = $cache['average_rating']->aggeragate;
                            $star = 0;
                        @endphp

                        @for($i = 0; $i < floor($avg_rating); $i++, $star++)
                            <i class="fa fa-star bg-yellow"></i>
                        @endfor

                        @if(fmod($avg_rating, 1))
                            @php
                                $star++;
                            @endphp
                            <i class="fa fa-star-half-empty bg-yellow"></i>
                        @endif

                        @for($i = 1; $i < $star; $i++)
                            <i class="fa fa-star-o bg-yellow"></i>
                        @endfor

                    @else
                        @for($i = 0; $i < 5; $i++)
                            <i class="fa fa-star-o bg-yellow"></i>
                        @endfor
                    @endif
                </td>
            </tr>
        @endif

        <tr>
            <td colspan="3" class="text-center">
                <a href="{{route('expert.messages')}}"><i class="fa fa-fw fa-envelope-o"></i>
                    Inbox &nbsp;
                    @if($cache['unread_message']->count())
                        <span class="badge badge-danger">{{$cache['unread_message']->count()}}</span>
                    @endif
                </a>
            </td>
        </tr>
    </table>
</div>

@if(!request()->session()->get('incomplete', false))
    <div class="list-group visible-md visible-lg">
        <a href="{{route('expert.tasks')}}" class="list-group-item"><i class="fa fa-fw fa-tasks"></i> Tugas</a>
        <a href="{{route('expert.articles')}}" class="list-group-item"><i class="fa fa-fw icon-docs"></i> Tulisan</a>
        <a href="{{route('expert.packets')}}" class="list-group-item"><i class="fa fa-fw fa-columns"></i> Paket</a>
    </div>
@endif
