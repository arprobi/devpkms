<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ secure_asset('/css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body style="margin-top: 80px">
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">500 - Terjadi kesalahan pada internal server.</div>
            <div class="panel-body">
                {!! $exception->getMessage() !!}

                @if(Auth::user())
                    @if(Auth::user()->is_admin)
                        <a href="{{route('admin.experts')}}" class="btn btn-default">Kembali ke Control Panel</a>
                    @else
                        <a href="{{route('expert.dashboard')}}" class="btn btn-default">Kembali ke dashboard</a>
                    @endif
                @endif
            </div>
        </div>
    </div>
</body>
</html>
