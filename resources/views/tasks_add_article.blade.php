@extends('layouts.app')

@section('content')
<div id="article-edit-page" class="container">
    <div class="row">
        <div class="col-md-2">
            @include('_box_profile')
        </div>
        <div class="col-md-10">
            @include('_incomplete')
            <div class="panel panel-default">
                <div class="panel-heading">Buat artikel</div>
                {!! Former::open_for_files(route('expert.articles.create', $task->id)) !!}
                    <div class="panel-body">
                        {!! Former::select('article_code')->label('Kodefikasi')->fromQuery(PKMS\Models\MasterArticleCode::get(),'name','id')->required() !!}
                        {!! Former::select('article_subcode')->label('Subkodefikasi')->fromQuery(PKMS\Models\MasterArticleSubcode::where('code', 1)->get(),'name','id')->required() !!}
                        {!! Former::text('title')->label('Judul')->placeholder('judul tulisan')->required() !!}
                        {!! Former::textarea('overview')->label('Ringkasan')->rows(5)->placeholder('ringkasan')->required() !!}
                        {!! Former::textarea('content')->label('Konten')->placeholder('konten')->required() !!}
                        {!! Former::number('revision')->label('Revisi ke')->min(0)->placeholder('No Revisi')->value(0) !!}
                    </div>
                    <div class="panel-heading"><i class="fa fa-paperclip"></i> Lampiran</div>
                    <div class="panel-body">
                        {!! Former::file('lampiran[]')->label('Lampiran')->multiple()
                            ->accept('.jpg, .jpeg, .png, .bmp, .pdf, .docx, .doc, .ppt, .pptx, .zip, .rar, .xls, .xlsx')
                            ->help('tipe: .jpg, .jpeg, .png, .bmp, .pdf, .docx, .doc, .ppt, .pptx, .zip, .rar, .xls, .xlsx, Ukuran file: 8MB')
                        !!}
                    </div>
                    <div class="panel-footer">
                        <a href="{{route('expert.tasks.detail', $task->id)}}" class="btn btn-default">Kembali</a>
                        <button class="btn btn-success">Simpan</button>
                    </div>
                {!! Former::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
