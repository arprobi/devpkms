@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-2">
            @include('_box_profile')
        </div>

        <div class="col-md-10">

            @include('_incomplete')

            {{-- Tugas Baru  --}}
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-fw fa-info"></i> Tugas Masuk</div>

                @if($new_tasks->count())
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="120">Tgl Terima</th>
                                    <th width="175">Judul</th>
                                    <th width="250">Deskripsi</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($new_tasks as $task)
                                    <tr>
                                        <td>{{$task->assigned_at}}</td>
                                        <td><a href="{{route('expert.tasks.detail', $task->id)}}">{{$task->title}}</a></td>
                                        <td>{{str_limit($task->description, 100)}}</td>
                                        <td align="right">
                                            <div class="btn-group btn-group-sm">
                                                <a href="{{route('expert.tasks.accept', $task->id)}}" class="btn btn-accept btn-default"><i class="fa fa-check text-success"></i> Terima</a>
                                                <a href="{{route('expert.tasks.deny', $task->id)}}" class="btn btn-deny btn-default"><i class="fa fa-times text-danger"></i> Tolak</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="panel-body">
                        <p>Tidak ada penugasan baru.</p>
                    </div>
                @endif
            </div>

            {{-- Tugas Berjalan  --}}
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-fw fa-tasks"></i> Tugas berjalan</div>

                @if($my_tasks->count())
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="120">Tgl Penugasan</th>
                                    <th width="175">Judul</th>
                                    <th width="250">Deskripsi</th>
                                    <th>Progress</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($my_tasks as $task)
                                    <tr>
                                        <td>{{$task->accepted_at}}</td>
                                        <td><a href="{{route('expert.tasks.detail', $task->id)}}">{{$task->title}}</a></td>
                                        <td>{{str_limit($task->description, 100)}}</td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{$task->progress}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$task->progress}}%;">{{$task->progress}}%</div>
                                            </div>
                                        </td>
                                        <td>
                                            {{ ($task->progress >= 100) ? 'Menunggu approval' : 'Belum selesai' }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="panel-body">
                        <p>Belum ada tugas yang sedang berjalan.</p>
                    </div>
                @endif
            </div>

            {{-- Tugas Selesai --}}
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-fw fa-check text-success"></i> Tugas selesai</div>
                @if($completed_tasks->count())
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="120">Tgl Penugasan</th>
                                    <th width="175">Judul</th>
                                    <th width="250"Deskripsi</th>
                                    <th>Rating</th>
                                    <th>Review</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($completed_tasks as $task)
                                    <tr>
                                        <td>{{$task->accepted_at}}</td>
                                        <td><a href="{{route('expert.tasks.detail', $task->id)}}">{{$task->title}}</a></td>
                                        <td>{{str_limit($task->description, 100)}}</td>
                                        <td>
                                            @for($i = 0; $i < floor($task->rating->rating); $i++)
                                                <i class="fa fa-star bg-yellow"></i>
                                            @endfor

                                            @if(fmod($task->rating->rating, 1))
                                                <i class="fa fa-star-half bg-yellow"></i>
                                            @endif
                                        </td>
                                        <td>{{$task->rating->review or '-'}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="panel-body">
                        <p>Belum ada tugas yang selesai.</p>
                    </div>
                @endif
            </div>

        </div>
    </div>
</div>
@endsection
