@php
    $kodefikasi = PKMS\Models\MasterArticleCode::take(4)->get();
    $subkodefikasi = PKMS\Models\MasterArticleSubcode::orderBy('code','desc')->get();
    $limit = PKMS\Models\MasterArticleSubcode::select(DB::raw('`code`, count(`id`) as number'))->groupBy('code')->orderBy('number','desc')->first();
@endphp

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="LKPP" />
        <meta name="description" content="PKMS Expert Management System">
        <meta name="keywords" content="pkms,pemerintah, pengadaan barang dan jasa, perpres 16 tahun 2018, dasar hukum, lembaga pemerintah, pengetahuan, infografis, studi kasus, materi paparan">
        <meta name="platform" content="desktop">
        <meta name="googlebot-news" content="index,follow" />
        <meta  name="googlebot" content="index,follow" />
        <meta name="author" content="LKPP">
        <meta name="robots" content="index, follow" />
        <meta name="language" content="id" />
        <meta name="geo.country" content="id" />
        <meta http-equiv="content-language" content="In-Id" />
        <meta name="geo.placename" content="Indonesia" />

        <meta property="og:type" content="article" />
        <meta property="og:image" content="{{url('/fav.png')}}" />
        <meta property="og:title" content="PKMS Expert Management System" />
        <meta property="og:description" content="Procurement Knowledge Management System (PKMS) merupakan sebuah aplikasi dimana anda dapat membaca berbagai pengetahuan mengenai pengadaan barang/jasa pemerintah Indonesia. Pengetahuan yang ada pada aplikasi ini didasarkan pada Perpres 16 Tahun 2018 serta beberapa sumber hukum lain.">
        <meta property="og:url" content="{{url('/')}}/" />
        <meta property="og:site_name" content="{{url('/')}}" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content="PKMS Expert Management System" />
        <meta name="twitter:description" content="Procurement Knowledge Management System (PKMS) merupakan sebuah aplikasi dimana anda dapat membaca berbagai pengetahuan mengenai pengadaan barang/jasa pemerintah Indonesia. Pengetahuan yang ada pada aplikasi ini didasarkan pada Perpres 16 Tahun 2018 serta beberapa sumber hukum lain." />
        <meta name="twitter:image:src" content="{{url('/fav.png')}}" />

        <link rel="canonical" href="{{url('/')}}" />
        <title>PKMS Expert Management System</title>

        <!-- Fonts -->
        <link href="//fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="/fav.png" rel="shortcut icon"/>
        <link rel="stylesheet" href="{{secure_asset('pkms/assets/includes/bootstrap/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{secure_asset('pkms/assets/css/style.css')}}">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            {{-- @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        @if (Auth::user()->is_admin)
                            <a href="{{ route('admin.articles') }}">Control Panel</a>
                        @else
                            <a href="{{ route('expert.dashboard') }}">Dashboard</a>
                        @endif
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif --}}

            <div class="jumbotron" style="height:625px">
                <div class="inner">
                    <div class="container">
                        <h1>Procurement Knowledge Management System</h1>
                        <p>Memberi Solusi Untuk Segala Kondisi</p>
                        <br/>
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <form method="get" id="search">
                                    <div class="input-group input-group-lg">
                                        <input type="text" name="q" class="form-control" placeholder="Masukan kata kunci..." autofocus="" required="">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                    <p class="help-block">Tidak menemukan yang anda cari? <a href="javascript:;">Klik disini</a></p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Category --}}
            <div id="category">
                <div class="container">
                    <h1 class="text-center">Kategori</h1>
                    <br/>
                    <div class="row">
                        @if($kodefikasi->count())
                            @foreach($kodefikasi as $kode)
                                <div class="col-md-3">
                                    <a href="javascript:;" class="thumbnail" data-id="">
                                        <img src="{{secure_asset('pkms/assets/images/kodefikasi.jpg')}}">
                                        <strong>{{ $kode->name }}</strong>
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="border"></div>

            {{-- Subcategory --}}
            <div id="sub-category">
                <div class="container">
                    <h1 class="text-center">Sub Kategori</h1>
                    <br/>
                    <div class="row">
                        @foreach($kodefikasi as $kode)
                            <div class="col-md-3">
                                <div class="row">
                                    @php
                                        $subcode_chunk = $subkodefikasi->where('code', $kode->id);
                                    @endphp

                                    @foreach($subcode_chunk as $subkode)
                                        <div class="col-md-12 col-sm-6">
                                            <a href="javascript:;">{{$subkode->name}}</a>
                                        </div>
                                    @endforeach

                                    @for($i = $subcode_chunk->count(); $i < $limit->number; $i++)
                                        <div class="col-md-12 col-sm-6">
                                            <a href="javascript:;">&nbsp;</a>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div id="footer">
                <div class="container">
                    <p class="text-center">&COPY; 2016 &CenterDot; LKPP Deputi Bidang Hukum dan Penyelesaian Sanggah</p>
                </div>
            </div>
        </div>
    </body>
</html>
