
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('sweetalert2');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

var App = {
    baseUrl: $('meta[name="prefix"]').attr('content'),
    wizardForm: function () {
        if ($('.wizardform').length) {
            $('.wizardform').bootstrapWizard();
        }
    },
    profilePage: {
        setupRegion: function () {
            var $province = $('#province_code'),
                    $city = $('#city_code'),
                    $district = $('#district_code'),
                    $village = $('#village_code'),
                    addOptions = function (items) {
                        var output = '<option disabled selected value="">Pilih</option>';
                        for (var i = 0; i < items.length; i++) {
                            output += '<option value="' + items[i]['code'] + '">' + items[i]['name'] + '</option>';
                        }
                        return output;
                    },
                    enableDate = function (names) {
                        for (var i = 0; i < names.length; i++) {
                            names[i].prop('disabled', false);
                        }
                    },
                    disableDate = function (names) {
                        for (var i = 0; i < names.length; i++) {
                            names[i].html('<option disabled selected value="">Pilih</option>');
                            names[i].prop('disabled', true);
                        }
                    };

            $province.change(function () {
                $.get(App.baseUrl + '/api/getCityByProvinceId/' + $province.val(), function (data) {
                    $city.html(addOptions(data));
                    enableDate([$city]);
                    disableDate([$district, $village]);
                });
            });

            $city.change(function () {
                $.get(App.baseUrl + '/api/getDistrictByCityId/' + $city.val(), function (data) {
                    $district.html(addOptions(data));
                    enableDate([$district]);
                    disableDate([$village]);
                });
            });

            $district.change(function () {
                $.get(App.baseUrl + '/api/getVillageByDistrictId/' + $district.val(), function (data) {
                    $village.html(addOptions(data));
                    enableDate([$village]);
                });
            });

            $('#reset').click(function () {
                disableDate([$city, $district, $village]);
            });
        },
        init: function () {
            this.setupRegion();

            $('select').select2();
            $('#hobby').select2({tags: true});
            $('input[data-type="date"]').datepicker({
                format: 'dd-mm-yyyy'
            });
        }
    },
    experiencePage: {
        init: function () {
            $('input[data-type="date"]').datepicker({
                format: 'MM yyyy',
                startView: 1
            });

            $('#from').change(function () {
                var $this = $(this),
                        $to = $('#to');
                var from = $this.val(),
                        to = $to.val();

                if (from !== '' && to !== '' && (new Date(from) > new Date(to))) {
                    $to.val('');
                }
            });
        }
    },
    taskDetailPage: {
        init: function () {
            $('input#progress').on('change', function () {
                var $this = $(this);
                var value = $this.val();

                if (value > 100) {
                    $this.val(100);
                }

                if (value < 0 || value == '') {
                    $this.val(0);
                }
            });
        }
    },
    taskUnassignedListPage: {
        addOption: function (data) {
            var output = '';

            if (data.length > 0) {

                for (var i = 0; i < data.length; i++) {
                    output += '<option value="' + data[i].id + '">ID: ' + data[i].id + ' - ' + data[i].name + '</option>';
                }

            } else {
                output = '<option disabled selected="true">Tidak ada tenaga ahli yang telah diverifikasi.</option>';
            }

            return output;
        },
        taskListener: function () {
            var modal = $('#modal-assign-task');

            modal.on('shown.bs.modal', function (e) {
                var $userId = $('#user_id'),
                        $btn = $(e.relatedTarget);

                modal.find('form').attr('action', '/admin/tasks/assign/' + $btn.data('task-id'));
                modal.find('#assigned-task').find('.form-control').html($btn.data('title'));

                $.get(App.baseUrl + '/api/users/verified', function (data) {
                    $userId.html(App.taskUnassignedListPage.addOption(data));
                }).then(function () {
                    $userId.select2({
                        placeholder: 'Pilih tenaga ahli'
                    });
                });
            });
        },
        init: function () {
            this.taskListener();
        }
    },
    expertListPage: {
        addOption: function (data) {
            var output = '';

            if (data.length > 0) {

                for (var i = 0; i < data.length; i++) {
                    output += '<option value="' + data[i].id + '">ID: ' + data[i].id + ' - ' + data[i].title + '</option>';
                }

            } else {
                output = '<option disabled selected="true">Tidak ada tugas yang belum ditugaskan.</option>';
            }

            return output;
        },
        filters: function () {
            var processResults = function (data) {
                var items = [];
                for (var i = 0; i < data.length; i++) {
                    items.push({id: data[i], text: data[i]});
                }
                return {
                    results: items
                };
            };
            $('.filter_occupations').select2({
                placeholder: 'Pilih pekerjaan',
                tags: true,
                ajax: {
                    url: function (params) {
                        var term = params.term || '';
                        return App.baseUrl + '/api/occupations?name=' + term;
                    },
                    processResults: processResults,
                    delay: 500
                }
            });
            $('.filter_interests').select2({
                placeholder: 'Pilih minat',
                tags: true,
                ajax: {
                    url: function (params) {
                        var term = params.term || '';
                        return App.baseUrl + '/api/interests?name=' + term;
                    },
                    processResults: processResults,
                    delay: 500
                }
            });
            $('.filter_hobbies').select2({
                placeholder: 'Pilih hobi',
                tags: true,
                ajax: {
                    url: function (params) {
                        var term = params.term || '';
                        return App.baseUrl + '/api/hobbies?name=' + term;
                    },
                    processResults: processResults,
                    delay: 500
                }
            });
            $('.filter_rating').select2({
                minimumResultsForSearch: Infinity,
                templateSelection: function (data, container) {
                    var output = '',
                            rating = parseInt(data.id);

                    if (rating === NaN || rating <= 0) {
                        return $('<span>Pilih minimal rating</span>');
                    }

                    for (var i = 0; i < rating; i++) {
                        output += '<i class="fa fa-fw fa-star bg-yellow"></i>';
                    }

                    return $(output);
                },
                templateResult: function (data) {
                    var output = '',
                            rating = parseInt(data.id);

                    if (rating === NaN || rating <= 0) {
                        return $('<span>Pilih rating</span>');
                    }

                    for (var i = 0; i < rating; i++) {
                        output += '<i class="fa fa-fw fa-star bg-yellow"></i>';
                    }

                    return $(output);
                }
            });
        },
        taskListener: function () {
            var modal = $('#modal-assign-task');

            modal.on('shown.bs.modal', function (e) {
                var $taskId = $('#task_id'),
                        $btn = $(e.relatedTarget);

                modal.find('form').attr('action', '/admin/experts/assign/' + $btn.data('user-id'));
                modal.find('#assigned-user').find('.form-control').html($btn.data('username'));

                $.get(App.baseUrl + '/api/tasks/unassigned', function (data) {
                    $taskId.html(App.expertListPage.addOption(data));
                }).then(function () {
                    $taskId.select2({
                        placeholder: 'Pilih tugas'
                    });
                });
            });

            $('[data-toggle="bootbox"]').on('click', function (event) {
                event.preventDefault();
                var $this = $(this);
                var title = $this.data('title'),
                        link = $this.data('remote');

                $.get(link, function (data) {
                    bootbox.dialog({
                        title: title,
                        message: data,
                        closeButton: true
                    });
                });
            });
        },
        init: function () {
            this.taskListener();
            this.filters();
        }
    },
    adminTaskDetailPage: {
        init: function () {
            $('input#progress').on('change', function () {
                var $this = $(this);
                var value = $this.val();

                if (value > 100) {
                    $this.val(100);
                }

                if (value < 0 || value == '') {
                    $this.val(0);
                }
            });
        }
    },
    adminTaskAssignPage: {
        init: function () {
            var btnAssign = $('.btn-assign');

            btnAssign.on('click', function () {
                event.preventDefault();
                var $this = $(this);
                $this.prop('disabled', true);
                bootbox.confirm('Lanjutkan proses penugasan?', function (ok) {
                    if (ok) {
                        $this.closest('form').submit();
                    } else {
                        $this.prop('disabled', false);
                    }
                });
            });
        }
    },
    unverifiedListPage: {
        init: function () {
            var $btnReminder = $('.btn-reminder'),
                    $target = $('input#to');

            $btnReminder.on('click', function () {
                var $this = $(this);
                var name = $this.data('name'),
                        id = $this.data('id');

                $target.val(id);
                $('#user-name').html(name);
            });
        }
    },
    faqCreatePage: {
        addOptions: function (data) {
            var output = '';

            if (data.length > 0) {

                for (var i = 0; i < data.length; i++) {
                    output += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                }

            } else {
                output = '<option disabled selected="true">Tidak ada subkodefikasi untuk kodefikasi ini.</option>';
            }

            return output;
        },
        articleCode: function () {
            $('select#article_code').on('change', function () {
                var $this = $(this),
                        $subcode = $('select#article_subcode');

                $.get(App.baseUrl + '/api/article_subcode/by_article_code/' + $this.val(), function (data) {
                    $subcode.html(App.articleEditPage.addOptions(data));
                });
            });
            $('input#revision').on('change', function () {
                var $this = $(this);

                if ($this.val() < 0) {
                    $this.val(0);
                }
            });
        },
        init: function () {
            $('textarea#faq').summernote({height: 300, callbacks: function () {}});
            $('textarea#qa').summernote({height: 300, callbacks: function () {}});
            $('textarea#nama_pasal').summernote({height: 75, callbacks: function () {}});
            $('textarea#penjelasan').summernote({height: 300, callbacks: function () {}});
            $('textarea#isi_pasal').summernote({height: 300, callbacks: function () {}});
            $('textarea#highlighted').summernote({height: 300, callbacks: function () {}});
            $('textarea#question').summernote({height: 300, callbacks: function () {}});
            $('textarea#answer').summernote({height: 300, callbacks: function () {}});
            $('textarea#content_letter').summernote({height: 300, callbacks: function () {}});
            $('textarea#content').summernote({height: 400, callbacks: function () {}});
            $('select').select2({placeholder: 'Pilih'});
            $('#tags').select2({tags: true});
            $('#image').on('change', function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.image-placeholder img:first-child').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });

            this.articleCode();
        }
    },
    articleEditPage: {
        addOptions: function (data) {
            var output = '';

            if (data.length > 0) {

                for (var i = 0; i < data.length; i++) {
                    output += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                }

            } else {
                output = '<option disabled selected="true">Tidak ada subkodefikasi untuk kodefikasi ini.</option>';
            }

            return output;
        },
        articleCode: function () {
            $('select#article_code').on('change', function () {
                var $this = $(this),
                        $subcode = $('select#article_subcode');

                $.get(App.baseUrl + '/api/article_subcode/by_article_code/' + $this.val(), function (data) {
                    $subcode.html(App.articleEditPage.addOptions(data));
                });
            });
            $('input#revision').on('change', function () {
                var $this = $(this);

                if ($this.val() < 0) {
                    $this.val(0);
                }
            });
        },
        init: function () {
            $('textarea#content').summernote({
                callbacks: function () {}
            });
            $('select').select2({placeholder: 'Pilih'});
            this.articleCode();
        }
    },
    invitationPage: {
        listener: function () {
            $('input[data-type="date"]').datepicker({
                format: 'dd MM yyyy',
                startView: 0
            });

            $('#from').change(function () {
                var $this = $(this),
                        $until = $('#until');
                var from = $this.val(),
                        until = $until.val();

                if (from !== '' && until !== '' && (new Date(from) > new Date(until))) {
                    $until.val('');
                }
            });

            $('#until').change(function () {
                var $this = $(this),
                        $from = $('#from');
                var until = $this.val(),
                        from = $from.val();

                if (from !== '' && from !== '' && (new Date(until) < new Date(from))) {
                    $from.val('');
                }
            });

            $('#lists').select2({tags: true});
        },
        init: function () {
            this.listener();
        }
    },
    adminPacketAddPage: {
        setupRegion: function () {
            var $province = $('#province_code'),
                    $city = $('#city_code'),
                    addOptions = function (items) {
                        var output = '<option disabled selected value="">Pilih</option>';
                        for (var i = 0; i < items.length; i++) {
                            output += '<option value="' + items[i]['code'] + '">' + items[i]['name'] + '</option>';
                        }
                        return output;
                    },
                    enableDate = function (names) {
                        for (var i = 0; i < names.length; i++) {
                            names[i].prop('disabled', false);
                        }
                    },
                    disableDate = function (names) {
                        for (var i = 0; i < names.length; i++) {
                            names[i].html('<option disabled selected value="">Pilih</option>');
                            names[i].prop('disabled', true);
                        }
                    };

            $province.change(function () {
                $.get(App.baseUrl + '/api/getCityByProvinceId/' + $province.val(), function (data) {
                    $city.html(addOptions(data));
                    enableDate([$city]);
                });
            });

            $('#reset').click(function () {
                disableDate([$city]);
            });
        },
        setupBayar: function () {
            var $interval = $('#interval_waktu'),
                    $pembayaran = $('#cara_pembayaran'),
                    $termin = $("#termin");


            $interval.change(function () {
                $pembayaran.val("sekaligus");
                switch ($interval.val()) {
                    case "hari":
                    case "minggu":
                        $pembayaran.find("option[value='termin']").prop("disabled", true);
                        $pembayaran.find("option[value='bulanan']").prop("disabled", true);
                        break;
                    default:
                        $pembayaran.find("option").each(function () {
                            $(this).prop("disabled", false);
                        });
                        break;

                }
                $pembayaran.select2();
            });

            $pembayaran.change(function () {
                $termin.prop('disabled', ($pembayaran.val() != 'termin'));
            });
        },
        setupFormQualification: function () {
            $('#admin-packet-add-page').on('click', '.btn-add', function (e) {
                e.preventDefault();
                var controlForm = $('.controls .form:first'),
                        currentEntry = $(this).parents('.entry:first'),
                        newEntry = $(currentEntry.clone()).appendTo(controlForm);

                newEntry.find('input').attr('name', 'qualifications[]');
                newEntry.find('input').val('');
                controlForm.find('.entry:not(:last) .btn-add')
                        .removeClass('btn-add').addClass('btn-remove')
                        .removeClass('btn-success').addClass('btn-danger')
                        .html('<span class="glyphicon glyphicon-minus"></span>');
            }).on('click', '.btn-remove', function (e) {
                $(this).parents('.entry:first').remove();
                e.preventDefault();
                return false;
            });
        },
        init: function () {
            this.setupRegion();
            this.setupFormQualification();
            this.setupBayar();
            $('select').select2();
            $('input[data-type="date"]').datepicker({
                format: 'dd-mm-yyyy'
            });
        }
    },
    adminPacketSchedulePage: {
        init: function () {
            $.fn.datetimepicker.defaults.icons = {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-dot-circle-o',
                clear: 'fa fa-trash',
                close: 'fa fa-times'
            };
            $('input[data-type="datetime"]').datetimepicker({
                format: 'YYYY-MM-DD H:mm',
                sideBySide: true
            });
        }
    },
    adminPacketAssignPage: {
        init: function () {
            $('#datatables').on('click', '.btn-assign', function () {
                event.preventDefault();
                var $this = $(this);
                $this.prop('disabled', true);
                bootbox.confirm('Anda yakin memilih dia?', function (ok) {
                    if (ok) {
                        $this.closest('form').submit();
                    } else {
                        $this.prop('disabled', false);
                    }
                });
            });

            var dTable = $('#datatables').DataTable({
                processing: true,
                serverSide: true,
                lengthChange: false,
                ajax: {
                    url: $('#datatables').data('url'),
                    data: function (d) {
                        var search = $('#datatables').data('search');
                        if (search) {
                            d.params = search;
                        }
                    }
                },
                order: [[3, 'asc']],
                columns: [
                    {data: 'avatar', name: 'avatar', orderable: false, searchable: false},
                    {data: 'name', name: 'user.name'},
                    {data: 'kualifikasi', name: 'kualifikasi', orderable: false, searchable: false},
                    {data: 'penawaran', name: 'offer'},
                    {data: 'nego', name: 'nego', orderable: false, searchable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page",
                    "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ Data",
                    "sInfoEmpty": "Data Kosong",
                    "sSearch": "Cari",
                    "sZeroRecords": "Tidak Ada Data",
                },
                initComplete: function () {
                    App.buttonAssign.init();
                }
            });
            $('#interest').select2();
            $('#interest').on('change', function (e) {
                e.preventDefault();
                var data = $(this).val();
                var params = {
                    'interest': data
                };
                $("#datatables").data('search', params);
                dTable.draw();
            });
        }
    },
    buttonApprove: {
        taskListener: function () {
            var modal = $('#modal-approve-task');

            modal.on('shown.bs.modal', function (e) {
                var $btn = $(e.relatedTarget);

                modal.find('form').find('input#task_id').val($btn.data('task-id'));
                modal.find('form').find('#approve-task').find('.form-control').html($btn.data('task-title'));
            });
        },
        packetListener: function () {
            var modal = $('#modal-approve-packet');

            modal.on('shown.bs.modal', function (e) {
                var $btn = $(e.relatedTarget);

                modal.find('form').find('input#packet_id').val($btn.data('packet-id'));
                modal.find('form').find('#approve-packet').find('.form-control').html($btn.data('packet-title'));
            });
        },
        init: function () {
            this.taskListener();
            this.packetListener();
        }
    },
    buttonAssign: {

        packetListener: function () {
            var modal = $('#modal-assign-packet'),
                    addOptionsOffer = function (items) {
                        var output = '';
                        $.each(items, function (key, value) {
                            output += '<option value="' + key + '">' + value + '</option>';
                        });

                        return output;
                    };

            modal.on('shown.bs.modal', function (e) {
                var $btn = $(e.relatedTarget);

                modal.find('#user-name').text($btn.data('user-name'));
                modal.find('form').find('input#packet_id').val($btn.data('packet-id'));
                modal.find('form').find('input#user_id').val($btn.data('user-id'));
                modal.find('form').find('#approve-packet').find('.form-control').html($btn.data('packet-title'));

                $.get(App.baseUrl + '/api/packet/offer/' + $btn.data('packet-id') + '/' + $btn.data('user-id'), function (data) {
                    console.log(addOptionsOffer(data));
                    modal.find('form').find('select#pembayaran').empty();
                    modal.find('form').find('select#pembayaran').html(addOptionsOffer(data));


                });
            });
        },
        init: function () {
            this.packetListener();
        }
    },
    defaultButtons: function () {
        $('.btn-delete').on('click', function (event) {
            event.preventDefault();
            var $this = $(this);
            $this.prop('disabled', true);
            bootbox.confirm('Hapus data ini?', function (ok) {
                if (ok) {
                    $this.closest('form').submit();
                } else {
                    $this.prop('disabled', false);
                }
            });
        });

        $('.btn-deny').on('click', function (event) {
            event.preventDefault();
            var $this = $(this);
            bootbox.confirm('Tolak permintaan ini?', function (ok) {
                if (ok) {
                    window.location = $this.attr('href');
                }
            });
        });

        $('.btn-accept').on('click', function (event) {
            event.preventDefault();
            var $this = $(this);
            bootbox.confirm('Terima permintaan ini?', function (ok) {
                if (ok) {
                    window.location = $this.attr('href');
                }
            });
        });

        $('.btn-verify').on('click', function (event) {
            event.preventDefault();
            var $this = $(this);
            $this.prop('disabled', true);
            bootbox.confirm('Verifikasi pengguna ini?', function (ok) {
                if (ok) {
                    $this.closest('form').submit();
                } else {
                    $this.prop('disabled', false);
                }
            });
        });

        $('.btn-cancel').on('click', function (event) {
            event.preventDefault();
            var $this = $(this);
            bootbox.confirm('Batalkan tugas ini?', function (ok) {
                if (ok) {
                    window.location = $this.attr('href');
                }
            });
        });

        $('[data-confirm]').on('click', function (event) {
            event.preventDefault();
            var $this = $(this);
            var message = $this.data('message') || 'Apakah anda yakin?';
            $this.prop('disabled', true);
            if (this.nodeName == 'A') {
                bootbox.confirm(message, function (ok) {
                    if (ok) {
                        window.location = $this.attr('href');
                    }
                });
            } else {
                bootbox.confirm(message, function (ok) {
                    if (ok) {
                        $this.closest('form').submit();
                    } else {
                        $this.prop('disabled', false);
                    }
                });
            }

        });

        $('.btn-hapus').on('click', function (event) {
            event.preventDefault();
            var id = $(this).data('id');
            var url = $(this).data('url');
            
            swal({
                title: 'Anda yakin ingin menghapus data ini?',
                text: "Data akan terhapus dari database!",
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: url,
                        type: 'GET'
                    }).done(function(result) {
                        swal(
                            'Data berhasil dihapus!',
                            'Data di hapus dari database!.',
                            'success'
                        ).then((result) => {
                            location.reload();
                        });
                    }).fail(function (err) {
                        swal(
                            'Gagal!',
                            'Terjadi kesalahan, coba lagi!',
                            'error'
                        );  
                    });
                } else {
                    swal(
                        'Batal!',
                        'Hapus data dibatalkan',
                        'error'
                    );
                }
            })
        });
    },
    alerts: function () {
        setTimeout(function () {
            $('.alert.alert-success').hide()
        }, 5000);
    },
    fieldCurrency: function () {
        $('input.number').keyup(function (event) {
            var val = $(this).val();
            // skip for arrow keys
            if (event.which >= 37 && event.which <= 40)
                return;

            // format number
            $(this).val(function (index, value) {
                return value
                        .replace(/\D/g, "")
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            });
            if ($('#terbilang').length > 0) {
                $.get(App.baseUrl + '/api/terbilang/' + val, function (data) {
                    $('#terbilang').text(data);
                });
            }
        });
    },
    init: function () {
        $.fn.select2.defaults.set("theme", "bootstrap");
        this.defaultButtons();
        this.alerts();
        this.wizardForm();
        this.fieldCurrency();
        $('input[data-type="date"]').datepicker({
            format: 'dd-mm-yyyy'
        });
    }
};

App.init();

if ($('#admin-packet-assign-page').length > 0) {
    App.adminPacketAssignPage.init();
}

if ($('#profile-page').length > 0) {
    App.profilePage.init();
}

if ($('#experiences-page').length > 0) {
    App.experiencePage.init();
}

if ($('#task-detail-page').length > 0) {
    App.taskDetailPage.init();
}

if ($('#faq-create-page').length > 0) {
    App.faqCreatePage.init();
}

if ($('#disclaimer-create-page').length > 0) {
    App.faqCreatePage.init();
}

if ($('#bantuan-create-page').length > 0) {
    App.faqCreatePage.init();
}

if ($('#pasal-create-page').length > 0) {
    App.faqCreatePage.init();
}

if ($('#article-edit-page').length > 0) {
    App.articleEditPage.init();
}

if ($('#expert-list-page').length > 0) {
    App.expertListPage.init();
}

if ($('#task-unassigned-list-page').length > 0) {
    App.taskUnassignedListPage.init();
}

// ADMIN
if ($('#unverified-list-page').length > 0) {
    App.unverifiedListPage.init();
    App.expertListPage.init();
}

if ($('#invitation-page').length > 0) {
    App.invitationPage.init();
}

if ($('#admin-task-detail-page').length > 0) {
    App.adminTaskDetailPage.init();
}

if ($('#admin-task-assign-page').length > 0) {
    App.adminTaskAssignPage.init();
}

if ($('.btn-approve').length > 0) {
    App.buttonApprove.init();
}

if ($('#admin-packet-add-page').length > 0 || $('#admin-packet-detail-page').length > 0) {
    App.adminPacketAddPage.init();
}

if ($('#admin-packet-schedule-page').length > 0) {
    App.adminPacketSchedulePage.init();
}
