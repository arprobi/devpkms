window._ = require('lodash');
window.$ = window.jQuery = require('jquery');
window.moment = require('moment');
window.bootbox = require('bootbox');

require('bootstrap-sass');
require('select2');
require('moment/locale/id');
