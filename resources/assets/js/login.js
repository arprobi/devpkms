require('./base');
var App = {
    addExp: function(x){
        var source   = $("#entry-template").html();
        var template = Handlebars.compile(source);
        var context =  { x: x };
        var html    = template(context);

        $('.exp-box').append(html);
    },
    setupButtons: function(){
        $('#btn-add').click(function(e){
            e.preventDefault();
            App.addExp($('.exp-item').length);
            if($('.exp-item').length > 0) {
                $('.exp-warning').fadeOut();
            } else {
                $('.exp-warning').fadeIn();
            }
        });

        $('.exp-box').on('click','.btn-delete', function(){
            var $this = $(this);
                $this.closest('.exp-item').remove();

                if($('.exp-item').length > 0) {
                    $('.exp-warning').fadeOut();
                } else {
                    $('.exp-warning').fadeIn();
                }
        });
    },
    setupRegion: function(){
        var $province = $('#province_code'),
            $city = $('#city_code'),
            $district = $('#district_code'),
            $village = $('#village_code'),
            addOptions = function(items){
                var output = '<option disabled selected value="">Pilih</option>';
                for(var i = 0; i < items.length; i++){
                    output += '<option value="' + items[i]['code'] + '">' + items[i]['name'] + '</option>';
                }
                return output;
            },
            enableDate = function(names){
                for(var i = 0; i < names.length; i++){
                    names[i].prop('disabled', false);
                }
            },
            disableDate = function(names){
                for(var i = 0; i < names.length; i++){
                    names[i].html('<option disabled selected value="">Pilih</option>');
                    names[i].prop('disabled', true);
                }
            };

        $province.change(function(){
            $.get('/api/getCityByProvinceId/' + $province.val(), function(data){
                $city.html(addOptions(data));
                enableDate([$city]);
                disableDate([$district, $village]);
            });
        });

        $city.change(function(){
            $.get('/api/getDistrictByCityId/' + $city.val(), function(data){
                $district.html(addOptions(data));
                enableDate([$district]);
                disableDate([$village]);
            });
        });

        $district.change(function(){
            $.get('/api/getVillageByDistrictId/' + $district.val(), function(data){
                $village.html(addOptions(data));
                enableDate([$village]);
            });
        });

        $('#reset').click(function(){
            disableDate([$city,$district,$village]);
        });
    },
    setupDates: function(){
        var maxDate = moment().year - 15,
            $birth_date = new Pikaday({
                field: document.getElementById('birth_date_view'),
                format: 'DD MMMM YYYY',
                yearRange: [1960, maxDate],
                i18n: {
                    previousMonth : 'Bulan sebelumnya',
                    nextMonth     : 'Bulan selanjutnya',
                    months        : ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'],
                    weekdays      : ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'],
                    weekdaysShort : ['Min','Sen','Sel','Rab','Kam','Jum','Sab']
                }
            });

            $('#birth_date_view').on('change', function(){
                var $this = $(this),
                    $target = $('#birth_date');

                if(! moment($this.val()).isValid()){
                    $this.val("");
                    $('#birth_date').val("");
                } else {
                    $('#birth_date').val( moment($this.val()).format('YYYY-MM-DD'));
                }
            });
    },
    init: function(){
        this.setupButtons();
        this.setupRegion();
        this.setupDates();
    }
};

App.init();